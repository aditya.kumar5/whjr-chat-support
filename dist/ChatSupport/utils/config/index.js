"use strict";

require("core-js/modules/web.dom-collections.iterator.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.variable = exports.h__isDevEnv = exports.config = exports.badgesPath = void 0;

require("core-js/modules/es.object.assign.js");

var development = _interopRequireWildcard(require("./environments/development"));

var production = _interopRequireWildcard(require("./environments/production"));

var preproduction = _interopRequireWildcard(require("./environments/preproduction"));

var stage = _interopRequireWildcard(require("./environments/stage"));

var stageMaster = _interopRequireWildcard(require("./environments/stage-master"));

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

const variable = {
  development,
  stage,
  production,
  preproduction,
  "stage-master": stageMaster
};
exports.variable = variable;
const ENV = process.env.NODE_ENV || "development";
const branch_name = process.env.REACT_APP_VERCEL_GIT_COMMIT_REF || process.env.REACT_APP_BRANCH;
const CRA_MAP = {
  release: "production",
  pre_release: "preproduction",
  master: "stage-master",
  develop: "stage"
};
const NEXT_ENV = ENV === "development" ? "development" : CRA_MAP[branch_name] || "development";
const envConfig = variable[NEXT_ENV];
const badgesPath = (process.env.REACT_APP_PUBLIC_URL || "") + "/images/BadgesMap/"; // const envConfig = variable[ENV];

exports.badgesPath = badgesPath;
const config = Object.assign({
  // env: ENV,
  env: NEXT_ENV
}, envConfig);
exports.config = config;

const h__isDevEnv = () => NEXT_ENV === "development";

exports.h__isDevEnv = h__isDevEnv;
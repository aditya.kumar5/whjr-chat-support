"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.CTA_THROTTLE_TIMEOUT = void 0;
const CTA_THROTTLE_TIMEOUT = 5000; //time in ms

exports.CTA_THROTTLE_TIMEOUT = CTA_THROTTLE_TIMEOUT;
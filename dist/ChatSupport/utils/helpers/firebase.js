"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.trackFirebaseEvent = exports.firebaseSync = exports.default = void 0;

var _app = _interopRequireDefault(require("firebase/compat/app"));

var _config = require("../config");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const firebaseConfigStore = {
  DEFAULT: {
    initConfig: {
      apiKey: "AIzaSyDgiobwYTWm3H2UFOYDUS1f1xv9WZg_L-E",
      authDomain: "wh-realtime.firebaseapp.com",
      databaseURL: _config.config.FIREBASE_DB_URL,
      projectId: "wh-realtime",
      storageBucket: "wh-realtime.appspot.com",
      messagingSenderId: "909404073095",
      appId: "1:909404073095:web:b1faba3c04bd9c16085c22",
      measurementId: "G-ZL1WK5ZP9Q"
    }
  },
  SANDBOX: {
    initConfig: {
      apiKey: "AIzaSyCIeDwz5RrmOz0tRgwibrhsvg-KTeP9VLw",
      authDomain: "whitehatjr-sandbox.firebaseapp.com",
      databaseURL: "https://whitehatjr-sandbox-default-rtdb.firebaseio.com",
      projectId: "whitehatjr-sandbox",
      storageBucket: "whitehatjr-sandbox.appspot.com",
      messagingSenderId: "470480502479",
      appId: "1:470480502479:web:689a4cfaef6f8de63a81ba"
    }
  },
  MATH: {
    initConfig: {
      apiKey: "AIzaSyCmgnTWlvYzu9KRBcu7-2Wr7BD4jVhkUrM",
      authDomain: "whjr-math-93db5.firebaseapp.com",
      databaseURL: "https://whjr-math-93db5-default-rtdb.firebaseio.com",
      projectId: "whjr-math-93db5",
      storageBucket: "whjr-math-93db5.appspot.com",
      messagingSenderId: "791532997636",
      appId: "1:791532997636:web:b87908ec02f54968143951",
      measurementId: "G-WEQ7F801BV"
    }
  },
  ART: {
    initConfig: {
      apiKey: "AIzaSyBzh4IyX9JFTIrl4GUz9kJyEkDWmaaN_jI",
      authDomain: "whjr-arts.firebaseapp.com",
      databaseURL: "https://whjr-arts-default-rtdb.firebaseio.com",
      projectId: "whjr-arts",
      storageBucket: "whjr-arts.appspot.com",
      messagingSenderId: "83258445776",
      appId: "1:83258445776:web:969fbc23465fd2e122b3d5",
      measurementId: "G-4DBGPCDQMQ"
    }
  }
};

const firebaseMath = _app.default.initializeApp(firebaseConfigStore.MATH.initConfig, "whjr-math");

const trackFirebaseEvent = (matchId, handleFirebaseMessage) => {
  const firebaseRef = _app.default.database().ref(matchId);

  let count = 0; // ignore the first default read

  let eventFunction = data => {
    let value = data.val();

    if (value && count > 0) {
      console.log("firebase event called", matchId, value); // used for debugging

      handleFirebaseMessage(data);
    } else if (count === 0) {
      console.log("firebase: ignoring the first default read"); // used for debugging
    }

    count++;
  };

  firebaseRef.on("value", eventFunction);
  return () => {
    firebaseRef.off("value", eventFunction);
  };
};

exports.trackFirebaseEvent = trackFirebaseEvent;

const firebaseSync = (matchId, handleFirebaseMessage) => {
  const firebaseRef = firebaseMath.database().ref(matchId);

  let eventFunction = data => {
    let value = data.val();

    if (value && value.lastUpdatedTimestamp) {
      console.log("firebase event called", matchId, value);
    }

    handleFirebaseMessage(data);
  };

  firebaseRef.on("value", eventFunction);
  return {
    set: (value, onComplete) => firebaseRef.set(value, onComplete),
    // callback = (error) => {}
    update: (value, onComplete) => firebaseRef.update(value, onComplete),
    push: (value, onComplete) => firebaseRef.push(value, onComplete),
    get: () => firebaseRef.once("value"),
    pushToChild: (childValue, value, onComplete) => firebaseRef.child(childValue).push(value, onComplete),
    unregister: () => firebaseRef.off("value", eventFunction)
  };
};

exports.firebaseSync = firebaseSync;
var _default = _app.default;
exports.default = _default;
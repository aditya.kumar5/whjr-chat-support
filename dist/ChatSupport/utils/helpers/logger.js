"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getCustomLogger = void 0;

var _get2 = _interopRequireDefault(require("lodash/get"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//Datadog logger utils
const getCustomLogger = function getCustomLogger(name) {
  let forceCreate = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;
  let logger;

  if ((0, _get2.default)(window, "DD_LOGS.getLogger")) {
    logger = window.DD_LOGS.getLogger(name);
  }

  if (!logger) {
    console.error("".concat(name, " not initialized"));

    if (forceCreate && (0, _get2.default)(window, "DD_LOGS.createLogger")) {
      window.DD_LOGS.createLogger(name);
      logger = window.DD_LOGS.getLogger(name);
    }
  }

  return logger;
};

exports.getCustomLogger = getCustomLogger;
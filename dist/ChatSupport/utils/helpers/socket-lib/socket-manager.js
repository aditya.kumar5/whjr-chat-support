"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.socketManager = void 0;

var _socketInstance = require("./socket-instance");

// Example on getting socket instance
// const socketInstance = socketManager({ uid, roomId })

/*
  const unsubscribeSocketConnect = socketInstance.onConnect(() => {});
  const unsubscribeSocketMessage = socketInstance.onMessage((data) => {});
*/
// Example send message to other users
// socketInstance.send({target:"*",state:{hello:"World!"}});
// Example request state on socket
// socketInstance.requestState();
// Example update state on socket
// socketInstance.updateState({hello:"World!"});
// Example close socket
// socketInstance.close();
// Note that this action will remove all event listeners from this instance and mark it close
let connections = {};
const socketUrl = "wss://hzqpapvxa4.execute-api.eu-west-1.amazonaws.com/prod";

const socketManager = _ref => {
  let {
    uid,
    roomId,
    socketToken
  } = _ref;
  const key = "".concat(uid, "-").concat(roomId);

  if (!connections[key] || connections[key].closed) {
    connections[key] = (0, _socketInstance.socketInstance)({
      uid,
      roomId,
      url: socketUrl
    });
    connections[key].init(socketToken);
  }

  return connections[key];
};

exports.socketManager = socketManager;
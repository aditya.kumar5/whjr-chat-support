"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.socket__getLatestSpeed = exports.sendSocketMessage = exports.initSocketService = exports.closeSocketService = void 0;

require("core-js/modules/es.json.stringify.js");

var _socketManager = require("./socket-manager");

let socketInstance = null;

const initSocketService = _ref => {
  let {
    roomId,
    userId,
    socketToken,
    cb,
    connectCb
  } = _ref;
  const uid = userId;
  socketInstance = (0, _socketManager.socketManager)({
    uid,
    roomId,
    socketToken: socketToken || btoa(JSON.stringify({
      uid: uid,
      roomId: roomId
    }))
  });

  if (socketInstance && socketInstance.connected && connectCb) {
    connectCb();
  } else {
    socketInstance.onConnect(connectCb);
    socketInstance.onMessage(cb);
  }
};

exports.initSocketService = initSocketService;

const closeSocketService = () => {
  if (socketInstance) socketInstance.close();
};

exports.closeSocketService = closeSocketService;

const sendSocketMessage = msg => {
  socketInstance && socketInstance.send(msg);
};

exports.sendSocketMessage = sendSocketMessage;

const socket__getLatestSpeed = source => {
  sendSocketMessage({
    type: "GET_LATEST_SPEED",
    state: {
      source
    },
    target: "*"
  });
};

exports.socket__getLatestSpeed = socket__getLatestSpeed;
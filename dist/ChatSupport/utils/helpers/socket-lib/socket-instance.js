"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.socketInstance = void 0;

var _sockette = _interopRequireDefault(require("sockette"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

const PING_INTERVAL = 50000;

const isJSON = function isJSON(s) {
  try {
    return JSON.parse(s);
  } catch (e) {
    return false;
  }
};

const getSocketData = _ref => {
  let {
    uid,
    roomId,
    target = "*",
    type = "COMMAND",
    state = {},
    action = "GLOBAL_STATE",
    updateState = false
  } = _ref;
  return {
    action: "sendMessage",
    body: {
      version: "1.0",
      type,
      payload: {
        action: action,
        updateState,
        state
      },
      priority: 3,
      senderDetails: {
        uid,
        roomId
      },
      target,
      relatedMessageId: "",
      sentAt: new Date().toISOString(),
      forwardedAt: null
    }
  };
};

const getSocketMessage = _ref2 => {
  let {
    data
  } = _ref2;
  const json = isJSON(data);

  if (json && json.body) {
    return {
      target: json.body.target,
      type: json.body.type,
      state: json.body.payload ? json.body.payload.state : {}
    };
  } else {
    return data;
  }
};

const newEvents = () => ({
  onConnect: {},
  onMessage: {},
  onReconnect: {},
  onMaximum: {},
  onClose: {},
  onError: {}
});

const socketInstance = function socketInstance(_ref3) {
  let {
    uid,
    roomId,
    url
  } = _ref3;
  let events = newEvents();
  let ws = {
    closed: false,
    onConnect: callback => subscribe("onConnect", callback),
    onMessage: callback => subscribe("onMessage", callback),
    onReconnect: callback => subscribe("onReconnect", callback),
    onMaximum: callback => subscribe("onMaximum", callback),
    onClose: callback => subscribe("onClose", callback),
    onError: callback => subscribe("onError", callback),
    init: function init(token) {
      let pollId = null;
      const socket = new _sockette.default("".concat(url, "?token=").concat(token), {
        timeout: 5e3,
        maxAttempts: 5,
        onopen: e => {
          pollId = setInterval(() => {
            socket.json({
              action: "sendMessage",
              body: {
                data: "PING"
              }
            });
          }, PING_INTERVAL);
          Object.keys(events.onConnect).forEach(k => {
            events.onConnect[k](e);
          });
          ws.connected = true; // appletLogger && appletLogger.debug("web socket connection established Successfully")
        },
        onmessage: e => {
          const msg = getSocketMessage(e);
          Object.keys(events.onMessage).forEach(k => {
            events.onMessage[k](msg);
          });
        },
        onreconnect: e => {
          console.log("OnReconnect socket", e);
          Object.keys(events.onReconnect).forEach(k => {
            events.onReconnect[k](e);
          });
        },
        onmaximum: e => {
          Object.keys(events.onMaximum).forEach(k => {
            events.onMaximum[k](e);
          });
        },
        onclose: e => {
          console.log("Onclose socket", e);
          Object.keys(events.onClose).forEach(k => {
            events.onClose[k](e);
          });
        },
        onerror: e => {
          console.log("OnError socket", e);
          Object.keys(events.onError).forEach(k => {
            events.onError[k](e);
          });
        }
      });

      ws.send = function (options) {
        const json = getSocketData(_objectSpread({
          uid,
          roomId
        }, options)); // appletLogger && appletLogger.debug("web socket applet data sent to parent")

        socket.json(json);
      };

      ws.requestState = function () {
        const json = getSocketData({
          uid,
          roomId,
          type: "REQUEST_STATE"
        });
        socket.json(json);
      };

      ws.updateState = function (state) {
        const json = getSocketData({
          uid,
          roomId,
          type: "COMMAND",
          updateState: true,
          state
        });
        socket.json(json);
      };

      ws.close = function () {
        socket.close();
        events = newEvents();
        ws.closed = true;
        ws.connected = false;
      };

      delete ws.init;
    }
  };
  let i = 0;

  const subscribe = function subscribe(event, callback) {
    i++;
    const k = i;

    if (!events[event]) {
      events[event] = {};
    }

    events[event][k] = callback;
    return () => {
      delete events[event][k];
    };
  };

  return ws;
};

exports.socketInstance = socketInstance;
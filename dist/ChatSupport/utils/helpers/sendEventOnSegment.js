"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.sendEventsOnSegmentHOF = void 0;

const sendEventsOnSegmentHOF = _ref => {
  let {
    sendEvents,
    eventName,
    properties
  } = _ref;
  let eventDetails = {
    event: eventName,
    properties
  };

  if (sendEvents) {
    sendEvents(eventDetails);
  }
};

exports.sendEventsOnSegmentHOF = sendEventsOnSegmentHOF;
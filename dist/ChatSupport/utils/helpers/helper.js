"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.subscribe = exports.setPaidStatus = exports.h__isArtScope = exports.h__getCourseScope = exports.getSupportData = exports.getStudentsInfo = exports.getStudentsBookingInfo = exports.getCodingOrMathSkill = exports.endClassSubmit = exports.closePIP = void 0;

require("core-js/modules/es.array.sort.js");

require("core-js/modules/es.promise.js");

require("core-js/modules/web.dom-collections.iterator.js");

var _constant = require("../constants/constant");

var _get2 = _interopRequireDefault(require("lodash/get"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

const h__getCourseScope = () => _constant.courseNames[window.currentCourseName];

exports.h__getCourseScope = h__getCourseScope;

const closePIP = () => {
  const isPIPActive = document.pictureInPictureElement !== null;

  if (isPIPActive) {
    try {
      document.exitPictureInPicture();
    } catch (e) {
      console.log(e);
    }
  }
};

exports.closePIP = closePIP;

const h__isArtScope = () => h__getCourseScope() === _constant.courseNames.ART;

exports.h__isArtScope = h__isArtScope;

const getStudentsInfo = booking => {
  if (!booking || !Object.keys(booking).length) {
    return [];
  }

  let studentsBookings = (0, _get2.default)(booking, "student_bookings");
  return studentsBookings.map(studentBooking => {
    return _objectSpread(_objectSpread({}, studentBooking.student), {}, {
      bookingStatus: studentBooking.status,
      ncReasonCode: studentBooking.ncReasonCode,
      ncReasonCodeFormatted: studentBooking.ncReasonCode === "STUDENT_ABSENT" ? "STUDENT ABSENT" : "STUDENT TECH ISSUE"
    });
  }).sort(student => student.bookingStatus === "cancelled" ? 1 : 0);
};

exports.getStudentsInfo = getStudentsInfo;

const getStudentsBookingInfo = function getStudentsBookingInfo(booking) {
  if (!Object.keys(booking).length) {
    return [];
  }

  let studentsBookings = (0, _get2.default)(booking, "student_bookings");
  return studentsBookings.sort(booking => booking.status === "cancelled" ? 1 : 0);
}; // TO GET THE SCREENSHOT CAPTURED URLS FROM PLAYGROUND


exports.getStudentsBookingInfo = getStudentsBookingInfo;

const getGIFArrayFromFirebase = firebaseSyncGIF => {
  try {
    if (firebaseSyncGIF) {
      return firebaseSyncGIF.get().then(snapshot => {
        try {
          const imagesArr = [];
          const screenshotsArr = [];

          if (snapshot.val()) {
            if (snapshot.val() && snapshot.val()["GIFScreenshots"] && snapshot.val()["GIFScreenshots"]["screenshots"]) {
              for (let obj in snapshot.val()["GIFScreenshots"]["screenshots"]) {
                if (snapshot.val()["GIFScreenshots"]["screenshots"].hasOwnProperty(obj)) {
                  const url = snapshot.val()["GIFScreenshots"]["screenshots"][obj];
                  imagesArr.push(url);
                }
              }
            }
          }

          if (snapshot.val()) {
            if (snapshot.val() && snapshot.val()["screenshots"] && snapshot.val()["screenshots"]["whiteboardAndActivityLinks"]) {
              for (let obj in snapshot.val()["screenshots"]["whiteboardAndActivityLinks"]) {
                if (snapshot.val()["screenshots"]["whiteboardAndActivityLinks"].hasOwnProperty(obj)) {
                  const url = snapshot.val()["screenshots"]["whiteboardAndActivityLinks"][obj];
                  screenshotsArr.push(url);
                }
              }
            }
          }

          return {
            imagesList: imagesArr.slice(1),
            screenshotList: screenshotsArr
          };
        } catch (e) {
          console.error(e);
          return {};
        }
      }).catch(err => {
        console.log(err);
        return {};
      });
    } else {
      return {};
    }
  } catch (e) {
    console.error(e);
    return {};
  }
}; // UPDATING/ADDING DATA TO THE ARGUMENT "GIFAndScreenshots"


const enrichGIFAndScreenshots = (GIFAndScreenshots, classInterfaceData) => {
  GIFAndScreenshots.courseClassId = (0, _get2.default)(classInterfaceData, "class_booking.course_class.id");
  GIFAndScreenshots.studentId = (0, _get2.default)(classInterfaceData, "class_booking.studentId");
  GIFAndScreenshots.courseItemId = (0, _get2.default)(classInterfaceData, "class_booking.course_class.courseItemId");
  GIFAndScreenshots.className = (0, _get2.default)(classInterfaceData, "class_booking.course_class.name");
  GIFAndScreenshots.classNumber = (0, _get2.default)(classInterfaceData, "class_booking.course_class.classNumber");
  const studentBookings = (0, _get2.default)(classInterfaceData, "class_booking.student_bookings");
  const studentDetails = studentBookings && studentBookings.length > 0 ? studentBookings[0].student : {};
  GIFAndScreenshots.studentName = studentDetails.name || "";
  GIFAndScreenshots.grade = studentDetails.grade || "";
}; // ONE TO ONE END CLASS FUNCTION


const endClassSubmit = async function endClassSubmit() {
  let feedbackResponseResult = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
  let isSos = arguments.length > 1 ? arguments[1] : undefined;
  let classData = arguments.length > 2 ? arguments[2] : undefined;
  let classInterfaceData = arguments.length > 3 ? arguments[3] : undefined;
  let isQuesAvailable = arguments.length > 4 ? arguments[4] : undefined;
  let firebaseEvents = arguments.length > 5 ? arguments[5] : undefined;
  let firebaseSyncGIF = arguments.length > 6 ? arguments[6] : undefined;
  const {
    class_booking = {}
  } = classInterfaceData;
  let studentsBooking = getStudentsBookingInfo(class_booking);
  let messageClassData = {
    id: classData.id,
    classBookingId: classData.bookingId,
    teacherId: classData.teacherId,
    studentStatus: studentsBooking.map(booking => ({
      id: booking.student.id,
      classStatus: classData.classStatus,
      bookingId: booking.id,
      ncReasonCode: classData.ncReasonCode ? classData.ncReasonCode : null,
      ncReasonDesc: classData.ncReasonDesc,
      remark: classData.remark
    })),
    isSOSEmergency: isSos
  };
  let studentStatusData = (0, _get2.default)(messageClassData, "studentStatus.0", {});

  if (studentStatusData.classStatus === "not_completed" && (!studentStatusData.ncReasonCode || !studentStatusData.ncReasonDesc) && class_booking.isTrial) {
    alert("You are unable to change class status. Please contact our agent through chat and say the code AB11Q");
    return;
  }

  if (feedbackResponseResult.length > 0) {
    messageClassData["feedbackResponseResult"] = feedbackResponseResult;
  }

  const presentStudents = messageClassData.studentStatus.filter(student => student.classStatus === "completed");
  let GIFAndScreenshots;

  try {
    GIFAndScreenshots = await getGIFArrayFromFirebase(firebaseSyncGIF);
    enrichGIFAndScreenshots(GIFAndScreenshots, classInterfaceData);
  } catch (error) {
    console.log(error);
  }

  const req = {
    classData: messageClassData,
    GIFAndScreenshots: GIFAndScreenshots,
    navigateToDashboardOnFinish: !isSos && (presentStudents.length === 0 || !(class_booking.isTrial || isQuesAvailable))
  }; // removing firebase events after class end is confirmed

  firebaseEvents.forEach(offEventFunction => offEventFunction());
  if (firebaseSyncGIF) firebaseSyncGIF.unregister();
  return {
    presentStudents: presentStudents,
    requestParam: req
  };
};

exports.endClassSubmit = endClassSubmit;
let subscribers = {};

const subscribe = (event, callback) => {
  let index;

  if (!subscribers[event]) {
    subscribers[event] = [];
  }

  index = subscribers[event].push(callback) - 1;
  return () => {
    subscribers[event].splice(index, 1);
  };
};

exports.subscribe = subscribe;

const getSupportData = function getSupportData(supportData) {
  let classType = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "";
  let supportNumber = _constant.supportMedium["phone"]["link"];
  let supportEmail = _constant.supportMedium["email"]["link"];

  if (supportData && supportData.hasOwnProperty("customerSupport")) {
    supportNumber = supportData["customerSupport"]["number"];
    supportEmail = supportData["customerSupport"]["email"];
  }

  if (supportData && supportData.hasOwnProperty("number")) {
    supportNumber = supportData["number"];
  }

  if (supportData && supportData.hasOwnProperty("email")) {
    supportEmail = supportData["email"];
  }

  return {
    supportNumber,
    supportEmail
  };
};

exports.getSupportData = getSupportData;

const setPaidStatus = (userType, userValue) => {
  if (userType === "STUDENT" && userValue) {
    if (userValue.trialStatus === "paid" && (0, _get2.default)(userValue, "activeCourse.course_item.classType") === "ONE_TO_TWO") {
      return "paid_one_to_two";
    } else if (userValue.trialStatus === "paid" && (0, _get2.default)(userValue, "activeCourse.course_item.classType") === "ONE_TO_ONE") {
      return "paid";
    } else if (userValue.trialStatus === "paid" && !["ONE_TO_TWO", "ONE_TO_ONE"].includes((0, _get2.default)(userValue, "activeCourse.course_item.classType"))) {
      return "paid_one_to_many";
    } else {
      return "trial";
    }
  } else if (userType === "TEACHER" && userValue) {
    const teacherSkill = userValue.teacher_skills.filter(x => x.skillValue.toUpperCase() === "ONE_TO_MANY" || x.skillValue.toUpperCase() === "ONE_TO_TWO");

    if (teacherSkill.length > 0) {
      return "paid_".concat(teacherSkill[0].skillValue.toLowerCase());
    } else {
      return null;
    }
  }
};

exports.setPaidStatus = setPaidStatus;

const getCodingOrMathSkill = data => {
  //["coding","math"]
  const courses = data.filter(x => x.course);
  const skillList = [];
  courses.forEach(course => {
    skillList.push(course.course.courseType.toLowerCase());
  });
  return [...new Set(skillList)];
};

exports.getCodingOrMathSkill = getCodingOrMathSkill;
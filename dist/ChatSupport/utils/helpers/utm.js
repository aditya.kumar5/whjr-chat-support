"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getCookie = getCookie;
exports.setCookie = exports.getDomain = void 0;

require("core-js/modules/es.regexp.exec.js");

require("core-js/modules/es.string.match.js");

require("core-js/modules/es.string.includes.js");

require("core-js/modules/es.string.replace.js");

require("core-js/modules/es.string.split.js");

// fetch a particular cookie
function getCookie(name) {
  var v = document.cookie.match("(^|;) ?" + name + "=([^;]*)(;|$)");
  return v ? v[2] : null;
}

const setCookie = (key, val, expiry) => {
  const domain = getDomain();

  if (!expiry) {
    expiry = new Date(new Date().setMonth(new Date().getMonth() + 1)).toUTCString();
  }

  return document.cookie = "".concat(key, "=").concat(val, ";domain=").concat(domain, ";expires=").concat(expiry, ";path=/; ").concat(["production", "preview"].includes(process.env.REACT_APP_ENV) ? "" : "");
};

exports.setCookie = setCookie;

const getDomain = function getDomain() {
  let url = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : window.location.origin;

  if (url.includes("localhost")) {
    return "";
  }

  url = url.replace(/(https?:\/\/)?(www.)?/i, "");
  url = url.split(".");
  url = url.slice(url.length - 2).join(".");

  if (url.indexOf("/") !== -1) {
    url = url.split("/")[0];
  }

  return ".".concat(url);
};

exports.getDomain = getDomain;
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getWindow = void 0;

const getWindow = () => {
  const w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
  const h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
  return {
    w,
    h
  };
};

exports.getWindow = getWindow;
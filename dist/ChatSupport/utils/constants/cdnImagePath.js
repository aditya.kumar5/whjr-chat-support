"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.isValidUrl = exports.QUESTION_MARK = exports.ISSUES_TAB = exports.IMMEDIATE_TAB = exports.HELP_MAIL_WHITE = exports.HELP_MAIL = exports.HELP_ICON = exports.CONNECT_TAB = void 0;

require("core-js/modules/es.regexp.exec.js");

require("core-js/modules/es.regexp.test.js");

var _get2 = _interopRequireDefault(require("lodash/get"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const isValidUrl = value => {
  return /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/i.test(value);
};

exports.isValidUrl = isValidUrl;

const cu = path => {
  const REACT_APP_PUBLIC_URL = process.env.REACT_APP_PUBLIC_URL;
  const originUrl = (0, _get2.default)(window, "location.origin");
  let showPubUrl; // isValidUrl is breaking in storybook so we are using this syntax

  if (isValidUrl) {
    showPubUrl = isValidUrl(REACT_APP_PUBLIC_URL);
  } else {
    showPubUrl = !originUrl;
  }

  return (showPubUrl ? REACT_APP_PUBLIC_URL : originUrl) + "/images/" + path;
};

const HELP_MAIL_WHITE = cu("HelpSection/mail-white.png");
exports.HELP_MAIL_WHITE = HELP_MAIL_WHITE;
const HELP_ICON = cu("icons/help.svg");
exports.HELP_ICON = HELP_ICON;
const HELP_MAIL = cu("HelpSection/mail.png");
exports.HELP_MAIL = HELP_MAIL;
const QUESTION_MARK = cu("HelpSection/question.png");
exports.QUESTION_MARK = QUESTION_MARK;
const ISSUES_TAB = cu("Tablet/icon-consierge-issues.svg");
exports.ISSUES_TAB = ISSUES_TAB;
const IMMEDIATE_TAB = cu("Tablet/Icon-consierge-immediate.svg");
exports.IMMEDIATE_TAB = IMMEDIATE_TAB;
const CONNECT_TAB = cu("Tablet/icon-consierge-connect.svg");
exports.CONNECT_TAB = CONNECT_TAB;
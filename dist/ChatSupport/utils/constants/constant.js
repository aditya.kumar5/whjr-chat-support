"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.supportMedium = exports.courseNames = exports.checkReschedule = exports.allowedExtensions = void 0;
const courseNames = {
  MATH: "MATH",
  CODING: "CODING",
  MUSIC: "MUSIC",
  MUSIC_FOR_ALL: "MUSIC_FOR_ALL",
  ART: "ART",
  ENGLISH: "ENGLISH"
};
exports.courseNames = courseNames;
const allowedExtensions = ".pdf, jpg, .png, .gif, .txt, .ppt, .pptx, .docx, .doc, .csv, .mp4, .mov, .m4v, .avi, .3gp, .m4a, .aac, .mp3, .bmp, .webp, .webm, .xlsx";
exports.allowedExtensions = allowedExtensions;
const checkReschedule = ["grade mismatch", "language mismatch", "desfase de grado", "nota incompatível", "desfase de lenguaje", "língua incompatível"];
exports.checkReschedule = checkReschedule;
const supportMedium = {
  email: {
    link: "support@whitehatjr.com",
    desc: "support@whitehatjr.com"
  },
  phone: {
    link: "02248933975",
    desc: "022 - 489 33 975"
  }
};
exports.supportMedium = supportMedium;
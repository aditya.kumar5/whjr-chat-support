"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DeviceTypeContext = exports.DEVICE_TYPES = void 0;

var _react = _interopRequireDefault(require("react"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const DeviceTypeContext = /*#__PURE__*/_react.default.createContext();

exports.DeviceTypeContext = DeviceTypeContext;
const DEVICE_TYPES = {
  MOBILE: 1,
  DESKTOP: 2,
  TAB: {
    LRN10: false,
    isTab: 3
  }
};
exports.DEVICE_TYPES = DEVICE_TYPES;
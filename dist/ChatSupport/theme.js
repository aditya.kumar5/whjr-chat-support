"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _styledComponents = require("styled-components");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const theme = {
  font: {
    fontSize: "16px",
    fontFamily: "Muli-Regular"
  },
  colors: {
    primary: "#266ad1",
    secondary: "#fff",
    textColor: {
      primary: "#1e1e1e",
      secondary: "#636363",
      error: "#c31807"
    }
  }
};

const Theme = _ref => {
  let {
    children
  } = _ref;
  return /*#__PURE__*/_react.default.createElement(_styledComponents.ThemeProvider, {
    theme: theme
  }, children);
};

var _default = Theme;
exports.default = _default;
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.HelpSection = exports.HelpContainer = exports.ChatContainer = void 0;

require("core-js/modules/web.dom-collections.iterator.js");

require("core-js/modules/es.string.includes.js");

require("core-js/modules/es.json.stringify.js");

require("core-js/modules/es.promise.js");

require("core-js/modules/es.regexp.exec.js");

require("core-js/modules/es.string.split.js");

require("core-js/modules/es.parse-int.js");

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _styledComponents = _interopRequireWildcard(require("styled-components"));

var _moment = _interopRequireDefault(require("moment"));

var _get2 = _interopRequireDefault(require("lodash/get"));

var _isEqual2 = _interopRequireDefault(require("lodash/isEqual"));

var _theme = _interopRequireDefault(require("./theme"));

var _ChatBubble = _interopRequireDefault(require("./components/ChatBubble"));

var _ChatTitle = _interopRequireDefault(require("./components/ChatTitle"));

var _Prechat = _interopRequireDefault(require("./components/Prechat"));

var _Prechat_AB = _interopRequireDefault(require("./components/Prechat_AB"));

var _InternetSpeedTest = _interopRequireDefault(require("./components/InternetSpeedTest"));

var _Chat = _interopRequireDefault(require("./components/Chat"));

var _OfflineChat = _interopRequireDefault(require("./components/OfflineChat"));

var _ChatHistory = _interopRequireDefault(require("./components/ChatHistory"));

var _NeedHelp = _interopRequireDefault(require("./components/NeedHelp"));

var _ConciergeHelpDesk = _interopRequireDefault(require("./components/ConciergeHelpDesk"));

var _WriteToCeo = _interopRequireDefault(require("./components/WriteToCeo"));

var _logger = require("./utils/helpers/logger");

var _utm = require("./utils/helpers/utm");

var _routesMap = require("./utils/constants/routesMap");

var _networkSpeed = _interopRequireDefault(require("network-speed"));

var _segmentEvents = require("./utils/constants/segmentEvents");

var _reactVisibilitySensor = _interopRequireDefault(require("react-visibility-sensor"));

var _sendEventOnSegment = require("./utils/helpers/sendEventOnSegment");

var _helper = require("./utils/helpers/helper");

var _firebase = _interopRequireWildcard(require("./utils/helpers/firebase"));

var _helpCenter = _interopRequireDefault(require("./components/helpCenter"));

var _config = require("./utils/config");

var _VcEvents = require("./utils/constants/VcEvents");

var _opsConfig = require("./utils/constants/opsConfig");

var _socketUtil = require("./utils/helpers/socket-lib/socket-util");

var _templateObject, _templateObject2, _templateObject3, _templateObject4, _templateObject5;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

const openChatContainer = (0, _styledComponents.keyframes)(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n0% {\n  visibility: hidden;\n  opacity: 0;\n}\n1% {\n  visibility: visible;\n  opacity: 0;\n  transform: translateY(50%);\n}\n100% {\n  visibility: visible;\n  opacity: 1;\n  transform: translateY(0);\n}\n"])));
const hideChatContainer = (0, _styledComponents.keyframes)(_templateObject2 || (_templateObject2 = _taggedTemplateLiteral(["\n0% {\n  transform: translateY(0);\n}\n99% {\n  opacity: 0;\n  transform: translateY(50%);\n}\n100% {\n  visibility: hidden;\n  opacity: 0;\n}\n"])));

const HelpSection = _styledComponents.default.div(_templateObject3 || (_templateObject3 = _taggedTemplateLiteral(["\n  .help-section {\n    max-height: 14rem;\n    padding: 1.5rem;\n    bottom: 7%;\n    max-width: 17rem;\n    right: 2%;\n    left: auto;\n    top: auto;\n  }\n"])));

exports.HelpSection = HelpSection;

const ChatContainer = _styledComponents.default.div(_templateObject4 || (_templateObject4 = _taggedTemplateLiteral(["\n  position: fixed;\n  right: ", ";\n  bottom: 25px;\n  width: 320px;\n  display: flex;\n  flex-direction: column;\n  max-width: 100vw;\n  height: 430px;\n  max-height: 85vh;\n  visibility: hidden;\n  opacity: 0;\n  overflow: hidden;\n  border-radius: 8px;\n  box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.38);\n  background-color: #ffffff;\n  z-index: 9999999;\n  line-height: normal;\n  letter-spacing: normal;\n  font-size: ", ";\n  font-family: ", ";\n  color: ", ";\n  animation: ", " 0.5s backwards;\n  &.chat-visible {\n    animation: ", " 0.5s forwards;\n  }\n  @media (max-width: 768px) {\n    max-height: 100%;\n    height: 100vh;\n    width: 100vw;\n    border-radius: 0;\n    left: 0;\n    top: 0;\n  }\n"])), props => props.right, props => props.theme.font.fontSize, props => props.theme.font.fontFamily, props => props.theme.colors.textColor.primary, hideChatContainer, openChatContainer);

exports.ChatContainer = ChatContainer;

const HelpContainer = _styledComponents.default.div(_templateObject5 || (_templateObject5 = _taggedTemplateLiteral(["\n  position: fixed;\n  right: ", ";\n  bottom: 124px;\n  // width: 317px;\n  // width: 330px;\n  width: 345px;\n  display: flex;\n  flex-direction: column;\n  max-width: 100vw;\n  // height: 383px;\n  height: 371px;\n  max-height: 85vh;\n  visibility: hidden;\n  opacity: 0;\n  // overflow: hidden;\n  border-radius: 8px;\n  box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.38);\n  background-color: #ffffff;\n  z-index: 9999999;\n  line-height: normal;\n  letter-spacing: normal;\n  font-size: ", ";\n  font-family: ", ";\n  color: ", ";\n  animation: ", " 0.5s backwards;\n  :before {\n    width: 0;\n    height: 0;\n    border-left: 20px solid transparent;\n    border-right: 20px solid transparent;\n    border-top: 19px solid #fff;\n    content: \"\";\n    position: absolute;\n    z-index: 99999999;\n    bottom: -13px;\n    right: 45px;\n  }\n  &.help-visible {\n    animation: ", " 0.5s forwards;\n  }\n  @media (max-width: 768px) {\n    max-height: 100%;\n    height: 100vh;\n    width: 100vw;\n    border-radius: 0;\n    left: 0;\n    top: 0;\n  }\n"])), props => props.right, props => props.theme.font.fontSize, props => props.theme.font.fontFamily, props => props.theme.colors.textColor.primary, hideChatContainer, openChatContainer);

exports.HelpContainer = HelpContainer;
const zendeskScript = "https://dev.zopim.com/web-sdk/1.11.2/web-sdk.js";
let zChat = window.zChat;
const chatLogger = (0, _logger.getCustomLogger)("chatLogger");
const chatLogSpanId = "ZENDESK CHAT";
const testNetworkSpeed = new _networkSpeed.default();
let tabArray = [];
const chatbotLogger = (0, _logger.getCustomLogger)("chatbotLogger");
const chatbotLogId = "YM CHATBOT";

class ChatSupport extends _react.Component {
  constructor() {
    var _this;

    super(...arguments);
    _this = this;

    _defineProperty(this, "state", {
      chatInit: false,
      chatVisible: false,
      openChat: false,
      curState: // this.props.location.pathname.split("/")[2] === "joinClass" &&
      this.props.teacher && (0, _get2.default)(this.props, "getChatABdata.dataList", []).length ? "prechat_ab" : "prechat",
      //prechat_ab, prechat, chat, rate-chat, chat-history, offline-chat
      curDeptInfo: null,
      offlineMessage: "",
      chatInfo: {},
      notificationCount: 0,
      currTimestamp: null,
      pastChats: [],
      pastChatsAvailable: true,
      otpRequired: false,
      openHelpBox: false,
      helpCurState: "",
      //chat, email, email-ceo
      helpboxView: false,
      studentJoined: false,
      helpSectionStatus: false,
      isMinimized: false,
      activeParams: {},
      studentInternetSpeed: {},
      teacherInternetSpeed: {},
      techDeptInfo: null,
      firebaseEvents: [],
      firebaseSyncGIF: null,
      totalParticipants: [],
      inClassReschedule: false
    });

    _defineProperty(this, "checkStudentAvailability", () => {
      (0, _helper.subscribe)(_VcEvents.VCEvents.PARTICIPANTS_UPDATE, participants => {
        const allIdentities = participants.map(_ref => {
          let {
            identity
          } = _ref;
          return identity;
        });
        this.setState({
          totalParticipants: allIdentities
        }, () => {
          tabArray.forEach(user => {
            const matchFound = allIdentities.find(item2 => item2 === user.userId);
            const matchNotFound = !allIdentities.find(item2 => item2 === user.userId);

            if (matchFound) {
              user.isJoined = true;
            }

            if (matchNotFound && user.name !== "Me") {
              user.isJoined = false;
            }
          });
        });
      });
    });

    _defineProperty(this, "setClassTab", () => {
      const teacherInfo = (0, _get2.default)(this.props, "classInterfaceData.teacher", "");
      let classBooking = (0, _get2.default)(this.props, "classInterfaceData.class_booking", "");
      const studentBookings = (0, _helper.getStudentsInfo)(classBooking);
      studentBookings.map(std => {
        tabArray.push({
          type: "student",
          name: std.name,
          id: std.id,
          userId: std.userId,
          isJoined: false,
          speed: {}
        });
      });
      tabArray = [{
        type: "teacher",
        name: "Me",
        id: teacherInfo.id,
        userId: teacherInfo.userId,
        isJoined: true,
        speed: {}
      }, ...tabArray];
    });

    _defineProperty(this, "resetTabData", () => {
      this.setState({
        teacherInternetSpeed: {}
      }, () => {
        this.props.d__resetFormData();
        tabArray = tabArray.map(tab => _objectSpread(_objectSpread({}, tab), {}, {
          speed: {}
        }));
      });
    });

    _defineProperty(this, "handleSocketEvents", _ref2 => {
      let {
        type,
        target,
        state
      } = _ref2;

      switch (type) {
        case "GET_LATEST_SPEED":
          if (state && this.props.mode === "student") {
            this.getNetworkDownloadSpeed();
          }

          break;

        case "LATEST_SPEED":
          if (state && this.props.mode === "teacher") {
            this.setState({
              studentInternetSpeed: state.internetStatus
            }, () => {
              const studentId = (0, _get2.default)(this.props.student, "id", null);
              this.setSpeedData(state.source, state.internetStatus);
            });
          }

          break;

        default:
          break;
      }
    });

    _defineProperty(this, "initializeParams", () => {
      const studentId = (0, _get2.default)(this.props.student, "id", null);
      const {
        teacherId,
        vcMeetingId
      } = this.props.classInterfaceData;
      let params = {
        roomId: vcMeetingId,
        studentId,
        teacherId,
        mode: this.props.mode
      };
      this.setState({
        activeParams: params
      });
      (0, _socketUtil.initSocketService)(_objectSpread(_objectSpread({}, params), {}, {
        userId: params.mode === "teacher" ? teacherId : studentId,
        cb: this.handleSocketEvents,
        connectCb: () => {}
      }));
    });

    _defineProperty(this, "enableHelpSection", () => {
      let {
        teacher,
        student,
        appConfigData
      } = this.props;
      let configCourse = (0, _get2.default)(appConfigData, "configs.HELP_SECTION_ACTIVE.courseList", []);
      let teacherSkillsSet = (0, _get2.default)(teacher, "teacher_skills", []);
      let teacherCourseSkills = teacherSkillsSet.filter(item => item.skillName === "course");
      let teacherCourse = teacherCourseSkills.find(item => configCourse.includes((0, _get2.default)(item, "course.courseType", "")));
      let studentSkillsSet = configCourse.find(item => item == (0, _get2.default)(student, "activeCourse.courseType", ""));
      let helpSectionStatus = (0, _get2.default)(appConfigData.configs, "HELP_SECTION_ACTIVE.help_section_active_status", false);

      if (!window.location.pathname.includes(_routesMap.R__JOIN_CLASS) && (teacherCourse || studentSkillsSet)) {
        this.setState({
          helpSectionStatus
        });
      }
    });

    _defineProperty(this, "setHelpSectionFlag", status => {
      this.setState({
        helpSectionStatus: false
      }, () => {
        this.setState({
          openChat: true
        });
      });
    });

    _defineProperty(this, "resetHelpSectionFlag", status => {
      this.setState({
        helpSectionStatus: true
      }, () => {
        this.setState({
          openChat: false
        });
      });
    });

    _defineProperty(this, "checkIfStudentJoined", () => {
      if ((0, _get2.default)(this.props, "classInterfaceData.session_events", []).filter(x => x.eventName.toLowerCase() === "studentjoined").length > 0) {
        this.setState({
          studentJoined: true
        });
      } else {
        this.setState({
          studentJoined: false
        });
      }
    });

    _defineProperty(this, "getEvents", () => {
      return ["account_status", "chat"];
    });

    _defineProperty(this, "initScript", () => {
      let chatScript = document.createElement("script");
      chatScript.setAttribute("src", zendeskScript);
      chatScript.setAttribute("id", "zdk-snippet");
      chatScript.onload = this.initChat.bind(this, this.props.accountKey);
      document.body.appendChild(chatScript);
    });

    _defineProperty(this, "initChat", accountKey => {
      try {
        zChat = window.zChat;

        if (this.props.authenticatedUser && this.props.accountKey && this.props.getChatToken) {
          this.authenticateUser(this.props.accountKey, this.props.getChatToken);
        }

        this.getEvents().forEach(evt => {
          zChat.on(evt, this.evtHandler = data => {
            this.onEvent(evt, data);
          });
        });
      } catch (e) {
        chatLogger && chatLogger.error("".concat(chatLogSpanId, " : "), JSON.stringify(e));
        console.error(e);
      }
    });

    _defineProperty(this, "sendSegmentEventAction", async (obj, eventName) => {
      let segmentObj = this.toLowerCaseprops.eventSegmentProperties();
      let userPropRemain = await this.props.eventUserSegmentProperties();
      this.props.sendEventAction({
        event: eventName,
        properties: _objectSpread(_objectSpread(_objectSpread({}, segmentObj), userPropRemain), obj)
      });
    });

    _defineProperty(this, "onEvent", (evt, data) => {
      switch (evt) {
        case "account_status":
          if (zChat.isChatting() && !this.state.chatInit) {
            //another tab/browser/device
            this.setState({
              curState: "chat"
            });
          }

          this.setState({
            chatInit: true
          });
          break;

        case "chat":
          if (this.state.chatInit && zChat.isChatting() && this.state.curState !== "chat") {
            this.setState({
              curState: "chat"
            });
          }

          break;
      }
    });

    _defineProperty(this, "authenticateUser", (accountKey, getJwtToken) => {
      const {
        teacher,
        student
      } = this.props;

      try {
        zChat.init({
          account_key: accountKey,
          authentication: {
            jwt_fn: callback => {
              getJwtToken().then(res => {
                if (res.token) {
                  chatLogger && chatLogger.debug("".concat(chatLogSpanId, " : JWT authentication"));
                  callback(res.token);

                  if (this.props.showChat) {
                    this.showChat(true);
                  }
                }
              }).catch(e => {
                chatLogger && chatLogger.error("".concat(chatLogSpanId, " : JWT authentication failed"), JSON.stringify(e));
              });
            }
          }
        });
      } catch (e) {
        chatLogger && chatLogger.error("".concat(chatLogSpanId, " : Error in auth "), JSON.stringify(e));
        console.error(e);
      }
    });

    _defineProperty(this, "handleChatABwhenChatisOpen", prevProps => {
      if (this.props.getChatABdata) {
        if (!["chat", "rate-chat", "chat-history", "offline-chat"].includes(this.state.curState)) {
          if (!(0, _isEqual2.default)(prevProps.getChatABdata.dataList, this.props.getChatABdata.dataList)) {
            if (this.props.getChatABdata.dataList.length && this.props.teacher && this.state.curState !== "prechat_ab") {
              this.updateChatState("prechat_ab", null);
            } else if (!this.props.getChatABdata.dataList.length && this.state.curState !== "prechat") {
              this.updateChatState("prechat", null);
            }
          }
        }
      }
    });

    _defineProperty(this, "handleAutoChatStart", () => {
      const chatData = (0, _get2.default)(this.props, "chatTriggerData", null);

      if (this.props.showChat && chatData) {
        this.openChat(true);
        this.handleAgentOnlineState(chatData);
        chatLogger && chatLogger.debug("".concat(chatLogSpanId, " : Triggering chat from outer source"), {
          chatData
        });
      }
    });

    _defineProperty(this, "handleChatABwhenChatisOpen", prevProps => {
      if (this.props.getChatABdata) {
        if (!["chat", "rate-chat", "chat-history", "offline-chat"].includes(this.state.curState)) {
          if (!(0, _isEqual2.default)(prevProps.getChatABdata.dataList, this.props.getChatABdata.dataList)) {
            if (this.props.getChatABdata.dataList.length && this.props.teacher && this.state.curState !== "prechat_ab") {
              this.updateChatState("prechat_ab", null);
            } else if (!this.props.getChatABdata.dataList.length && this.state.curState !== "prechat") {
              this.updateChatState("prechat", null);
            }
          }
        }
      }
    });

    _defineProperty(this, "handleAgentOnlineState", data => {
      const departmentId = data.deptId;
      const departmentStatus = zChat.getDepartment(departmentId);

      if (departmentStatus && departmentStatus.status !== "online") {
        this.addOfflineChatTag();
        this.setState({
          curState: "offline-chat",
          curDeptInfo: data
        });
        chatLogger && chatLogger.debug("".concat(chatLogSpanId, " : Handling offline chat"), {
          chatData: {
            data: data,
            zendeskDepartmentData: departmentStatus,
            departmentStatus: departmentStatus.status
          }
        });
      } else {
        this.setState({
          curState: "chat",
          chatInfo: data
        });
        chatLogger && chatLogger.debug("".concat(chatLogSpanId, " : Handling online chat"), {
          chatData: {
            data: data,
            zendeskDepartmentData: departmentStatus,
            departmentStatus: departmentStatus.status
          }
        });
      }
    });

    _defineProperty(this, "addOfflineChatTag", () => {
      let offlineTags = null;

      try {
        const tags = JSON.parse((0, _utm.getCookie)("chatSupportTags"));
        offlineTags = JSON.parse((0, _utm.getCookie)("chatOfflineTags"));

        if (tags && typeof tags === "object" && tags.length) {
          //remove all tags from previous chat
          zChat.removeTags(tags);
        }
      } catch (e) {
        chatLogger && chatLogger.error("".concat(chatLogSpanId, " : Error in addding tags "), JSON.stringify(e));
        console.error(e);
      }

      zChat.addTags(offlineTags);
      (0, _utm.setCookie)("chatSupportTags", JSON.stringify(offlineTags));
    });

    _defineProperty(this, "handleOfflineAgentMessage", message => {
      const {
        display_name,
        email
      } = zChat.getVisitorInfo();
      chatLogger && chatLogger.debug("".concat(chatLogSpanId, " : Sending offline message"), {
        chatData: {
          department: this.state.curDeptInfo.deptId
        }
      });
      zChat.sendOfflineMsg({
        name: display_name,
        email: email,
        phone: "",
        message: message,
        department: this.state.curDeptInfo.deptId
      }, err => {
        if (!err) {
          this.resetChatState(false);
        }

        chatLogger && chatLogger.error("".concat(chatLogSpanId, " : Error in sending offline message"), JSON.stringify(err));
      });

      if (Object.keys(this.props.chatTriggerData || {}).length) {
        this.props.d__triggerChatEnd();
        this.setState({
          isMinimized: false
        });
      }
    });

    _defineProperty(this, "resetChatState", function () {
      let openChatWindow = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;

      _this.setState({
        curState: _this.props.location.pathname.split("/")[2] === "joinClass" && (0, _get2.default)(_this.props, "getChatABdata.dataList", []).length && _this.props.teacher ? "prechat_ab" : "prechat",
        openChat: openChatWindow,
        offlineMessage: ""
      }, () => {
        if (Object.keys(_this.props.chatTriggerData || {}).length) _this.props.d__triggerChatEnd();

        _this.resetTabData();

        _this.setState({
          isMinimized: false
        });
      });
    });

    _defineProperty(this, "openChat", show => {
      const {
        student,
        isParentalVerified,
        teacher,
        appConfigData
      } = this.props; //enable help section based on student and teacher courses

      let configCourse = (0, _get2.default)(appConfigData, "configs.HELP_SECTION_ACTIVE.courseList", []);
      let teacherSkillsSet = (0, _get2.default)(teacher, "teacher_skills", []);
      let teacherCourseSkills = teacherSkillsSet.filter(item => item.skillName === "course");
      let teacherCourse = teacherCourseSkills.find(item => configCourse.includes((0, _get2.default)(item, "course.courseType", "")));
      let studentSkillsSet = configCourse.find(item => item == (0, _get2.default)(student, "activeCourse.courseType", ""));
      const isOtpOnCancellation = (0, _get2.default)(student, "isOtpOnCancellation", false);

      if (isOtpOnCancellation && !isParentalVerified) {
        this.props.d__showOtpDialog({
          showDialog: "CHAT_WITH_US",
          title: "Chat Confirm",
          extraData: {
            actionType: "CHAT_WITH_US",
            data: {}
          }
        });
        this.setState({
          otpRequired: true
        });
      } else {
        this.openChatConfirmed(show);
        if ((0, _get2.default)(this.props, "appConfigData.configs.HELP_SECTION_ACTIVE.help_section_active_status", false) && !window.location.pathname.includes(_routesMap.R__JOIN_CLASS) && (teacherCourse || studentSkillsSet)) this.setState({
          helpSectionStatus: !this.state.helpSectionStatus
        });
      }
    });

    _defineProperty(this, "setHelpSection", show => {
      let {
        teacher,
        student
      } = this.props;
      this.setState({
        helpSection: !this.state.helpSection
      });
      (0, _sendEventOnSegment.sendEventsOnSegmentHOF)({
        sendEvents: this.props.sendEventAction,
        eventName: "helpdesk_entity_clicked",
        properties: {
          user_id: teacher.id || student.id,
          user_email: teacher.email || student.email,
          user: teacher ? "Teacher" : "Student",
          action_time: new Date().getTime(),
          entity_source: (0, _get2.default)(window, "location.pathname", "").split("/").pop(),
          entity: "Help"
        }
      });
    });

    _defineProperty(this, "openChatConfirmed", show => {
      this.inClassSegmentEvent(show);

      try {
        if (!show) this.setState({
          timestamp: Date.now()
        });
        this.setState({
          openChat: show,
          notificationCount: 0
        });

        if (this.props.invokeNotificationCountEvent) {
          this.handleNotificationCountChange(0);
        }

        chatLogger && chatLogger.debug("".concat(chatLogSpanId, " : chat visible"), {
          chatData: {
            show
          }
        });

        if (zChat && zChat.isChatting() && this.state.chatInit) {
          //another tab/browser/device
          this.setState({
            curState: "chat"
          });
          chatLogger && chatLogger.debug("".concat(chatLogSpanId, " : previous chat to continue"));
        }
      } catch (e) {
        console.error(e);
        chatLogger && chatLogger.error("".concat(chatLogSpanId, " : Error in openChatConfirmed() method "), JSON.stringify(e));
      }
    });

    _defineProperty(this, "showChat", show => {
      this.setState({
        chatVisible: show
      });
    });

    _defineProperty(this, "setSpeedData", (id, speed) => {
      tabArray.map(arr => {
        if (arr.id === id) {
          arr.speed = speed;
        }

        return arr;
      });
    });

    _defineProperty(this, "getNetworkDownloadSpeed", async () => {
      const fileSize = parseInt((0, _get2.default)(this.props.appConfigData, "configs.TEACHER_LED_CONFIG.fileSize", 5));
      const baseUrl = "".concat(_config.config.SPEED_TEST_URL).concat(fileSize, "mb.bin?timestamp=").concat((0, _moment.default)().format());
      const fileSizeInBytes = fileSize * Math.pow(1024, 2);
      const speed = await testNetworkSpeed.checkDownloadSpeed(baseUrl, fileSizeInBytes);

      if (this.props.mode === "teacher") {
        if (this.state.curState === "tech-issue") {
          let teacherInfo = (0, _get2.default)(this.props, "classInterfaceData.teacher", "");
          speed["id"] = teacherInfo.id;
          this.setState({
            teacherInternetSpeed: speed
          }, () => {
            this.setSpeedData(teacherInfo.id, speed);
          });
        }
      } else if (this.props.mode === "student") {
        const studentId = (0, _get2.default)(this.props.student, "id", null);
        speed["id"] = studentId;
        this.setState({
          studentInternetSpeed: speed
        }, () => {
          this.setSpeedData(studentId, speed);
        });
      }

      const args = {
        user_details: speed,
        mode: this.props.mode
      };
      this.handleEventTrack(_segmentEvents.EVENT_NAMES.INTERNET_SPEED, args);
    });

    _defineProperty(this, "updateChatState", (state, data) => {
      switch (state) {
        case "prechat_ab":
          this.setState({
            curState: "prechat_ab",
            curDeptInfo: null,
            chatInfo: {}
          });
          chatLogger && chatLogger.debug("".concat(chatLogSpanId, " : chat action"), {
            chatData: {
              state
            }
          });
          break;

        case "prechat":
          this.setState({
            curState: "prechat",
            curDeptInfo: null,
            chatInfo: {}
          });
          chatLogger && chatLogger.debug("".concat(chatLogSpanId, " : chat action"), {
            chatData: {
              state
            }
          });
          break;

        case "start-chat":
          if (data.deptId && Number.isInteger(data.deptId)) {
            this.handleAgentOnlineState(data);
          }

          chatLogger && chatLogger.debug("".concat(chatLogSpanId, " : chat action"), {
            chatData: {
              state
            }
          });
          break;

        case "chat-history":
          this.setState({
            curState: "chat-history",
            curDeptInfo: null
          });
          const args = {
            button_type: _segmentEvents.BUTTON_TYPE.ARROW
          };
          this.handleEventTrack(_segmentEvents.EVENT_NAMES.CONCIERGE_PAST_CHAT_CLICKED, args);
          chatLogger && chatLogger.debug("".concat(chatLogSpanId, " : chat action"), {
            chatData: {
              state
            }
          });
          break;

        case "tech-issue":
          const isChatABData = (0, _get2.default)(this.props, "getChatABdata.dataList", []);

          if (isChatABData.length > 0) {
            const techFilteredData = isChatABData.find(obj => {
              return obj.action === "CUSTOM_INTERNET_SPEED";
            });
            const techIssueInfo = {
              deptId: parseInt(techFilteredData.dept),
              deptName: "",
              issueType: techFilteredData.name,
              issueDetail: ""
            };
            this.setState({
              techDeptInfo: techIssueInfo
            });
          }

          this.setState({
            curState: "tech-issue"
          }, () => {
            this.getNetworkDownloadSpeed();
            const {
              teacherId,
              vcMeetingId
            } = this.props.classInterfaceData;
            const event_name = _segmentEvents.EVENT_NAMES.TECH_ISSUE;
            const args = {
              teacherId,
              vcMeetingId
            };
            this.handleEventTrack(event_name, args);
          });
          break;

        case "connect-agent":
          this.setState(prevState => ({
            techDeptInfo: _objectSpread(_objectSpread({}, prevState.techDeptInfo), {}, {
              issueDetail: data.responseMsgs,
              speedData: data.speed ? data : {}
            })
          }), () => {
            this.props.d__triggerChatStart(this.state.techDeptInfo);
            this.handleAgentOnlineState(this.state.techDeptInfo);
          });
      }
    });

    _defineProperty(this, "handlePastChats", function (data) {
      let pastChatsAvailable = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;

      if (data) {
        _this.state.pastChats = data;
      } else {
        _this.state.pastChatsAvailable = pastChatsAvailable;
      }
    });

    _defineProperty(this, "handleNewChatIncrement", () => {
      let notificationCount = this.state.notificationCount;
      notificationCount++;
      this.setState({
        notificationCount
      });

      if (this.props.invokeNotificationCountEvent) {
        this.handleNotificationCountChange(notificationCount);
      }
    });

    _defineProperty(this, "handleChatEnd", function () {
      let isCancel = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;

      if (isCancel) {
        const args = {
          button_type: _segmentEvents.BUTTON_TYPE.ARROW,
          concierge_issue_type: _this.state.chatInfo.issueType ? _this.state.chatInfo.issueType : ""
        };

        _this.handleEventTrack(_segmentEvents.EVENT_NAMES.CONCIERGE_CHAT_CANCELLED_CLICKED, args);
      }

      if (!isCancel) {
        _this.openChat(false);

        _this.resetTabData();

        if (Object.keys(_this.props.chatTriggerData || {}).length) {
          _this.props.d__triggerChatEnd();
        }

        const args = {
          button_type: _segmentEvents.BUTTON_TYPE.ARROW,
          concierge_issue_type: _this.state.chatInfo.issueType ? _this.state.chatInfo.issueType : ""
        };

        _this.handleEventTrack(_segmentEvents.EVENT_NAMES.CONCIERGE_CHAT_END_CLICKED, args);

        _this.setState({
          isMinimized: false
        });
      }

      _this.updateChatState(window.location.pathname.split("/")[2] === "joinClass" && (0, _get2.default)(_this.props, "getChatABdata.dataList", []).length && _this.props.teacher ? "prechat_ab" : "prechat");

      chatLogger && chatLogger.debug("".concat(chatLogSpanId, " : chat has ended"));
    });

    _defineProperty(this, "openHelpModal", function (show) {
      let isClicked = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

      _this.setState({
        openHelpBox: show,
        helpCurState: ""
      }); //triggered the event when user manually clicked for minimized need help


      if (isClicked) {
        const args = {
          button_type: _segmentEvents.BUTTON_TYPE.CROSS
        };

        _this.handleEventTrack(_segmentEvents.EVENT_NAMES.CONCIERGE_MINIMIZED_CLICKED, args);
      }
    });

    _defineProperty(this, "handleHelpCurState", curState => {
      this.setState({
        openHelpBox: false,
        helpCurState: curState
      });

      if (curState === "chat") {
        this.openChat(true);
        const args = {
          button_type: _segmentEvents.BUTTON_TYPE.ARROW
        };
        this.handleEventTrack(_segmentEvents.EVENT_NAMES.CONCIERGE_CHAT_CLICKED, args);
      }
    });

    _defineProperty(this, "closeHelpHandler", (type, isBackButton) => {
      this.setState({
        openHelpBox: false,
        helpCurState: ""
      });

      if (type === "chat") {
        this.openChat(false);
      }

      if (isBackButton) {
        this.setState({
          openHelpBox: true
        });
        const args = {
          button_type: _segmentEvents.BUTTON_TYPE.ARROW
        };
        this.handleEventTrack(_segmentEvents.EVENT_NAMES.CONCIERGE_BACK_ARROW_CLICKED, args);
      }
    });

    _defineProperty(this, "helpVisibleEventB", visible => {
      this.setState({
        helpboxView: visible
      });

      if (visible) {
        (0, _sendEventOnSegment.sendEventsOnSegmentHOF)({
          sendEvents: this.props.sendEventAction,
          eventName: _segmentEvents.EVENT_NAMES.IMPRESSION_SENT,
          properties: {
            ab_version: _segmentEvents.AB_VERSIONS.B_VERSION,
            position: _segmentEvents.COMPONENT_POSITION.RIGHT_PANEL,
            page_source: _segmentEvents.PAGE_SOURCE.DASHBOARD,
            event_category: _segmentEvents.EVENT_CATEGORY.CONCIERGE
          }
        });
      }
    });

    _defineProperty(this, "handleEventTrack", (eventName, args) => {
      if (this.props.student || this.props.teacher) {
        (0, _sendEventOnSegment.sendEventsOnSegmentHOF)({
          sendEvents: this.props.sendEventAction,
          eventName: eventName,
          properties: _objectSpread({
            ab_version: _segmentEvents.AB_VERSIONS.B_VERSION,
            position: _segmentEvents.COMPONENT_POSITION.RIGHT_PANEL,
            event_category: _segmentEvents.EVENT_CATEGORY.CONCIERGE,
            page_source: _segmentEvents.PAGE_SOURCE.DASHBOARD
          }, args)
        });
      }
    });

    _defineProperty(this, "handleChatbot", () => {
      chatbotLogger && chatbotLogger.debug("".concat(chatbotLogId, " opened by teacher with Id "), {
        teacherId: (0, _get2.default)(this.props, "teacher.id", "")
      });
      this.props.showChatbot();
    });

    _defineProperty(this, "checkJoinClass", () => window.location.pathname === "/s/joinClass");

    _defineProperty(this, "handleNotificationCountChange", notificationCount => {
      const anotherEvent = new CustomEvent("onNotificationCountChange", {
        detail: {
          notificationCount
        }
      });
      document.dispatchEvent(anotherEvent);
    });

    _defineProperty(this, "openActiveChat", () => {
      const activeChatData = localStorage.getItem("activeChat");

      try {
        this.props.d__openChatwindow(_objectSpread({}, JSON.parse(activeChatData)));
      } catch (e) {
        console.log(e);
      }
    });

    _defineProperty(this, "teacherEndClass", function () {
      let feedbackResponseResult = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
      let classData = _this.props.classInterfaceData;
      classData = _objectSpread(_objectSpread({}, classData), {}, {
        ncReasonCode: "Tech Issue",
        ncReasonDesc: "NST",
        classStatus: "not_completed",
        remark: "Slow Internet Speed"
      });
      (0, _helper.endClassSubmit)(feedbackResponseResult, false, classData, _this.props.classInterfaceData, _this.props.isReportCardFeedbackQuestionsAvailable, _this.state.firebaseEvents, _this.state.firebaseSyncGIF).then(response => {
        _this.props.endClass(response.requestParam);
      });

      const firebaseRef = _firebase.default.database().ref("public/mobile-sessions-sync/".concat(classData.bookingId));

      firebaseRef.once("value").then(function (snapshot) {
        const data = snapshot.val();

        if (data && data.currentActivity) {
          firebaseRef.update({
            completed: true
          });
        }
      });
      (0, _helper.closePIP)();

      _this.sendSegmentEventAction({}, "Class end confirmed"); // trackEvent(
      //   {
      //     booking_id: _get(classData, "bookingId"),
      //     nc_reason_code: _get(classData, "ncReasonCode"),
      //     nc_reason_desc: _get(classData, "ncReasonDesc"),
      //     marked_from: "Teacher",
      //     status: _get(classData, "status"),
      //     course_type: "Math",
      //     class_session_id: _get(classData, "class_booking.sessionId")
      //   },
      //   "End Class due to network issue"
      // );


      _this.props.navigateToPath("/t/dashboard");

      _this.handleEventTrack(_segmentEvents.EVENT_NAMES.END_CLASS, classData);

      _this.setState({
        curState: "prechat"
      }, () => {
        _this.openChat(false);
      });
    });
  }

  componentDidMount() {
    this.setState({
      currTimestamp: Date.now()
    });
    this.initScript();
    this.checkIfStudentJoined();

    if (this.props.classInterfaceData && this.props.classInterfaceData.id) {
      this.checkStudentAvailability();
      this.initFirebaseSessionEvent();
      this.setClassTab();
    }
  }

  componentDidUpdate(prevProps, prevState) {
    // In order to check whether the teacher lies in INCLASS_INSTANT_CLASS_RESCHEDULE feature config or not.
    if ((0, _get2.default)(this.props, "location.pathname") === _routesMap.T__JOIN_CLASS && (0, _get2.default)(this.props, "featureConfig", []) !== (0, _get2.default)(prevProps, "featureConfig", [])) {
      let checkReschedule = false;
      (0, _get2.default)(this.props, "featureConfig", []).map(item => {
        item.name === _opsConfig.OPS_CONFIG_CONSTANTS.INCLASS_INSTANT_CLASS_RESCHEDULE && (checkReschedule = true);
      });
      let checkIsTrial = (0, _get2.default)(this.props, "classInterfaceData.isTrial", false);
      checkReschedule && checkIsTrial && this.setState({
        inClassReschedule: true
      });
    }

    if (prevProps.showChat !== this.props.showChat) {
      this.showChat(this.props.showChat);
    }

    if ((0, _get2.default)(prevProps, "classInterfaceData.session_events", []).length !== (0, _get2.default)(this.props, "classInterfaceData.session_events", []).length) {
      this.checkIfStudentJoined();
    }

    this.handleChatABwhenChatisOpen(prevProps); // for handling ab_chat screen

    if ((0, _helper.h__isArtScope)() && prevProps.outputActivityState !== this.props.outputActivityState) {
      if (this.props.outputActivityState.openHelpChatModal) {
        this.openHelpModal(true, true);
      }
    }

    if (prevProps.appConfigData !== this.props.appConfigData) {
      //enable help section based on student and teacher courses
      this.enableHelpSection();
    }

    if (this.state.helpSectionStatus && window.location.pathname.includes(_routesMap.R__JOIN_CLASS)) {
      this.setState({
        helpSectionStatus: false
      });
    }

    if (!(0, _isEqual2.default)(prevProps.chatTriggerData, this.props.chatTriggerData) && Object.keys(this.props.chatTriggerData).length) {
      this.handleAutoChatStart();
    }

    if (prevProps.classInterfaceData.teacherId !== this.props.classInterfaceData.teacherId) {
      this.initializeParams();
    }

    if (prevState.studentInternetSpeed !== this.state.studentInternetSpeed) {
      if (Object.keys(this.state.studentInternetSpeed).length !== 0 && this.props.mode === "student") {
        const studentId = (0, _get2.default)(this.props.student, "id", null);
        const {
          teacherId
        } = this.props.classInterfaceData;
        (0, _socketUtil.sendSocketMessage)({
          type: "LATEST_SPEED",
          state: {
            internetStatus: this.state.studentInternetSpeed,
            source: this.props.mode === "student" ? studentId : teacherId
          },
          target: "*"
        });
      }
    }

    if ((this.props.classInterfaceData && this.props.classInterfaceData.id) !== (prevProps.classInterfaceData && prevProps.classInterfaceData.id)) {
      this.initFirebaseSessionEvent();
    } // CHECK WHETHER STUDENT IS IN THE CLASS USING TWILIO


    if ((this.props.classInterfaceData && this.props.classInterfaceData.id) !== (prevProps.classInterfaceData && prevProps.classInterfaceData.id) || this.state.totalParticipants.length !== prevState.totalParticipants.length) {
      this.checkStudentAvailability();
    } // TO RESET THE CHAT TAB STATES WHEN NOT ON JOIN CLASS


    if (!(0, _isEqual2.default)((0, _get2.default)(prevProps, "getChatABdata.dataList", []).length, (0, _get2.default)(this.props, "getChatABdata.dataList", []).length)) {
      if (window.location.pathname.includes(_routesMap.R__JOIN_CLASS)) {
        this.setClassTab();
      } else {
        this.setState({
          teacherInternetSpeed: {}
        }, () => {
          tabArray = [];
        });
      }
    }
  }

  onSessionChangeEvent(snapshot) {
    let value = snapshot.val();

    if (value && value.lastUpdatedTimestamp) {
      if (this.props.classInterfaceData.id) {
        this.props.refreshSessionAction({
          sessionId: this.props.classInterfaceData.id
        });
      }
    }
  }

  onTimerGIFEvent(snapshot, timerGIFEvent) {
    this.setState({
      firebaseSyncGIF: timerGIFEvent
    });
  }

  initFirebaseSessionEvent() {
    if (this.props.classInterfaceData.id) {
      let eventTrack = (0, _firebase.trackFirebaseEvent)("protected/sessions/".concat(this.props.classInterfaceData.id), snapshot => this.onSessionChangeEvent(snapshot));
      let timerGIFEvent = (0, _firebase.firebaseSync)("protected_math_session/".concat(this.props.classInterfaceData.id), snapshot => this.onTimerGIFEvent(snapshot, timerGIFEvent));
      let firebaseEvents = [...this.state.firebaseEvents];
      firebaseEvents.push(eventTrack);
      this.setState({
        firebaseEvents
      });
    }
  }

  componentWillUnmount() {
    this.getEvents().forEach(evt => {
      zChat && zChat.un(evt, this.evtHandler);
    });
    zChat && zChat.logout();
    this.showChat(false);
  }

  inClassSegmentEvent(show) {
    const {
      teacher,
      student
    } = this.props;
    let classSessionData = (0, _get2.default)(this.props, "classInterfaceData.class_booking.session_id", "");

    if (classSessionData != "") {
      if ((0, _get2.default)(teacher, "email", null) != null) {
        if (show) {
          this.sendSegmentEventAction({}, "Teacher concierge chat opened");
        } else {
          this.sendSegmentEventAction({}, "Teacher concierge chat closed");
        }
      } else if ((0, _get2.default)(student, "email", null) != null) {
        if (show) {
          this.sendSegmentEventAction({}, "Student concierge chat opened");
        } else {
          this.sendSegmentEventAction({}, "Student concierge chat closed");
        }
      }
    }
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const isOtpOnCancellation = nextProps.student && nextProps.student.isOtpOnCancellation;
    const openChat = isOtpOnCancellation && prevState.otpRequired && nextProps.isParentalVerified;

    if (isOtpOnCancellation && prevState.otpRequired) {
      const st = _objectSpread(_objectSpread({}, prevState), {}, {
        otpRequired: isOtpOnCancellation && !nextProps.isParentalVerified,
        openChat: openChat,
        notificationCount: 0
      });

      if (openChat) {
        st.timestamp = Date.now();
      }

      if (zChat && zChat.isChatting() && prevState.chatInit) {
        st.curState = "chat";
      }

      return st;
    }

    return null;
  }

  render() {
    console.log("props and state internal>>>", this.props, this.state);
    const studentId = (0, _get2.default)(this.props.student, "id", null);
    const chatBubbleVisible = !this.state.chatVisible || this.state.openChat;
    return /*#__PURE__*/_react.default.createElement(_theme.default, null, /*#__PURE__*/_react.default.createElement("div", {
      id: "chat-zdk"
    }, /*#__PURE__*/_react.default.createElement(ChatContainer, {
      className: "".concat(this.state.openChat && this.state.chatVisible && !this.state.otpRequired ? "chat-visible" : ""),
      right: this.props.showPlayGround ? "34%" : "25px"
    }, /*#__PURE__*/_react.default.createElement(_ChatTitle.default, {
      chatState: this.state.curState,
      resetChatState: this.resetChatState,
      openChat: val => {
        this.setState({
          isMinimized: true
        });
        this.openChat(val);
      },
      isClickedFromHelpBox: this.state.helpCurState === "chat" ? true : false,
      closeHelpHandler: this.closeHelpHandler
    }), this.state.curState === "prechat_ab" && /*#__PURE__*/_react.default.createElement(_Prechat_AB.default, {
      issueData_AB: (0, _get2.default)(this.props, "getChatABdata.dataList", []),
      chatTrigger: this.updateChatState,
      showHistoryBtn: true,
      handleEventTrack: this.handleEventTrack,
      chatBotShow: this.handleChatbot,
      hideChat: this.openChat,
      studentJoined: this.state.studentJoined,
      classStartTime: (0, _get2.default)(this.props, "classInterfaceData.class_booking.startTime", "")
    }), this.state.curState === "prechat" && /*#__PURE__*/_react.default.createElement(_Prechat.default, {
      issueData: this.props.getChatIssues,
      prechatABlayer: (0, _get2.default)(this.props, "getChatABdata.dataList", []),
      chatTrigger: this.updateChatState,
      showHistoryBtn: true,
      handleEventTrack: this.handleEventTrack,
      openChat: this.openChat,
      chatLogger: chatLogger,
      handleReschedulePopup: this.props.d__toggleClassNotCompletedPopupVisibility,
      inClassReschedule: this.state.inClassReschedule
    }), this.state.curState === "tech-issue" && /*#__PURE__*/_react.default.createElement(_InternetSpeedTest.default, {
      mode: this.props.mode,
      activeParams: this.state.activeParams,
      chatIssueHandler: this.updateChatState,
      openChat: this.openChat,
      resetChatState: this.resetChatState,
      teacherInternetSpeed: this.state.teacherInternetSpeed,
      studentInternetSpeed: this.state.studentInternetSpeed,
      handleEventTrack: this.handleEventTrack,
      endTeacherClass: this.teacherEndClass,
      tabArray: tabArray,
      speedLimit: (0, _get2.default)(this.props.appConfigData, "configs.TEACHER_LED_CONFIG.internetSpeed", 0)
    }), this.state.curState === "offline-chat" && /*#__PURE__*/_react.default.createElement(_OfflineChat.default, {
      message: this.state.offlineMessage,
      onSubmit: this.handleOfflineAgentMessage,
      takeBack: this.resetChatState
    }), this.state.curState === "chat" && /*#__PURE__*/_react.default.createElement(_Chat.default, {
      chatInfo: this.state.chatInfo,
      chatEnded: this.handleChatEnd,
      tags: this.props.tags,
      newChatHandle: this.handleNewChatIncrement,
      timestamp: this.state.currTimestamp,
      autoStart: this.props.autoStart,
      handleEventTrack: this.handleEventTrack
    }), this.state.curState === "chat-history" && /*#__PURE__*/_react.default.createElement(_ChatHistory.default, {
      handlePastChats: this.handlePastChats,
      pastChats: this.state.pastChats,
      chatAvailable: this.state.pastChatsAvailable
    })), /*#__PURE__*/_react.default.createElement(HelpContainer, {
      className: "".concat(this.state.openHelpBox ? "help-visible" : ""),
      right: this.props.showPlayGround ? "34%" : "25px"
    }, this.state.helpCurState === "email" && /*#__PURE__*/_react.default.createElement(_ConciergeHelpDesk.default, {
      classes: "",
      supportNumber: "",
      id: studentId,
      type: "STUDENT",
      student: this.props.student,
      teacher: null,
      isClickedFromHelpBox: true,
      closeHelpHandler: this.closeHelpHandler
    }), this.state.helpCurState === "email-ceo" && /*#__PURE__*/_react.default.createElement(_WriteToCeo.default, {
      nameInfo: this.props.teacher ? this.props.teacher : this.props.student,
      submitFeedbackEscalation: this.props.submitFeedbackEscalation,
      ab_version: _segmentEvents.AB_VERSIONS.A_VERSION,
      sendEvents: this.props.sendEventAction,
      isClickedFromHelpBox: true,
      closeHelpHandler: this.closeHelpHandler
    }), this.state.openHelpBox && /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, /*#__PURE__*/_react.default.createElement(_reactVisibilitySensor.default, {
      active: !this.state.helpboxView,
      onChange: this.helpVisibleEventB
    }, /*#__PURE__*/_react.default.createElement(_NeedHelp.default, {
      student: this.props.student,
      openHelpModal: this.openHelpModal,
      handleHelpCurState: this.handleHelpCurState,
      appConfigData: this.props.appConfigData
    })))), /*#__PURE__*/_react.default.createElement(_helpCenter.default, {
      helpSectionStatus: this.state.helpSectionStatus,
      helpSection: this.state.helpSection,
      showPlayGround: this.state.showPlayGround,
      close: () => this.setState({
        helpSection: false
      }),
      sendEvents: this.props.sendEventAction,
      student: this.props.student,
      teacher: this.props.teacher,
      chatDataAll: this.props.chatDataAll
    }), this.state.helpSectionStatus && this.state.isMinimized && this.props.chatDataAll( /*#__PURE__*/_react.default.createElement(_ChatBubble.default, {
      hidden: this.props.invokeNotificationCountEvent ? this.checkJoinClass() ? true : chatBubbleVisible : chatBubbleVisible,
      handleClick: this.openChat,
      notificationCount: this.state.notificationCount,
      showPlayGround: this.props.isPlaygroundOpen,
      pathName: (0, _get2.default)(this.props, "location.pathname"),
      isTeacher: this.props.teacher ? true : false,
      className: "minimized-chat",
      setHelpSectionFlag: this.setHelpSectionFlag,
      resetHelpSectionFlag: this.resetHelpSectionFlag,
      resetHelpCenter: () => this.setState({
        helpSectionStatus: false
      }),
      location: this.props.location.pathname,
      chatDataAll: this.props.chatDataAll
    })), this.props.chatDataAll && /*#__PURE__*/_react.default.createElement(_ChatBubble.default, {
      hidden: this.props.invokeNotificationCountEvent ? this.checkJoinClass() ? true : chatBubbleVisible : chatBubbleVisible,
      handleClick: this.state.helpSectionStatus ? this.setHelpSection : this.openChat,
      handleHelpClick: this.openHelpModal,
      notificationCount: this.state.notificationCount,
      showPlayGround: this.props.isPlaygroundOpen,
      pathName: (0, _get2.default)(this.props, "location.pathname"),
      isTeacher: this.props.teacher ? true : false,
      isHelpSectionActive: this.state.helpSectionStatus,
      setHelpSectionFlag: this.setHelpSectionFlag,
      resetHelpSectionFlag: this.resetHelpSectionFlag,
      resetHelpCenter: () => this.setState({
        helpSectionStatus: false
      }),
      location: this.props.location.pathname
    })));
  }

}

ChatSupport.defaultProps = {
  authenticatedUser: true,
  showChat: true,
  autoStart: true,
  tags: {}
};
ChatSupport.propTypes = {
  accountKey: _propTypes.default.string.isRequired,
  authenticatedUser: _propTypes.default.bool,
  getChatToken: _propTypes.default.func,
  tags: _propTypes.default.object,
  showChat: _propTypes.default.bool,
  autoStart: _propTypes.default.bool
}; // const mapStateToProps = (state) => ({
//   student: s__getStudent(state),
//   teacher: s__getTeacher(state),
//   classInterfaceData: getClassInterfacingDetails(state),
//   isParentalVerified: isParentalVerified(state),
//   outputActivityState: s__outputActivity(state),
//   appConfigData: getAppConfigData(state),
//   isReportCardFeedbackQuestionsAvailable: s__isReportCardFeedbackQuestionsAvailable(
//     state
//   ),
//   featureConfig: s__featureConfig(state),
//   teacherUpcomingClasses: getTeacherUpcomingClass(state),
// });
// const mapDispatchToProps = (dispatch) => ({
//   d__showOtpDialog: (data) => {
//     dispatch(otpDialogShow.set(data));
//   },
//   submitFeedbackEscalation: (payload) => {
//     dispatch(submitFeedbackEscalation.request({ payload }));
//   },
//   sendEventAction: (data) => {
//     dispatch(segmentEvents.trackEvent(data));
//   },
//   showChatbot: () => {
//     dispatch(chatbotAction.show());
//   },
//   d__triggerChatEnd: () => {
//     dispatch(triggerChat.end({}));
//   },
//   d__triggerChatStart: (data) => {
//     dispatch(triggerChat.start(data));
//   },
//   chatMinimizeForToggleState: data =>
//     dispatch(chatMinimizeForToggle.chatMinimizeState(data)),
//   endClass: (data) => {
//     dispatch(endClassTeacher.request(data));
//   },
//   navigateToPath: (path) => {
//     dispatch(navigateToPath.request(path));
//   },
//   refreshSessionAction: () => {
//     dispatch(refreshSessionClass.request());
//   },
//   d__resetFormData: () => dispatch(fetchTypeformResponse.reset()),
//   d__toggleClassNotCompletedPopupVisibility: (data) =>
//     dispatch(classNotCompletedPopup.toggleVisibility(data))
// });
// export default connect(
//   mapStateToProps,
//   mapDispatchToProps
// )(withRouter(ChatSupport));

var _default = ChatSupport;
exports.default = _default;
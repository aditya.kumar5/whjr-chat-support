"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _styledComponents = require("styled-components");

var _styles = require("../styles");

var _backIcon = require("../assets/backIcon.svg");

var _templateObject, _templateObject2, _templateObject3;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

const ChatTitle = _ref => {
  let {
    chatState,
    resetChatState,
    openChat,
    isClickedFromHelpBox,
    closeHelpHandler
  } = _ref;

  const ChatHeader = _styledComponents.styled.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-align-items: center;\n  -webkit-box-align: center;\n  -ms-flex-align: center;\n  align-items: center;\n  padding-left: 20px;\n  padding-right: 10px;\n  color: ", ";\n  background-color: ", ";\n  font-size: 0.875em;\n  position: relative;\n  font-weight: bold;\n  height: 40px;\n  min-height: 40px;\n  h1 {\n    flex-grow: 1;\n    margin: 0px 10px 0px 25px;\n    text-align: center;\n    font-size: inherit;\n    font-weight: inherit;\n  }\n  .minimize-chat {\n    width: 30px;\n    height: 40px;\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    line-height: normal;\n    background-color: transparent;\n    border: none;\n    padding: 0px;\n    color: inherit;\n    outline: none;\n    > span {\n      border-bottom: 2px solid #fff;\n      width: 13px;\n    }\n  }\n  .back-icon-chat {\n    flex: 0 0 auto;\n    display: flex;\n    width: 16px;\n    > path {\n      fill: #fff;\n    }\n  }\n"])), props => props.theme.colors.secondary, props => props.theme.colors.primary);

  const TakeBackButton = (0, _styledComponents.styled)(_styles.TakeBackBtn)(_templateObject2 || (_templateObject2 = _taggedTemplateLiteral(["\n  top: 16px;\n  left: 20px;\n  border-color: #fff;\n"])));

  const TakeBackButtonWrap = _styledComponents.styled.div(_templateObject3 || (_templateObject3 = _taggedTemplateLiteral(["\n  position: absolute;\n  padding: 20px 20px;\n  cursor: pointer;\n  left: 0;\n"])));

  const backToHelpBox = () => {
    closeHelpHandler("chat", true);
  };

  return /*#__PURE__*/_react.default.createElement(ChatHeader, null, chatState === "chat-history" && /*#__PURE__*/_react.default.createElement(TakeBackButtonWrap, {
    onClick: resetChatState
  }, /*#__PURE__*/_react.default.createElement(TakeBackButton, null)), isClickedFromHelpBox && chatState !== "chat-history" && /*#__PURE__*/_react.default.createElement(TakeBackButtonWrap, {
    onClick: backToHelpBox
  }, /*#__PURE__*/_react.default.createElement(_backIcon.ReactComponent, {
    className: "back-icon-chat",
    fill: "#fff"
  })), /*#__PURE__*/_react.default.createElement("h1", null, "Chat with us"), /*#__PURE__*/_react.default.createElement("button", {
    className: "minimize-chat",
    onClick: () => openChat(false)
  }, /*#__PURE__*/_react.default.createElement("span", null)));
};

ChatTitle.displayName = "ChatTitle";
ChatTitle.propTypes = {
  chatState: _propTypes.default.string,
  resetChatState: _propTypes.default.func,
  openChat: _propTypes.default.func.isRequired
};
var _default = ChatTitle;
exports.default = _default;
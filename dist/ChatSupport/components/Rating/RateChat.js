"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _styles = require("../../styles");

var _templateObject, _templateObject2, _templateObject3, _templateObject4, _templateObject5;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

const AgentName = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n  margin-top: 5px;\n  margin-bottom: 5px;\n  font-size: 0.75em;\n  color: ", ";\n"])), props => props.theme.colors.textColor.secondary);

const Title = _styledComponents.default.h1(_templateObject2 || (_templateObject2 = _taggedTemplateLiteral(["\n  margin-bottom: 12px;\n  font-size: 0.875em;\n"])));

const Rating = _styledComponents.default.div(_templateObject3 || (_templateObject3 = _taggedTemplateLiteral(["\n  display: flex;\n  justify-content: center;\n  font-size: 0.75em;\n  font-weight: 700;\n  color: ", ";\n  > div:first-child {\n    margin-right: 20px;\n  }\n  .rate-img {\n    min-width: 40px;\n    min-height: 40px;\n    padding: 8px;\n    border: 2px solid #dedede;\n    border-radius: 100%;\n    cursor: pointer;\n  }\n  .rate-bad {\n    color: #df0c0c;\n    .rate-img {\n      border: 1px solid rgba(151, 151, 151, 0.16);\n      background-color: #ffc8c8;\n    }\n  }\n"])), props => props.theme.colors.textColor.secondary);

const LinkBtn = _styledComponents.default.div(_templateObject4 || (_templateObject4 = _taggedTemplateLiteral(["\n  margin-top: 15px;\n  font-size: 0.75em;\n  font-weight: 600;\n  color: #1153b5;\n  text-decoration: underline;\n  cursor: pointer;\n"])));

const Comment = _styledComponents.default.textarea.attrs(props => ({
  rows: 3
}))(_templateObject5 || (_templateObject5 = _taggedTemplateLiteral(["\n  display: block;\n  margin-top: 1.5rem;\n  margin-bottom: 10px;\n  padding: 10px 15px;\n  width: 100%;\n  font-size: 0.75em;\n  font-weight: bold;\n  color: ", ";\n  border-radius: 4px;\n  border: solid 1px #dedede;\n  outline: none;\n  resize: none;\n"])), props => props.theme.colors.textColor.secondary);

const RateChat = _ref => {
  let {
    endedBy,
    agentName = "",
    skipChatRating,
    rateChat,
    handleRatingChange,
    askComment = false,
    isRated = false,
    submitComment,
    comment = "",
    handleCommentChange
  } = _ref;
  return /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, endedBy === "agent-end" && /*#__PURE__*/_react.default.createElement(AgentName, null, "".concat(agentName, " has left.")), /*#__PURE__*/_react.default.createElement(Title, null, "How would you rate this chat?"), /*#__PURE__*/_react.default.createElement(Rating, null, /*#__PURE__*/_react.default.createElement("div", null, /*#__PURE__*/_react.default.createElement("div", {
    className: "rate-img",
    onClick: () => handleRatingChange("good")
  }, /*#__PURE__*/_react.default.createElement("svg", {
    xmlns: "http://www.w3.org/2000/svg",
    width: "21",
    height: "19"
  }, /*#__PURE__*/_react.default.createElement("path", {
    d: "M1 7.2v9.6c0 .663.56 1.2 1.25 1.2H6V6H2.25C1.56 6 1 6.537 1 7.2zm16.818-.129h-3.492a.582.582 0 01-.51-.299.617.617 0 01-.007-.602l1.231-2.278a1.85 1.85 0 00.077-1.608A1.774 1.774 0 0013.92 1.24l-.868-.223a.58.58 0 00-.582.183L7.758 6.58A3.075 3.075 0 007 8.61v6.354C7 16.638 8.326 18 9.955 18h5.878c1.327 0 2.5-.919 2.85-2.234l1.261-5.948A2.31 2.31 0 0020 9.314c0-1.236-.98-2.243-2.182-2.243h0z",
    fill: isRated && !askComment ? "#44CB47" : "#FFF",
    stroke: isRated && !askComment ? "" : "#979797"
  }))), /*#__PURE__*/_react.default.createElement("div", null, "Good")), /*#__PURE__*/_react.default.createElement("div", {
    className: "".concat(askComment ? "rate-bad" : "")
  }, /*#__PURE__*/_react.default.createElement("div", {
    className: "rate-img",
    onClick: () => handleRatingChange("bad")
  }, /*#__PURE__*/_react.default.createElement("svg", {
    xmlns: "http://www.w3.org/2000/svg",
    width: "21",
    height: "19"
  }, /*#__PURE__*/_react.default.createElement("path", {
    d: "M20 11.8V2.2c0-.663-.56-1.2-1.25-1.2H15v12h3.75c.69 0 1.25-.537 1.25-1.2zm-16.818.129h3.492c.305 0 .458.209.51.299.05.09.154.329.007.602L5.96 15.108a1.85 1.85 0 00-.077 1.608c.221.525.657.905 1.197 1.044l.868.223a.58.58 0 00.582-.183l4.712-5.38c.49-.559.758-1.28.758-2.03V4.036C14 2.362 12.674 1 11.045 1H5.167c-1.327 0-2.5.919-2.85 2.234L1.056 9.182A2.31 2.31 0 001 9.686c0 1.236.98 2.243 2.182 2.243h0z",
    fill: askComment ? "#DF0C0C" : "#FFF",
    stroke: askComment ? "" : "#979797"
  }))), /*#__PURE__*/_react.default.createElement("div", null, "Bad"))), askComment && /*#__PURE__*/_react.default.createElement("form", {
    onSubmit: e => {
      e.preventDefault();
    }
  }, /*#__PURE__*/_react.default.createElement(Comment, {
    placeholder: "Share your comment here",
    onChange: e => handleCommentChange(e)
  }), /*#__PURE__*/_react.default.createElement(_styles.Button, {
    type: "submit",
    style: {
      width: "100%"
    },
    onClick: () => submitComment(comment)
  }, "Submit")), /*#__PURE__*/_react.default.createElement(LinkBtn, {
    onClick: () => skipChatRating()
  }, endedBy === "agent-end" ? /*#__PURE__*/_react.default.createElement("span", null, "Continue this chat") : !askComment && /*#__PURE__*/_react.default.createElement("span", null, "I don't want to rate")));
};

RateChat.displayName = "RateChat";
RateChat.propTypes = {
  endedBy: _propTypes.default.string.isRequired,
  agentName: _propTypes.default.string,
  skipChatRating: _propTypes.default.func.isRequired,
  rateChat: _propTypes.default.func.isRequired,
  handleRatingChange: _propTypes.default.func.isRequired,
  askComment: _propTypes.default.bool.isRequired,
  isRated: _propTypes.default.bool.isRequired,
  submitComment: _propTypes.default.func.isRequired,
  comment: _propTypes.default.string.isRequired,
  handleCommentChange: _propTypes.default.func.isRequired
};
var _default = RateChat;
exports.default = _default;
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/web.dom-collections.iterator.js");

require("core-js/modules/es.string.trim.js");

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _RateChat = _interopRequireDefault(require("./RateChat"));

var _ThankYou = _interopRequireDefault(require("./ThankYou"));

var _templateObject, _templateObject2, _templateObject3;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

const RatingContainer = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n  position: absolute;\n  top: 0px;\n  left: 0px;\n  width: 100%;\n  height: 100%;\n  background-color: rgba(0, 0, 0, 0.42);\n"])));

const RatingCard = _styledComponents.default.div(_templateObject2 || (_templateObject2 = _taggedTemplateLiteral(["\n  position: absolute;\n  bottom: 0px;\n  left: 0px;\n  padding: 25px 20px;\n  width: 100%;\n  min-height: 45%;\n  height: auto;\n  text-align: center;\n  background-color: #fff;\n  @media (max-width: 768px) {\n    min-height: 22%;\n  }\n"])));

const CloseBtn = _styledComponents.default.span(_templateObject3 || (_templateObject3 = _taggedTemplateLiteral(["\n  position: absolute;\n  top: 0px;\n  right: 5px;\n  padding: 2px;\n  font-size: 1.5em;\n  line-height: 0.7;\n  color: ", ";\n  cursor: pointer;\n"])), props => props.theme.colors.textColor.secondary);

let zChat = null;

class Rating extends _react.default.Component {
  constructor() {
    super(...arguments);

    _defineProperty(this, "state", {
      ratingGiven: false,
      //rating + comment (if applicable)
      askComment: false,
      comment: "",
      showThanks: false
    });

    _defineProperty(this, "handleRatingChange", rating => {
      switch (rating) {
        case "good":
          this.setState({
            askComment: false
          }, () => this.rateChat("good"));
          this.props.handleEventRating("good");
          break;

        case "bad":
          this.setState({
            askComment: true
          }, () => this.rateChat("bad"));
          this.props.handleEventRating("bad");
          break;
      }
    });

    _defineProperty(this, "handleCommentChange", e => {
      this.setState({
        comment: e.target.value
      });
    });

    _defineProperty(this, "rateChat", rating => {
      zChat.sendChatRating(rating);

      if (rating === "good") {
        this.setState({
          ratingGiven: true
        }, () => setTimeout(() => {
          this.setState({
            showThanks: true
          }, () => setTimeout(() => this.close(), 1000));
        }, 1000));
        this.props.endChat(false);
      }
    });

    _defineProperty(this, "submitComment", comment => {
      if (this.state.askComment && comment && comment.trim()) {
        zChat.sendChatComment(comment);
      }

      this.setState({
        ratingGiven: true,
        showThanks: true
      }, () => setTimeout(() => this.close(), 1000));
      this.props.endChat(false);
    });

    _defineProperty(this, "skipChatRating", () => {
      const isRated = this.state.askComment ? true : false;

      if (this.state.askComment && this.state.comment && this.state.comment.trim()) {
        zChat.sendChatComment(this.state.comment);
      }

      this.props.skipChatRating(isRated);
    });

    _defineProperty(this, "close", () => {
      if (this.state.ratingGiven) {
        this.props.endChat();
      } else {
        this.props.close();
      }
    });
  }

  componentDidMount() {
    zChat = window.zChat;
  }

  render() {
    return /*#__PURE__*/_react.default.createElement(RatingContainer, null, /*#__PURE__*/_react.default.createElement(RatingCard, null, !this.state.showThanks ? /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, /*#__PURE__*/_react.default.createElement(CloseBtn, {
      onClick: () => this.close()
    }, "\xD7"), /*#__PURE__*/_react.default.createElement(_RateChat.default, {
      endedBy: this.props.endedBy,
      agentName: this.props.agentName,
      skipChatRating: this.skipChatRating,
      rateChat: this.rateChat,
      handleRatingChange: this.handleRatingChange,
      askComment: this.state.askComment,
      isRated: this.state.ratingGiven,
      submitComment: this.submitComment,
      comment: this.state.comment,
      handleCommentChange: this.handleCommentChange
    })) : /*#__PURE__*/_react.default.createElement(_ThankYou.default, null)));
  }

}

Rating.displayName = "Rating";
Rating.propTypes = {
  endedBy: _propTypes.default.string.isRequired,
  agentName: _propTypes.default.string,
  close: _propTypes.default.func.isRequired,
  skipChatRating: _propTypes.default.func.isRequired,
  endChat: _propTypes.default.func.isRequired
};
var _default = Rating;
exports.default = _default;
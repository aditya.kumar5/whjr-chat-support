"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _templateObject, _templateObject2;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

const Tick = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n  width: 60px;\n  height: 60px;\n  position: relative;\n  margin: 25px auto 15px auto;\n  padding: 20px 15px;\n  border: 2px solid #7fc77e;\n  border-radius: 100%;\n  .check {\n    display: inline-block;\n    height: 25px;\n    width: 13px;\n    position: absolute;\n    top: 50%;\n    right: 50%;\n    transform: rotate(45deg) translate(-50%, -60%);\n    border-bottom: 3px solid #7fc77e;\n    border-right: 3px solid #7fc77e;\n  }\n"])));

const Message = _styledComponents.default.div(_templateObject2 || (_templateObject2 = _taggedTemplateLiteral(["\n  margin-bottom: 25px;\n  font-size: 0.875em;\n  font-weight: 700;\n"])));

const ThankYou = _ref => {
  let {} = _ref;
  return /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, /*#__PURE__*/_react.default.createElement(Tick, null, /*#__PURE__*/_react.default.createElement("span", {
    className: "check"
  })), /*#__PURE__*/_react.default.createElement(Message, null, "Thanks for your feedback."));
};

ThankYou.displayName = "ThankYou";
var _default = ThankYou;
exports.default = _default;
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/web.dom-collections.iterator.js");

require("core-js/modules/es.string.trim.js");

var _react = _interopRequireWildcard(require("react"));

var _styles = require("../../styles");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _templateObject, _templateObject2, _templateObject3, _templateObject4, _templateObject5, _templateObject6, _templateObject7;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

const OfflineChatContainer = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n  padding: 0px 23px;\n  overflow: auto;\n"])));

const HeadingWrap = _styledComponents.default.div(_templateObject2 || (_templateObject2 = _taggedTemplateLiteral(["\n  font-weight: 600;\n"])));

const LabelWrap = _styledComponents.default.div(_templateObject3 || (_templateObject3 = _taggedTemplateLiteral(["\n  margin-top: 1.875rem;\n  font-weight: 600;\n"])));

const ButtonWrap = (0, _styledComponents.default)(_styles.Button)(_templateObject4 || (_templateObject4 = _taggedTemplateLiteral(["\n  background-color: #266ad1;\n  color: #fff;\n  width: 100%;\n  height: 50px;\n  border-radius: 4px;\n  margin-top: 1.4375rem;\n  &:disabled {\n    background-color: #266ad191;\n  }\n"])));
const TextArea = (0, _styledComponents.default)(_styles.TextBox)(_templateObject5 || (_templateObject5 = _taggedTemplateLiteral(["\n  height: 132px;\n  margin: 11px 0;\n  padding: 5px;\n"])));

const TakeBackWrap = _styledComponents.default.div(_templateObject6 || (_templateObject6 = _taggedTemplateLiteral(["\n  position: relative;\n  padding: 17px 0 17px;\n  padding-left: 20px;\n"])));

const TakeBackButton = (0, _styledComponents.default)(_styles.TakeBackBtn)(_templateObject7 || (_templateObject7 = _taggedTemplateLiteral(["\n  top: 23px;\n  left: 0;\n  border-color: #2f70d3;\n"])));

const OfflineChat = _ref => {
  let {
    message,
    onSubmit,
    takeBack
  } = _ref;
  const [offlineMessage, setOfflineMessage] = (0, _react.useState)(message);
  const [btnDisable, setBtnDisable] = (0, _react.useState)(true);
  return /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, /*#__PURE__*/_react.default.createElement(OfflineChatContainer, null, /*#__PURE__*/_react.default.createElement(HeadingWrap, null, /*#__PURE__*/_react.default.createElement(TakeBackWrap, null, /*#__PURE__*/_react.default.createElement(TakeBackButton, {
    onClick: () => takeBack()
  }), /*#__PURE__*/_react.default.createElement(_styles.Font14, null, "Go back to menu")), /*#__PURE__*/_react.default.createElement(_styles.Font12, null, "Sorry, we are not online at the moment. Leave", /*#__PURE__*/_react.default.createElement("br", null), "a message and we'll get back to you.")), /*#__PURE__*/_react.default.createElement(LabelWrap, null, /*#__PURE__*/_react.default.createElement(_styles.Font12, null, "Message")), /*#__PURE__*/_react.default.createElement(TextArea, {
    onChange: e => {
      setOfflineMessage(e.target.value.trim());
      e.target.value.length == 0 ? setBtnDisable(true) : setBtnDisable(false);
    }
  }), /*#__PURE__*/_react.default.createElement(ButtonWrap, {
    type: "button",
    disabled: btnDisable,
    onClick: () => onSubmit(offlineMessage)
  }, "Submit")));
};

var _default = OfflineChat;
exports.default = _default;
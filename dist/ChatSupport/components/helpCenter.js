"use strict";

require("core-js/modules/web.dom-collections.iterator.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.HelpSection = exports.ChatContainer = void 0;

var _react = _interopRequireDefault(require("react"));

var _styledComponents = _interopRequireWildcard(require("styled-components"));

var _cdnImagePath = require("../utils/constants/cdnImagePath");

var _helper = _interopRequireDefault(require("./helper"));

var _core = require("@material-ui/core");

var _templateObject, _templateObject2, _templateObject3, _templateObject4;

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

const hideChatContainer = (0, _styledComponents.keyframes)(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n0% {\n  transform: translateY(0);\n}\n99% {\n  opacity: 0;\n  transform: translateY(50%);\n}\n100% {\n  visibility: hidden;\n  opacity: 0;\n}\n"])));
const openChatContainer = (0, _styledComponents.keyframes)(_templateObject2 || (_templateObject2 = _taggedTemplateLiteral(["\n0% {\n  visibility: hidden;\n  opacity: 0;\n}\n1% {\n  visibility: visible;\n  opacity: 0;\n  transform: translateY(50%);\n}\n100% {\n  visibility: visible;\n  opacity: 1;\n  transform: translateY(0);\n}\n"])));

const ChatContainer = _styledComponents.default.div(_templateObject3 || (_templateObject3 = _taggedTemplateLiteral(["\n  position: fixed;\n  right: ", ";\n  bottom: 25px;\n  width: 320px;\n  display: flex;\n  flex-direction: column;\n  max-width: 100vw;\n  height: 430px;\n  max-height: 85vh;\n  visibility: hidden;\n  opacity: 0;\n  overflow: hidden;\n  border-radius: 8px;\n  box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.38);\n  background-color: #ffffff;\n  z-index: 9999999;\n  line-height: normal;\n  letter-spacing: normal;\n  font-size: ", ";\n  font-family: ", ";\n  color: ", ";\n  animation: ", " 0.5s backwards;\n  &.chat-visible {\n    animation: ", " 0.5s forwards;\n  }\n  @media (max-width: 768px) {\n    max-height: 100%;\n    height: 100vh;\n    width: 100vw;\n    border-radius: 0;\n    left: 0;\n    top: 0;\n  }\n"])), props => props.right, props => props.theme.font.fontSize, props => props.theme.font.fontFamily, props => props.theme.colors.textColor.primary, hideChatContainer, openChatContainer);

exports.ChatContainer = ChatContainer;

const HelpSection = _styledComponents.default.div(_templateObject4 || (_templateObject4 = _taggedTemplateLiteral(["\n  .help-section {\n    max-height: 14rem;\n    padding: 1.5rem;\n    bottom: 7%;\n    max-width: 17rem;\n    right: 2%;\n    left: auto;\n    top: auto;\n  }\n"])));

exports.HelpSection = HelpSection;

const HelpCenter = props => {
  let {
    intl,
    helpSectionStatus,
    helpSection,
    showPlayGround,
    close,
    sendEvents,
    student,
    teacher
  } = props;
  return helpSectionStatus ? /*#__PURE__*/_react.default.createElement(_core.Dialog, {
    onClose: close,
    open: helpSection
  }, /*#__PURE__*/_react.default.createElement(HelpSection, null, /*#__PURE__*/_react.default.createElement(ChatContainer, {
    className: "".concat(helpSection ? "chat-visible help-section" : ""),
    right: showPlayGround ? "34%" : "25px"
  }, /*#__PURE__*/_react.default.createElement("div", null, /*#__PURE__*/_react.default.createElement(_helper.default, {
    text: "Visit Help Center",
    heading: "Find solution to your issues",
    srcURL: _cdnImagePath.HELP_MAIL,
    path: "/t/helpCenter",
    sendEvents: sendEvents,
    close: close,
    teacher: teacher,
    student: student
  }), /*#__PURE__*/_react.default.createElement("hr", null), /*#__PURE__*/_react.default.createElement(_helper.default, {
    text: "Write to Us",
    heading: "Send an Email",
    srcURL: _cdnImagePath.QUESTION_MARK,
    path: "/t/write-to-us",
    sendEvents: sendEvents,
    close: close,
    teacher: teacher,
    student: student
  }))))) : null;
};

var _default = HelpCenter;
exports.default = _default;
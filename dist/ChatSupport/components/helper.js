"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/es.string.includes.js");

var _react = _interopRequireDefault(require("react"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _templateObject;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

const HelpContainer = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n  color: #5a7184;\n  img {\n    width: 1.5rem;\n  }\n  .content {\n    color: #488da4;\n    cursor: pointer;\n  }\n"])));

const HelpButton = props => {
  let {
    heading = "",
    srcURL = "",
    text = "",
    path = "",
    close,
    sendEvents,
    student,
    teacher
  } = props;

  const handler = () => {
    props.history.push(path);
    close();
    localStorage.setItem("WriteToUSsubNode", "desktop");
    sendEvents({
      event: "helpdesk_entity_clicked",
      properties: {
        user_id: teacher.id || student.id,
        user_email: teacher.email || student.email,
        user: teacher ? "Teacher" : "Student",
        action_time: new Date().getTime(),
        entity: path.includes("/write-to-us") ? "write_to_us" : "visit_help_center"
      }
    });
  };

  return /*#__PURE__*/_react.default.createElement(HelpContainer, {
    onClick: handler
  }, /*#__PURE__*/_react.default.createElement("div", null, heading), /*#__PURE__*/_react.default.createElement("div", {
    className: "heading content d-flex align-items-center font18 mt-1"
  }, /*#__PURE__*/_react.default.createElement("img", {
    src: srcURL,
    className: "mr-2"
  }), /*#__PURE__*/_react.default.createElement("div", {
    className: "cursor-pointer"
  }, text)));
};

var _default = HelpButton;
exports.default = _default;
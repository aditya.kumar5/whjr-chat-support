"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/es.string.includes.js");

require("core-js/modules/web.dom-collections.iterator.js");

require("core-js/modules/es.number.to-fixed.js");

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _doc = _interopRequireDefault(require("../assets/images/doc.png"));

var _ppt = _interopRequireDefault(require("../assets/images/ppt.png"));

var _pdf = _interopRequireDefault(require("../assets/images/pdf.png"));

var _video = _interopRequireDefault(require("../assets/images/video.png"));

var _xls = _interopRequireDefault(require("../assets/images/xls.png"));

var _audio = _interopRequireDefault(require("../assets/images/audio.png"));

var _img = _interopRequireDefault(require("../assets/images/img.png"));

var _attachment = _interopRequireDefault(require("../assets/images/attachment.png"));

var _Lightbox = _interopRequireDefault(require("./Lightbox"));

var _templateObject, _templateObject2, _templateObject3, _templateObject4, _templateObject5, _templateObject6, _templateObject7, _templateObject8, _templateObject9, _templateObject10, _templateObject11, _templateObject12;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

const ImageAttachment = _styledComponents.default.img(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n  width: 100%;\n  cursor: pointer;\n  border-radius: 8px;\n"])));

const ImageAttachments = (0, _styledComponents.default)(ImageAttachment)(_templateObject2 || (_templateObject2 = _taggedTemplateLiteral(["\n  &.agent-image {\n    border-top-left-radius: 0;\n  }\n  &.user-image {\n    border-top-right-radius: 0;\n  }\n"])));

const AttachmentWrap = _styledComponents.default.div(_templateObject3 || (_templateObject3 = _taggedTemplateLiteral(["\n  display: flex;\n  align-items: center;\n  padding: 8px 10px;\n  border-radius: 8px;\n  font-size: 0.75em;\n  font-weight: 600;\n  word-break: break-word;\n  white-space: pre-line;\n  justify-content: space-between;\n  cursor: pointer;\n"])));

const AttachmentWraps = (0, _styledComponents.default)(AttachmentWrap)(_templateObject4 || (_templateObject4 = _taggedTemplateLiteral(["\n  &.agent-attachment {\n    margin-right: auto;\n    background-color: #fff;\n    border-top-left-radius: 0px;\n    border: solid 1px #e4e4e4;\n  }\n  &.user-attachment {\n    margin-left: auto;\n    background-color: #e8eaec;\n    border-top-right-radius: 0px;\n  }\n"])));

const AttachmentImage = _styledComponents.default.img(_templateObject5 || (_templateObject5 = _taggedTemplateLiteral(["\n  width: 24px;\n  height: 27px;\n"])));

const AttachmentContentWrap = _styledComponents.default.div(_templateObject6 || (_templateObject6 = _taggedTemplateLiteral(["\n  max-width: 84%;\n"])));

const AttachmentTitle = _styledComponents.default.div(_templateObject7 || (_templateObject7 = _taggedTemplateLiteral(["\n  white-space: nowrap;\n  overflow: hidden;\n  text-overflow: ellipsis;\n  width: 130px;\n"])));

const AttachmentSubtitle = _styledComponents.default.div(_templateObject8 || (_templateObject8 = _taggedTemplateLiteral(["\n  display: flex;\n  align-items: center;\n  &.agent-subtitle {\n    justify-content: flex-start;\n  }\n  &.user-subtitle {\n    justify-content: flex-end;\n  }\n"])));

const AttachmentDownload = _styledComponents.default.a(_templateObject9 || (_templateObject9 = _taggedTemplateLiteral(["\n  font-size: 0.825em;\n  margin-left: 10px;\n  color: #007bff !important;\n"])));

const AttachmentSize = _styledComponents.default.span(_templateObject10 || (_templateObject10 = _taggedTemplateLiteral(["\n  text-transform: uppercase;\n"])));

const ProgressBar = _styledComponents.default.div(_templateObject11 || (_templateObject11 = _taggedTemplateLiteral(["\n  width: 100%;\n  height: 3px;\n  background-color: #fff;\n  border-radius: 1.5px;\n  margin: 10px 0 6px;\n"])));

const ProgressBarInner = _styledComponents.default.div(_templateObject12 || (_templateObject12 = _taggedTemplateLiteral(["\n  height: 100%;\n  background-color: #266ad1;\n  border-radius: 1.5px;\n  width: 0%;\n"])));

const Attachment = _ref => {
  let {
    url,
    name,
    mime_type,
    size,
    type,
    msgType,
    progressBarWidth,
    err
  } = _ref;

  const getFileImageBasisExtension = () => {
    if (mime_type.includes("audio")) return _audio.default;else if (mime_type.includes("video")) return _video.default;else if (mime_type.includes("pdf")) return _pdf.default;else if (mime_type.includes("msword") || mime_type.includes("wordprocessingml")) return _doc.default;else if (mime_type.includes("excel") || mime_type.includes("spreadsheetml") || mime_type.includes("csv")) return _xls.default;else if (mime_type.includes("powerpoint") || mime_type.includes("presentationml")) return _ppt.default;else if (mime_type.includes("image")) return _img.default;else return _attachment.default;
  };

  const handleMediaShow = () => {// if (url) {
    //   if (
    //     !mime_type.includes("audio") &&
    //     !mime_type.includes("video") &&
    //     !mime_type.includes("image")
    //   )
    //     return;
    //   const mediaObj = {
    //     type: mime_type,
    //     src: url,
    //     name: name
    //   };
    //   setMedia(new Array(mediaObj));
    //   setShowLightBox(true);
    // }
  };

  const [media, setMedia] = (0, _react.useState)([]);
  const [showLightBox, setShowLightBox] = (0, _react.useState)(false);

  const handleLightBoxClose = () => {// setShowLightBox(false);
    // setMedia([]);
  };

  const handleFileDownload = e => {
    e.persist();
    e.stopPropagation();
    setTimeout(() => {
      e.target.href = url;
    }, 0);
  };

  const barWidth = {
    width: progressBarWidth,
    transition: "width 3s"
  };
  return /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, mime_type.includes("image") && url && msgType == "chat.file" && /*#__PURE__*/_react.default.createElement(ImageAttachments, {
    src: url,
    alt: name,
    onClick: () => handleMediaShow(),
    className: type === "agent" ? "agent-image" : "user-image"
  }), (msgType == "chat.file" && !mime_type.includes("image") || mime_type.includes("image") && !msgType) && /*#__PURE__*/_react.default.createElement(AttachmentWraps, {
    className: type === "agent" ? "agent-attachment" : "user-attachment",
    onClick: () => handleMediaShow()
  }, /*#__PURE__*/_react.default.createElement(AttachmentImage, {
    src: getFileImageBasisExtension()
  }), /*#__PURE__*/_react.default.createElement(AttachmentContentWrap, null, /*#__PURE__*/_react.default.createElement(AttachmentTitle, null, name), /*#__PURE__*/_react.default.createElement(AttachmentSubtitle, {
    className: type === "agent" ? "agent-subtitle" : "user-subtitle"
  }, url ? /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, /*#__PURE__*/_react.default.createElement(AttachmentSize, null, (size / 1000000).toFixed(2), " MB"), /*#__PURE__*/_react.default.createElement(AttachmentDownload, {
    download: name,
    onClick: e => handleFileDownload(e)
  }, "Download")) : /*#__PURE__*/_react.default.createElement(ProgressBar, {
    hidden: err
  }, /*#__PURE__*/_react.default.createElement(ProgressBarInner, {
    style: barWidth
  }))))), showLightBox && /*#__PURE__*/_react.default.createElement(_Lightbox.default, {
    media: media,
    handleClose: handleLightBoxClose
  }));
};

Attachment.displayName = "Attachment";
Attachment.propTypes = {
  url: _propTypes.default.string,
  name: _propTypes.default.string,
  mime_type: _propTypes.default.string.isRequired,
  size: _propTypes.default.number,
  type: _propTypes.default.string.isRequired,
  progressBarWidth: _propTypes.default.string,
  err: _propTypes.default.string
};
var _default = Attachment;
exports.default = _default;
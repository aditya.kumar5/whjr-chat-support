"use strict";

require("core-js/modules/web.dom-collections.iterator.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.NotificationBubble = void 0;

require("core-js/modules/es.string.includes.js");

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _styles = require("../styles");

var _cdnImagePath = require("../utils/constants/cdnImagePath");

var _chat = _interopRequireDefault(require("../assets/images/chat.png"));

var _deviceType = require("../utils/constants/device-type");

var _icons = require("@material-ui/icons");

var _templateObject, _templateObject2, _templateObject3, _templateObject4, _templateObject5, _templateObject6;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

const helpIcon = _cdnImagePath.HELP_ICON;
const ChatIcon = _cdnImagePath.HELP_MAIL_WHITE;

const ChatBubbleWrap = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n  width: ", ";\n  position: fixed;\n  right: ", ";\n  bottom: ", ";\n  z-index: ", ";\n  @media (max-width: 768px) {\n    right: 12px;\n    bottom: 25px;\n  }\n"])), props => props.isTabView && "45px", props => props.isAskEnabled ? "50px" : "25px", props => props.showPlayGround || props.isAskEnabled ? "60px" : "10px", props => props.isAskEnabled ? "9" : "9999999");

const StyledBubble = (0, _styledComponents.default)(_styles.Button)(_templateObject2 || (_templateObject2 = _taggedTemplateLiteral(["\n  font-size: ", ";\n  font-weight: ", ";\n  background-image: ", ";\n  width: 100%;\n  padding: 12px 20px;\n  border-radius: 2rem;\n  display: ", ";\n  box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.38);\n  font-family: ", ";\n  background: var(--inclass-zendesk-button-bg);\n  color: var(--inclass-zendesk-button-color);\n  img {\n    margin-right: ", ";\n  }\n\n  @media screen and (orientation: landscape) {\n    height: ", ";\n    width: ", ";\n    padding: ", ";\n    img {\n      margin-right: ", ";\n    }\n  }\n  @media (max-width: 768px) {\n    height: 42px;\n    width: 42px;\n    padding: 0;\n    img {\n      margin-right: 0;\n    }\n  }\n"])), props => props.isTabView && "18px", props => props.isTabView && "normal", props => props.isTabView && "linear-gradient(to left, rgb(8, 127, 237), rgb(1, 183, 238));", props => props.hidden ? "none" : "initial", props => props.theme.font.fontFamily, props => props.showPlayGround ? "0ox" : "6px", props => props.isTabView && "42px", props => props.isTabView && "42px", props => props.isTabView && "0", props => props.isTabView && "0");

const NotificationBubble = _styledComponents.default.span(_templateObject3 || (_templateObject3 = _taggedTemplateLiteral(["\n  position: absolute;\n  background-color: #e52828;\n  height: 30px;\n  width: 30px;\n  z-index: 9999999;\n  bottom: 35px;\n  right: 118px;\n  color: #fff;\n  border-radius: 100%;\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  border: 1px solid #fff;\n  @media (max-width: 768px) {\n    right: 15px;\n  }\n"])));

exports.NotificationBubble = NotificationBubble;

const ChatBubbleText = _styledComponents.default.span(_templateObject4 || (_templateObject4 = _taggedTemplateLiteral(["\n  display: ", ";\n  @media (max-width: 768px) {\n    display: none;\n  }\n"])), props => props.isTabView && "none");

const DesktopChatImage = _styledComponents.default.img(_templateObject5 || (_templateObject5 = _taggedTemplateLiteral(["\n  display: inline;\n  width: 1.5rem;\n  @media (max-width: 768px) {\n    display: none;\n  }\n"])));

const MobileChatImage = _styledComponents.default.img(_templateObject6 || (_templateObject6 = _taggedTemplateLiteral(["\n  width: 21px;\n  height: 20px;\n  @media (min-width: 769px) {\n    display: none;\n  }\n"])));

const ChatBubble = _ref => {
  let {
    chatDataAll,
    handleClick,
    handleHelpClick,
    hidden = true,
    notificationCount,
    showPlayGround,
    isTeacher = false,
    pathName,
    isHelpSectionActive = false,
    setHelpSectionFlag,
    d__openChatwindow,
    resetHelpSectionFlag,
    resetHelpCenter,
    className,
    location
  } = _ref;
  let chatBubbleLabel = null;

  if (chatDataAll) {
    chatBubbleLabel = chatDataAll.chatBubbleLabel;
  }

  const isDashBoardPage = pathName.includes("dashboard");

  let deviceType = _react.default.useContext(_deviceType.DeviceTypeContext);

  let isTabView = deviceType === _deviceType.DEVICE_TYPES.TAB.isTab ? true : false;
  let showTabBubble = isTabView && isDashBoardPage;
  console.log("chatData>>>", chatDataAll); // useEffect(() => {
  //   console.log("chatData>>>", chatDataAll)
  //   if (chatDataAll && chatDataAll.openChatWindow) {
  //     setHelpSectionFlag();
  //     d__openChatwindow(!chatDataAll.openChatWindow);
  //   }
  //   if (chatDataAll && chatDataAll.openChatWindow == "RESET") {
  //     resetHelpSectionFlag();
  //   }
  //   if (chatDataAll && chatDataAll.openChatWindow == "RESET_HELP_CENTER") {
  //     resetHelpCenter();
  //   }
  // }, [chatDataAll]);

  return /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, /*#__PURE__*/_react.default.createElement(ChatBubbleWrap, {
    isAskEnabled: location.indexOf("dashboard") !== -1 || location.indexOf("my-projects") !== -1,
    className: className,
    showPlayGround: showPlayGround,
    isTabView: isTabView
  }, notificationCount > 0 && !isHelpSectionActive && /*#__PURE__*/_react.default.createElement(NotificationBubble, {
    hidden: hidden
  }, notificationCount), isTeacher ? /*#__PURE__*/_react.default.createElement(StyledBubble, {
    onClick: () => handleClick(true),
    hidden: hidden,
    showPlayGround: showPlayGround,
    isTabView: isTabView
  }, /*#__PURE__*/_react.default.createElement("span", null, !chatBubbleLabel && /*#__PURE__*/_react.default.createElement(DesktopChatImage, {
    src: ChatIcon
  }), !chatBubbleLabel && /*#__PURE__*/_react.default.createElement(MobileChatImage, {
    src: _chat.default
  }), /*#__PURE__*/_react.default.createElement(ChatBubbleText, null, chatBubbleLabel ? chatBubbleLabel : !showPlayGround && !showTabBubble && (isHelpSectionActive ? "HELP" : "Chat with us")))) : /*#__PURE__*/_react.default.createElement(StyledBubble, {
    onClick: () => handleHelpClick(true),
    hidden: hidden,
    showPlayGround: showPlayGround,
    isTabView: isTabView
  }, /*#__PURE__*/_react.default.createElement("span", null, !chatBubbleLabel && /*#__PURE__*/_react.default.createElement(DesktopChatImage, {
    src: helpIcon
  }), !chatBubbleLabel && /*#__PURE__*/_react.default.createElement(MobileChatImage, {
    src: helpIcon
  }), /*#__PURE__*/_react.default.createElement(ChatBubbleText, {
    isTabView: isTabView
  }, chatBubbleLabel ? chatBubbleLabel : !showPlayGround && !showTabBubble && "Need Help")))));
};

ChatBubble.displayName = "ChatBubble";
ChatBubble.propTypes = {
  hidden: _propTypes.default.bool,
  handleClick: _propTypes.default.func,
  notificationCount: _propTypes.default.number
}; // const mapStateToProps = (state) => ({
//   chatDataAll: getChatConfig(state)
// });
// const mapDispatchToProps = (dispatch) => ({
//   d__openChatwindow: (data) => {
//     dispatch(openChatWindow.chatWindow(data));
//   }
// });
// export default connect(mapStateToProps, mapDispatchToProps)(ChatBubble);

var _default = ChatBubble;
exports.default = _default;
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _styles = require("../../styles");

var _agent = _interopRequireDefault(require("../../assets/images/agent.svg"));

var _templateObject;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

const ActionBox = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n  display: flex;\n  align-items: center;\n  margin: 10px 15px;\n  padding: 6px 6px 0px 6px;\n  border-radius: 7px;\n  background-color: #f0f5ff;\n  font-size: 0.75em;\n  img {\n    margin-right: 8px;\n  }\n  .text {\n    flex: 2;\n    padding-bottom: 2px;\n    font-weight: 700;\n    color: ", ";\n  }\n  .actions {\n    flex: 3;\n    justify-content: flex-end;\n    align-items: center;\n    display: flex;\n    margin-bottom: 3px;\n    button {\n      padding: 5px 4px;\n      &:first-child {\n        margin-right: 6px;\n      }\n      color: ", ";\n      background-color: #fff;\n    }\n  }\n"])), props => props.theme.colors.primary, props => props.theme.colors.textColor.primary);

const StartChat = _ref => {
  let {
    startChat,
    cancel
  } = _ref;
  return /*#__PURE__*/_react.default.createElement(ActionBox, null, /*#__PURE__*/_react.default.createElement("img", {
    src: _agent.default
  }), /*#__PURE__*/_react.default.createElement("div", {
    className: "text"
  }, "Connect with an agent."), /*#__PURE__*/_react.default.createElement("div", {
    className: "actions"
  }, /*#__PURE__*/_react.default.createElement(_styles.Button, {
    onClick: startChat
  }, "Start"), /*#__PURE__*/_react.default.createElement(_styles.Button, {
    onClick: cancel
  }, "Cancel")));
};

StartChat.displayName = "StartChat";
StartChat.propTypes = {
  startChat: _propTypes.default.func.isRequired,
  cancel: _propTypes.default.func.isRequired
};
var _default = StartChat;
exports.default = _default;
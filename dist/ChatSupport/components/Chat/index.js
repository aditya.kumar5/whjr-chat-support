"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/es.json.stringify.js");

require("core-js/modules/es.regexp.exec.js");

require("core-js/modules/es.regexp.test.js");

require("core-js/modules/es.string.split.js");

require("core-js/modules/es.string.replace.js");

require("core-js/modules/es.array.sort.js");

require("core-js/modules/es.string.trim.js");

require("core-js/modules/es.string.includes.js");

require("core-js/modules/es.regexp.constructor.js");

require("core-js/modules/es.regexp.to-string.js");

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _isEqual = _interopRequireDefault(require("lodash/isEqual"));

var _debounce = _interopRequireDefault(require("lodash/debounce"));

var _isEmpty = _interopRequireDefault(require("lodash/isEmpty"));

var _dompurify = _interopRequireDefault(require("dompurify"));

var _MessageList = _interopRequireDefault(require("../MessageList"));

var _InputBox = _interopRequireDefault(require("./InputBox"));

var _StartChat = _interopRequireDefault(require("./StartChat"));

var _Rating = _interopRequireDefault(require("../Rating"));

var _constant = require("../../utils/constants/constant");

var _dom = require("../../utils/helpers/dom");

var _logger = require("../../utils/helpers/logger");

var _utm = require("../../utils/helpers/utm");

var _segmentEvents = require("../../utils/constants/segmentEvents");

var _templateObject;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

const ChatContainer = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n  position: relative;\n  display: flex;\n  flex-direction: column;\n  height: 100%;\n  overflow: hidden;\n"])));

let zChat = null;
const barLength = {
  "0": "50%",
  "10": "80%",
  "30": "95%",
  "60": "95%"
};
const isMobile = (0, _dom.getWindow)().w < 768 ? true : false;
const chatLogger = (0, _logger.getCustomLogger)("chatLogger");
const chatLogSpanId = "ZENDESK CHAT";

class Chat extends _react.default.Component {
  constructor(props) {
    var _this;

    super(props);
    _this = this;

    _defineProperty(this, "progressIndex", 0);

    _defineProperty(this, "initChat", () => {
      let beforeChatInitTime = null;

      try {
        chatLogger && chatLogger.debug("".concat(chatLogSpanId, " : chat init"), {
          chatData: {
            deptId: this.props.chatInfo.deptId
          }
        });
      } catch (e) {
        console.error(e);
        chatLogger && chatLogger.error("".concat(chatLogSpanId, " : error in initChat()"), JSON.stringify(e));
      }

      beforeChatInitTime = performance.now();
      zChat.setVisitorDefaultDepartment(this.props.chatInfo.deptId, err => {
        if (err) {
          console.error(err);
          chatLogger && chatLogger.error("".concat(chatLogSpanId, " : error in setting default dept"), JSON.stringify(err));
          return;
        }

        this.setState({
          chatReady: true
        }, () => {
          if (this.props.autoStart) {
            this.initMessages(this.sendChat);
            this.initChatTags();
          } else {
            this.initMessages(this.addDummyMessages);
          }
        });
        this.handleRUMevents(beforeChatInitTime);
      });
    });

    _defineProperty(this, "handleRUMevents", beforeChatInitTime => {
      const afterChatInitTime = performance.now();

      if (window.DD_RUM && beforeChatInitTime && zChat) {
        window.DD_RUM.addAction("chatMeta", {
          data: {
            time: afterChatInitTime - beforeChatInitTime,
            //actual time taken by chat to start
            errorExist: false,
            connectionStatus: zChat.getConnectionStatus(),
            accountConnectionStatus: zChat.getAccountStatus()
          }
        });
      }
    });

    _defineProperty(this, "startChat", () => {
      this.setState({
        messages: []
      }, () => {
        this.initMessages(this.sendChat);
        this.initChatTags();
      });

      if (this.props.handleEventTrack) {
        const args = {
          button_type: _segmentEvents.BUTTON_TYPE.ARROW,
          concierge_issue_type: this.props.chatInfo.issueType ? this.props.chatInfo.issueType : ""
        };
        this.props.handleEventTrack(_segmentEvents.EVENT_NAMES.CONCIERGE_CHAT_START_CLICKED, args);
      }
    });

    _defineProperty(this, "getEvents", () => {
      return ["account_status", "connection_update", "department_update", "chat"];
    });

    _defineProperty(this, "initEvents", () => {
      this.getEvents().forEach(evt => {
        zChat.on(evt, this.evtHandler = data => {
          this.onEvent(evt, data);
        });
      });
    });

    _defineProperty(this, "initMessages", sendChat => {
      this.props.chatInfo.issueType && sendChat(this.props.chatInfo.issueType);
      /*timeout required because Zendesk is unnecessarily sending the chat.msg event 
              upon sending this message, causing duplication in the messages list. Probably a bug in Zendesk because 
              2 messages (inclusing the above one) are being sent very quickly*/

      setTimeout(() => this.props.chatInfo.issueDetail && sendChat(this.props.chatInfo.issueDetail), 0);

      if (this.props.tags && this.props.tags.list) {
        for (let i = 0; i < this.props.tags.list.length; i++) {
          if (/session_id__/.test(this.props.tags.list[i])) {
            let sessionId = this.props.tags.list[i].split("__").join("_");
            sessionId = sessionId.replace(/_/gi, " ");

            if (sessionId && sessionId.length) {
              sessionId = sessionId.charAt(0).toUpperCase() + sessionId.slice(1);
            }

            setTimeout(() => sendChat(sessionId), 0); //timeout added due to zendesk issue of duplicates

            break;
          }
        }
      }
    });

    _defineProperty(this, "triggerAlertIfInactive", () => {
      if (window.DD_RUM && zChat) {
        window.DD_RUM.addAction("chatMeta", {
          data: {
            time: null,
            errorExist: true,
            connectionStatus: zChat.getConnectionStatus(),
            accountConnectionStatus: zChat.getAccountStatus()
          }
        });
      }
    });

    _defineProperty(this, "addDummyMessages", msg => {
      let messages = this.state.messages;
      messages.push({
        nick: "visitor",
        msg: msg
      });
      this.setState({
        messages
      });
    });

    _defineProperty(this, "onEvent", (evt, data) => {
      switch (evt) {
        case "connection_update":
          if (this.state.connectionStatus !== "connected" && data === "connected" && this.state.status === null && zChat.isChatting()) {
            //ongoing chat on page load
            this.getOngoingChatMessages();
          } else if (this.state.connectionStatus !== "connected" && data === "connected" && this.state.status !== null && this.state.status !== "connecting") {
            //on reconnect
            if (zChat.isChatting()) {
              this.setState({
                messages: [],
                agents: [],
                uploadedFileIndex: -1
              }, () => this.getOngoingChatMessages());
            } else {
              this.endChat();
            }
          } else if (this.state.connectionStatus !== "connected" && data === "connected" && this.state.status === null) {
            //offline upon init, new chat
            this.initChat();
          }

          this.setState({
            connectionStatus: data
          });
          break;

        case "chat":
          if (this.state.connectionStatus !== "connected") {
            return;
          }

          switch (data.type) {
            case "chat.msg":
              if (data.nick === "visitor") {
                this.addNewMessage(data);
              } else {
                if (this.props.timestamp < data.timestamp) this.props.newChatHandle();
                this.addNewMessage(data);
                this.agentUpdate("add", data);
                this.notificationSound();
                zChat.markAsRead();

                if (this.state.status === "connecting") {
                  this.handleStatusUpdate("chatting");
                }
              }

              break;

            case "chat.file":
              if (data.nick === "visitor") {
                this.addNewMessage(data);
              } else {
                if (this.props.timestamp < data.timestamp) this.props.newChatHandle();
                this.addNewMessage(data);
                this.agentUpdate("add", data);
                zChat.markAsRead();

                if (this.state.status === "connecting") {
                  this.handleStatusUpdate("chatting");
                }
              }

              break;

            case "chat.memberjoin":
              if (data.nick !== "visitor") {
                this.handleAgentUpdate(data, "has joined.");
                this.agentUpdate("add", data);

                if (this.state.status === "connecting") {
                  this.handleStatusUpdate("chatting");
                }
              }

              break;

            case "chat.memberleave":
              if (data.nick === "visitor") {
                if (!this.state.endedChat) {
                  setTimeout(() => this.endChat(), 1000);
                }
              } else {
                this.handleAgentUpdate(data, "has left.");

                if (this.state.status === "connecting") {
                  this.handleStatusUpdate("chatting"); //when the agent already has your previous chat open on Zendesk and we start a new chat quickly
                } else if (this.state.agents.length === 1) {
                  this.agentEndChat(data.display_name);
                } else {
                  this.agentUpdate("delete", data);
                }
              }

              break;

            case "typing":
              this.handleAgentTyping(data);
              break;
          }

          break;

        case "account_status":
          this.setState({
            accountConnectionStatus: data
          });
          break;
      }
    });

    _defineProperty(this, "getOngoingChatMessages", () => {
      const agents = this.state.agents.concat(zChat.getServingAgentsInfo().filter(agent => !this.state.agents.some(agnt => agnt.nick === agent.nick)));
      this.setState({
        agents
      });
      let chatStarted = false;
      let chatLog = zChat.getChatLog();
      chatLog.sort((a, b) => {
        if (a.timestamp < b.timestamp) {
          return -1;
        } else if (a.timestamp > b.timestamp) {
          return 1;
        }

        return 0;
      });
      const curStartIndex = chatLog.map(item => item.nick + "-" + item.type).lastIndexOf("visitor-chat.memberjoin");
      chatLog.splice(0, curStartIndex);
      chatLog.forEach((data, index) => {
        switch (data.type) {
          case "chat.msg":
            this.addNewMessage(data);
            break;

          case "chat.memberjoin":
            if (data.nick === "visitor") {
              if (index === 0) {
                this.handleTimestampUpdate(data.timestamp);
              }
            } else {
              this.handleAgentUpdate(data, "has joined.");
              chatStarted = true;
            }

            break;

          case "chat.memberleave":
            this.handleAgentUpdate(data, "has left.");
            break;

          case "chat.file":
            this.addNewMessage(data);
            break;
        }
      });

      if (chatStarted) {
        this.handleStatusUpdate("chatting");
      } else {
        this.handleConnectAgent();
      }

      this.setState({
        chatReady: true
      });
    });

    _defineProperty(this, "sendChat", function (msg) {
      let resendId = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
      if (typeof msg == "object" && msg.target.value === "") return; //msg -> can be msg or event

      if (typeof msg == "object" && msg.target) msg.persist(); //attachment event

      let newMessage = null;

      if (!resendId || resendId && msg.type === "chat.file") {
        newMessage = msg;
      } else {
        newMessage = msg.msg;
      }

      if (_this.isOffline()) return;
      if (!newMessage && !newMessage.trim()) return;

      _this.sendStopTyping.flush();

      if (_this.state.messages.length === 0) {
        _this.handleTimestampUpdate(Date.now());

        _this.handleConnectAgent();
      }

      let msgTimestamp = null;

      if (resendId) {
        msgTimestamp = resendId;
      } else {
        msgTimestamp = Date.now();

        _this.setState({
          newMessageText: ""
        }, () => {
          if (_this.chatRef && _this.chatRef.current) {
            const textarea = _this.chatRef.current.querySelector("textarea");

            if (textarea) {
              textarea.style.height = isMobile ? "25px" : "20px";
            }
          }

          _this.addNewMessage({
            nick: "visitor",
            msg: newMessage,
            timestamp: msgTimestamp
          });
        });
      }

      if (typeof newMessage == "string") {
        zChat.sendChatMsg(newMessage, err => {
          if (err) {
            _this.handleSendErr(msgTimestamp);

            return;
          }
        });
      } else {
        //file object
        let fileAttachment = null;
        newMessage.target ? fileAttachment = newMessage.target.files[0] : fileAttachment = _this.state.previousSelectedFile;

        _this.setState({
          disableFileUpload: true,
          previousSelectedFile: fileAttachment
        });

        _this.progressIndex = 0; // required to initialize progressbar

        zChat.sendFile(fileAttachment, (err, data) => {
          clearInterval(_this.state.intervalId);
          const intervalId = null;
          const barWidth = "0%";

          if (!err) {
            _this.handleUploadedFile(data, null);
          } else {
            _this.handleUploadedFile(fileAttachment, err, msgTimestamp);

            _this.progressIndex++; //preventing it to be 0 in case of err
          }

          _this.setState({
            intervalId,
            barWidth
          });
        });
      }
    });

    _defineProperty(this, "handleUploadedFile", function (data, err) {
      let timestamp = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
      let messages = _this.state.messages;
      const msgData = {};
      msgData.type = "chat.file";
      msgData.nick = "visitor";

      if (err) {
        msgData.sendErr = true;
        msgData.errMsg = _this.handleFileUploadErrors(err);
        msgData.attachment = data;
        msgData.attachment.mime_type = data.type;
        msgData.attachment.url = null;
        msgData.timestamp = timestamp;
        messages[_this.state.uploadedFileIndex] = msgData;

        _this.setState({
          messages,
          uploadedFileIndex: -1,
          disableFileUpload: false
        });
      } else {
        msgData.attachment = data;
        msgData.sendErr = false;
        messages[_this.state.uploadedFileIndex] = msgData;

        _this.setState({
          messages,
          uploadedFileIndex: -1,
          disableFileUpload: false,
          previousSelectedFile: null //setting previously selected file to null on success

        });
      }
    });

    _defineProperty(this, "handleFileUploadErrors", err => {
      switch (err.message) {
        case "INVALID_EXTENSION":
          return "Upload failed. File type not supported.";

        case "NOT_ALLOWED":
          return "Upload failed. File type not supported.";

        case "NOT_SUPPORTED":
          return "Upload failed. File sending is not supported on this browser.";

        case "EXCEED_SIZE_LIMIT":
          return "Upload failed. File sending failed due to file size exceeding limit.";

        case "INTERNAL_ERROR":
          return "Upload failed. Something went wrong.";

        default:
          return "";
      }
    });

    _defineProperty(this, "initChatTags", () => {
      try {
        const tags = JSON.parse((0, _utm.getCookie)("chatSupportTags"));

        if (tags && typeof tags === "object" && tags.length) {
          this.removeTags(tags); //remove all tags from previous chat
        }
      } catch (e) {
        console.error(e);
        chatLogger && chatLogger.error("".concat(chatLogSpanId, " : error in initChatTags()"), JSON.stringify(e));
      }

      let list = [];

      if (this.props.chatInfo.issueType) {
        let speedTag = [];
        let issueTag = this.props.chatInfo.issueType.toLowerCase().split(" ").join("_");

        if (this.props.chatInfo.issueDetail) {
          issueTag += "__" + this.props.chatInfo.issueDetail.toLowerCase().split(" ").join("_");
        }

        if (!(0, _isEmpty.default)(this.props.chatInfo.speedData)) {
          const speedData = this.props.chatInfo.speedData;
          speedTag.push(speedData.type + "_internetSpeed_" + speedData.speed + "_mbps");
        }

        list.push(issueTag);
        list = list.concat(speedTag);
      }

      setTimeout(() => {
        this.handleTagsChange({
          action: "add",
          list
        });

        if (this.props.tags) {
          this.handleTagsChange(this.props.tags);
        }
      }, 1000);
    });

    _defineProperty(this, "addNewFile", data => {
      const msgData = {};
      msgData.type = "chat.file";
      msgData.nick = "visitor";
      msgData.attachment = data ? data : {};
      msgData.attachment.url = null;
      msgData.attachment.mime_type = data ? data.type : null;
      msgData.timestamp = Date.now();
      if (data.type.includes("image")) msgData.type = null; // should be null to show different state of image while uploading

      msgData.sendErr = false;
      return msgData;
    });

    _defineProperty(this, "isOffline", () => this.state.accountConnectionStatus !== "online" || this.state.connectionStatus !== "connected");

    _defineProperty(this, "addNewMessage", msg => {
      if (msg && msg.msg && typeof msg.msg === "string") {
        msg = this.getFormattedMessage(msg);
      }

      let messages = this.state.messages;

      if (messages.length && messages[messages.length - 1].messageType && messages[messages.length - 1].messageType === "agent-typing") {
        messages.splice(messages.length - 1, 0, msg);
      } else {
        if (msg.msg && msg.msg.target && msg.msg.target.files.length) {
          // only for attachment push
          messages.push(this.addNewFile(msg.msg.target.files[0], null));
          this.setState({
            uploadedFileIndex: messages.length - 1
          });
        } else {
          messages.push(msg);
        }
      }

      this.setState({
        messages
      }, () => {
        if (this.state.status === null && msg.nick === "visitor") {
          this.handleConnectAgent();
        }

        this.scrollToBottom();
      });
    });

    _defineProperty(this, "scrollToBottom", () => {
      let scrollRef = this.chatRef.current.firstChild.firstChild;

      if (this.chatRef.current.firstChild.scrollHeight > this.chatRef.current.firstChild.offsetHeight || this.chatRef.current.firstChild.scrollHeight > this.chatRef.current.firstChild.firstChild.scrollHeight) {
        scrollRef = this.chatRef.current.firstChild;
      }

      scrollRef.scroll(0, scrollRef.scrollHeight);
    });

    _defineProperty(this, "handleSendErr", timestamp => {
      let messages = this.state.messages;

      for (let i = messages.length - 1; i >= 0; i--) {
        if (messages[i].timestamp && messages[i].timestamp === timestamp) {
          messages[i].sendErr = true;
          this.setState({
            messages
          });
          break;
        }
      }
    });

    _defineProperty(this, "resendChat", index => {
      let messages = this.state.messages;
      if (messages[index].errMsg) return;
      let newTimestamp = Date.now();
      messages[index].sendErr = false;
      messages[index].timestamp = newTimestamp;

      if (messages[index].type === "chat.file") {
        this.setState({
          messages,
          uploadedFileIndex: index
        }, () => {
          this.sendChat(messages[index], newTimestamp);
        });
      } else {
        this.setState({
          messages
        }, () => {
          this.sendChat(messages[index], newTimestamp);
        });
      }
    });

    _defineProperty(this, "handleConnectAgent", () => {
      if (this.state.status === "connecting") {
        return;
      }

      this.handleStatusUpdate("connecting", "Connecting to an agent...");
      setTimeout(() => this.state.status === "connecting" && this.handleStatusUpdate("connecting", "Estimated wait time: 30 sec"), 2000);
      setTimeout(() => this.state.status === "connecting" && this.handleStatusUpdate("connecting", "Connecting to an agent..."), 30000);
    });

    _defineProperty(this, "endChat", goBack => {
      if (!this.state.endedChat) {
        this.setState({
          endedChat: true
        }, () => {
          if (!zChat.isChatting()) {
            if (goBack !== false) {
              this.handleStatusUpdate("finish");
              this.props.chatEnded();
            }

            return;
          }

          zChat.endChat({
            clear_dept_id_on_chat_ended: true
          }, err => {
            if (err) {
              this.setState({
                endedChat: false
              });
            } else if (goBack !== false) {
              this.handleStatusUpdate("finish");
              this.props.chatEnded();
            }
          });
        });
      } else if (goBack !== false) {
        this.handleStatusUpdate("finish");
        this.props.chatEnded();
      }
    });

    _defineProperty(this, "visitorEndChat", () => {
      if (this.state.agents.length) {
        //only if an agent has joined
        this.handleStatusUpdate("visitor-end");
      } else {
        this.endChat();
      }
    });

    _defineProperty(this, "agentEndChat", () => {
      this.handleStatusUpdate("agent-end");
    });

    _defineProperty(this, "handleMsgTextChange", e => {
      if (!this.state.typing) {
        zChat.sendTyping(true);
        this.setState({
          typing: true
        });
      }

      this.setState({
        newMessageText: e.target.value
      });
      this.sendStopTyping();
    });

    _defineProperty(this, "stopTyping", () => {
      if (!this.state.typing) return;
      zChat.sendTyping(false);
      this.setState({
        typing: false
      });
    });

    _defineProperty(this, "handleAgentUpdate", (data, msg) => {
      this.addNewMessage(_objectSpread({
        messageType: "info",
        msg: "".concat(data.display_name, " ").concat(msg)
      }, data));
    });

    _defineProperty(this, "agentUpdate", (action, data) => {
      let agents = this.state.agents;

      switch (action) {
        case "add":
          if (data.nick !== "agent:trigger" && !agents.some(agent => agent.nick === data.nick)) {
            agents.push({
              display_name: data.display_name,
              nick: data.nick
            });
            this.setState({
              agents
            });
          }

          break;

        case "delete":
          for (let i = 0; i < agents.length; i++) {
            if (agents[i].nick === data.nick) {
              agents.splice(i);
              this.setState({
                agents
              });
              break;
            }
          }

          break;
      }
    });

    _defineProperty(this, "handleTimestampUpdate", date => {
      this.addNewMessage({
        messageType: "date",
        msg: date
      });
    });

    _defineProperty(this, "handleStatusUpdate", function (status) {
      let msg = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "";

      _this.setState({
        status,
        statusMsg: msg
      }, () => {
        _this.scrollToBottom();
      });
    });

    _defineProperty(this, "handleAgentTyping", data => {
      if (data.typing) {
        let agent = this.state.agents.filter(agent => agent.nick === data.nick);
        this.addNewMessage(_objectSpread({
          messageType: "agent-typing",
          display_name: agent.length ? agent[0].display_name : "",
          msg: "..."
        }, data));
      } else {
        let messages = this.state.messages;
        messages.splice(messages.length - 1);
        this.setState({
          messages
        });
      }
    });

    _defineProperty(this, "closeRatingPop", () => {
      if (this.state.status === "visitor-end") {
        this.handleStatusUpdate("chatting");
      } else if (this.state.status === "agent-end") {
        this.handleStatusUpdate("finish");
      }

      if (this.props.handleEventTrack) {
        const args = {
          button_type: _segmentEvents.BUTTON_TYPE.ARROW,
          concierge_issue_type: this.props.chatInfo.issueType ? this.props.chatInfo.issueType : "",
          concierge_rating: "dont want to rate"
        };
        this.props.handleEventTrack(_segmentEvents.EVENT_NAMES.CONCIERGE_CHAT_RATED, args);
      }
    });

    _defineProperty(this, "skipChatRating", function () {
      let isRated = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;

      if (_this.state.status === "visitor-end") {
        _this.endChat();
      } else if (_this.state.status === "agent-end") {
        if (isRated && _this.state.agents.length) {
          _this.agentUpdate("delete", _this.state.agents[0]);
        }

        _this.handleStatusUpdate(null);

        _this.sendChat("Continue this chat");
      }
    });

    _defineProperty(this, "addTags", function () {
      let tags = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
      tags = tags.map(tag => tag.replace(/,/g, ""));
      zChat.addTags(tags);

      _this.updateTagsCache("add", tags);
    });

    _defineProperty(this, "removeTags", function () {
      let tags = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
      zChat.removeTags(tags);

      _this.updateTagsCache("remove", tags);
    });

    _defineProperty(this, "updateTagsCache", (action, tags) => {
      try {
        let setTags = JSON.parse((0, _utm.getCookie)("chatSupportTags"));

        if (action === "add") {
          setTags = setTags && setTags.length ? setTags.concat(tags) : tags;
        } else {
          setTags = setTags && setTags.length ? setTags.filter(tag => !tags.includes(tag)) : [];
        }

        (0, _utm.setCookie)("chatSupportTags", JSON.stringify(setTags));
      } catch (e) {
        console.error(e);
        chatLogger && chatLogger.error("".concat(chatLogSpanId, " : error in updateTagsCache()"), JSON.stringify(e));
      }
    });

    _defineProperty(this, "handleTagsChange", tags => {
      switch (tags.action) {
        case "add":
          this.addTags(tags.list);
          break;

        case "remove":
          this.removeTags(tags.list);
      }
    });

    _defineProperty(this, "reconnect", () => {
      zChat.reconnect();
    });

    _defineProperty(this, "notificationSound", () => {
      this.notifRef && this.notifRef.current && this.notifRef.current.play();
    });

    _defineProperty(this, "getFormattedMessage", msg => {
      const urlExp = new RegExp("(?:(?:https?):\\/\\/)?(?:\\S+(?::\\S*)?@)?(?:(?!(?:10|127)(?:\\.\\d{1,3}){3})(?!(?:169\\.254|192\\.168)(?:\\.\\d{1,3}){2})(?!172\\.(?:1[6-9]|2\\d|3[0-1])(?:\\.\\d{1,3}){2})(?:[1-9]\\d?|1\\d\\d|2[01]\\d|22[0-3])(?:\\.(?:1?\\d{1,2}|2[0-4]\\d|25[0-5])){2}(?:\\.(?:[1-9]\\d?|1\\d\\d|2[0-4]\\d|25[0-4]))|(?:(?:[a-z\xA1-\uFFFF0-9]-*)*[a-z\xA1-\uFFFF0-9]+)(?:\\.(?:[a-z\xA1-\uFFFF0-9]-*)*[a-z\xA1-\uFFFF0-9]+)*(?:\\.(?:[a-z\xA1-\uFFFF]{2,}))\\.?)(?::\\d{2,5})?(?:[/?#]\\S*)?", "gi");

      if (urlExp.test(msg.msg)) {
        const checkProtocol = new RegExp("^(?:[a-z]+:)?//.+", "i");
        const hasProtocol = checkProtocol.test(msg.msg);
        msg.msg = _dompurify.default.sanitize(msg.msg.replace(urlExp, "<a href=\"".concat(hasProtocol ? "" : "//", "$&\" target=\"_blank\">$&</a>")), {
          ALLOWED_TAGS: ["a"],
          ALLOWED_ATTR: ["href", "target"]
        });
        return _objectSpread(_objectSpread({}, msg), {}, {
          hasLink: true
        });
      }

      return msg;
    });

    _defineProperty(this, "handleEventRating", rate => {
      if (this.props.handleEventTrack) {
        const args = {
          button_type: _segmentEvents.BUTTON_TYPE.ARROW,
          concierge_issue_type: this.props.chatInfo.issueType ? this.props.chatInfo.issueType : "",
          concierge_rating: rate
        };
        this.props.handleEventTrack(_segmentEvents.EVENT_NAMES.CONCIERGE_CHAT_RATED, args);
      }
    });

    this.state = {
      connectionStatus: null,
      accountConnectionStatus: null,
      //from the zendesk agent portal end
      messages: [],
      newMessageText: "",
      status: null,
      //null -> connecting -> chatting -> reconnecting -> chatting -> visitor-end/agent-end -> finish
      statusMsg: "",
      agents: [],
      typing: false,
      disableFileUpload: false,
      uploadedFileIndex: -1,
      previousSelectedFile: null,
      barWidth: "0%",
      intervalId: null,
      endedChat: false,
      //to know whether ended in current tab
      chatReady: false
    };
    this.sendStopTyping = (0, _debounce.default)(this.stopTyping.bind(this), 1000);
    this.chatRef = /*#__PURE__*/_react.default.createRef();
    this.notifRef = /*#__PURE__*/_react.default.createRef();
  }

  componentDidMount() {
    zChat = window.zChat;
    this.setState({
      connectionStatus: zChat.getConnectionStatus(),
      accountConnectionStatus: zChat.getAccountStatus()
    }, () => {
      this.initEvents();

      if (this.state.connectionStatus === "connected" && !zChat.isChatting()) {
        this.initChat();
      } else if (this.state.connectionStatus === "connected") {
        this.getOngoingChatMessages();
      } else if (this.isOffline()) {
        this.triggerAlertIfInactive();
      }
    });
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.tags && this.props.tags.action && !(0, _isEqual.default)(prevProps.tags, this.props.tags) && !this.props.tags.onlyOnChatStart) {
      this.handleTagsChange(this.props.tags);
    }

    if (this.state.uploadedFileIndex >= 0 && this.state.intervalId === null && this.progressIndex === 0) {
      const intervalId = setInterval(() => {
        if (barLength[this.progressIndex]) {
          const barWidth = barLength[this.progressIndex];
          this.setState({
            barWidth
          });
        }

        this.progressIndex++;
      }, 1000);
      this.setState({
        intervalId
      });
    }
  }

  componentWillUnmount() {
    this.getEvents().forEach(evt => {
      zChat.un(evt, this.evtHandler);
    });

    if (this.state.intervalId) {
      // to prevent memory leaks
      clearInterval(this.state.intervalId);
    }
  }

  render() {
    return /*#__PURE__*/_react.default.createElement(ChatContainer, {
      ref: this.chatRef
    }, /*#__PURE__*/_react.default.createElement(_MessageList.default, {
      messages: this.state.messages,
      statusMsg: this.state.statusMsg,
      resendMessage: this.resendChat,
      connectionStatus: this.state.connectionStatus,
      accountConnectionStatus: this.state.accountConnectionStatus,
      reconnect: this.reconnect,
      barWidth: this.state.barWidth
    }), !this.props.autoStart && this.state.status === null ? /*#__PURE__*/_react.default.createElement(_StartChat.default, {
      startChat: this.startChat,
      cancel: this.props.chatEnded.bind(this, true)
    }) : /*#__PURE__*/_react.default.createElement(_InputBox.default, {
      sendMessage: this.sendChat,
      endChat: this.state.status === "finish" ? this.endChat : this.visitorEndChat,
      messageHandler: this.handleMsgTextChange,
      message: this.state.newMessageText,
      chatEnded: this.state.status === "finish" ? true : false,
      allowedExtensions: _constant.allowedExtensions,
      disabledUpload: this.state.disableFileUpload,
      enableActions: this.state.chatReady
    }), (this.state.status === "visitor-end" || this.state.status === "agent-end") && /*#__PURE__*/_react.default.createElement(_Rating.default, {
      endedBy: this.state.status,
      agentName: this.state.agents.length && this.state.agents[0].display_name,
      close: this.closeRatingPop,
      skipChatRating: this.skipChatRating,
      endChat: this.endChat,
      handleEventRating: this.handleEventRating
    }), /*#__PURE__*/_react.default.createElement("audio", {
      ref: this.notifRef,
      src: "https://s3-cdnwhjr.whjr.online/code/audio/notification.mp3",
      style: {
        display: "none"
      }
    }));
  }

}

Chat.displayName = "Chat";
Chat.defaultProps = {
  chatInfo: {},
  autoStart: true
};
Chat.propTypes = {
  chatInfo: _propTypes.default.object.isRequired,
  chatEnded: _propTypes.default.func.isRequired,
  tags: _propTypes.default.object,
  newChatHandle: _propTypes.default.func,
  timestamp: _propTypes.default.number,
  autoStart: _propTypes.default.bool
};
var _default = Chat;
exports.default = _default;
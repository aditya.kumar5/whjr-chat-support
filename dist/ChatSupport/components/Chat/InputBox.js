"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _styles = require("../../styles");

var _dom = require("../../utils/helpers/dom");

var _templateObject, _templateObject2, _templateObject3, _templateObject4, _templateObject5, _templateObject6, _templateObject7, _templateObject8;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

const MessageContainer = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n  width: 100%;\n  padding: 10px 15px;\n  border-top: 1px solid rgba(210, 210, 210, 0.5);\n  font-size: 0.75em;\n  font-weight: 700;\n  form {\n    display: flex;\n    justify-content: ", ";\n    align-items: center;\n  }\n"])), props => props.centered ? "center" : "space-between");

const EndChat = (0, _styledComponents.default)(_styles.Button)(_templateObject2 || (_templateObject2 = _taggedTemplateLiteral(["\n  padding: 8px;\n  background-color: #c31807;\n"])));

const MessageBox = _styledComponents.default.div(_templateObject3 || (_templateObject3 = _taggedTemplateLiteral(["\n  display: flex;\n  align-items: center;\n  flex-grow: 1;\n  margin: 0px 10px;\n  textarea {\n    margin: 0px;\n    padding: 0px;\n    height: ", ";\n    min-height: ", ";\n    max-height: 60px;\n    font-size: 1em;\n    border: none;\n  }\n  @media (max-width: 768px) {\n    textarea {\n      font-size: 16px;\n    }\n  }\n"])), props => props.mobile ? "25px" : "20px", props => props.mobile ? "25px" : "20px");

const SendButton = (0, _styledComponents.default)(_styles.Button)(_templateObject4 || (_templateObject4 = _taggedTemplateLiteral(["\n  padding: 6px 7px;\n  border-radius: 100%;\n  svg {\n    position: relative;\n    right: 1px;\n  }\n"])));

const InputFileWrap = _styledComponents.default.div(_templateObject5 || (_templateObject5 = _taggedTemplateLiteral(["\n  position: relative;\n  background-color: rgba(210, 210, 210, 0.5);\n  width: 30px;\n  height: 30px;\n  border-radius: 100%;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n"])));

const AttachImg = _styledComponents.default.svg(_templateObject6 || (_templateObject6 = _taggedTemplateLiteral(["\n  width: 16px;\n  height: 18px;\n"])));

const InputFileStyled = (0, _styledComponents.default)(_styles.InputFile)(_templateObject7 || (_templateObject7 = _taggedTemplateLiteral(["\n  position: absolute;\n  width: 100%;\n  padding: 5px;\n  right: 0px;\n  opacity: 0;\n  border-radius: 100%;\n"])));

const InitializingChat = _styledComponents.default.div(_templateObject8 || (_templateObject8 = _taggedTemplateLiteral(["\n  display: flex;\n  flex-grow: 1;\n  align-items: center;\n  .chat-end-load,\n  .chat-input-load > div {\n    opacity: 0.99;\n    border-radius: 4px;\n    background-image: linear-gradient(\n        to right,\n        rgba(226, 226, 226, 0.5) 2%,\n        rgba(255, 255, 255, 0.5) 98%\n      ),\n      linear-gradient(to bottom, #d4d4d4, #d4d4d4);\n  }\n  .chat-end-load {\n    width: 34px;\n    height: 30px;\n    margin-right: 8px;\n  }\n  .chat-input-load {\n    flex-grow: 1;\n    margin-right: 8px;\n  }\n  .chat-input-load > div {\n    flex-grow: 1;\n    height: 12px;\n    margin-bottom: 3px;\n    &:last-child {\n      width: 35%;\n    }\n  }\n"])));

const isMobile = (0, _dom.getWindow)().w < 768 ? true : false;

const InputBox = _ref => {
  let {
    message,
    messageHandler,
    sendMessage,
    endChat,
    chatEnded = false,
    allowedExtensions,
    disabledUpload,
    enableActions = true
  } = _ref;
  return /*#__PURE__*/_react.default.createElement(MessageContainer, {
    onSubmit: e => {
      e.preventDefault();
    },
    centered: chatEnded
  }, enableActions ? /*#__PURE__*/_react.default.createElement("form", null, /*#__PURE__*/_react.default.createElement(EndChat, {
    type: "button",
    onClick: () => endChat()
  }, !chatEnded ? /*#__PURE__*/_react.default.createElement("span", null, "End") : /*#__PURE__*/_react.default.createElement("span", null, "End this chat")), !chatEnded && /*#__PURE__*/_react.default.createElement(MessageBox, {
    mobile: isMobile
  }, /*#__PURE__*/_react.default.createElement(_styles.TextBox, {
    placeholder: "Write your query here...",
    value: message,
    onChange: e => {
      messageHandler(e);

      if (!e.target.value) {
        e.target.style.height = isMobile ? "25px" : "20px";
      }
    },
    onKeyPress: e => {
      if (e.key === "Enter") {
        if (!e.shiftKey) {
          e.preventDefault();
          sendMessage(message);
        } else {
          e.target.style.height = e.target.scrollHeight + "px";
        }
      } else if (e.target.scrollHeight > (isMobile ? 25 : 20) && e.target.scrollHeight < 60) {
        e.target.style.height = e.target.scrollHeight + "px";
      }
    }
  })), !chatEnded && message.length ? /*#__PURE__*/_react.default.createElement(SendButton, {
    type: "submit",
    onClick: () => sendMessage(message)
  }, /*#__PURE__*/_react.default.createElement("svg", {
    xmlns: "http://www.w3.org/2000/svg",
    width: "18",
    height: "18"
  }, /*#__PURE__*/_react.default.createElement("g", {
    fill: "none"
  }, /*#__PURE__*/_react.default.createElement("path", {
    fill: "#FFF",
    d: "M17.889 1.348l-7.032 15.937c-.325.737-1.37.747-1.709.015l-2.667-5.78-5.78-2.668c-.731-.338-.724-1.384.014-1.71L16.653.113c.78-.345 1.58.454 1.236 1.236z"
  }), /*#__PURE__*/_react.default.createElement("path", {
    fill: "#266AD1",
    d: "M7.123 9.544l1.42-1.42a.943.943 0 011.333 1.333l-1.42 1.42 1.677 3.633 5.244-11.887L3.49 7.867l3.633 1.677z"
  })))) : !chatEnded && !message.length && /*#__PURE__*/_react.default.createElement(InputFileWrap, null, /*#__PURE__*/_react.default.createElement(InputFileStyled, {
    onChange: e => sendMessage(e),
    accept: allowedExtensions,
    disabled: disabledUpload
  }), /*#__PURE__*/_react.default.createElement(AttachImg, {
    xmlns: "http://www.w3.org/2000/svg",
    width: "16",
    height: "18",
    viewBox: "0 0 16 18"
  }, /*#__PURE__*/_react.default.createElement("path", {
    d: "M14.691 1.311c-1.745-1.748-4.585-1.748-6.33 0L1.696 7.988c-.205.206-.205.54 0 .746.206.206.54.206.745 0l6.665-6.677c1.293-1.295 3.547-1.295 4.84 0 1.335 1.337 1.335 3.512 0 4.85L4.64 16.333c-.796.797-2.183.797-2.979 0-.821-.823-.821-2.161 0-2.984l8.934-9.055c.299-.299.819-.299 1.117 0 .308.308.308.81 0 1.12l-8.561 8.681c-.178.178-.178.568 0 .746.206.206.539.206.744 0l8.562-8.682c.719-.72.719-1.89 0-2.611-.696-.698-1.911-.696-2.606 0L.917 12.604c-1.223 1.225-1.223 3.251 0 4.476.597.598 1.39.92 2.234.92.844 0 1.637-.322 2.234-.92l9.306-9.428c1.745-1.748 1.745-4.592 0-6.34z"
  })))) : /*#__PURE__*/_react.default.createElement(InitializingChat, null, /*#__PURE__*/_react.default.createElement("div", {
    className: "chat-end-load"
  }), /*#__PURE__*/_react.default.createElement("div", {
    className: "chat-input-load"
  }, /*#__PURE__*/_react.default.createElement("div", null), /*#__PURE__*/_react.default.createElement("div", null))));
};

InputBox.displayName = "InputBox";
InputBox.propTypes = {
  sendMessage: _propTypes.default.func.isRequired,
  endChat: _propTypes.default.func.isRequired,
  messageHandler: _propTypes.default.func.isRequired,
  message: _propTypes.default.string.isRequired,
  chatEnded: _propTypes.default.bool,
  allowedExtensions: _propTypes.default.string,
  enableActions: _propTypes.default.bool
};
var _default = InputBox;
exports.default = _default;
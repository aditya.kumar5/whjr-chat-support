"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/web.dom-collections.iterator.js");

require("core-js/modules/es.string.includes.js");

var _react = _interopRequireWildcard(require("react"));

var _core = require("@material-ui/core");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _ChevronRight = _interopRequireDefault(require("@material-ui/icons/ChevronRight"));

var _ChevronLeft = _interopRequireDefault(require("@material-ui/icons/ChevronLeft"));

var _templateObject;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

const StyledContainer = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n  position: fixed;\n  top: 50%;\n  left: 50%;\n  transform: translate(-50%, -50%);\n  width: 55vw;\n  height: 50vh;\n  border-radius: 8px;\n  background-color: rgba(255, 255, 255, 0.8);\n  .media {\n    position: relative;\n    width: 100%;\n    height: 100%;\n    overflow: hidden;\n  }\n  img,\n  video,\n  iframe {\n    width: 100%;\n    height: 100%;\n    max-width: 100%;\n    max-height: 100%;\n    border-radius: 8px;\n  }\n  audio {\n    position: absolute;\n    top: 50%;\n    left: 50%;\n    transform: translate(-50%, -50%);\n  }\n  .icon-close {\n    position: absolute;\n    right: -20px;\n    top: -20px;\n  }\n  .prev,\n  .next {\n    position: absolute;\n    top: 50%;\n    transform: translate(-50%, -50%);\n    color: #fff;\n    &:hover {\n      cursor: pointer;\n    }\n    svg {\n      font-size: 5rem;\n    }\n  }\n  .prev {\n    left: -40px;\n  }\n  .next {\n    right: -120px;\n  }\n  .object-contain {\n    object-fit: contain;\n  }\n\n  @media only screen and (max-width: 992px) {\n    width: 75vw;\n  }\n  @media only screen and (max-width: 767px) {\n    .prev {\n      left: -25px;\n    }\n    .next {\n      right: -95px;\n    }\n  }\n"])));

class Lightbox extends _react.PureComponent {
  constructor() {
    super(...arguments);

    _defineProperty(this, "state", {
      curItem: null,
      curIndex: null
    });

    _defineProperty(this, "changeItem", step => {
      const next = this.state.curIndex + step;

      if (step < 0 && this.state.curIndex > 0) {
        this.setState({
          curItem: this.props.media[next],
          curIndex: next
        });
      } else if (step > 0 && this.state.curIndex < this.props.media.length - 1) {
        this.setState({
          curItem: this.props.media[next],
          curIndex: next
        });
      }
    });
  }

  componentDidMount() {
    if (this.props.media.length) {
      this.setState({
        curItem: this.props.media[0],
        curIndex: 0
      });
    }
  }

  render() {
    return /*#__PURE__*/_react.default.createElement(_core.Modal, {
      open: true // onClose={this.props.handleClose}
      ,
      "aria-labelledby": "media carousel",
      "aria-describedby": "media carousel",
      style: {
        zIndex: "1000000"
      }
    }, /*#__PURE__*/_react.default.createElement(StyledContainer, null, /*#__PURE__*/_react.default.createElement("span", {
      // onClick={() => this.props.handleClose()}
      className: "icon-close text_disabled cursor_pointer"
    }), this.props.media.length && this.props.media.length > 1 && /*#__PURE__*/_react.default.createElement("span", {
      className: "prev",
      onClick: () => this.changeItem(-1)
    }, /*#__PURE__*/_react.default.createElement(_ChevronLeft.default, null)), this.props.media.length && this.props.media.length > 1 && /*#__PURE__*/_react.default.createElement("span", {
      className: "next",
      onClick: () => this.changeItem(1)
    }, /*#__PURE__*/_react.default.createElement(_ChevronRight.default, null)), this.state.curItem && /*#__PURE__*/_react.default.createElement("div", {
      className: "media"
    }, (() => {
      if (this.state.curItem.type.includes("video")) {
        if (!this.state.curItem.src.includes("youtube")) {
          return /*#__PURE__*/_react.default.createElement("video", {
            controls: true
          }, /*#__PURE__*/_react.default.createElement("source", {
            src: this.state.curItem.src,
            type: "video/mp3"
          }), /*#__PURE__*/_react.default.createElement("source", {
            src: this.state.curItem.src,
            type: "video/mp4"
          }), /*#__PURE__*/_react.default.createElement("source", {
            src: this.state.curItem.src,
            type: "video/mov"
          }), /*#__PURE__*/_react.default.createElement("source", {
            src: this.state.curItem.src,
            type: "video/avi"
          }), /*#__PURE__*/_react.default.createElement("source", {
            src: this.state.curItem.src,
            type: "video/ogg"
          }), /*#__PURE__*/_react.default.createElement("source", {
            src: this.state.curItem.src,
            type: "video/webm"
          }), "Sorry, your browser doesn't support embedded videos.");
        } else {
          return /*#__PURE__*/_react.default.createElement("iframe", {
            src: this.state.curItem.src,
            frameBorder: "0",
            allow: "accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
          });
        }
      } else if (this.state.curItem.type.includes("audio")) {
        return /*#__PURE__*/_react.default.createElement("audio", {
          controls: true
        }, /*#__PURE__*/_react.default.createElement("source", {
          src: this.state.curItem.src,
          type: "audio/mp3"
        }), /*#__PURE__*/_react.default.createElement("source", {
          src: this.state.curItem.src,
          type: "audio/mp4"
        }), "Your browser does not support this audio.");
      } else {
        return /*#__PURE__*/_react.default.createElement("img", {
          className: "object-contain",
          alt: "Project Image",
          src: this.state.curItem.src
        });
      }
    })())));
  }

}

var _default = Lightbox;
exports.default = _default;
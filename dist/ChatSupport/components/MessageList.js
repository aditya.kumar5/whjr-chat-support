"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _styles = require("../styles");

var _Message = _interopRequireDefault(require("./Message"));

var _templateObject, _templateObject2, _templateObject3, _templateObject4, _templateObject5, _templateObject6;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

const MessageListContainer = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n  position: relative;\n  display: flex;\n  flex-grow: 1;\n  overflow-x: hidden;\n  overflow-y: scroll;\n  -webkit-overflow-scrolling: touch;\n  > div {\n    margin-top: auto;\n    padding: 10px 16px;\n    display: flex;\n    flex-direction: column;\n    align-items: stretch;\n    overflow-y: auto;\n    overflow-x: hidden;\n    width: 100%;\n    height: 100%;\n  }\n"])));

const MessageItem = _styledComponents.default.div(_templateObject2 || (_templateObject2 = _taggedTemplateLiteral(["\n  display: flex;\n  &:first-child {\n    margin-top: auto;\n  }\n  &.agent-message {\n    margin-left: 38px;\n  }\n"])));

const AgentIcon = (0, _styledComponents.default)(_styles.UserIcon)(_templateObject3 || (_templateObject3 = _taggedTemplateLiteral(["\n  align-self: flex-end;\n  margin-left: -38px;\n  margin-right: 6px;\n  margin-bottom: 6px;\n"])));

const StatusMessage = _styledComponents.default.div(_templateObject4 || (_templateObject4 = _taggedTemplateLiteral(["\n  margin: 15px 0px 0px 0px;\n  font-size: 0.75em;\n  font-weight: 700;\n  color: ", ";\n  text-align: center;\n"])), props => props.theme.colors.textColor.secondary);

const Reconnecting = (0, _styledComponents.default)(_styles.LoadingBubble)(_templateObject5 || (_templateObject5 = _taggedTemplateLiteral(["\n  bottom: 15px;\n"])));
const Retry = (0, _styledComponents.default)(_styles.Button)(_templateObject6 || (_templateObject6 = _taggedTemplateLiteral(["\n  background-color: transparent;\n  border: none;\n  color: #266ad1;\n  text-decoration: underline;\n"])));

const MessageList = _ref => {
  let {
    messages = [],
    connectionStatus = "",
    accountConnectionStatus = "",
    statusMsg = "",
    resendMessage: _resendMessage,
    reconnect,
    barWidth
  } = _ref;
  return /*#__PURE__*/_react.default.createElement(MessageListContainer, null, /*#__PURE__*/_react.default.createElement("div", null, messages.map((message, index) => /*#__PURE__*/_react.default.createElement(MessageItem, {
    key: index,
    className: "".concat((message.type === "chat.msg" || message.type === "chat.file" || message.type === "typing" && message.display_name) && message.nick && message.nick !== "visitor" ? "agent-message" : "")
  }, (message.type === "chat.msg" || message.type === "chat.file" || message.type === "typing") && message.nick && message.nick !== "visitor" && message.display_name && (index === messages.length - 1 || messages[index + 1].type !== "chat.msg" && messages[index + 1].type !== "typing" || messages[index + 1].nick && messages[index + 1].nick === "visitor" || message.nick !== messages[index + 1].nick) && /*#__PURE__*/_react.default.createElement(AgentIcon, null, message.display_name.charAt(0).toUpperCase()), /*#__PURE__*/_react.default.createElement(_Message.default, {
    message: message,
    resendMessage: () => {
      _resendMessage(index);
    },
    barWidth: barWidth
  }))), statusMsg && /*#__PURE__*/_react.default.createElement(StatusMessage, null, statusMsg), connectionStatus === "closed" && /*#__PURE__*/_react.default.createElement(StatusMessage, null, "Unable to Connect -", " ", /*#__PURE__*/_react.default.createElement(Retry, {
    onClick: () => reconnect()
  }, "Retry now")), (connectionStatus === "connecting" || accountConnectionStatus === "offline" || accountConnectionStatus === "away") && /*#__PURE__*/_react.default.createElement(Reconnecting, null, "Reconnecting", " ")));
};

MessageList.displayName = "MessageList";
MessageList.propTypes = {
  messages: _propTypes.default.array.isRequired,
  statusMsg: _propTypes.default.string,
  resendMessage: _propTypes.default.func,
  connectionStatus: _propTypes.default.string,
  accountConnectionStatus: _propTypes.default.string,
  reconnect: _propTypes.default.func,
  barWidth: _propTypes.default.string
};
var _default = MessageList;
exports.default = _default;
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _Dialog = _interopRequireDefault(require("@material-ui/core/Dialog"));

var _DialogActions = _interopRequireDefault(require("@material-ui/core/DialogActions"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _CustomThrottleButton = _interopRequireDefault(require("./CustomThrottleButton"));

var _escalateAvatar = _interopRequireDefault(require("../../assets/images/escalateAvatar.png"));

var _helpDeskMessage = _interopRequireDefault(require("../../assets/images/helpDeskMessage.png"));

var _helpDeskQuote = _interopRequireDefault(require("../../assets/images/helpDeskQuote.png"));

var _backIcon = _interopRequireDefault(require("../../assets/images/backIcon.svg"));

var _templateObject, _templateObject2;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

const StyleFeedbackContainer = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n  background-color: #def5ff;\n  padding: 15px;\n  .rad4 {\n    border-radius: 4px;\n  }\n  .content-block {\n    display: block;\n    padding: 0 15px;\n  }\n  .text_black_imp:focus {\n    outline: none;\n    border: 1px solid #ff6400;\n  }\n  .wrap-block {\n    border: solid 1px #f5f5f5;\n    background-color: #007ae1;\n    border-radius: 68px;\n    border-bottom-left-radius: 0;\n  }\n  .quote-block {\n    width: 120px;\n    text-align: left;\n    position: absolute;\n    left: -130px;\n    top: -25px;\n  }\n  .align-text {\n    text-align: justify;\n  }\n  .back-icon-email {\n    flex: 0 0 auto;\n    display: flex;\n    width: 16px;\n  }\n\n  @media only screen and (max-width: 767px) {\n    .content-block {\n      padding: 0;\n    }\n    .quote-block {\n      width: 50px;\n      text-align: left;\n      position: absolute;\n      left: -20px;\n      top: 0;\n    }\n    .align-text {\n      text-align: left;\n    }\n  }\n"])));

const TakeBackButtonWrap = _styledComponents.default.div(_templateObject2 || (_templateObject2 = _taggedTemplateLiteral(["\n  position: absolute;\n  padding: 20px 20px;\n  cursor: pointer;\n  left: 0;\n"])));

const FeedbackEscalation = props => {
  const isMobile = window.orientation > -1;
  return /*#__PURE__*/_react.default.createElement(_Dialog.default, {
    open: true,
    fullWidth: true,
    maxWidth: "md",
    fullScreen: false,
    onClose: props.closeFeedbackEscalation
  }, /*#__PURE__*/_react.default.createElement(StyleFeedbackContainer, null, /*#__PURE__*/_react.default.createElement(_DialogActions.default, null, props.isClickedFromHelpBox && /*#__PURE__*/_react.default.createElement(TakeBackButtonWrap, {
    onClick: props.backToHelpBox
  }, /*#__PURE__*/_react.default.createElement("img", {
    src: _backIcon.default,
    className: "back-icon-email",
    alt: "back"
  })), /*#__PURE__*/_react.default.createElement("span", {
    className: "icon-close font14",
    onClick: props.closeFeedbackEscalation
  })), /*#__PURE__*/_react.default.createElement("div", {
    className: "p-sm-0"
  }, /*#__PURE__*/_react.default.createElement("div", {
    className: "row col-md-11 content-block d-flex justify-content-center align-items-center mx-auto"
  }, /*#__PURE__*/_react.default.createElement("div", {
    className: "row col-md-12 heading_bold font18 d-flex align-items-center justify-content-center text-center py-5 px-3 wrap-block"
  }, !props.hideAvatar && /*#__PURE__*/_react.default.createElement("div", {
    className: "col-md-5"
  }, "Avatar", alt => /*#__PURE__*/_react.default.createElement("img", {
    src: _escalateAvatar.default,
    alt: alt,
    className: "w-75"
  })), /*#__PURE__*/_react.default.createElement("div", {
    className: "col-md-7"
  }, /*#__PURE__*/_react.default.createElement("div", {
    className: "position-relative"
  }, /*#__PURE__*/_react.default.createElement("img", {
    src: _helpDeskQuote.default,
    className: "text-left position-absolute quote-block"
  })), /*#__PURE__*/_react.default.createElement("div", {
    className: "heading_bold text_white font20 text-left my-3"
  }, "Dear ".concat(props.feedbackInfo.toName)), props.onSuccessEscalation ? /*#__PURE__*/_react.default.createElement("p", {
    className: "heading_reg text_white font14 align-text"
  }, "I am Trupti Mukker, CEO of WhiteHatJr. Feel free to raise any major issues (including ethical issues) you found with respect to your experience on WhiteHatJr. Complete anonymity is ensured for all ethical issues.") : /*#__PURE__*/_react.default.createElement("p", {
    className: "heading_reg text_white font14 align-text"
  }, props.feedbackInfo.textDesc, /*#__PURE__*/_react.default.createElement("br", null), " ", /*#__PURE__*/_react.default.createElement("br", null), props.feedbackInfo.bottomDesc))), !props.onSuccessEscalation ? /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, /*#__PURE__*/_react.default.createElement("div", {
    className: "row col-md-12 pt-4 px-0"
  }, /*#__PURE__*/_react.default.createElement("div", {
    className: "heading_bold font20 mb-3"
  }, "Reach out to the CEO"), "Please let us know your detailed concerns", placeholder => /*#__PURE__*/_react.default.createElement("textarea", {
    name: "feedback",
    value: props.escalateFeedbackComment,
    placeholder: placeholder,
    className: "heading_reg font14 chat_box_textarea rad4 text_black_imp p-3",
    style: {
      height: 110,
      borderRadius: "8px"
    },
    onChange: props.updateEscalateFeedback
  })), /*#__PURE__*/_react.default.createElement("div", {
    className: "col-12 my-3 text-right px-0"
  }, /*#__PURE__*/_react.default.createElement(_CustomThrottleButton.default, {
    color: "secondary",
    className: "btn btn_orange rad4 px-5 py-2 cursor_pointer",
    onClick: props.submitEscalateFeedback,
    disabled: !props.escalateFeedbackComment
  }, /*#__PURE__*/_react.default.createElement("div", {
    className: "heading_bold font14"
  }, "SEND")))) : /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, /*#__PURE__*/_react.default.createElement("div", {
    className: "row col-md-12 pt-4 px-0"
  }, /*#__PURE__*/_react.default.createElement("div", {
    className: "col-12 text-center mb-3"
  }, "Message", text => /*#__PURE__*/_react.default.createElement("img", {
    src: _helpDeskMessage.default,
    alt: text
  })), /*#__PURE__*/_react.default.createElement("div", {
    className: "col-12 heading_reg text-center mb-3"
  }, /*#__PURE__*/_react.default.createElement("div", null, "Your message has been received and my office"), /*#__PURE__*/_react.default.createElement("div", null, "will personally look into your matter immediately.")), /*#__PURE__*/_react.default.createElement("div", {
    className: "col-12 text-center my-3 px-0"
  }, /*#__PURE__*/_react.default.createElement("button", {
    className: "btn btn_orange rad4 px-5 py-2",
    onClick: props.closeFeedbackEscalation
  }, /*#__PURE__*/_react.default.createElement("div", {
    className: "heading_bold font14"
  }, "Close")))))))));
};

var _default = FeedbackEscalation;
exports.default = _default;
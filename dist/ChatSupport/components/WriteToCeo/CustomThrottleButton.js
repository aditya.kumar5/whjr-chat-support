"use strict";

require("core-js/modules/es.object.assign.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/web.dom-collections.iterator.js");

var _react = _interopRequireWildcard(require("react"));

var _throttle2 = _interopRequireDefault(require("lodash/throttle"));

var _global = require("../../utils/config/global");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _ButtonComponent = _interopRequireDefault(require("./ButtonComponent"));

var _templateObject;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

const StyledLoaderContainer = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n  position: absolute;\n  top: 0;\n  left: 0;\n  width: 100%;\n  height: 100%;\n  background: rgba(360, 360, 360, 0.6);\n"])));

const CustomThrottleButton = props => {
  const {
    throttleTime = _global.CTA_THROTTLE_TIMEOUT,
    disabled = false,
    showOverlay = false,
    loadingFinished = false,
    newButton = false,
    type
  } = props;
  const [isClicked, setClickedButtonState] = (0, _react.useState)(false);

  const clickEvent = e => {// setClickedButtonState(true);
    // props.onClick && props.onClick(e);
    // setTimeout(() => setClickedButtonState(false), throttleTime);
  };

  let ComponentName = newButton ? _ButtonComponent.default : "button";
  return /*#__PURE__*/_react.default.createElement(ComponentName, _extends({}, props, {
    onClick: (0, _throttle2.default)(clickEvent, throttleTime),
    disabled: isClicked || disabled,
    type: type
  }), isClicked && !showOverlay && !loadingFinished ? /*#__PURE__*/_react.default.createElement("span", {
    className: "spinner-border spinner-border-sm",
    role: "status",
    "aria-hidden": "true"
  }) : props.children, isClicked && showOverlay && !loadingFinished ? /*#__PURE__*/_react.default.createElement(StyledLoaderContainer, {
    className: "d-flex align-items-center justify-content-center"
  }, /*#__PURE__*/_react.default.createElement("div", {
    className: "spinner-border text-info spinner-border-sm",
    role: "status",
    "aria-hidden": "true"
  })) : null);
};

var _default = CustomThrottleButton;
exports.default = _default;
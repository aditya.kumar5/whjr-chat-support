"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.successGrievanceSubmitEvent = exports.ombudsmanOpenEventOnSegment = exports.ombudsmanCloseEventOnSegment = exports.feedbackEscalationCloseEvent = void 0;

var _sendEventOnSegment = require("../../utils/helpers/sendEventOnSegment");

var _segmentEvents = require("../../utils/constants/segmentEvents");

const ombudsmanCloseEventOnSegment = _ref => {
  let {
    ab_version,
    sendEvents
  } = _ref;
  (0, _sendEventOnSegment.sendEventsOnSegmentHOF)({
    sendEvents,
    eventName: _segmentEvents.EVENT_NAMES.OMBUDSMAN_CLOSED,
    properties: {
      ab_version,
      position: _segmentEvents.COMPONENT_POSITION.LEFT_PANEL,
      event_category: _segmentEvents.EVENT_CATEGORY.OMBUDSMAN_CLOSED,
      page_source: _segmentEvents.PAGE_SOURCE.DASHBOARD,
      button_type: _segmentEvents.BUTTON_TYPE.CROSS,
      button_state: _segmentEvents.BUTTON_STATE.CLICKABLE
    }
  });
};

exports.ombudsmanCloseEventOnSegment = ombudsmanCloseEventOnSegment;

const feedbackEscalationCloseEvent = _ref2 => {
  let {
    ab_version,
    sendEvents
  } = _ref2;
  (0, _sendEventOnSegment.sendEventsOnSegmentHOF)({
    sendEvents,
    eventName: _segmentEvents.EVENT_NAMES.GRIEVANCE_POPUP_CLOSED,
    properties: {
      ab_version,
      position: _segmentEvents.COMPONENT_POSITION.GRIEVANCE_POP_UP,
      event_category: _segmentEvents.EVENT_CATEGORY.OMBUDSMAN_OPEN,
      page_source: _segmentEvents.PAGE_SOURCE.DASHBOARD,
      button_type: _segmentEvents.BUTTON_TYPE.CROSS,
      button_state: _segmentEvents.BUTTON_STATE.CLICKABLE
    }
  });
};

exports.feedbackEscalationCloseEvent = feedbackEscalationCloseEvent;

const successGrievanceSubmitEvent = _ref3 => {
  let {
    ab_version,
    sendEvents
  } = _ref3;
  (0, _sendEventOnSegment.sendEventsOnSegmentHOF)({
    sendEvents,
    eventName: _segmentEvents.EVENT_NAMES.GRIEVANCE_MESSAGE_SENT,
    properties: {
      ab_version,
      position: _segmentEvents.COMPONENT_POSITION.GRIEVANCE_POP_UP,
      event_category: _segmentEvents.EVENT_CATEGORY.OMBUDSMAN_OPEN,
      page_source: _segmentEvents.PAGE_SOURCE.DASHBOARD,
      button_state: _segmentEvents.BUTTON_STATE.CLICKABLE
    }
  });
};

exports.successGrievanceSubmitEvent = successGrievanceSubmitEvent;

const ombudsmanOpenEventOnSegment = _ref4 => {
  let {
    ab_version,
    sendEvents,
    buttonType
  } = _ref4;
  (0, _sendEventOnSegment.sendEventsOnSegmentHOF)({
    sendEvents,
    eventName: _segmentEvents.EVENT_NAMES.OMBUDSMAN_OPENED,
    properties: {
      ab_version,
      position: _segmentEvents.COMPONENT_POSITION.LEFT_PANEL,
      event_category: _segmentEvents.EVENT_CATEGORY.OMBUDSMAN_OPEN,
      page_source: _segmentEvents.PAGE_SOURCE.DASHBOARD,
      button_state: _segmentEvents.BUTTON_STATE.CLICKABLE
    }
  });
};

exports.ombudsmanOpenEventOnSegment = ombudsmanOpenEventOnSegment;
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _templateObject;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

const StyledButton = _styledComponents.default.button(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n  background: ", ";\n  background-image : ", ";\n  border: ", ";\n  color: ", ";\n  transition: 0.3s ease-in;\n  cursor: ", ";\n  :hover {\n    color: ", "!important;\n  }\n  border-radius: ", "!important;\n  width: ", "; \n  margin: 0.3rem;\n  outline: none !important;\n"])), props => props.isTabView === "true" ? "" : props.disabled ? "#e7e7e7" : props.background || "#fff", props => props.isTabView === "true" ? "linear-gradient(to left, #087fed, #01b7ee)" : "", props => props.border || "", props => props.disabled ? "#bcb9b9" : props.color, props => props.disabled ? "not-allowed" : "pointer", props => props.disabled ? "none" : props.color, props => props.borderRadius ? props.borderRadius : "", props => props.width ? "160px" : "");

const ButtonComponent = props => {
  const {
    text = "",
    icon = null,
    onClick,
    background = null,
    color = "#fb7a27",
    buttonClass = "font16 heading_bold p-3 px-4 border_radius5",
    border = "1px solid #fb7a27",
    disable = false,
    disabled = false,
    children,
    color1,
    color2,
    direction,
    borderRadius,
    isTabView
  } = props;
  return /*#__PURE__*/_react.default.createElement(StyledButton, {
    className: buttonClass,
    onClick: onClick ? onClick : undefined,
    background: background,
    color: color,
    border: border,
    color1: color1,
    color2: color2,
    disabled: disable || disabled,
    direction: direction,
    borderRadius: borderRadius,
    isTabView: isTabView
  }, icon, text, children);
};

var _default = ButtonComponent;
exports.default = _default;
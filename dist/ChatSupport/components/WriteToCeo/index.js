"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/web.dom-collections.iterator.js");

var _react = _interopRequireWildcard(require("react"));

var _FeedbackEscalation = _interopRequireDefault(require("./FeedbackEscalation"));

var _propTypes = _interopRequireDefault(require("prop-types"));

require("./style.scss");

var _get2 = _interopRequireDefault(require("lodash/get"));

var _helper = require("../../utils/helpers/helper");

var _segmentEvents = require("../../utils/constants/segmentEvents");

var _utils = require("./utils");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

const escalateAvatar = require("../../assets/images/CEO_icon.svg"); // const RenderFeedbackEscalation = ({student, onSubmit }) => {
//   const {name , parentName, id: studentId} = student;
//   const [showEscalation, setEscalation] = useState(false);
//   //TODO: change
//   const {
//     escalateFeedbackComment = "",
//     onSuccessEscalation = false
//   } = {};
//   const textDesc = `I am Karan Bajaj, Founder and CEO of WhitehatJr. ${name} is the heart of our platform and we care deeply for you. Please raise any critical, unresolved issues, including service or ethical issues, directly to me.`;
//   const bottomDesc = `Your feedback will be 100% anonymous and acted upon promptly.`;
//   const feedbackInfo = {
//     toName: parentName,
//     textDesc,
//     bottomDesc
//   };
//   return (
//     <>
//       {showEscalation ? (
//         <FeedbackEscalation
//           feedbackInfo={feedbackInfo}
//           onSuccessEscalation={false}
//           closeFeedbackEscalation={() =>
//             {
//               setEscalation(!showEscalation);
//             }
//             // this.setState({
//             //   isShowFeedbackEscalation: !isShowFeedbackEscalation,
//             //   onSuccessEscalation: false,
//             //   escalateFeedbackComment: ""
//             // })
//           }
//           updateEscalateFeedback={event =>
//             this.setState({ escalateFeedbackComment: event.target.value })
//           }
//           submitEscalateFeedback={onSubmitEscalateFeedback}
//           escalateFeedbackComment={escalateFeedbackComment}
//         />
//       ) : null}
//     </>
//   );
// }
// RenderFeedbackEscalation.propTypes = {
// student: PropTypes.shape({id: PropTypes.string , name: PropTypes.string, parentName: PropTypes.string}),
// onSubmit: PropTypes.func
// }
// // TODO: replace onSubmit with noops
// RenderFeedbackEscalation.defaultProps = {
//   student: {},
//   onSubmit: () => {}
// };


class WriteToCeo extends _react.Component {
  constructor() {
    super(...arguments);

    _defineProperty(this, "state", {
      showPopOver: false,
      showEscalationPopup: false,
      comment: "",
      onSuccessEscalation: false
    });

    _defineProperty(this, "onSubmitEscalateFeedback", () => {
      const {
        nameInfo,
        submitFeedbackEscalation
      } = this.props;
      const {
        parentName,
        activeCourse
      } = nameInfo;
      const {
        comment = ""
      } = this.state;
      const id = (0, _get2.default)(nameInfo, "id", null);
      const courseType = (0, _get2.default)(activeCourse, "courseType", null);
      const submitData = {
        submittedById: id,
        submittedByType: !parentName ? "TEACHER" : "STUDENT",
        message: comment,
        paidStatus: (0, _helper.setPaidStatus)(!parentName ? "TEACHER" : "STUDENT", this.props.nameInfo),
        courseType: parentName ? courseType : null
      };

      if (submitData && id) {
        const {
          ab_version,
          sendEvents
        } = this.props; // onSubmit(submitData);

        submitFeedbackEscalation(submitData);
        (0, _utils.successGrievanceSubmitEvent)({
          ab_version,
          sendEvents
        });
        setTimeout(() => {
          this.setState({
            onSuccessEscalation: true
          });
        }, 1000);
      }
    });

    _defineProperty(this, "closeFeedbackPopup", () => {
      // setEscalation(!showEscalation);
      const {
        ab_version,
        sendEvents
      } = this.props;
      this.setState({
        comment: "",
        showEscalationPopup: false,
        onSuccessEscalation: false
      });
      (0, _utils.feedbackEscalationCloseEvent)({
        ab_version,
        sendEvents
      }); //when ceo popup open from the needhelp box

      if (this.props.isClickedFromHelpBox) {
        this.props.closeHelpHandler("email-ceo", false);
      }
    });

    _defineProperty(this, "backToHelpBox", () => {
      const {
        ab_version,
        sendEvents
      } = this.props;
      this.setState({
        comment: "",
        showEscalationPopup: false,
        onSuccessEscalation: false
      });
      (0, _utils.feedbackEscalationCloseEvent)({
        ab_version,
        sendEvents
      }); //when ceo popup open from the needhelp box

      this.props.closeHelpHandler("email-ceo", true);
    });
  }

  componentDidMount() {
    if (this.props.isClickedFromHelpBox) {
      this.setState({
        showPopOver: false,
        showEscalationPopup: true
      });
    }
  }

  render() {
    const {
      state: {
        showPopOver,
        comment,
        showEscalationPopup,
        onSuccessEscalation = false
      },
      props: {
        nameInfo
      },
      onSubmitEscalateFeedback
    } = this;
    const {
      ab_version,
      sendEvents
    } = this.props; // const {name:teacherName} = teacher;

    const {
      name,
      parentName,
      id
    } = nameInfo;
    const textDesc = parentName ? "".concat(name, " is the heart of our platform and we care deeply for you. Please raise any critical unresolved issues, including service or ethical issues, directly to our CEO.") : "Teachers are the heart of our platform and we care deeply for you. Please raise any critical unresolved issues, including service or ethical issues, directly to our CEO.";
    const bottomDesc = "Your feedback will be 100% anonymous and acted upon promptly.";
    const feedbackInfo = {
      toName: parentName || name,
      textDesc,
      bottomDesc
    };
    return /*#__PURE__*/_react.default.createElement(_react.Fragment, null, showEscalationPopup && /*#__PURE__*/_react.default.createElement(_FeedbackEscalation.default, {
      hideAvatar: true,
      feedbackInfo: feedbackInfo,
      onSuccessEscalation: onSuccessEscalation,
      closeFeedbackEscalation: this.closeFeedbackPopup,
      updateEscalateFeedback: _ref => {
        let {
          target: {
            value
          }
        } = _ref;
        this.setState({
          comment: value
        });
      } // this.setState({ escalateFeedbackComment: event.target.value })
      ,
      submitEscalateFeedback: onSubmitEscalateFeedback,
      escalateFeedbackComment: comment,
      isClickedFromHelpBox: this.props.isClickedFromHelpBox ? true : false,
      backToHelpBox: this.backToHelpBox
    }), /*#__PURE__*/_react.default.createElement("div", {
      className: "parent"
    }, /*#__PURE__*/_react.default.createElement(_react.Fragment, null, !this.props.isClickedFromHelpBox && /*#__PURE__*/_react.default.createElement("div", {
      className: "parent-img-wrap"
    }, /*#__PURE__*/_react.default.createElement("img", {
      className: "cursor_pointer",
      draggable: false,
      src: escalateAvatar,
      onClick: () => {
        this.setState({
          showEscalationPopup: true
        });
        (0, _utils.ombudsmanOpenEventOnSegment)({
          ab_version,
          sendEvents,
          button_type: _segmentEvents.BUTTON_TYPE.KARAN_FACE
        });
      }
    })), showPopOver && /*#__PURE__*/_react.default.createElement(_react.Fragment, null, /*#__PURE__*/_react.default.createElement("span", {
      className: "icon-close font10 cross-butn",
      onClick: () => {
        this.setState({
          showPopOver: false
        });
        (0, _utils.ombudsmanCloseEventOnSegment)({
          ab_version,
          sendEvents
        });
      }
    }), /*#__PURE__*/_react.default.createElement("div", {
      onClick: () => {
        this.setState({
          showEscalationPopup: true
        });
        (0, _utils.ombudsmanOpenEventOnSegment)({
          ab_version,
          sendEvents,
          button_type: _segmentEvents.BUTTON_TYPE.RAISE
        });
      },
      className: "text-left tooltip tooltip-right text_dec_none m-0"
    }, /*#__PURE__*/_react.default.createElement("div", {
      className: "heading_bold font14"
    }, "Raise your Grievance"), /*#__PURE__*/_react.default.createElement("div", {
      className: "heading_reg font12"
    }, "Critical, Unresolved Issues? Write directly to CEO."))))));
  }

}

_defineProperty(WriteToCeo, "propTypes", {
  nameInfo: _propTypes.default.shape({
    id: _propTypes.default.string,
    name: _propTypes.default.string,
    parentName: _propTypes.default.string
  }),
  // teacher: PropTypes.shape({
  //   name: PropTypes.string
  // }),
  submitFeedbackEscalation: _propTypes.default.func,
  onSubmit: _propTypes.default.func,
  isClickedFromHelpBox: _propTypes.default.bool,
  closeHelpHandler: _propTypes.default.func
});

var _default = WriteToCeo;
exports.default = _default;
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _get2 = _interopRequireDefault(require("lodash/get"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _helper = require("../utils/helpers/helper");

var _conciergeIcon = _interopRequireDefault(require("../assets/images/conciergeIcon.png"));

var _chatIcon = _interopRequireDefault(require("../assets/images/chatIcon.png"));

var _phoneIcon = _interopRequireDefault(require("../assets/images/phoneIcon.png"));

var _emailIcon = _interopRequireDefault(require("../assets/images/emailIcon.png"));

var _emailCeoIcon = _interopRequireDefault(require("../assets/images/emailCeoIcon.png"));

var _templateObject, _templateObject2;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

const NeedHelp = _ref => {
  let {
    student,
    handleHelpCurState,
    openHelpModal,
    appConfigData
  } = _ref;
  //set student information here
  const studentStatus = student && student.trialStatus ? student.trialStatus : "";
  const {
    configs = null
  } = appConfigData;
  const {
    supportNumber,
    supportEmail
  } = (0, _helper.getSupportData)(configs ? studentStatus === "paid" ? configs["studentHelpDesk"] : configs["studentHelpDesk_Trial"] : {});

  const HelpHeader = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n    display: -webkit-box;\n    display: -webkit-flex;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-align-items: center;\n    -webkit-box-align: center;\n    -ms-flex-align: center;\n    align-items: center;\n    padding-left: 20px;\n    padding-right: 20px;\n    color: black;\n    background-color: white;\n    font-size: 0.875em;\n    position: relative;\n    font-weight: bold;\n    height: 69px;\n    min-height: 40px;\n    border-bottom: 1px solid rgb(0, 0, 0, 0.1);\n    border-radius: 8px 8px 0px 0px;\n    h1 {\n      flex-grow: 1;\n      // margin: 0px 10px 0px 25px;\n      margin: 0px 10px 0px 14px;\n      text-align: center;\n      font-size: 18px;\n      font-weight: 800;\n      color: #1e1e1e;\n    }\n    .minimize-help {\n      width: 30px;\n      height: 40px;\n      display: flex;\n      justify-content: center;\n      align-items: center;\n      line-height: normal;\n      background-color: transparent;\n      border: none;\n      padding: 0px;\n      color: inherit;\n      outline: none;\n      > span {\n        border-bottom: 2px solid #1e1e1e;\n        width: 13px;\n      }\n    }\n  "])));

  const HelpContent = _styledComponents.default.div(_templateObject2 || (_templateObject2 = _taggedTemplateLiteral(["\n    // height: 74px;\n    -webkit-align-items: center;\n    -webkit-box-align: center;\n    -ms-flex-align: center;\n    align-items: center;\n    padding-left: 24px;\n    padding-right: 24px;\n    cursor: pointer;\n    h2 {\n      font-weight: bold;\n      font-size: 16px;\n      color: #191919;\n      // padding-bottom: 19px;\n    }\n    h4 {\n      font-weight: normal;\n      font-size: 12px;\n      color: #191919;\n      opacity: 0.7;\n      // padding-top: 14px;\n    }\n    .text_black {\n      text-decoration: none;\n    }\n    .text_black:hover {\n      color: #191919;\n    }\n    .help-options {\n      display: flex;\n      flex-direction: row;\n      flex-wrap: nowrap;\n      justify-content: space-between;\n      padding-top: 14px;\n      padding-bottom: 14px;\n      align-items: center;\n    }\n    :not(:last-child) .help-options {\n      border-bottom: 1px solid rgb(0, 0, 0, 0.1);\n    }\n    .help-icons {\n      right: 0;\n    }\n    .main-title {\n      // border-bottom: 1px solid rgb(0,0,0, .1);\n    }\n    :hover {\n      background-color: #f9f9f9;\n    }\n  "])));

  return /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, /*#__PURE__*/_react.default.createElement(HelpHeader, null, /*#__PURE__*/_react.default.createElement("div", {
    className: "concierge-icon"
  }, /*#__PURE__*/_react.default.createElement("img", {
    src: _conciergeIcon.default,
    alt: "Concierge"
  })), /*#__PURE__*/_react.default.createElement("h1", null, "Your Personal Concierge"), /*#__PURE__*/_react.default.createElement("button", {
    className: "minimize-help",
    onClick: openHelpModal.bind(void 0, false, true)
  }, /*#__PURE__*/_react.default.createElement("span", null))), /*#__PURE__*/_react.default.createElement(HelpContent, {
    onClick: handleHelpCurState.bind(void 0, "chat")
  }, /*#__PURE__*/_react.default.createElement("div", {
    className: "help-options"
  }, /*#__PURE__*/_react.default.createElement("div", {
    className: "help-texts"
  }, /*#__PURE__*/_react.default.createElement("h4", null, "Chat - Instant Help"), /*#__PURE__*/_react.default.createElement("h2", {
    className: "main-title"
  }, "Chat with us")), /*#__PURE__*/_react.default.createElement("div", {
    className: "help-icons"
  }, /*#__PURE__*/_react.default.createElement("img", {
    src: _chatIcon.default,
    alt: "Chat"
  })))), /*#__PURE__*/_react.default.createElement(HelpContent, null, /*#__PURE__*/_react.default.createElement("div", {
    className: "help-options"
  }, /*#__PURE__*/_react.default.createElement("div", {
    className: "help-texts"
  }, /*#__PURE__*/_react.default.createElement("h4", null, "Call - Wait time less than 30s"), /*#__PURE__*/_react.default.createElement("h2", {
    className: "main-title"
  }, /*#__PURE__*/_react.default.createElement("a", {
    className: "text_black",
    href: "tel:".concat(supportNumber)
  }, supportNumber))), /*#__PURE__*/_react.default.createElement("div", {
    className: "help-icons"
  }, /*#__PURE__*/_react.default.createElement("img", {
    src: _phoneIcon.default,
    alt: "Phone"
  })))), /*#__PURE__*/_react.default.createElement(HelpContent, {
    onClick: handleHelpCurState.bind(void 0, "email")
  }, /*#__PURE__*/_react.default.createElement("div", {
    className: "help-options"
  }, /*#__PURE__*/_react.default.createElement("div", {
    className: "help-texts"
  }, /*#__PURE__*/_react.default.createElement("h4", null, "Email - Takes upto 24 hours"), /*#__PURE__*/_react.default.createElement("h2", {
    className: "main-title"
  }, supportEmail)), /*#__PURE__*/_react.default.createElement("div", {
    className: "help-icons"
  }, /*#__PURE__*/_react.default.createElement("img", {
    src: _emailIcon.default,
    alt: "Email"
  })))), /*#__PURE__*/_react.default.createElement(HelpContent, {
    onClick: handleHelpCurState.bind(void 0, "email-ceo")
  }, /*#__PURE__*/_react.default.createElement("div", {
    className: "help-options"
  }, /*#__PURE__*/_react.default.createElement("div", {
    className: "help-texts"
  }, /*#__PURE__*/_react.default.createElement("h4", null, "No Resolution?"), /*#__PURE__*/_react.default.createElement("h2", null, "Write to our CEO")), /*#__PURE__*/_react.default.createElement("div", {
    className: "help-icons"
  }, /*#__PURE__*/_react.default.createElement("img", {
    src: _emailCeoIcon.default,
    alt: "Email CEO"
  })))));
}; // const mapStateToProps = state => {
//   return {
//     appConfigData: getAppConfigData(state)
//   };
// };
// export default connect(mapStateToProps, null)(NeedHelp);


var _default = NeedHelp;
exports.default = _default;
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

var _Button = _interopRequireDefault(require("@material-ui/core/Button"));

var _CheckCircle = _interopRequireDefault(require("@material-ui/icons/CheckCircle"));

var _Error = _interopRequireDefault(require("@material-ui/icons/Error"));

var _Info = _interopRequireDefault(require("@material-ui/icons/Info"));

var _Close = _interopRequireDefault(require("@material-ui/icons/Close"));

var _green = _interopRequireDefault(require("@material-ui/core/colors/green"));

var _amber = _interopRequireDefault(require("@material-ui/core/colors/amber"));

var _IconButton = _interopRequireDefault(require("@material-ui/core/IconButton"));

var _Snackbar = _interopRequireDefault(require("@material-ui/core/Snackbar"));

var _SnackbarContent = _interopRequireDefault(require("@material-ui/core/SnackbarContent"));

var _Warning = _interopRequireDefault(require("@material-ui/icons/Warning"));

var _styles = require("@material-ui/core/styles");

var _snackbar = require("./snackbar");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const variantIcon = {
  success: _CheckCircle.default,
  warning: _Warning.default,
  error: _Error.default,
  info: _Info.default
};

const styles1 = theme => ({
  root: {
    width: "100%"
  },
  success: {
    backgroundColor: _green.default[600]
  },
  error: {
    //backgroundColor: theme.palette.error.dark
    backgroundColor: "#FF6400"
  },
  info: {
    backgroundColor: theme.palette.primary.dark
  },
  warning: {
    backgroundColor: "#1a91a9"
  },
  icon: {
    fontSize: 20
  },
  iconVariant: {
    opacity: 0.9,
    marginRight: theme.spacing.unit
  },
  message: {
    display: "flex",
    alignItems: "center"
  }
});

const FlashSnackbar = props => {
  const {
    message,
    onClose,
    variant,
    open,
    classes,
    timeout = 4000,
    anchorOrigin = {
      vertical: "top",
      horizontal: "center"
    },
    disableClickAway = false,
    customeClass = {}
  } = props;
  const Icon = variantIcon[variant];

  const handleClose = (e, reason) => {
    if (!(disableClickAway && reason === "clickaway")) onClose(e, reason);
  };

  return /*#__PURE__*/_react.default.createElement(_Snackbar.default, {
    anchorOrigin: anchorOrigin,
    open: open,
    autoHideDuration: timeout,
    onClose: handleClose,
    className: classes
  }, /*#__PURE__*/_react.default.createElement(_snackbar.MySnackbarContentWrapper, {
    onClose: handleClose,
    variant: variant,
    message: message,
    classes: customeClass
  }));
};

FlashSnackbar.propTypes = {
  classes: _propTypes.default.object.isRequired,
  className: _propTypes.default.object,
  message: _propTypes.default.node,
  onClose: _propTypes.default.func,
  variant: _propTypes.default.oneOf(["success", "warning", "error", "info"]).isRequired
};

var _default = (0, _styles.withStyles)(styles1)(FlashSnackbar);

exports.default = _default;
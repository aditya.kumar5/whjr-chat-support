"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/web.dom-collections.iterator.js");

require("core-js/modules/es.array.reduce.js");

require("core-js/modules/es.string.includes.js");

require("core-js/modules/es.symbol.description.js");

var _react = _interopRequireWildcard(require("react"));

var _Dialog = _interopRequireDefault(require("@material-ui/core/Dialog"));

var _DialogContent = _interopRequireDefault(require("@material-ui/core/DialogContent"));

var _formik = require("formik");

var _TextField = _interopRequireDefault(require("@material-ui/core/TextField"));

var _DialogTitle = _interopRequireDefault(require("@material-ui/core/DialogTitle"));

var _IconButton = _interopRequireDefault(require("@material-ui/core/IconButton"));

var _Close = _interopRequireDefault(require("@material-ui/icons/Close"));

var _FlashSnackbar = _interopRequireDefault(require("./FlashSnackbar"));

var _MenuItem = _interopRequireDefault(require("@material-ui/core/MenuItem"));

var _deviceType = require("../../../utils/constants/device-type");

require("./styles.scss");

var yup = _interopRequireWildcard(require("yup"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _backIcon = _interopRequireDefault(require("../../../assets/images/backIcon.svg"));

var _iconUpload = _interopRequireDefault(require("../../../assets/images/icon-upload.svg"));

var _templateObject;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

const crossBtn = require("../../../assets/images/cancel@2x.png");

const ConciergeEmailModal = _ref => {
  let {
    showDialog,
    values,
    closeModal,
    modalTitle,
    onSubmit: _onSubmit,
    showCourseTypeDropDown,
    teacherSkill,
    isClickedFromHelpBox = false
  } = _ref;
  const [selectedFile, setSelectedFile] = (0, _react.useState)([]);
  const [fileName, setFileName] = (0, _react.useState)("Pick a file");
  const [disableBtn, setDisabledBtn] = (0, _react.useState)(false);
  const [showSnackBar, setSnackBar] = (0, _react.useState)(false);
  const [disableSubmit, setDisableSubmit] = (0, _react.useState)(false);

  const handleFile = (e, setFieldValue) => {
    if (e.target.files && e.target.files.length) {
      const file = [...selectedFile];
      file.push(e.target.files[0]);
      file.length >= 5 ? setDisabledBtn(true) : setDisabledBtn(false);
      setSelectedFile(file);
      setFieldValue("fileData", file);
      checkFileSize(file);
      setFileName(e.target.files[0].name);
    }
  };

  const checkFileSize = files => {
    const initialFileSize = 0;
    const maxFileSize = 100000000; //100MB

    const currentSize = files.reduce((total, curr) => total + curr.size, initialFileSize);

    if (currentSize > maxFileSize) {
      setSnackBar(true);
      setDisableSubmit(true);
    } else {
      setDisableSubmit(false);
    }
  };

  const closeModalHandler = () => {
    setSelectedFile([]);
    setDisableSubmit(false);
    setFileName("Pick a file");
    closeModal();
  }; //used for back to helpbox


  const backToHelpBox = () => {
    setSelectedFile([]);
    setDisableSubmit(false);
    setFileName("Pick a file");
    closeModal(true);
  };

  const removeFileHandler = (index, setFieldValue) => {
    const file = [...selectedFile];
    file.splice(index, 1);
    file.length >= 5 ? setDisabledBtn(true) : setDisabledBtn(false);
    setSelectedFile(file);
    setFieldValue("fileData", file);
    checkFileSize(file);
  };

  const resetStateHandler = () => {
    setDisabledBtn(false);
    setDisableSubmit(false);
    setFileName("Pick a file");
    setSelectedFile([]);
  };

  const hideSnackBar = () => setSnackBar(false);

  const CONCIERGE_VALIDATION = courseTypeNotReqd => {
    const schema = {
      subject: yup.string().required("Subject is required"),
      description: yup.string().required("Description is required")
    };
    const dropDownSchema = {
      courseType: yup.string().required("Course type is required")
    };
    return yup.object().shape(courseTypeNotReqd ? schema : _objectSpread(_objectSpread({}, schema), dropDownSchema));
  };

  let deviceType = _react.default.useContext(_deviceType.DeviceTypeContext);

  let isTabView = deviceType === _deviceType.DEVICE_TYPES.TAB.isTab ? true : false;

  const TakeBackButtonWrap = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n    position: absolute;\n    cursor: pointer;\n    z-index: 10;\n    .back-icon-email {\n      flex: 0 0 auto;\n      display: flex;\n      width: 16px;\n    }\n  "])));

  return /*#__PURE__*/_react.default.createElement(_Dialog.default, {
    open: showDialog,
    maxWidth: "xs",
    "aria-labelledby": "form-dialog-title",
    classes: {
      paper: "dialog_width500 pl-4 pr-4 pt-4 ".concat(isTabView ? "border-wrapper-tab" : "")
    }
  }, isClickedFromHelpBox && /*#__PURE__*/_react.default.createElement(TakeBackButtonWrap, {
    onClick: backToHelpBox
  }, /*#__PURE__*/_react.default.createElement("img", {
    src: _backIcon.default,
    className: "back-icon-email",
    alt: "back"
  })), /*#__PURE__*/_react.default.createElement(_IconButton.default, {
    "aria-label": "close",
    className: "closeButton closeBtnPos",
    onClick: closeModalHandler
  }, /*#__PURE__*/_react.default.createElement(_Close.default, null)), /*#__PURE__*/_react.default.createElement(_DialogTitle.default, {
    id: "customized-dialog-title",
    className: "dialogStyle",
    onClick: closeModalHandler
  }, /*#__PURE__*/_react.default.createElement("strong", null, modalTitle)), /*#__PURE__*/_react.default.createElement(_DialogContent.default, null, /*#__PURE__*/_react.default.createElement(_formik.Formik, {
    enableReinitialize: true,
    initialValues: values,
    validationSchema: CONCIERGE_VALIDATION(showCourseTypeDropDown),
    onSubmit: values => {
      resetStateHandler();

      _onSubmit(values);
    }
  }, _ref2 => {
    let {
      values,
      handleChange,
      setFieldValue,
      handleSubmit,
      errors,
      touched
    } = _ref2;
    return /*#__PURE__*/_react.default.createElement("form", {
      onSubmit: handleSubmit
    }, /*#__PURE__*/_react.default.createElement(_TextField.default, {
      name: "courseType",
      select: true,
      variant: "outlined",
      fullWidth: true,
      hidden: showCourseTypeDropDown,
      label: "Course Type",
      className: "custom-select-concierge mt-2 mr-3",
      onChange: e => handleChange(e),
      value: values.courseType || "",
      error: errors.courseType && touched.courseType,
      helperText: errors.courseType && touched.courseType ? "( ".concat(errors.courseType, " )") : "",
      margin: "normal"
    }, /*#__PURE__*/_react.default.createElement(_MenuItem.default, {
      value: "",
      disabled: true
    }, text), /*#__PURE__*/_react.default.createElement(_MenuItem.default, {
      value: "coding",
      hidden: teacherSkill && !teacherSkill.includes("coding")
    }, "Coding"), /*#__PURE__*/_react.default.createElement(_MenuItem.default, {
      value: "math",
      hidden: teacherSkill && !teacherSkill.includes("math")
    }, "Math"), /*#__PURE__*/_react.default.createElement(_MenuItem.default, {
      value: "music",
      hidden: teacherSkill && !teacherSkill.includes("music")
    }, "MUSIC")), /*#__PURE__*/_react.default.createElement(_TextField.default, {
      type: "text",
      fullWidth: true,
      className: "mt-2 mb-2",
      variant: "outlined",
      label: "Subject",
      placeholder: "Subject",
      onChange: e => {
        handleChange(e);
        setFieldValue("subject", e.target.value);
      },
      name: "subject",
      value: values.subject || "",
      error: errors.subject && touched.subject,
      helperText: errors.subject && touched.subject ? "( ".concat(errors.subject, " )") : ""
    }), /*#__PURE__*/_react.default.createElement(_TextField.default, {
      fullWidth: true,
      className: "mt-2 mb-2",
      multiline: true,
      rows: isTabView ? 8 : 12,
      rowsMax: isTabView ? 8 : 12,
      variant: "outlined",
      label: "Description",
      placeholder: "Write your detailed concern here",
      onChange: e => {
        handleChange(e);
        setFieldValue("description", e.target.value);
      },
      name: "description",
      value: values.description || "",
      error: errors.description && touched.description,
      helperText: errors.description && touched.description ? "( ".concat(errors.description, " )") : ""
    }), /*#__PURE__*/_react.default.createElement("div", {
      className: "".concat(isTabView ? "d-flex align-items-center" : "fileInputLabel mt-2")
    }, /*#__PURE__*/_react.default.createElement("div", {
      className: "fileName_wrap"
    }, selectedFile.length ? selectedFile.map((file, index) => {
      return /*#__PURE__*/_react.default.createElement("div", {
        className: "fileNameLabel",
        key: "".concat(file.name + index)
      }, /*#__PURE__*/_react.default.createElement("div", {
        className: "file_wrap font12"
      }, file ? file.name : ""), /*#__PURE__*/_react.default.createElement("img", {
        className: "file_cross",
        onClick: () => removeFileHandler(index, setFieldValue),
        src: crossBtn
      }));
    }) : isTabView ? /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, /*#__PURE__*/_react.default.createElement("div", {
      className: "mb-2"
    }), /*#__PURE__*/_react.default.createElement("button", {
      className: "uploadBtnTab_helpdesk font18 d-flex align-items-center justify-content-center",
      onClick: e => {
        handleChange(e);
        handleFile(e, setFieldValue);
      }
    }, /*#__PURE__*/_react.default.createElement("input", {
      type: "file",
      name: "fileData",
      onChange: e => {
        handleChange(e);
        handleFile(e, setFieldValue);
      },
      className: "fileInputTablet",
      disabled: disableBtn,
      accept: "image/*,audio/*,video/*,.pdf,.ppt,.pptx, .docx, .doc, .csv,.xlsx,.webm"
    }), /*#__PURE__*/_react.default.createElement("img", {
      src: _iconUpload.default,
      alt: "upload" // className="icon-image"
      ,
      style: {
        width: "25px",
        marginRight: "10px",
        color: "#03a2ed"
      }
    }), "Click here to Upload")) : /*#__PURE__*/_react.default.createElement("div", {
      className: "emptyFileLabel"
    }, "Upload Attachments")), !isTabView && /*#__PURE__*/_react.default.createElement("span", {
      className: "fileInputButton font32"
    }, "+"), !isTabView && /*#__PURE__*/_react.default.createElement("input", {
      type: "file",
      name: "fileData",
      onChange: e => {
        handleChange(e);
        handleFile(e, setFieldValue);
      },
      className: "fileInput",
      disabled: disableBtn,
      accept: "image/*,audio/*,video/*,.pdf,.ppt,.pptx, .docx, .doc, .csv,.xlsx,.webm"
    })), /*#__PURE__*/_react.default.createElement(_FlashSnackbar.default, {
      message: "File size exceeding 100MB not allowed",
      onClose: () => hideSnackBar(),
      variant: "error",
      open: showSnackBar
    }), /*#__PURE__*/_react.default.createElement("div", {
      className: "text-center d-flex justify-content-center mt-3 mb-4"
    }, /*#__PURE__*/_react.default.createElement("button", {
      disabled: disableSubmit,
      className: "btn btn-dark",
      type: "submit",
      className: "".concat(isTabView ? "sendBtnTab" : "sendBtn")
    }, "SEND")));
  })));
};

var _default = ConciergeEmailModal;
exports.default = _default;
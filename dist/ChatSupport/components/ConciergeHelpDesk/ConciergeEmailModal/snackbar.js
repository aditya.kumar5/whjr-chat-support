"use strict";

require("core-js/modules/es.object.assign.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.MySnackbarContentWrapper = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

var _Button = _interopRequireDefault(require("@material-ui/core/Button"));

var _CheckCircle = _interopRequireDefault(require("@material-ui/icons/CheckCircle"));

var _Error = _interopRequireDefault(require("@material-ui/icons/Error"));

var _Info = _interopRequireDefault(require("@material-ui/icons/Info"));

var _Close = _interopRequireDefault(require("@material-ui/icons/Close"));

var _green = _interopRequireDefault(require("@material-ui/core/colors/green"));

var _IconButton = _interopRequireDefault(require("@material-ui/core/IconButton"));

var _SnackbarContent = _interopRequireDefault(require("@material-ui/core/SnackbarContent"));

var _Warning = _interopRequireDefault(require("@material-ui/icons/Warning"));

var _styles = require("@material-ui/core/styles");

const _excluded = ["classes", "className", "message", "onClose", "variant"];

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

const variantIcon = {
  success: _CheckCircle.default,
  warning: _Warning.default,
  error: _Error.default,
  info: _Info.default
};

const styles1 = theme => ({
  root: {
    flexWrap: "nowrap"
  },
  success: {
    backgroundColor: _green.default[600]
  },
  error: {
    //backgroundColor: theme.palette.error.dark
    backgroundColor: "#FF6400"
  },
  info: {
    backgroundColor: theme.palette.primary.dark
  },
  warning: {
    backgroundColor: "#1a91a9"
  },
  icon: {
    fontSize: 20
  },
  iconVariant: {
    opacity: 0.9,
    marginRight: theme.spacing.unit
  },
  message: {
    display: "flex",
    alignItems: "center"
  }
});

function MySnackbarContent(props) {
  const {
    classes,
    className,
    message,
    onClose,
    variant
  } = props,
        other = _objectWithoutProperties(props, _excluded);

  const Icon = variantIcon[variant];
  return /*#__PURE__*/_react.default.createElement(_SnackbarContent.default, _extends({
    classes: {
      root: classes.root
    },
    className: (0, _classnames.default)(classes[variant], className),
    "aria-describedby": "client-snackbar",
    message: /*#__PURE__*/_react.default.createElement("span", {
      id: "client-snackbar",
      className: classes.message
    }, /*#__PURE__*/_react.default.createElement(Icon, {
      className: (0, _classnames.default)(classes.icon, classes.iconVariant)
    }), message),
    action: [/*#__PURE__*/_react.default.createElement(_IconButton.default, {
      key: "close",
      "aria-label": "Close",
      color: "inherit",
      className: classes.close,
      onClick: onClose
    }, /*#__PURE__*/_react.default.createElement(_Close.default, {
      className: classes.icon
    }))]
  }, other));
}

MySnackbarContent.propTypes = {
  classes: _propTypes.default.object.isRequired,
  className: _propTypes.default.string,
  message: _propTypes.default.node,
  onClose: _propTypes.default.func,
  variant: _propTypes.default.oneOf(["success", "warning", "error", "info"]).isRequired
};
const MySnackbarContentWrapper = (0, _styles.withStyles)(styles1)(MySnackbarContent);
exports.MySnackbarContentWrapper = MySnackbarContentWrapper;
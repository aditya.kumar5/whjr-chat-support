"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _helpDeskDownloadLogo = _interopRequireDefault(require("../../assets/images/helpDeskDownloadLogo.png"));

var _at2x = _interopRequireDefault(require("../../assets/images/at@2x.png"));

var _telephone12x = _interopRequireDefault(require("../../assets/images/telephone-1@2x.png"));

var _customerCare2x = _interopRequireDefault(require("../../assets/images/customer-care@2x.png"));

var _fast2x = _interopRequireDefault(require("../../assets/images/fast@2x.png"));

var _question22x = _interopRequireDefault(require("../../assets/images/question-2@2x.png"));

var _cdnImagePath = require("../../utils/constants/cdnImagePath");

var _deviceType = require("../../utils/constants/device-type");

require("./styles.scss");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const HelpDesk = _ref => {
  let {
    supportNumber,
    modalHandler,
    classes
  } = _ref;

  let deviceType = _react.default.useContext(_deviceType.DeviceTypeContext);

  let isTabView = deviceType === _deviceType.DEVICE_TYPES.TAB.isTab ? true : false;
  return /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, isTabView ? /*#__PURE__*/_react.default.createElement("div", {
    className: "concierge_wrap_tab text-align-center mb-3"
  }, /*#__PURE__*/_react.default.createElement("div", {
    className: "row mb-4 mt-4 width_control_tab"
  }, /*#__PURE__*/_react.default.createElement("div", {
    className: "col-8"
  }, /*#__PURE__*/_react.default.createElement("div", {
    className: "heading_bold font24 mb-3 text-left"
  }, "Your Personal Helper"), /*#__PURE__*/_react.default.createElement("div", {
    className: "row"
  }, /*#__PURE__*/_react.default.createElement("div", {
    className: "content_wrapper_tab col-xs-12 text-center ".concat(isTabView && "content_wrapper_tablet", " ")
  }, /*#__PURE__*/_react.default.createElement("img", {
    className: "image_position",
    height: 30,
    src: _cdnImagePath.CONNECT_TAB
  }), /*#__PURE__*/_react.default.createElement("div", {
    className: "content_title heading_bold font16",
    style: {
      color: "#535353"
    }
  }, "Connect with", /*#__PURE__*/_react.default.createElement("br", null), "dedicated experts")), /*#__PURE__*/_react.default.createElement("div", {
    className: " content_wrapper_tab col-xs-12 text-center ".concat(isTabView && "content_wrapper_tablet")
  }, /*#__PURE__*/_react.default.createElement("img", {
    className: "image_position",
    height: 30,
    src: _cdnImagePath.IMMEDIATE_TAB
  }), /*#__PURE__*/_react.default.createElement("div", {
    className: "content_title heading_bold font16",
    style: {
      color: "#535353"
    }
  }, "Get Immediate", /*#__PURE__*/_react.default.createElement("br", null), "assistance")), /*#__PURE__*/_react.default.createElement("div", {
    className: "content_wrapper_tab col-xs-12 text-center ".concat(isTabView && "content_wrapper_tablet")
  }, /*#__PURE__*/_react.default.createElement("img", {
    className: "image_position",
    height: 30,
    src: _cdnImagePath.ISSUES_TAB
  }), /*#__PURE__*/_react.default.createElement("div", {
    className: "content_title heading_bold font16",
    style: {
      color: "#535353"
    }
  }, "Raise all", /*#__PURE__*/_react.default.createElement("br", null), "your issues")))), /*#__PURE__*/_react.default.createElement("div", {
    className: "col-4"
  }, /*#__PURE__*/_react.default.createElement("div", {
    className: "contact_wrap_tab"
  }, /*#__PURE__*/_react.default.createElement("div", {
    className: " ".concat(classes.left_center, " col-xs-12")
  }, /*#__PURE__*/_react.default.createElement("div", {
    className: " text_white",
    style: {
      backgroundImage: "linear-gradient(to bottom right, #ffa227, #ee6d2d)",
      borderRadius: "2.25rem",
      padding: "".concat(isTabView ? "15px 14px" : "18px 14px", " "),
      width: "260px"
    }
  }, /*#__PURE__*/_react.default.createElement("div", {
    className: "heading_bold font16"
  }, "Call us at", " ", /*#__PURE__*/_react.default.createElement("span", null, /*#__PURE__*/_react.default.createElement("a", {
    className: "text_white",
    href: "tel:".concat(supportNumber)
  }, supportNumber)))), /*#__PURE__*/_react.default.createElement("div", {
    className: "heading_light font12 mt-2"
  }, "Wait time: less than 30 seconds")), /*#__PURE__*/_react.default.createElement("div", {
    className: " ".concat(classes.left_center, " col-xs-12 mt-4"),
    onClick: modalHandler
  }, /*#__PURE__*/_react.default.createElement("div", {
    className: "pl-1"
  }, /*#__PURE__*/_react.default.createElement("div", {
    className: "heading_bold font16 text_white",
    style: {
      backgroundImage: "linear-gradient(to left,#087fed,#01b7ee)",
      borderRadius: "2.25rem",
      padding: "".concat(isTabView ? "15px 14px" : "18px 14px", " "),
      width: "260px"
    }
  }, "Write to us"), /*#__PURE__*/_react.default.createElement("div", {
    className: "heading_light font12 mt-2"
  }, "Wait time: less than 24 hours"))))))) : /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, /*#__PURE__*/_react.default.createElement("div", {
    className: "concierge_wrap text-align-center mb-3"
  }, /*#__PURE__*/_react.default.createElement("div", {
    className: "heading_bold font26 mb-3"
  }, "Your Personal Concierge"), /*#__PURE__*/_react.default.createElement("div", {
    className: "".concat(classes.flex_wrap_align, " row mb-4 mt-4 width_control")
  }, /*#__PURE__*/_react.default.createElement("div", {
    className: "".concat(classes.flex_wrap_align, " content_wrapper col-xs-12")
  }, /*#__PURE__*/_react.default.createElement("img", {
    className: "image_position",
    height: 30,
    src: _customerCare2x.default
  }), /*#__PURE__*/_react.default.createElement("div", {
    className: "content_title heading_bold font14"
  }, "Connect with", /*#__PURE__*/_react.default.createElement("br", null), "dedicated experts")), /*#__PURE__*/_react.default.createElement("div", {
    className: "".concat(classes.flex_wrap_align, " content_wrapper col-xs-12")
  }, /*#__PURE__*/_react.default.createElement("img", {
    className: "image_position",
    height: 30,
    src: _fast2x.default
  }), /*#__PURE__*/_react.default.createElement("div", {
    className: "content_title heading_bold font14"
  }, "Get Immediate", /*#__PURE__*/_react.default.createElement("br", null), "assistance")), /*#__PURE__*/_react.default.createElement("div", {
    className: "".concat(classes.flex_wrap_align, " content_wrapper col-xs-12")
  }, /*#__PURE__*/_react.default.createElement("img", {
    className: "image_position",
    height: 30,
    src: _question22x.default
  }), /*#__PURE__*/_react.default.createElement("div", {
    className: "content_title heading_bold font14"
  }, "Raise all", /*#__PURE__*/_react.default.createElement("br", null), "your issues"))), /*#__PURE__*/_react.default.createElement("div", {
    className: "contact_wrap row"
  }, /*#__PURE__*/_react.default.createElement("div", {
    className: "content_wrapper_white ".concat(classes.flex, " ").concat(classes.left_center, " col-xs-12")
  }, /*#__PURE__*/_react.default.createElement("img", {
    className: "ml-1 mr-2",
    width: 24,
    src: _telephone12x.default
  }), /*#__PURE__*/_react.default.createElement("div", {
    className: "pl-1"
  }, /*#__PURE__*/_react.default.createElement("div", {
    className: "heading_bold call_wrap font16"
  }, "Call us at:", " ", /*#__PURE__*/_react.default.createElement("span", null, /*#__PURE__*/_react.default.createElement("a", {
    className: "text_black",
    href: "tel:".concat(supportNumber)
  }, supportNumber))), /*#__PURE__*/_react.default.createElement("div", {
    className: "heading_light font12"
  }, "Wait time: less than 30 seconds"))), /*#__PURE__*/_react.default.createElement("div", {
    className: "content_wrapper_white ".concat(classes.flex, " ").concat(classes.left_center, " col-xs-12"),
    onClick: modalHandler
  }, /*#__PURE__*/_react.default.createElement("img", {
    className: "ml-1 mr-2",
    width: 24,
    src: _at2x.default
  }), /*#__PURE__*/_react.default.createElement("div", {
    className: "pl-1"
  }, /*#__PURE__*/_react.default.createElement("div", {
    className: "heading_bold font16"
  }, "Write to us"), /*#__PURE__*/_react.default.createElement("div", {
    className: "heading_light font12"
  }, "Wait time: less than 24 hours"))))), /*#__PURE__*/_react.default.createElement("div", {
    className: "".concat(classes.border_wrap, " row p-4")
  }, /*#__PURE__*/_react.default.createElement("div", {
    className: "col-xs-12 font20 mr-3"
  }, "To help our team troubleshoot technical issues"), /*#__PURE__*/_react.default.createElement("div", {
    className: "".concat(classes.button_border, " ").concat(classes.center_flex, " ").concat(classes.flex, " heading_reg btn-margin_mobile p-1 col-xs-12")
  }, /*#__PURE__*/_react.default.createElement("div", {
    className: "font14"
  }, /*#__PURE__*/_react.default.createElement("a", {
    className: "".concat(classes.flex, " heading_bold text_blue ").concat(classes.center_flex),
    href: "https://anydesk.com/en/downloads",
    target: "_blank"
  }, /*#__PURE__*/_react.default.createElement("img", {
    className: "ml-1",
    className: "mr-1",
    width: 22,
    src: _helpDeskDownloadLogo.default
  }), "Download Anydesk"))))));
};

var _default = HelpDesk;
exports.default = _default;
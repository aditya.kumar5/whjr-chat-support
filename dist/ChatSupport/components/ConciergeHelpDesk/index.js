"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/es.symbol.description.js");

require("core-js/modules/web.dom-collections.iterator.js");

var _react = _interopRequireWildcard(require("react"));

var _reactIntl = require("react-intl");

var _ConciergeEmailModal = _interopRequireDefault(require("./ConciergeEmailModal"));

var _helpDesk = _interopRequireDefault(require("./helpDesk"));

var _helper = require("../../utils/helpers/helper");

var _get2 = _interopRequireDefault(require("lodash/get"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

class ConciergeHelpDesk extends _react.Component {
  constructor(props) {
    var _this;

    super(props);
    _this = this;

    _defineProperty(this, "handleHelpDeskClose", () => {
      this.setState({
        showHelpDesk: false
      });
    });

    _defineProperty(this, "showEmailModalHandler", () => {
      this.setState({
        showEmailModal: true
      });
    });

    _defineProperty(this, "hideEmailModalHandler", function () {
      let backToHelpBox = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;

      _this.setState({
        showEmailModal: false
      });

      if (_this.props.isClickedFromHelpBox) {
        _this.props.closeHelpHandler("email", backToHelpBox ? true : false);
      }
    });

    _defineProperty(this, "resetState", () => {
      const values = {
        subject: "",
        description: "",
        courseType: "",
        fileData: []
      };
      this.setState({
        values
      });
    });

    _defineProperty(this, "createTicketWithMedia", () => {
      if (this.props.mediaToken && this.props.mediaToken.fileTokens && this.state.values.description && this.state.values.subject) {
        if (this.state.token && this.props.mediaToken.fileTokens.length && this.state.token !== this.props.mediaToken.fileTokens[0] || this.state.token == "") {
          this.setState({
            token: this.props.mediaToken.fileTokens[0]
          });
          const {
            id,
            type
          } = this.props;
          const submitData = {
            submittedById: id,
            submittedByType: type,
            message: this.state.values.description,
            subject: this.state.values.subject,
            courseType: type === "STUDENT" ? this.getStudentCourse() : this.state.values.courseType,
            fileTokens: this.props.mediaToken.fileTokens,
            paidStatus: type === "STUDENT" ? (0, _helper.setPaidStatus)(type, this.props.student) : (0, _helper.setPaidStatus)(type, this.props.teacher)
          };
          this.props.d__submitFeedbackEscalationConcierge(submitData);
          this.resetState();
        }
      }
    });

    _defineProperty(this, "getStudentCourse", () => {
      const {
        student
      } = this.props;
      const {
        activeCourse
      } = student;
      return (0, _get2.default)(activeCourse, "courseType", null);
    });

    _defineProperty(this, "createTicketWithoutMedia", values => {
      const {
        id,
        type
      } = this.props;
      const submitData = {
        submittedById: id,
        submittedByType: type,
        message: values.description,
        subject: values.subject,
        courseType: type === "STUDENT" ? this.getStudentCourse() : values.courseType,
        fileTokens: [],
        paidStatus: type === "STUDENT" ? (0, _helper.setPaidStatus)(type, this.props.student) : (0, _helper.setPaidStatus)(type, this.props.teacher)
      };
      this.props.d__submitFeedbackEscalationConcierge(submitData);
      this.resetState();
    });

    _defineProperty(this, "submitData", values => {
      this.setState({
        values
      });
      values.fileData.length ? this.props.d__submitMediaFeedbackEscalation(values.fileData) : this.createTicketWithoutMedia(values);
      this.hideEmailModalHandler();
    });

    this.state = {
      value: 0,
      showEmailModal: false,
      values: {
        courseType: "",
        subject: "",
        description: "",
        fileData: []
      },
      token: ""
    };
  }

  componentDidMount() {
    //only call when option selection from
    if (this.props.isClickedFromHelpBox) {
      this.showEmailModalHandler();
    }
  }

  componentDidUpdate() {
    this.createTicketWithMedia();
  }

  render() {
    const {
      classes,
      supportNumber
    } = this.props;
    return /*#__PURE__*/_react.default.createElement(_react.Fragment, null, !this.props.isClickedFromHelpBox && /*#__PURE__*/_react.default.createElement(_helpDesk.default, {
      supportNumber: supportNumber,
      modalHandler: this.showEmailModalHandler,
      classes: classes
    }), /*#__PURE__*/_react.default.createElement(_ConciergeEmailModal.default, {
      showDialog: this.state.showEmailModal,
      showCourseTypeDropDown: this.props.type === "STUDENT" ? true : false,
      values: this.state.values,
      closeModal: this.hideEmailModalHandler,
      modalTitle: "Write to us",
      onSubmit: this.submitData,
      teacherSkill: this.props.type === "STUDENT" ? null : (0, _helper.getCodingOrMathSkill)(this.props.teacher.teacher_skills),
      isClickedFromHelpBox: this.props.isClickedFromHelpBox ? true : false
    }));
  }

} // const mapStateToProps = state => {
//   return {
//     mediaToken: getUploadedImageToken(state)
//   };
// };
// const mapDispatchToProps = dispatch => {
//   return {
//     d__submitFeedbackEscalationConcierge: payload => {
//       dispatch(submitFeedbackEscalationConcierge.request({ payload }));
//     },
//     d__submitMediaFeedbackEscalation: payload => {
//       dispatch(submitMediaFeedbackEscalation.request(payload));
//     }
//   };
// };
// export default connect(mapStateToProps, mapDispatchToProps)(ConciergeHelpDesk);


var _default = ConciergeHelpDesk;
exports.default = _default;
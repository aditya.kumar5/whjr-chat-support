"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/es.string.includes.js");

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _momentTimezone = _interopRequireDefault(require("moment-timezone"));

var _Attachment = _interopRequireDefault(require("./Attachment"));

var _alert = _interopRequireDefault(require("../assets/images/alert.png"));

var _templateObject, _templateObject2, _templateObject3, _templateObject4, _templateObject5, _templateObject6, _templateObject7, _templateObject8, _templateObject9, _templateObject10, _templateObject11;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

const MessageContainer = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n  overflow: hidden;\n  flex-grow: 1;\n"])));

const MessageTextContainer = _styledComponents.default.div(_templateObject2 || (_templateObject2 = _taggedTemplateLiteral(["\n  display: flex;\n  flex-direction: column;\n"])));

const MessageText = _styledComponents.default.div(_templateObject3 || (_templateObject3 = _taggedTemplateLiteral(["\n  max-width: 80%;\n  margin-top 2px;\n  margin-bottom: 6px;\n  padding: 4px 10px;\n  border-radius: 8px;\n  font-size: 0.75em;\n  font-weight: 600;\n  word-break: break-word;\n  white-space: pre-line;\n"])));

const AgentMessage = (0, _styledComponents.default)(MessageText)(_templateObject4 || (_templateObject4 = _taggedTemplateLiteral(["\n  margin-right: auto;\n  background-color: #fff;\n  border: solid 1px #e4e4e4;\n  border-top-left-radius: 0px;\n"])));
const UserMessage = (0, _styledComponents.default)(MessageText)(_templateObject5 || (_templateObject5 = _taggedTemplateLiteral(["\n  margin-left: auto;\n  background-color: #e8eaec;\n  border-top-right-radius: 0px;\n"])));

const InfoMessage = _styledComponents.default.div(_templateObject6 || (_templateObject6 = _taggedTemplateLiteral(["\n  margin: 10px 0px;\n  font-size: 0.75em;\n  font-weight: 700;\n  color: ", ";\n  text-align: center;\n"])), props => props.theme.colors.textColor.secondary);

const StatusDate = (0, _styledComponents.default)(InfoMessage)(_templateObject7 || (_templateObject7 = _taggedTemplateLiteral(["\n  margin-bottom: 20px;\n  &::before,\n  &::after {\n    content: \"\";\n    border-top: 1px solid #dedede;\n    position: relative;\n    width: 50%;\n    display: inline-block;\n    vertical-align: middle;\n  }\n  &::before {\n    margin-left: -50%;\n    right: 1rem;\n  }\n  &::after {\n    margin-right: -50%;\n    left: 1rem;\n  }\n"])));

const ErrorRetry = _styledComponents.default.div(_templateObject8 || (_templateObject8 = _taggedTemplateLiteral(["\n  max-width: 80%;\n  margin-left: auto;\n  margin-bottom: 6px;\n  font-size: 0.625em;\n  font-weight: 600;\n  color: ", ";\n  cursor: pointer;\n  .icon-warning {\n    margin-right: 3px;\n  }\n"])), props => props.theme.colors.textColor.error);

const ErrorImage = _styledComponents.default.img(_templateObject9 || (_templateObject9 = _taggedTemplateLiteral(["\n  width: 14px;\n  height: 13px;\n  margin-bottom: 1px;\n  margin-right: 4px;\n"])));

const AttachmentContainerUser = _styledComponents.default.div(_templateObject10 || (_templateObject10 = _taggedTemplateLiteral(["\n  margin-left: auto;\n  width: 65%;\n  margin-bottom: 6px;\n  text-align: end;\n"])));

const AttachmentContainerAgent = _styledComponents.default.div(_templateObject11 || (_templateObject11 = _taggedTemplateLiteral(["\n  margin-right: auto;\n  width: 75%;\n  margin-bottom: 6px;\n"])));

const Message = _ref => {
  let {
    message,
    resendMessage,
    barWidth
  } = _ref;
  return /*#__PURE__*/_react.default.createElement(MessageContainer, null, {
    info: /*#__PURE__*/_react.default.createElement(InfoMessage, null, message.msg),
    date: /*#__PURE__*/_react.default.createElement(StatusDate, null, (0, _momentTimezone.default)(message.msg).local().format("ddd, hh:mm A"))
  }[message.messageType] || /*#__PURE__*/_react.default.createElement(MessageTextContainer, null, message.nick && message.nick === "visitor" && message.msg && !message.attachment && /*#__PURE__*/_react.default.createElement(UserMessage, null, message.hasLink ? /*#__PURE__*/_react.default.createElement("span", {
    dangerouslySetInnerHTML: {
      __html: message.msg
    }
  }) : /*#__PURE__*/_react.default.createElement("span", null, message.msg)), (message.nick && message.nick.includes("agent") && message.type === "chat.msg" || message.messageType === "agent-typing") && /*#__PURE__*/_react.default.createElement(AgentMessage, null, message.hasLink ? /*#__PURE__*/_react.default.createElement("span", {
    dangerouslySetInnerHTML: {
      __html: message.msg
    }
  }) : /*#__PURE__*/_react.default.createElement("span", null, message.msg)), (message.type === "chat.file" || "attachment" in message) && message.nick && message.nick === "visitor" && /*#__PURE__*/_react.default.createElement(AttachmentContainerUser, null, /*#__PURE__*/_react.default.createElement(_Attachment.default, {
    url: message.attachment.url,
    name: message.attachment.name,
    mime_type: message.attachment.mime_type,
    size: message.attachment.size,
    type: "visitor",
    msgType: message.type,
    progressBarWidth: barWidth,
    err: message.errMsg
  })), message.nick && message.nick.includes("agent") && message.type === "chat.file" && /*#__PURE__*/_react.default.createElement(AttachmentContainerAgent, null, /*#__PURE__*/_react.default.createElement(_Attachment.default, {
    url: message.attachment.url,
    name: message.attachment.name,
    mime_type: message.attachment.mime_type,
    size: message.attachment.size,
    type: "agent",
    msgType: message.type
  })), message.sendErr && /*#__PURE__*/_react.default.createElement(ErrorRetry, {
    onClick: () => resendMessage()
  }, /*#__PURE__*/_react.default.createElement(ErrorImage, {
    src: _alert.default,
    alt: "warning"
  }), message.errMsg ? message.errMsg : "Couldn\u2019t send. Tap to try again.")));
};

Message.displayName = "Message";
Message.propTypes = {
  message: _propTypes.default.object.isRequired,
  resendMessage: _propTypes.default.func,
  barWidth: _propTypes.default.string
};
var _default = Message;
exports.default = _default;
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/web.dom-collections.iterator.js");

var _react = _interopRequireWildcard(require("react"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _styles = require("../../styles");

var _MessageList = _interopRequireDefault(require("../MessageList"));

var _debounce = _interopRequireDefault(require("lodash/debounce"));

var _templateObject, _templateObject2, _templateObject3;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

const HistoryChatWrap = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n  overflow-y: auto;\n"])));

const Loading = (0, _styledComponents.default)(_styles.LoadingBubble)(_templateObject2 || (_templateObject2 = _taggedTemplateLiteral(["\n  top: 47px;\n  z-index: 99999;\n"])));

const HistoryNotFoundWrap = _styledComponents.default.div(_templateObject3 || (_templateObject3 = _taggedTemplateLiteral(["\n  margin: auto;\n  display: flex;\n  height: 100%;\n  align-items: center;\n  color: #1e1e1e;\n"])));

let zChat = null;

class ChatHistory extends _react.Component {
  constructor() {
    var _this;

    super(...arguments);
    _this = this;

    _defineProperty(this, "state", {
      connectionStatus: null,
      accountConnectionStatus: null,
      historyMessages: this.props.pastChats.length ? this.props.pastChats : [],
      loadingMoreChats: false,
      moreChatsAvailable: false,
      loadCounter: 0
    });

    _defineProperty(this, "prevChats", []);

    _defineProperty(this, "onEvent", (evt, data) => {
      this.handleChatInsertion(data);
    });

    _defineProperty(this, "checkIfChatExist", (oldChat, newChat) => {
      if (oldChat.timestamp === newChat.timestamp && oldChat.type === newChat.type) {
        if (oldChat.type === "chat.msg" && oldChat.msg === newChat.msg) {
          return true;
        } else if (oldChat.type === "chat.memberjoin" || oldChat.type === "chat.memberleave" || oldChat.type === "chat.rating") {
          return true;
        } else {
          return false;
        }
      }
    });

    _defineProperty(this, "insertPosition", -1);

    _defineProperty(this, "handleChatInsertion", function (chatData) {
      let checkIfExist = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

      if (checkIfExist) {
        if (_this.prevChats.filter(x => _this.checkIfChatExist(x, chatData)).length) return;
      }

      if (_this.prevChats.length === 0 && _this.props.pastChats.length === 0) {
        _this.prevChats.push(chatData);
      } else {
        let positionAvailable = true; //first element will decide

        if (new Date(chatData.timestamp) > new Date(_this.prevChats[0].timestamp)) {
          //greater than 1st element //below existing element
          _this.insertPosition = _this.prevChats.findIndex(x => new Date(x.timestamp) > new Date(chatData.timestamp)); // if index == -1 then push at last
        } else {
          //smaller than 1st element //above existing element
          _this.prevChats.unshift(chatData);

          positionAvailable = false;
        }

        if (positionAvailable) {
          if (_this.insertPosition == -1) {
            _this.prevChats.push(chatData);
          } else {
            _this.prevChats.splice(_this.insertPosition, 0, chatData);

            _this.insertPosition = -1;
          }
        }
      }

      const tempData = [..._this.prevChats];
      tempData.forEach((data, index) => {
        if (data.type === "chat.memberjoin") {
          if (data.nick === "visitor" && data.first) {
            tempData[index] = {
              messageType: "date",
              msg: data.timestamp
            };
          } else {
            tempData[index] = _this.handleMemberJoinObjects(data, "joined");
          }
        } else if (data.type === "chat.memberleave") {
          tempData[index] = _this.handleMemberJoinObjects(data, "left");
        }
      });

      _this.setState({
        historyMessages: tempData,
        loadingMoreChats: false
      });

      _this.props.handlePastChats(tempData);

      if (_this.prevChats.length > 0 && _this.props.pastChats.length === 0 && _this.refs.chatHistory) _this.refs.chatHistory.scroll(0, _this.refs.chatHistory.firstChild.offsetHeight);
      if (_this.state.loadCounter >= 1 && _this.refs.chatHistory) _this.refs.chatHistory.scroll(0, _this.refs.chatHistory.offsetHeight);
    });

    _defineProperty(this, "handleMemberJoinObjects", (data, msg) => {
      const chatData = _objectSpread({}, data);

      if (data.type === "chat.memberleave" || data.type === "chat.memberjoin") {
        chatData.msg = "".concat(chatData.display_name, " has ").concat(msg, ".");
        chatData.messageType = "info";
      }

      return chatData;
    });

    _defineProperty(this, "initEvents", () => {
      zChat.on("history", this.evtHandler = data => {
        this.onEvent("history", data);
      });
    });

    _defineProperty(this, "reconnect", () => {
      zChat.reconnect();
    });

    _defineProperty(this, "prevChatAvailable", true);

    _defineProperty(this, "fetchHistory", () => {
      this.setState({
        loadingMoreChats: true
      });
      const loadCounter = this.state.loadCounter + 1;
      zChat.fetchChatHistory((err, data) => {
        if (!err) {
          if (data.has_more) {
            this.setState({
              moreChatsAvailable: data.has_more,
              loadCounter
            });
            this.prevChatAvailable = data.has_more;
          } else {
            this.setState({
              moreChatsAvailable: data.has_more,
              loadingMoreChats: false,
              loadCounter
            });
            this.prevChatAvailable = data.has_more;
            this.props.handlePastChats(null, data.has_more);
          }
        } else {
          this.setState({
            moreChatsAvailable: true,
            loadingMoreChats: false,
            loadCounter
          });
        }
      });
    });

    _defineProperty(this, "scrollEvent", e => {
      e.persist();

      if (!this.debouncedFn) {
        this.debouncedFn = (0, _debounce.default)(() => {
          if (e.target && e.target.scrollTop === 0 && (this.state.moreChatsAvailable || this.props.chatAvailable)) {
            if (this.prevChats.length && this.state.loadCounter < 1) this.initEvents();
            if (this.props.chatAvailable && this.prevChatAvailable) this.fetchHistory();
          }
        }, 300);
      }

      this.debouncedFn();
    });
  }

  componentDidMount() {
    zChat = window.zChat;
    this.prevChats = this.props.pastChats.length ? [...this.props.pastChats] : [];
    const connectionStatus = zChat.getConnectionStatus();
    if (connectionStatus === "closed") this.reconnect();
    this.setState({
      connectionStatus,
      accountConnectionStatus: zChat.getAccountStatus()
    }, () => {
      const currentChatlog = zChat.getChatLog() || [];

      if (this.props.chatAvailable && this.prevChats.length == 0) {
        this.initEvents();
        this.fetchHistory();
      } else {
        setTimeout(() => {
          if (this.refs.chatHistory) {
            this.refs.chatHistory.scroll(0, this.refs.chatHistory.firstChild.offsetHeight);
          }
        }, 0);
      }

      if (currentChatlog.length > 0) {
        currentChatlog.forEach(data => this.handleChatInsertion(data, true));
      }
    });
  }

  componentWillUnmount() {
    zChat.un("history", this.evtHandler);
  }

  render() {
    return /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, this.state.loadingMoreChats && /*#__PURE__*/_react.default.createElement(Loading, null, "Loading..."), this.prevChats.length > 0 && /*#__PURE__*/_react.default.createElement(HistoryChatWrap, {
      ref: "chatHistory",
      onScroll: this.scrollEvent
    }, /*#__PURE__*/_react.default.createElement(_MessageList.default, {
      messages: this.state.historyMessages
    })), this.prevChats.length == 0 && !this.state.loadingMoreChats && /*#__PURE__*/_react.default.createElement(HistoryNotFoundWrap, null, "No past chats found."));
  }

}

ChatHistory.displayName = "ChatHistory";
var _default = ChatHistory;
exports.default = _default;
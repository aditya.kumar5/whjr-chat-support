"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/web.dom-collections.iterator.js");

var _react = _interopRequireWildcard(require("react"));

var _embedReact = require("@typeform/embed-react");

var _core = require("@material-ui/core");

var _Refresh = _interopRequireDefault(require("@material-ui/icons/Refresh"));

var _styles = require("@material-ui/core/styles");

var _get2 = _interopRequireDefault(require("lodash/get"));

var _segmentEvents = require("../../../utils/constants/segmentEvents");

var _config = require("../../../utils/config");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

// import { connect } from "react-redux";
// import { withRouter } from "react-router-dom";
// import { fetchTypeformResponse } from "../../../../actions";
// import { getChatConfig } from "../../../../reducers";
const useStyles = (0, _styles.makeStyles)(theme => ({
  internetChecking: {
    textAlign: "center"
  },
  loaderIcon: {
    margin: "50px",
    animation: "$spin 2s infinite linear ",
    "& svg": {
      fontSize: "48px"
    }
  },
  "@keyframes spin": {
    from: {
      transform: "rotate(0deg)"
    },
    to: {
      transform: "rotate(359deg)"
    }
  }
}));

const initTypeform = () => {
  var qs,
      js,
      q,
      s,
      d = document,
      gi = d.getElementById,
      ce = d.createElement,
      gt = d.getElementsByTagName,
      id = "typef_orm_share",
      b = "https://embed.typeform.com/";

  if (!gi.call(d, id)) {
    js = ce.call(d, "script");
    js.id = id;
    js.src = b + "embed.js";
    q = gt.call(d, "script")[0];
    q.parentNode.insertBefore(js, q);
  }
};

const TypeformHandler = _ref => {
  let {
    setIsResolved = () => null,
    setOpenEndClss,
    isChatSupport,
    resetChatState = () => null,
    startInternetTroubleShoot = false,
    setStartInternetTroubleShoot = () => null,
    fetchTypeformResponse,
    chatData,
    handleEventTrack = () => null,
    setHideProgressBar = () => null,
    sessionId = null,
    setAnswers = () => null
  } = _ref;
  const classes = useStyles();
  const [formResponseId, setFormResponseId] = (0, _react.useState)(false);
  const [isLoading, setIsLoading] = (0, _react.useState)(null);
  const linkUrl = "".concat(_config.config.TYPEFORM_URL, "?typeform-medium=embed-snippet#sessionid=").concat(sessionId);
  (0, _react.useEffect)(() => {
    initTypeform();
  }, []);
  (0, _react.useEffect)(() => {
    if (chatData.typeformResponse) {
      let response = chatData.typeformResponse;
      const responseItems = response && (0, _get2.default)(response, "items", []);
      handleEventTrack(_segmentEvents.EVENT_NAMES.TYPEFORM_RESPONSE, response);
      const submittedData = responseItems.find(item => item.response_id === formResponseId);

      if (response && response.length !== 0) {// setIsLoading(false);
      }

      if (submittedData) {
        let selectedAnswers = [];
        submittedData.answers.forEach(data => {
          selectedAnswers.push(data.choice.label);
        });
        setAnswers(selectedAnswers);
        const lastQuestion = submittedData.answers[submittedData.answers.length - 1];

        if (lastQuestion && lastQuestion.choice.label === "This solved the issue") {
          resetChatState(false);
          setIsResolved(true);
        } else if (lastQuestion && lastQuestion.choice.label === "The issue still persists") {
          setIsResolved(false);
          setOpenEndClss(true);
        }
      }

      setStartInternetTroubleShoot(false);
    }
  }, [chatData.typeformResponse]);

  const handleSubmit = event => {// setIsLoading(true);
    // let request = {
    //   form_id: config.TYPEFORM_TOKEN.FORM_ID,
    //   authToken: config.TYPEFORM_TOKEN.TOKEN,
    //   page_size: 1
    // };
    // setFormResponseId(event.responseId);
    // handleEventTrack(EVENT_NAMES.WIDGET_SUBMIT_CLICKED, request);
    // fetchTypeformResponse(request);
  };

  function stopTroubleshoot() {
    setHideProgressBar(true);
  }

  return /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, isChatSupport ? isLoading ? /*#__PURE__*/_react.default.createElement(_core.Box, {
    className: classes.internetChecking,
    p: 3
  }, /*#__PURE__*/_react.default.createElement(_core.Typography, null, "Submitting the form..."), /*#__PURE__*/_react.default.createElement("div", {
    className: classes.loaderIcon
  }, /*#__PURE__*/_react.default.createElement(_Refresh.default, null))) : /*#__PURE__*/_react.default.createElement(_embedReact.Widget, {
    id: _config.config.TYPEFORM_TOKEN.FORM_ID,
    style: {
      width: "100%",
      height: "320px",
      position: "absolute"
    },
    onSubmit: event => handleSubmit(event),
    className: "type-form",
    open: startInternetTroubleShoot ? "load" : "exit",
    onReady: () => stopTroubleshoot()
  }) : /*#__PURE__*/_react.default.createElement("div", {
    style: {
      position: "fixed",
      top: "calc(50% - 250px)",
      right: "20px",
      transition: "width 300ms ease-out",
      width: 0,
      zIndex: 2
    },
    "data-qa": "side_panel"
  }, /*#__PURE__*/_react.default.createElement("a", {
    className: "typeform-share button",
    href: linkUrl,
    "data-mode": "side_panel",
    style: {
      position: "absolute",
      top: "300px",
      height: "40px",
      padding: "0 20px",
      margin: 0,
      cursor: "pointer",
      background: "#0445AF",
      borderRadius: "4px 4px 0px 0px",
      boxShadow: "0px 2px 12px rgba(0, 0, 0, 0.06), 0px 2px 4px rgba(0, 0, 0, 0.08)",
      display: "flex",
      alignItems: "center",
      justifyContent: "flex-start",
      transform: "rotate(-90deg)",
      transformOrigin: "bottom left",
      color: "white",
      textDecoration: "none",
      zIndex: "9999"
    },
    "data-submit-close-delay": "1",
    "data-width": "320",
    "data-height": "500",
    target: "_blank"
  }, " ", /*#__PURE__*/_react.default.createElement("span", {
    className: "icon",
    style: {
      width: "32px",
      position: "relative",
      transform: "rotate(90deg) scale(0.85)",
      left: "-8px"
    }
  }, " ", /*#__PURE__*/_react.default.createElement("svg", {
    width: "24",
    height: "24",
    viewBox: "0 0 24 24",
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg",
    style: {
      marginTop: "10px"
    }
  }, " ", /*#__PURE__*/_react.default.createElement("path", {
    d: "M21 0H0V9L10.5743 24V16.5H21C22.6567 16.5 24 15.1567 24 13.5V3C24 1.34325 22.6567 0 21 0ZM7.5 9.75C6.672 9.75 6 9.07875 6 8.25C6 7.42125 6.672 6.75 7.5 6.75C8.328 6.75 9 7.42125 9 8.25C9 9.07875 8.328 9.75 7.5 9.75ZM12.75 9.75C11.922 9.75 11.25 9.07875 11.25 8.25C11.25 7.42125 11.922 6.75 12.75 6.75C13.578 6.75 14.25 7.42125 14.25 8.25C14.25 9.07875 13.578 9.75 12.75 9.75ZM18 9.75C17.172 9.75 16.5 9.07875 16.5 8.25C16.5 7.42125 17.172 6.75 18 6.75C18.828 6.75 19.5 7.42125 19.5 8.25C19.5 9.07875 18.828 9.75 18 9.75Z",
    fill: "white"
  }), " "), " "), " ", /*#__PURE__*/_react.default.createElement("span", {
    className: "text-center",
    style: {
      textDecoration: "none",
      fontSize: "18px",
      fontFamily: "Helvetica,Arial,sans-serif",
      whiteSpace: "nowrap",
      overflow: "hidden",
      textOverflow: "ellipsis",
      width: "100%"
    }
  }, " ", "Report an issue", " "))));
}; // const mapStateToProps = state => ({
//   chatData: getChatConfig(state),
// });
// const mapDispatchToProps = dispatch => ({
//   fetchTypeformResponse: payload => {
//     dispatch(fetchTypeformResponse.request({ payload }));
//   }
// });
// export default connect(
//   mapStateToProps,
//   mapDispatchToProps
// )(withRouter(TypeformHandler));


var _default = TypeformHandler;
exports.default = _default;
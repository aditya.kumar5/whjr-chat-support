"use strict";

require("core-js/modules/es.object.assign.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = InternetSpeedTest;

require("core-js/modules/web.dom-collections.iterator.js");

require("core-js/modules/es.parse-int.js");

var _react = _interopRequireWildcard(require("react"));

var _styles = require("@material-ui/core/styles");

var _core = require("@material-ui/core");

var _icons = require("@material-ui/icons");

var _style = _interopRequireDefault(require("./style"));

var _TypeformHandler = _interopRequireDefault(require("./TypeformHandler"));

var _segmentEvents = require("../../utils/constants/segmentEvents");

var _socketUtil = require("../../utils/helpers/socket-lib/socket-util");

const _excluded = ["children", "value", "index"];

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

const TabPanel = props => {
  const {
    children,
    value,
    index
  } = props,
        other = _objectWithoutProperties(props, _excluded);

  return /*#__PURE__*/_react.default.createElement("div", _extends({
    role: "tabpanel",
    hidden: value !== index,
    id: "wrapped-tabpanel-".concat(index),
    "aria-labelledby": "wrapped-tab-".concat(index)
  }, other), value === index && /*#__PURE__*/_react.default.createElement(_core.Box, {
    p: 1.2
  }, children));
};

const allyProps = index => {
  return {
    id: "wrapped-tab-".concat(index),
    "aria-controls": "wrapped-tabpanel-".concat(index)
  };
};

const SpeedSpinner = props => {
  const classes = (0, _style.default)();
  return /*#__PURE__*/_react.default.createElement(_core.Box, {
    className: classes.speedContainer,
    position: "relative",
    display: "inline-flex"
  }, /*#__PURE__*/_react.default.createElement(_core.CircularProgress, _extends({
    size: 110,
    thickness: 2,
    classes: {
      root: classes.circularProgress,
      colorPrimary: classes.circularProgressColor
    },
    variant: "determinate"
  }, props)), /*#__PURE__*/_react.default.createElement(_core.Box, {
    className: classes.circularInner,
    position: "absolute"
  }, /*#__PURE__*/_react.default.createElement(_core.Typography, {
    variant: "caption",
    component: "div",
    color: "textSecondary"
  }, props.value ? props.value : 0), /*#__PURE__*/_react.default.createElement(_core.Typography, {
    variant: "h6",
    component: "span",
    color: "textSecondary"
  }, "Mbps")));
};

function InternetSpeedTest(_ref) {
  let {
    chatIssueHandler,
    activeParams,
    mode,
    openChat,
    resetChatState,
    handleEventTrack,
    endTeacherClass,
    tabArray,
    speedLimit,
    teacherInternetSpeed,
    studentInternetSpeed
  } = _ref;
  const classes = (0, _style.default)();
  const [tabValue, setTabValue] = (0, _react.useState)("");
  const [progress, setProgress] = (0, _react.useState)(80);
  const [counter, setCounter] = (0, _react.useState)(10);
  const [openEndClss, setOpenEndClss] = (0, _react.useState)(false);
  const [hideProgressBar, setHideProgressBar] = (0, _react.useState)(false);
  const [isResolved, setIsResolved] = (0, _react.useState)(null);
  const [startInternetTroubleShoot, setStartInternetTroubleShoot] = (0, _react.useState)(false);
  const [answers, setAnswers] = (0, _react.useState)([]);
  const teacherKeysLength = Object.keys(teacherInternetSpeed).length;

  const handleTabChange = (event, newValue) => {
    setStartInternetTroubleShoot(false);
    setIsResolved(null);
    setTabValue(newValue);
    setHideProgressBar(false);
    const userInternetDetails = tabArray.find(data => {
      if (data.id === newValue) {
        return data;
      }
    });
    handleEventTrack(_segmentEvents.EVENT_NAMES.TAB_CHANGE, userInternetDetails);
  };

  const onTroubleShootclick = () => {
    setStartInternetTroubleShoot(true);
    handleEventTrack(_segmentEvents.EVENT_NAMES.TROUBLESHOOT_BUTTON_CLICK, {});
  };

  const callAgent = (type, speed) => {
    let agentData = {
      type,
      speed,
      responseMsgs: ""
    };

    if (isResolved === false && openEndClss) {
      agentData = _objectSpread(_objectSpread({}, agentData), {}, {
        responseMsgs: answers.join("/")
      });
    }

    chatIssueHandler("connect-agent", agentData);
    handleEventTrack(_segmentEvents.EVENT_NAMES.AGENT_BUTTON_CLICK, {});
  };

  const goToEndClass = () => {
    setOpenEndClss(true);
    setIsResolved(false);
  };

  (0, _react.useEffect)(() => {
    if (mode === "teacher") {
      (0, _socketUtil.socket__getLatestSpeed)(activeParams.teacherId);
    }
  }, []);
  (0, _react.useEffect)(() => {
    if (teacherKeysLength !== 0) {
      setTabValue(teacherInternetSpeed.id);
      handleEventTrack(_segmentEvents.EVENT_NAMES.TEACHER_TAB, teacherInternetSpeed);
    }
  }, [teacherInternetSpeed]);
  (0, _react.useEffect)(() => {
    if (counter !== 0) {
      const countInterval = setInterval(() => {
        setCounter(counter - 1);
      }, 1000);
      return () => clearInterval(countInterval);
    }
  }, [counter]);

  const troubleShootButton = () => {
    return /*#__PURE__*/_react.default.createElement(_core.Button, {
      onClick: () => onTroubleShootclick(),
      align: "center",
      variant: "contained",
      color: "primary",
      className: "".concat(classes.btnConnect, " ").concat(classes.btnOrange)
    }, "Start Troubleshooting");
  };

  const studentNotInclass = () => {
    return /*#__PURE__*/_react.default.createElement("div", {
      className: classes.mt25
    }, /*#__PURE__*/_react.default.createElement(_core.Typography, {
      align: "center"
    }, "Seems like the student is not in the class, please call them using the link above. If you reached the student, click on the start troubleshooting button below."), /*#__PURE__*/_react.default.createElement("div", {
      className: classes.btnContainer
    }, troubleShootButton(), /*#__PURE__*/_react.default.createElement(_core.Button, {
      align: "center",
      variant: "contained",
      color: "primary",
      onClick: () => goToEndClass(),
      className: "".concat(classes.btnConnect, " ").concat(classes.btnDark)
    }, "Not able to reach student")));
  };

  const connectAgent = (type, speed) => {
    return /*#__PURE__*/_react.default.createElement(_core.Button, {
      onClick: () => callAgent(type, speed),
      align: "center",
      variant: "contained",
      color: "primary",
      className: "".concat(classes.btnConnect, " ").concat(classes.btnOrange)
    }, "Connect with an agent");
  };

  return /*#__PURE__*/_react.default.createElement("div", {
    className: classes.root
  }, /*#__PURE__*/_react.default.createElement(_core.Tabs, {
    value: tabValue,
    onChange: handleTabChange,
    "aria-label": "ant example",
    classes: {
      root: classes.rootTabs,
      indicator: classes.indicator
    }
  }, /*#__PURE__*/_react.default.createElement(_icons.KeyboardArrowLeft, {
    className: classes.backButton,
    onClick: () => resetChatState()
  }), tabArray.map(item => {
    return /*#__PURE__*/_react.default.createElement(_core.Tab, _extends({
      value: item.id,
      label: teacherInternetSpeed.id === item.id ? "Me" : item.name
    }, allyProps(item.id), {
      classes: {
        root: classes.rootTab
      }
    }));
  })), /*#__PURE__*/_react.default.createElement("div", {
    className: classes.demo1
  }, teacherKeysLength === 0 ? /*#__PURE__*/_react.default.createElement(_core.Box, {
    className: classes.internetChecking,
    p: 3
  }, /*#__PURE__*/_react.default.createElement(_core.Typography, null, "Doing on initial internet Check..."), /*#__PURE__*/_react.default.createElement("div", {
    className: classes.loaderIcon
  }, /*#__PURE__*/_react.default.createElement(_icons.Refresh, {
    className: classes.refreshIcon
  }), counter !== 0 && /*#__PURE__*/_react.default.createElement("span", {
    className: classes.timer
  }, counter))) : tabArray.map(user => {
    return /*#__PURE__*/_react.default.createElement(TabPanel, {
      value: tabValue,
      index: user.id
    }, /*#__PURE__*/_react.default.createElement(_core.Box, {
      align: "center",
      className: classes.internetSpeedInfo
    }, /*#__PURE__*/_react.default.createElement(_core.Box, {
      className: classes.internetStatus
    }, !startInternetTroubleShoot && isResolved === null && user.isJoined && /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, /*#__PURE__*/_react.default.createElement(_core.Typography, {
      align: "center"
    }, "Internet Speed is"), /*#__PURE__*/_react.default.createElement(SpeedSpinner, {
      value: user.speed.mbps,
      speed: user.speed.mbps
    }), parseInt(user.speed.mbps) >= parseInt(speedLimit) ? /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, /*#__PURE__*/_react.default.createElement(_core.Typography, {
      className: "status",
      align: "left"
    }, /*#__PURE__*/_react.default.createElement(_icons.CheckCircleRounded, {
      htmlColor: "#44c886"
    }), /*#__PURE__*/_react.default.createElement("span", null, "The internet seems to be working fine. If the issue is still persisting chat with an agent.")), connectAgent(user.type, user.speed.mbps)) : /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, /*#__PURE__*/_react.default.createElement(_core.Typography, {
      className: "status",
      align: "center"
    }, /*#__PURE__*/_react.default.createElement(_icons.Error, {
      htmlColor: "#ff8d1b"
    }), /*#__PURE__*/_react.default.createElement("span", null, "Seems like there's an internet issue.")), user.id === teacherInternetSpeed.id ? connectAgent(user.type, user.speed.mbps) : troubleShootButton())), !startInternetTroubleShoot && isResolved === null && !openEndClss && !user.isJoined && studentNotInclass(), isResolved === false && openEndClss && /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, /*#__PURE__*/_react.default.createElement(_core.Typography, {
      className: "status summary",
      align: "center"
    }, "Seems like this problem is not solvable."), /*#__PURE__*/_react.default.createElement(_core.Button, {
      align: "center",
      variant: "contained",
      color: "primary",
      onClick: endTeacherClass,
      className: "".concat(classes.btnConnect, " ").concat(classes.btnOrange)
    }, "End Class"), /*#__PURE__*/_react.default.createElement(_core.Typography, {
      className: "status summary talk-to-agent",
      align: "center"
    }, "If you think an agent can help."), connectAgent("student", studentInternetSpeed.mbps)), startInternetTroubleShoot && user.id !== teacherInternetSpeed.id && /*#__PURE__*/_react.default.createElement("div", {
      className: classes.formDiv
    }, /*#__PURE__*/_react.default.createElement(_TypeformHandler.default, {
      setIsResolved: setIsResolved,
      setOpenEndClss: setOpenEndClss,
      isChatSupport: true,
      openChat: openChat,
      resetChatState: resetChatState,
      startInternetTroubleShoot: startInternetTroubleShoot,
      setStartInternetTroubleShoot: setStartInternetTroubleShoot,
      handleEventTrack: handleEventTrack,
      setHideProgressBar: setHideProgressBar,
      setAnswers: setAnswers
    }), !hideProgressBar && /*#__PURE__*/_react.default.createElement("div", {
      className: classes.progressDiv
    }, /*#__PURE__*/_react.default.createElement(_core.CircularProgress, null))))));
  })));
}
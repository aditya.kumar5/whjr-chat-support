"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _styles = require("../../styles");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _momentTimezone = _interopRequireDefault(require("moment-timezone"));

var _templateObject;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

const RaiseRequest = (0, _styledComponents.default)("span")(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n  font-size: 14px;\n  color: #a461d8;\n  position: relative;\n  left: 18px;\n  :before {\n    content: \"\";\n    background-color: #a461d8;\n    height: 10px;\n    width: 10px;\n    position: absolute;\n    border-radius: 50%;\n    top: 6px;\n    left: -14px;\n    border: 1px solid #f6effb;\n    animation: 1s blink ease infinite;\n  }\n  @keyframes blink {\n    from,\n    to {\n      opacity: 0;\n    }\n    50% {\n      opacity: 1;\n    }\n  }\n"])));

const DecisionTreeAB = _ref => {
  let {
    issues,
    chatIssueHandler,
    disableOption,
    studentNotJoined
  } = _ref;
  return /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, /*#__PURE__*/_react.default.createElement(_styles.UnorderedList, null, issues && issues.map((issue, index) => issue.name && /*#__PURE__*/_react.default.createElement(_styles.List, {
    onClick: () => chatIssueHandler(index),
    key: index,
    className: studentNotJoined === issue.name.toLowerCase() && disableOption ? "disabledOption" : ""
  }, issue.name, " ", /*#__PURE__*/_react.default.createElement(RaiseRequest, {
    hidden: !issue.highlight
  }, issue.highlight)))));
};

DecisionTreeAB.displayName = "DecisionTreeAB";
DecisionTreeAB.defaultProps = {};
DecisionTreeAB.propTypes = {
  issues: _propTypes.default.array,
  chatIssueHandler: _propTypes.default.func
};
var _default = DecisionTreeAB;
exports.default = _default;
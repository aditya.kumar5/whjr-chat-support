"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/web.dom-collections.iterator.js");

require("core-js/modules/es.json.stringify.js");

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _DecisionTreeAB = _interopRequireDefault(require("./DecisionTreeAB"));

var _styles = require("../../styles");

var _get2 = _interopRequireDefault(require("lodash/get"));

var _momentTimezone = _interopRequireDefault(require("moment-timezone"));

var _logger = require("../../utils/helpers/logger");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

const PRECHAT_ACTION = {
  CHATBOT: "CUSTOM_CHATBOT",
  INTERNET_SPEED: "CUSTOM_INTERNET_SPEED",
  CUSTOM_ZENDESK: "CUSTOM_ZENDESK",
  ROUTING: "ROUTING"
};
let enableOptionTimer = null;
const STUDENT_NOT_JOINED = "student has not joined";
const chatLogSpanId = "PRECHAT_AB";
const prechatABlogger = (0, _logger.getCustomLogger)("prechatABlogger");

class Prechat_AB extends _react.Component {
  constructor() {
    var _this;

    super(...arguments);
    _this = this;

    _defineProperty(this, "state", {
      disableOption: false
    });

    _defineProperty(this, "handleChatbotBtnEnable", () => {
      const classStart = (0, _momentTimezone.default)(this.props.classStartTime);
      const diffInTimeFromClassStart = (0, _momentTimezone.default)().diff(classStart, "minutes");

      if ((0, _momentTimezone.default)() >= classStart && //after class start
      diffInTimeFromClassStart < 5 && !this.state.disableOption) {
        if (!enableOptionTimer) {
          this.setState({
            disableOption: true
          });
          const timeDiff = 5 - diffInTimeFromClassStart;
          enableOptionTimer = setTimeout(this.handleDisableOption, 1000 * 60 * timeDiff);
        }
      } else if ((0, _momentTimezone.default)() < classStart && !this.state.disableOption) {
        // before class start
        this.setState({
          disableOption: true
        });
        const diffInTimeBeforeClassStart = classStart.diff((0, _momentTimezone.default)(), "minutes") + 5;
        enableOptionTimer = setTimeout(this.handleDisableOption, 1000 * 60 * diffInTimeBeforeClassStart);
      }
    });

    _defineProperty(this, "handleDisableOption", () => {
      this.setState({
        disableOption: false
      });
      clearTimeout(enableOptionTimer);
      enableOptionTimer = null;
    });

    _defineProperty(this, "handlePastChats", () => {
      this.props.chatTrigger("chat-history", null);
    });

    _defineProperty(this, "handleChatState", function () {
      let action = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "CUSTOM";
      let type = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "";

      //TODO: define custom action above before adding a case
      switch (action) {
        case PRECHAT_ACTION.CHATBOT:
          _this.props.chatBotShow();

          _this.props.hideChat(false);

          break;

        case PRECHAT_ACTION.INTERNET_SPEED:
          _this.props.chatTrigger("tech-issue", null);

          break;

        case PRECHAT_ACTION.CUSTOM_ZENDESK:
          //TODO: direct chat initialize
          break;

        case PRECHAT_ACTION.ROUTING:
          _this.props.chatTrigger("prechat", null);

          break;
      }
    });

    _defineProperty(this, "handleAllIssues", function () {
      let index = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
      const issue = _this.props.issueData_AB[index];

      if (_this.state.disableOption && issue.name.toLowerCase() === STUDENT_NOT_JOINED) {
        return;
      }

      const issueSelected = _this.props.issueData_AB[index];

      _this.handleChatState(issueSelected.action);
    });
  }

  componentDidUpdate() {
    if ((0, _get2.default)(this.props, "issueData_AB", []).length) {
      this.handleChatbotBtnEnable();
    }
  }

  componentDidMount() {
    try {
      if ((0, _get2.default)(this.props, "issueData_AB", []).length) {
        this.handleChatbotBtnEnable();
        prechatABlogger && prechatABlogger.debug("".concat(chatLogSpanId, " issues loaded : ").concat(JSON.stringify((0, _get2.default)(this.props, "issueData_AB", []))));
      }
    } catch (e) {
      prechatABlogger && prechatABlogger.debug("".concat(chatLogSpanId, " error in parsing issues"));
    }
  }

  componentWillUnmount() {
    clearTimeout(enableOptionTimer);
    enableOptionTimer = null;
  }

  render() {
    return /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, /*#__PURE__*/_react.default.createElement(_styles.ChatContainer, null, /*#__PURE__*/_react.default.createElement(_styles.ChatHead, null, "Select an issue type"), /*#__PURE__*/_react.default.createElement(_DecisionTreeAB.default, {
      issues: this.props.issueData_AB,
      chatIssueHandler: this.handleAllIssues,
      disableOption: this.state.disableOption,
      studentNotJoined: STUDENT_NOT_JOINED,
      studentJoined: this.props.studentJoined
    })));
  }

}

Prechat_AB.displayName = "Prechat";
Prechat_AB.propTypes = {
  startChat: _propTypes.default.func,
  issueData: _propTypes.default.object,
  showHistoryBtn: _propTypes.default.bool,
  chatTrigger: _propTypes.default.func
};
var _default = Prechat_AB;
exports.default = _default;
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _styles = require("../../styles");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const DecisionTreeSelector = _ref => {
  let {
    issues,
    chatIssueHandler,
    prechatABdata = []
  } = _ref;
  return /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, /*#__PURE__*/_react.default.createElement(_styles.UnorderedList, null, issues && issues.map((issue, index) => issue.name && /*#__PURE__*/_react.default.createElement(_styles.List, {
    onClick: () => chatIssueHandler(index),
    key: index,
    hidden: prechatABdata.filter(x => x.name.toLowerCase() === issue.name.toLowerCase()).length > 0
  }, issue.name))));
};

DecisionTreeSelector.displayName = "DecisionTreeSelector";
DecisionTreeSelector.defaultProps = {};
DecisionTreeSelector.propTypes = {
  issues: _propTypes.default.array,
  chatIssueHandler: _propTypes.default.func
};
var _default = DecisionTreeSelector;
exports.default = _default;
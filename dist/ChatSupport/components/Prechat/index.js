"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/web.dom-collections.iterator.js");

require("core-js/modules/es.json.stringify.js");

require("core-js/modules/es.string.includes.js");

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _styles = require("../../styles");

var _DecisionTreeSelector = _interopRequireDefault(require("./DecisionTreeSelector"));

var _past = _interopRequireDefault(require("../../assets/images/past.png"));

var _get2 = _interopRequireDefault(require("lodash/get"));

var _ChatIntermediateStep = _interopRequireDefault(require("../ChatIntermediateStep"));

var _segmentEvents = require("../../utils/constants/segmentEvents");

var _constant = require("../../utils/constants/constant");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

const chatLogSpanId = "ZENDESK CHAT";

class Prechat extends _react.Component {
  constructor() {
    var _this;

    super(...arguments);
    _this = this;

    _defineProperty(this, "state", {
      selected: false,
      selectedIndex: -1,
      issues: this.props.issueData ? this.props.issueData.issue_category : [],
      selectedIssue: "",
      selectedSubIssue: null,
      modal: false
    });

    _defineProperty(this, "handleArrayComarision", (arr1, arr2) => {
      let flag = false;
      if (arr1 && arr2 && arr1.length != arr2.length) return true;

      if (arr1 && arr2 && arr1.length == arr2.length) {
        for (let i = 0; i < arr1.length; i++) {
          if (arr1[i].issue_sub_category && arr2[i].issue_sub_category) {
            if (arr1[i].issue_sub_category.length != arr2[i].issue_sub_category.length) {
              flag = true;
              return flag;
            } else {
              for (let j = 0; j < arr1[i].issue_sub_category.length; j++) {
                if (arr1[i].issue_sub_category[j]._id && arr2[i].issue_sub_category[j]._id && arr1[i].issue_sub_category[j]._id != arr2[i].issue_sub_category[j]._id) flag = true;
              }
            }
          }
        }
      }

      if (arr1 && arr2 && arr1.length && arr2.length) {
        for (let i = 0; i < arr1.length; i++) {
          if (arr1[i]._id != arr2[i]._id) return true;
        }
      }

      if (arr1 && !arr2) {
        return true;
      }

      return flag;
    });

    _defineProperty(this, "handleSelectedSubIssue", index => {
      try {
        const department = this.state.issues[index].support_dept;
        this.props.chatLogger && this.props.chatLogger.debug("".concat(chatLogSpanId, " : sub-issue select"), {
          chatData: {
            connectionStatus: window.zChat ? window.zChat.getConnectionStatus() : "not initialized",
            issueCount: this.state.issues ? this.state.issues.length : 0,
            issueList: JSON.stringify(this.state.issues),
            userSelection: {
              issueName: this.state.issues[index].name,
              deptName: (0, _get2.default)(department, "name", null),
              deptId: (0, _get2.default)(department, "dept_id", null)
            }
          }
        });
        this.handleStartChat(department, index);
      } catch (e) {
        console.error(e);
        this.props.chatLogger && this.props.chatLogger.error("".concat(chatLogSpanId, " : Error in handleSelectedSubIssue()"), JSON.stringify(e));
      }
    });

    _defineProperty(this, "handleStartChat", function (department) {
      let index = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
      let status = arguments.length > 2 ? arguments[2] : undefined;

      _this.props.chatTrigger("start-chat", {
        deptId: department.dept_id,
        deptName: department.name,
        issueType: _this.state.selectedIssue,
        issueDetail: status ? _this.state.selectedSubIssue.name : index != null && index >= 0 ? _this.state.issues[index].name : ""
      });
    });

    _defineProperty(this, "handlingStateReset", () => {
      this.setState({
        selected: false,
        selectedIndex: -1,
        issues: this.props.issueData && this.props.issueData.issue_category ? this.props.issueData.issue_category : []
      });
      if (!this.state.selected && this.state.selectedIndex === -1) this.props.chatLogger && this.props.chatLogger.debug("".concat(chatLogSpanId, " : handling back from sub-issue to issue list"));
    });

    _defineProperty(this, "handlingStateReset", () => {
      this.setState({
        selected: false,
        selectedIndex: -1,
        issues: this.props.issueData && this.props.issueData.issue_category ? this.props.issueData.issue_category : []
      });
      if (!this.state.selected && this.state.selectedIndex === -1) this.props.chatLogger && this.props.chatLogger.debug("".concat(chatLogSpanId, " : handling back from sub-issue to issue list"));
    });

    _defineProperty(this, "handleStateReset", function () {
      let layer = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;

      try {
        if ((0, _get2.default)(_this.props, "prechatABlayer", []).length && !_this.state.selected && layer) {
          _this.props.chatTrigger("prechat_ab", null);
        } else {
          _this.handlingStateReset();
        }
      } catch (e) {
        console.error(e);
        _this.props.chatLogger && _this.props.chatLogger.error("".concat(chatLogSpanId, " : Error in handleStateReset()"), JSON.stringify(e));
      }
    });

    _defineProperty(this, "handleReschedule", (data, check) => {
      this.setState({
        selectedSubIssue: data,
        modal: !this.state.modal
      });
      this.props.openChat(check);
    });

    _defineProperty(this, "handleAllIssues", index => {
      try {
        if (this.state.selected) {
          //subIssues
          let data = this.state.issues[index];

          if ( // to check if teacher wasnts to reschedule (Reschedule POPUP)
          this.props.inClassReschedule && _constant.checkReschedule.includes(data.name.toLowerCase())) {
            data.index = index;
            this.handleReschedule(data, false);
          } else this.handleSelectedSubIssue(index);
        } else {
          const data = this.state.issues[index].issue_sub_category;

          if (data.length == 1 && !data[0].name) {
            //in case if there are no subIssues just issue + that issue should be unique
            const departmentData = data[0].support_dept;
            this.setState({
              selectedIssue: this.state.issues[index].name
            }, () => {
              this.handleStartChat(departmentData);
            });
          } else if (data.length == 1 && data[0].name) {
            //if we have 1 issue sub category then user should be directed to chat w/o selecting subIssue
            const departmentData = data[0].support_dept;
            const selectedIssue = this.state.issues[index].name;
            this.setState({
              selectedIssue,
              issues: data
            }, () => {
              this.handleStartChat(departmentData, 0);
            });
          } else {
            const selectedIssue = this.state.issues[index].name;
            this.setState({
              selected: true,
              issues: data,
              selectedIndex: index,
              selectedIssue
            });
          }

          this.props.chatLogger && this.props.chatLogger.debug("".concat(chatLogSpanId, " : issue select"), {
            chatData: {
              connectionStatus: window.zChat ? window.zChat.getConnectionStatus() : "not initialized",
              issueCount: this.state.issues ? this.state.issues.length : 0,
              issueList: JSON.stringify(this.state.issues),
              userSelection: {
                issueName: this.state.issues[index].name
              }
            }
          });

          if (this.props.handleEventTrack) {
            const args = {
              button_type: _segmentEvents.BUTTON_TYPE.ARROW,
              concierge_issue_type: this.state.issues[index].name
            };
            this.props.handleEventTrack(_segmentEvents.EVENT_NAMES.CONCIERGE_ISSUE_CLICKED, args);
          }
        }
      } catch (e) {
        this.props.chatLogger && this.props.chatLogger.error("".concat(chatLogSpanId, " : Error in handleAllIssues()"), JSON.stringify(e));
      }
    });

    _defineProperty(this, "handlePastChats", () => {
      this.props.chatTrigger("chat-history", null);
    });
  }

  componentDidUpdate(prevProps, currState) {
    if (this.props.issueData && this.handleArrayComarision(this.props.issueData.issue_category, currState.issues)) {
      this.handleStateReset();
    }
  }

  render() {
    return /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, /*#__PURE__*/_react.default.createElement(_styles.ChatContainer, null, (0, _get2.default)(this.props, "prechatABlayer", []).length ? /*#__PURE__*/_react.default.createElement(_styles.ChatBody, null, /*#__PURE__*/_react.default.createElement(_styles.BackButton, {
      onClick: () => this.handleStateReset("prechatLayer")
    }), !this.state.selected ? /*#__PURE__*/_react.default.createElement(_styles.IssueTitle, null, "Select an issue type") : /*#__PURE__*/_react.default.createElement(_styles.IssueTitle, null, "What issue are you facing?")) : !this.state.selected ? /*#__PURE__*/_react.default.createElement(_styles.ChatHead, null, "Select an issue type") : /*#__PURE__*/_react.default.createElement(_styles.ChatBody, null, /*#__PURE__*/_react.default.createElement(_styles.BackButton, {
      onClick: () => this.handleStateReset()
    }), /*#__PURE__*/_react.default.createElement(_styles.IssueTitle, null, "What issue are you facing?")), /*#__PURE__*/_react.default.createElement(_DecisionTreeSelector.default, {
      issues: this.state.issues,
      chatIssueHandler: this.handleAllIssues,
      prechatABdata: (0, _get2.default)(this.props, "prechatABlayer", [])
    }), this.props.showHistoryBtn && /*#__PURE__*/_react.default.createElement(_styles.PastChatBtn, {
      onClick: () => this.handlePastChats()
    }, /*#__PURE__*/_react.default.createElement(_styles.PastChatIcon, {
      src: _past.default
    }), "View past chats"), this.props.inClassReschedule && /*#__PURE__*/_react.default.createElement(_ChatIntermediateStep.default, {
      handleReschedulePopup: status => {
        this.props.handleReschedulePopup(status);
      },
      data: this.state.selectedSubIssue,
      modal: this.state.modal,
      handleReschedule: this.handleReschedule,
      handleStartChat: this.handleStartChat
    })));
  }

}

Prechat.displayName = "Prechat";
Prechat.propTypes = {
  startChat: _propTypes.default.func,
  issueData: _propTypes.default.object,
  showHistoryBtn: _propTypes.default.bool,
  chatTrigger: _propTypes.default.func,
  chatLogger: _propTypes.default.object,
  inClassReschedule: _propTypes.default.bool
};
var _default = Prechat;
exports.default = _default;
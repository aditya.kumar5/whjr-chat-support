"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.UserIcon = exports.UnorderedList = exports.TextBox = exports.TakeBackBtn = exports.PastChatIcon = exports.PastChatBtn = exports.LoadingBubble = exports.List = exports.IssueTitle = exports.InputFile = exports.Font14 = exports.Font12 = exports.ChatHead = exports.ChatContainer = exports.ChatBody = exports.Button = exports.BackButton = void 0;

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _templateObject, _templateObject2, _templateObject3, _templateObject4, _templateObject5, _templateObject6, _templateObject7, _templateObject8, _templateObject9, _templateObject10, _templateObject11, _templateObject12, _templateObject13, _templateObject14, _templateObject15, _templateObject16, _templateObject17;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

const Button = _styledComponents.default.button(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n  padding: 10px;\n  font-weight: 700;\n  border: none;\n  border-radius: 4px;\n  color: ", ";\n  background-color: ", ";\n  &:focus {\n    outline: none;\n  }\n"])), props => props.theme.colors.secondary, props => props.theme.colors.primary);

exports.Button = Button;

const TextBox = _styledComponents.default.textarea(_templateObject2 || (_templateObject2 = _taggedTemplateLiteral(["\n  font-size: 0.75em;\n  width: 100%;\n  border-radius: 4px;\n  border: 1px solid #dedede;\n  font-weight: 700;\n  color: #5e5e5e;\n  resize: none;\n  outline: none;\n  @media (max-width: 768px) {\n    font-size: 16px;\n    &:focus: {\n      font-size: 16px;\n    }\n  }\n"])));

exports.TextBox = TextBox;

const UserIcon = _styledComponents.default.span(_templateObject3 || (_templateObject3 = _taggedTemplateLiteral(["\n  display: inline-block;\n  padding: 6px 12px;\n  border-radius: 100%;\n  font-size: 0.875em;\n  font-weight: 700;\n  color: ", ";\n  background-color: ", ";\n"])), props => props.theme.colors.secondary, props => props.theme.colors.primary);

exports.UserIcon = UserIcon;

const InputFile = _styledComponents.default.input.attrs(props => ({
  type: "file"
}))(_templateObject4 || (_templateObject4 = _taggedTemplateLiteral(["\n  border: none;\n  outline: none;\n"])));

exports.InputFile = InputFile;

const LoadingBubble = _styledComponents.default.div(_templateObject5 || (_templateObject5 = _taggedTemplateLiteral(["\n  padding: 6px 10px;\n  position: absolute;\n  left: 50%;\n  transform: translate(-50%);\n  font-size: 0.75em;\n  font-weight: 600;\n  border-radius: 14px;\n  box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.11);\n  border: solid 1px #dedede;\n  background-color: #ffffff;\n"])));

exports.LoadingBubble = LoadingBubble;

const Font12 = _styledComponents.default.div(_templateObject6 || (_templateObject6 = _taggedTemplateLiteral(["\n  font-size: 0.75em;\n"])));

exports.Font12 = Font12;

const Font14 = _styledComponents.default.div(_templateObject7 || (_templateObject7 = _taggedTemplateLiteral(["\n  font-size: 0.875em;\n"])));

exports.Font14 = Font14;

const TakeBackBtn = _styledComponents.default.span(_templateObject8 || (_templateObject8 = _taggedTemplateLiteral(["\n  padding: 4.5px;\n  background-color: transparent;\n  border-top: 1px solid;\n  border-left: 1px solid;\n  position: absolute;\n  -webkit-transform: rotate(-45deg);\n  -ms-transform: rotate(-45deg);\n  transform: rotate(-45deg);\n  cursor: pointer;\n"])));

exports.TakeBackBtn = TakeBackBtn;

const UnorderedList = _styledComponents.default.ul(_templateObject9 || (_templateObject9 = _taggedTemplateLiteral(["\n  list-style-type: none;\n  padding: 0 0 4rem;\n  margin: 0;\n"])));

exports.UnorderedList = UnorderedList;

const List = _styledComponents.default.li(_templateObject10 || (_templateObject10 = _taggedTemplateLiteral(["\n  font-size: 14px;\n  border-bottom: 1px solid #dedede;\n  cursor: pointer;\n  position: relative;\n  padding: 16px;\n  color: #266ad1;\n  &:after {\n    content: \"\";\n    position: absolute;\n    padding: 3px;\n    right: 20px;\n    top: 45%;\n    border-top: 1px solid;\n    border-right: 1px solid;\n    color: initial;\n    transform: rotate(45deg);\n  }\n  &.disabledOption {\n    background-color: #e9e9e9;\n    filter: grayscale(1);\n    cursor: wait;\n  }\n"])));

exports.List = List;

const ChatContainer = _styledComponents.default.div(_templateObject11 || (_templateObject11 = _taggedTemplateLiteral(["\n  overflow: auto;\n  height: 100%;\n  position: relative;\n"])));

exports.ChatContainer = ChatContainer;

const ChatHead = _styledComponents.default.div(_templateObject12 || (_templateObject12 = _taggedTemplateLiteral(["\n  font-weight: 600;\n  font-size: 0.875em;\n  border-bottom: 1px solid #dedede;\n  padding: 16px;\n"])));

exports.ChatHead = ChatHead;
const ChatBody = (0, _styledComponents.default)(ChatHead)(_templateObject13 || (_templateObject13 = _taggedTemplateLiteral(["\n  position: relative;\n"])));
exports.ChatBody = ChatBody;

const BackButton = _styledComponents.default.span(_templateObject14 || (_templateObject14 = _taggedTemplateLiteral(["\n  padding: 4px;\n  left: 20px;\n  top: 41%;\n  border-top: 1px solid;\n  border-left: 1px solid;\n  transform: rotate(-45deg);\n  position: absolute;\n  border-color: #266ad1;\n  cursor: pointer;\n"])));

exports.BackButton = BackButton;

const IssueTitle = _styledComponents.default.div(_templateObject15 || (_templateObject15 = _taggedTemplateLiteral(["\n  padding-left: 24px;\n"])));

exports.IssueTitle = IssueTitle;

const PastChatBtn = _styledComponents.default.button(_templateObject16 || (_templateObject16 = _taggedTemplateLiteral(["\n  position: fixed;\n  bottom: 0;\n  width: 100%;\n  background-color: #f9fafc;\n  font-size: 0.875em;\n  height: 44px;\n  border: none;\n  border-top: 1px solid #dedede;\n  box-shadow: 0 -6px 5px 0 rgba(0, 0, 0, 0.05);\n"])));

exports.PastChatBtn = PastChatBtn;

const PastChatIcon = _styledComponents.default.img(_templateObject17 || (_templateObject17 = _taggedTemplateLiteral(["\n  width: 16px;\n  margin-right: 12px;\n  margin-bottom: 3px;\n"])));

exports.PastChatIcon = PastChatIcon;
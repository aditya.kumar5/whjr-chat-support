import styled from "styled-components";

export const Button = styled.button`
  padding: 10px;
  font-weight: 700;
  border: none;
  border-radius: 4px;
  color: ${(props) => props.theme.colors.secondary};
  background-color: ${(props) => props.theme.colors.primary};
  &:focus {
    outline: none;
  }
`;

export const TextBox = styled.textarea`
  font-size: 0.75em;
  width: 100%;
  border-radius: 4px;
  border: 1px solid #dedede;
  font-weight: 700;
  color: #5e5e5e;
  resize: none;
  outline: none;
  @media (max-width: 768px) {
    font-size: 16px;
    &:focus: {
      font-size: 16px;
    }
  }
`;

export const UserIcon = styled.span`
  display: inline-block;
  padding: 6px 12px;
  border-radius: 100%;
  font-size: 0.875em;
  font-weight: 700;
  color: ${(props) => props.theme.colors.secondary};
  background-color: ${(props) => props.theme.colors.primary};
`;

export const InputFile = styled.input.attrs((props) => ({
  type: "file",
}))`
  border: none;
  outline: none;
`;
export const LoadingBubble = styled.div`
  padding: 6px 10px;
  position: absolute;
  left: 50%;
  transform: translate(-50%);
  font-size: 0.75em;
  font-weight: 600;
  border-radius: 14px;
  box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.11);
  border: solid 1px #dedede;
  background-color: #ffffff;
`;

export const Font12 = styled.div`
  font-size: 0.75em;
`;
export const Font14 = styled.div`
  font-size: 0.875em;
`;
export const TakeBackBtn = styled.span`
  padding: 4.5px;
  background-color: transparent;
  border-top: 1px solid;
  border-left: 1px solid;
  position: absolute;
  -webkit-transform: rotate(-45deg);
  -ms-transform: rotate(-45deg);
  transform: rotate(-45deg);
  cursor: pointer;
`;

export const UnorderedList = styled.ul`
  list-style-type: none;
  padding: 0 0 4rem;
  margin: 0;
`;
export const List = styled.li`
  font-size: 14px;
  border-bottom: 1px solid #dedede;
  cursor: pointer;
  position: relative;
  padding: 16px;
  color: #266ad1;
  &:after {
    content: "";
    position: absolute;
    padding: 3px;
    right: 20px;
    top: 45%;
    border-top: 1px solid;
    border-right: 1px solid;
    color: initial;
    transform: rotate(45deg);
  }
  &.disabledOption {
    background-color: #e9e9e9;
    filter: grayscale(1);
    cursor: wait;
  }
`;
export const ChatContainer = styled.div`
  overflow: auto;
  height: 100%;
  position: relative;
`;
export const ChatHead = styled.div`
  font-weight: 600;
  font-size: 0.875em;
  border-bottom: 1px solid #dedede;
  padding: 16px;
`;
export const ChatBody = styled(ChatHead)`
  position: relative;
`;
export const BackButton = styled.span`
  padding: 4px;
  left: 20px;
  top: 41%;
  border-top: 1px solid;
  border-left: 1px solid;
  transform: rotate(-45deg);
  position: absolute;
  border-color: #266ad1;
  cursor: pointer;
`;
export const IssueTitle = styled.div`
  padding-left: 24px;
`;
export const PastChatBtn = styled.button`
  position: fixed;
  bottom: 0;
  width: 100%;
  background-color: #f9fafc;
  font-size: 0.875em;
  height: 44px;
  border: none;
  border-top: 1px solid #dedede;
  box-shadow: 0 -6px 5px 0 rgba(0, 0, 0, 0.05);
`;
export const PastChatIcon = styled.img`
  width: 16px;
  margin-right: 12px;
  margin-bottom: 3px;
`;

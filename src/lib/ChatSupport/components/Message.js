import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import moment from "moment-timezone";

import Attachment from "./Attachment";
import alert from "../assets/images/alert.png";

const MessageContainer = styled.div`
  overflow: hidden;
  flex-grow: 1;
`;

const MessageTextContainer = styled.div`
  display: flex;
  flex-direction: column;
`;

const MessageText = styled.div`
  max-width: 80%;
  margin-top 2px;
  margin-bottom: 6px;
  padding: 4px 10px;
  border-radius: 8px;
  font-size: 0.75em;
  font-weight: 600;
  word-break: break-word;
  white-space: pre-line;
`;

const AgentMessage = styled(MessageText)`
  margin-right: auto;
  background-color: #fff;
  border: solid 1px #e4e4e4;
  border-top-left-radius: 0px;
`;

const UserMessage = styled(MessageText)`
  margin-left: auto;
  background-color: #e8eaec;
  border-top-right-radius: 0px;
`;

const InfoMessage = styled.div`
  margin: 10px 0px;
  font-size: 0.75em;
  font-weight: 700;
  color: ${props => props.theme.colors.textColor.secondary};
  text-align: center;
`;

const StatusDate = styled(InfoMessage)`
  margin-bottom: 20px;
  &::before,
  &::after {
    content: "";
    border-top: 1px solid #dedede;
    position: relative;
    width: 50%;
    display: inline-block;
    vertical-align: middle;
  }
  &::before {
    margin-left: -50%;
    right: 1rem;
  }
  &::after {
    margin-right: -50%;
    left: 1rem;
  }
`;

const ErrorRetry = styled.div`
  max-width: 80%;
  margin-left: auto;
  margin-bottom: 6px;
  font-size: 0.625em;
  font-weight: 600;
  color: ${props => props.theme.colors.textColor.error};
  cursor: pointer;
  .icon-warning {
    margin-right: 3px;
  }
`;
const ErrorImage = styled.img`
  width: 14px;
  height: 13px;
  margin-bottom: 1px;
  margin-right: 4px;
`;
const AttachmentContainerUser = styled.div`
  margin-left: auto;
  width: 65%;
  margin-bottom: 6px;
  text-align: end;
`;
const AttachmentContainerAgent = styled.div`
  margin-right: auto;
  width: 75%;
  margin-bottom: 6px;
`;
const Message = ({ message, resendMessage, barWidth }) => (
  <MessageContainer>
    {{
      info: <InfoMessage>{message.msg}</InfoMessage>,
      date: (
        <StatusDate>
          {moment(message.msg)
            .local()
            .format("ddd, hh:mm A")}
        </StatusDate>
      )
    }[message.messageType] || (
      <MessageTextContainer>
        {message.nick &&
          message.nick === "visitor" &&
          message.msg &&
          !message.attachment && (
            <UserMessage>
              {message.hasLink ? (
                <span dangerouslySetInnerHTML={{ __html: message.msg }}></span>
              ) : (
                <span>{message.msg}</span>
              )}
            </UserMessage>
          )}
        {((message.nick &&
          message.nick.includes("agent") &&
          message.type === "chat.msg") ||
          message.messageType === "agent-typing") && (
          <AgentMessage>
            {message.hasLink ? (
              <span dangerouslySetInnerHTML={{ __html: message.msg }}></span>
            ) : (
              <span>{message.msg}</span>
            )}
          </AgentMessage>
        )}
        {(message.type === "chat.file" || "attachment" in message) &&
          message.nick &&
          message.nick === "visitor" && (
            <AttachmentContainerUser>
              <Attachment
                url={message.attachment.url}
                name={message.attachment.name}
                mime_type={message.attachment.mime_type}
                size={message.attachment.size}
                type={`visitor`}
                msgType={message.type}
                progressBarWidth={barWidth}
                err={message.errMsg}
              />
            </AttachmentContainerUser>
          )}
        {message.nick &&
          message.nick.includes("agent") &&
          message.type === "chat.file" && (
            <AttachmentContainerAgent>
              <Attachment
                url={message.attachment.url}
                name={message.attachment.name}
                mime_type={message.attachment.mime_type}
                size={message.attachment.size}
                type={`agent`}
                msgType={message.type}
              />
            </AttachmentContainerAgent>
          )}
        {message.sendErr && (
          <ErrorRetry onClick={() => resendMessage()}>
            <ErrorImage src={alert} alt="warning" />
            {message.errMsg
              ? message.errMsg
              : `Couldn’t send. Tap to try again.`}
          </ErrorRetry>
        )}
      </MessageTextContainer>
    )}
  </MessageContainer>
);

Message.displayName = "Message";

Message.propTypes = {
  message: PropTypes.object.isRequired,
  resendMessage: PropTypes.func,
  barWidth: PropTypes.string
};

export default Message;

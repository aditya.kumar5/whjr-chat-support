import React from "react";
import logoImage from "../../assets/images/helpDeskDownloadLogo.png";
import atImage from "../../assets/images/at@2x.png";
import callImg from "../../assets/images/telephone-1@2x.png";
import cc from "../../assets/images/customer-care@2x.png";
import clock from "../../assets/images/fast@2x.png";
import textMessage from "../../assets/images/question-2@2x.png";
import {
  ISSUES_TAB,
  IMMEDIATE_TAB,
  CONNECT_TAB
} from "../../utils/constants/cdnImagePath";
import { DeviceTypeContext, DEVICE_TYPES } from "../../utils/constants/device-type";
import "./styles.scss";

const HelpDesk = ({ supportNumber, modalHandler, classes }) => {
  let deviceType = React.useContext(DeviceTypeContext);
  let isTabView = deviceType === DEVICE_TYPES.TAB.isTab ? true : false;
  return (
    <>
      {isTabView ? (
        <div className={`concierge_wrap_tab text-align-center mb-3`}>
          <div className={`row mb-4 mt-4 width_control_tab`}>
            <div className="col-8">
              <div className="heading_bold font24 mb-3 text-left">
                Your Personal Helper
              </div>
              <div className="row">
                <div
                  className={`content_wrapper_tab col-xs-12 text-center ${isTabView &&
                    "content_wrapper_tablet"} `}
                >
                  <img
                    className="image_position"
                    height={30}
                    src={CONNECT_TAB}
                  />
                  <div
                    className="content_title heading_bold font16"
                    style={{
                      color: "#535353"
                    }}
                  >
                    Connect with
                    <br />
                    dedicated experts
                  </div>
                </div>
                <div
                  className={` content_wrapper_tab col-xs-12 text-center ${isTabView &&
                    "content_wrapper_tablet"}`}
                >
                  <img
                    className="image_position"
                    height={30}
                    src={IMMEDIATE_TAB}
                  />
                  <div
                    className="content_title heading_bold font16"
                    style={{
                      color: "#535353"
                    }}
                  >
                    Get Immediate
                    <br />
                    assistance
                  </div>
                </div>
                <div
                  className={`content_wrapper_tab col-xs-12 text-center ${isTabView &&
                    "content_wrapper_tablet"}`}
                >
                  <img
                    className="image_position"
                    height={30}
                    src={ISSUES_TAB}
                  />
                  <div
                    className="content_title heading_bold font16"
                    style={{
                      color: "#535353"
                    }}
                  >
                    Raise all
                    <br />
                    your issues
                  </div>
                </div>
              </div>
            </div>
            <div className="col-4">
              <div className="contact_wrap_tab">
                <div className={` ${classes.left_center} col-xs-12`}>
                  <div
                    className=" text_white"
                    style={{
                      backgroundImage:
                        "linear-gradient(to bottom right, #ffa227, #ee6d2d)",
                      borderRadius: "2.25rem",
                      padding: `${isTabView ? "15px 14px" : "18px 14px"} `,
                      width: "260px"
                    }}
                  >
                    <div className="heading_bold font16">
                    Call us at{" "}
                      <span>
                        <a className="text_white" href={`tel:${supportNumber}`}>
                          {supportNumber}
                        </a>
                      </span>
                    </div>
                  </div>
                  <div className="heading_light font12 mt-2">
                  Wait time: less than 30 seconds
                  </div>
                </div>
                <div
                  className={` ${classes.left_center} col-xs-12 mt-4`}
                  onClick={modalHandler}
                >
                  <div className="pl-1">
                    <div
                      className="heading_bold font16 text_white"
                      style={{
                        backgroundImage:
                          "linear-gradient(to left,#087fed,#01b7ee)",
                        borderRadius: "2.25rem",
                        padding: `${isTabView ? "15px 14px" : "18px 14px"} `,
                        width: "260px"
                      }}
                    >
                      Write to us
                    </div>
                    <div className="heading_light font12 mt-2">
                    Wait time: less than 24 hours
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      ) : (
        <>
          <div className={`concierge_wrap text-align-center mb-3`}>
            <div className="heading_bold font26 mb-3">
            Your Personal Concierge
            </div>
            <div
              className={`${classes.flex_wrap_align} row mb-4 mt-4 width_control`}
            >
              <div
                className={`${classes.flex_wrap_align} content_wrapper col-xs-12`}
              >
                <img className="image_position" height={30} src={cc} />
                <div className="content_title heading_bold font14">
                Connect with
                  <br />
                  dedicated experts
                </div>
              </div>
              <div
                className={`${classes.flex_wrap_align} content_wrapper col-xs-12`}
              >
                <img className="image_position" height={30} src={clock} />
                <div className="content_title heading_bold font14">
                Get Immediate
                  <br />
                  assistance
                </div>
              </div>
              <div
                className={`${classes.flex_wrap_align} content_wrapper col-xs-12`}
              >
                <img className="image_position" height={30} src={textMessage} />
                <div className="content_title heading_bold font14">
                Raise all
                  <br />
                  your issues
                </div>
              </div>
            </div>
            <div className="contact_wrap row">
              <div
                className={`content_wrapper_white ${classes.flex} ${classes.left_center} col-xs-12`}
              >
                <img className="ml-1 mr-2" width={24} src={callImg} />
                <div className="pl-1">
                  <div className="heading_bold call_wrap font16">
                  Call us at:{" "}
                    <span>
                      <a className="text_black" href={`tel:${supportNumber}`}>
                        {supportNumber}
                      </a>
                    </span>
                  </div>
                  <div className="heading_light font12">
                  Wait time: less than 30 seconds
                  </div>
                </div>
              </div>
              <div
                className={`content_wrapper_white ${classes.flex} ${classes.left_center} col-xs-12`}
                onClick={modalHandler}
              >
                <img className="ml-1 mr-2" width={24} src={atImage} />
                <div className="pl-1">
                  <div className="heading_bold font16">
                  Write to us
                  </div>
                  <div className="heading_light font12">
                  Wait time: less than 24 hours
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className={`${classes.border_wrap} row p-4`}>
            <div className="col-xs-12 font20 mr-3">
            To help our team troubleshoot technical issues
            </div>
            <div
              className={`${classes.button_border} ${classes.center_flex} ${classes.flex} heading_reg btn-margin_mobile p-1 col-xs-12`}
            >
              <div className="font14">
                <a
                  className={`${classes.flex} heading_bold text_blue ${classes.center_flex}`}
                  href={`https://anydesk.com/en/downloads`}
                  target={"_blank"}
                >
                  <img
                    className="ml-1"
                    className="mr-1"
                    width={22}
                    src={logoImage}
                  />
                  Download Anydesk
                </a>
              </div>
            </div>
          </div>
        </>
      )}
    </>
  );
};
export default HelpDesk;

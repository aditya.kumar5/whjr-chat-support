import React, { Component, Fragment } from "react";
import { FormattedMessage } from "react-intl";
// import { connect } from "react-redux";
// import {
//   submitFeedbackEscalationConcierge,
//   submitMediaFeedbackEscalation
// } from "../../actions";
// import { getUploadedImageToken } from "../../reducers";
import ConciergeEmailModal from "./ConciergeEmailModal";
import HelpDesk from "./helpDesk";
import { setPaidStatus, getCodingOrMathSkill } from "../../utils/helpers/helper";
import _get from "lodash/get";

class ConciergeHelpDesk extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: 0,
      showEmailModal: false,
      values: {
        courseType: "",
        subject: "",
        description: "",
        fileData: []
      },
      token: ""
    };
  }
  handleHelpDeskClose = () => {
    this.setState({ showHelpDesk: false });
  };
  showEmailModalHandler = () => {
    this.setState({ showEmailModal: true });
  };

  hideEmailModalHandler = (backToHelpBox = false) => {
    this.setState({ showEmailModal: false });

    if (this.props.isClickedFromHelpBox) {
      this.props.closeHelpHandler("email", backToHelpBox ? true : false);
    }
  };

  resetState = () => {
    const values = {
      subject: "",
      description: "",
      courseType: "",
      fileData: []
    };
    this.setState({ values });
  };

  componentDidMount() {
    //only call when option selection from
    if (this.props.isClickedFromHelpBox) {
      this.showEmailModalHandler();
    }
  }

  componentDidUpdate() {
    this.createTicketWithMedia();
  }
  createTicketWithMedia = () => {
    if (
      this.props.mediaToken &&
      this.props.mediaToken.fileTokens &&
      this.state.values.description &&
      this.state.values.subject
    ) {
      if (
        (this.state.token &&
          this.props.mediaToken.fileTokens.length &&
          this.state.token !== this.props.mediaToken.fileTokens[0]) ||
        this.state.token == ""
      ) {
        this.setState({ token: this.props.mediaToken.fileTokens[0] });
        const { id, type } = this.props;
        const submitData = {
          submittedById: id,
          submittedByType: type,
          message: this.state.values.description,
          subject: this.state.values.subject,
          courseType:
            type === "STUDENT"
              ? this.getStudentCourse()
              : this.state.values.courseType,
          fileTokens: this.props.mediaToken.fileTokens,
          paidStatus:
            type === "STUDENT"
              ? setPaidStatus(type, this.props.student)
              : setPaidStatus(type, this.props.teacher)
        };
        this.props.d__submitFeedbackEscalationConcierge(submitData);
        this.resetState();
      }
    }
  };

  getStudentCourse = () => {
    const { student } = this.props;
    const { activeCourse } = student;
    return _get(activeCourse, "courseType", null);
  };

  createTicketWithoutMedia = values => {
    const { id, type } = this.props;
    const submitData = {
      submittedById: id,
      submittedByType: type,
      message: values.description,
      subject: values.subject,
      courseType:
        type === "STUDENT" ? this.getStudentCourse() : values.courseType,
      fileTokens: [],
      paidStatus:
        type === "STUDENT"
          ? setPaidStatus(type, this.props.student)
          : setPaidStatus(type, this.props.teacher)
    };
    this.props.d__submitFeedbackEscalationConcierge(submitData);
    this.resetState();
  };

  submitData = values => {
    this.setState({ values });
    values.fileData.length
      ? this.props.d__submitMediaFeedbackEscalation(values.fileData)
      : this.createTicketWithoutMedia(values);
    this.hideEmailModalHandler();
  };

  render() {
    const { classes, supportNumber } = this.props;
    return (
      <Fragment>
        {!this.props.isClickedFromHelpBox && (
          <HelpDesk
            supportNumber={supportNumber}
            modalHandler={this.showEmailModalHandler}
            classes={classes}
          />
        )}
        <ConciergeEmailModal
          showDialog={this.state.showEmailModal}
          showCourseTypeDropDown={this.props.type === "STUDENT" ? true : false}
          values={this.state.values}
          closeModal={this.hideEmailModalHandler}
          modalTitle={"Write to us"
          }
          onSubmit={this.submitData}
          teacherSkill={
            this.props.type === "STUDENT"
              ? null
              : getCodingOrMathSkill(this.props.teacher.teacher_skills)
          }
          isClickedFromHelpBox={this.props.isClickedFromHelpBox ? true : false}
        />
      </Fragment>
    );
  }
}

// const mapStateToProps = state => {
//   return {
//     mediaToken: getUploadedImageToken(state)
//   };
// };

// const mapDispatchToProps = dispatch => {
//   return {
//     d__submitFeedbackEscalationConcierge: payload => {
//       dispatch(submitFeedbackEscalationConcierge.request({ payload }));
//     },
//     d__submitMediaFeedbackEscalation: payload => {
//       dispatch(submitMediaFeedbackEscalation.request(payload));
//     }
//   };
// };

// export default connect(mapStateToProps, mapDispatchToProps)(ConciergeHelpDesk);
export default ConciergeHelpDesk
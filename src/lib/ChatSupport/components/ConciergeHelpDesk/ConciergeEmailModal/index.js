import React, { useState } from "react";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import { Formik } from "formik";
import TextField from "@material-ui/core/TextField";
import DialogTitle from "@material-ui/core/DialogTitle";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import FlashSnackbar from "./FlashSnackbar";
import MenuItem from "@material-ui/core/MenuItem";
import { DeviceTypeContext, DEVICE_TYPES } from "../../../utils/constants/device-type";
import "./styles.scss";
import * as yup from "yup";
import styled from "styled-components";
import backIcon from "../../../assets/images/backIcon.svg";
import uploadIcon from "../../../assets/images/icon-upload.svg";

const crossBtn = require("../../../assets/images/cancel@2x.png");

const ConciergeEmailModal = ({
  showDialog,
  values,
  closeModal,
  modalTitle,
  onSubmit,
  showCourseTypeDropDown,
  teacherSkill,
  isClickedFromHelpBox = false
}) => {
  const [selectedFile, setSelectedFile] = useState([]);
  const [fileName, setFileName] = useState("Pick a file");
  const [disableBtn, setDisabledBtn] = useState(false);
  const [showSnackBar, setSnackBar] = useState(false);
  const [disableSubmit, setDisableSubmit] = useState(false);

  const handleFile = (e, setFieldValue) => {
    if (e.target.files && e.target.files.length) {
      const file = [...selectedFile];
      file.push(e.target.files[0]);
      file.length >= 5 ? setDisabledBtn(true) : setDisabledBtn(false);
      setSelectedFile(file);
      setFieldValue("fileData", file);
      checkFileSize(file);
      setFileName(e.target.files[0].name);
    }
  };

  const checkFileSize = files => {
    const initialFileSize = 0;
    const maxFileSize = 100000000; //100MB
    const currentSize = files.reduce(
      (total, curr) => total + curr.size,
      initialFileSize
    );
    if (currentSize > maxFileSize) {
      setSnackBar(true);
      setDisableSubmit(true);
    } else {
      setDisableSubmit(false);
    }
  };

  const closeModalHandler = () => {
    setSelectedFile([]);
    setDisableSubmit(false);
    setFileName("Pick a file");
    closeModal();
  };

  //used for back to helpbox
  const backToHelpBox = () => {
    setSelectedFile([]);
    setDisableSubmit(false);
    setFileName("Pick a file");
    closeModal(true);
  };

  const removeFileHandler = (index, setFieldValue) => {
    const file = [...selectedFile];
    file.splice(index, 1);
    file.length >= 5 ? setDisabledBtn(true) : setDisabledBtn(false);
    setSelectedFile(file);
    setFieldValue("fileData", file);
    checkFileSize(file);
  };

  const resetStateHandler = () => {
    setDisabledBtn(false);
    setDisableSubmit(false);
    setFileName("Pick a file");
    setSelectedFile([]);
  };

  const hideSnackBar = () => setSnackBar(false);

  const CONCIERGE_VALIDATION = courseTypeNotReqd => {
    const schema = {
      subject: yup.string().required("Subject is required"
      ),
      description: yup.string().required("Description is required"
      )
    };
    const dropDownSchema = {
      courseType: yup.string().required("Course type is required"
      )
    };
    return yup
      .object()
      .shape(courseTypeNotReqd ? schema : { ...schema, ...dropDownSchema });
  };
  let deviceType = React.useContext(DeviceTypeContext);
  let isTabView = deviceType === DEVICE_TYPES.TAB.isTab ? true : false;

  const TakeBackButtonWrap = styled.div`
    position: absolute;
    cursor: pointer;
    z-index: 10;
    .back-icon-email {
      flex: 0 0 auto;
      display: flex;
      width: 16px;
    }
  `;

  return (
    <Dialog
      open={showDialog}
      maxWidth={`xs`}
      aria-labelledby="form-dialog-title"
      classes={{
        paper: `dialog_width500 pl-4 pr-4 pt-4 ${
          isTabView ? "border-wrapper-tab" : ""
        }`
      }}
    >
      {isClickedFromHelpBox && (
        <TakeBackButtonWrap onClick={backToHelpBox}>
          <img src={backIcon} className="back-icon-email" alt="back"></img>
        </TakeBackButtonWrap>
      )}
      <IconButton
        aria-label="close"
        className={`closeButton closeBtnPos`}
        onClick={closeModalHandler}
      >
        <CloseIcon />
      </IconButton>
      <DialogTitle
        id="customized-dialog-title"
        className="dialogStyle"
        onClick={closeModalHandler}
      >
        <strong>{modalTitle}</strong>
      </DialogTitle>
      <DialogContent>
        <Formik
          enableReinitialize={true}
          initialValues={values}
          validationSchema={CONCIERGE_VALIDATION(showCourseTypeDropDown)}
          onSubmit={values => {
            resetStateHandler();
            onSubmit(values);
          }}
        >
          {({
            values,
            handleChange,
            setFieldValue,
            handleSubmit,
            errors,
            touched
          }) => {
            return (
              <form onSubmit={handleSubmit}>
                    <TextField
                      name="courseType"
                      select
                      variant="outlined"
                      fullWidth
                      hidden={showCourseTypeDropDown}
                      label={"Course Type"}
                      className={`custom-select-concierge mt-2 mr-3`}
                      onChange={e => handleChange(e)}
                      value={values.courseType || ""}
                      error={errors.courseType && touched.courseType}
                      helperText={
                        errors.courseType && touched.courseType
                          ? `( ${errors.courseType} )`
                          : ""
                      }
                      margin="normal"
                    >
                      <MenuItem value="" disabled>
                        {text}
                      </MenuItem>
                      <MenuItem
                        value={`coding`}
                        hidden={
                          teacherSkill && !teacherSkill.includes("coding")
                        }
                      >
                          {"Coding"}
                      </MenuItem>
                      <MenuItem
                        value={`math`}
                        hidden={teacherSkill && !teacherSkill.includes("math")}
                      >
                          {"Math"}
                      </MenuItem>
                      <MenuItem
                        value={`music`}
                        hidden={teacherSkill && !teacherSkill.includes("music")}
                      >
                          {"MUSIC"}
                      </MenuItem>
                    </TextField>

                    <TextField
                      type="text"
                      fullWidth
                      className="mt-2 mb-2"
                      variant="outlined"
                      label={"Subject"}
                      placeholder={"Subject"}
                      onChange={e => {
                        handleChange(e);
                        setFieldValue("subject", e.target.value);
                      }}
                      name="subject"
                      value={values.subject || ""}
                      error={errors.subject && touched.subject}
                      helperText={
                        errors.subject && touched.subject
                          ? `( ${errors.subject} )`
                          : ""
                      }
                    />
                    <TextField
                      fullWidth
                      className="mt-2 mb-2"
                      multiline
                      rows={isTabView ? 8 : 12}
                      rowsMax={isTabView ? 8 : 12}
                      variant="outlined"
                      label={
                        "Description"
                      }
                      placeholder={"Write your detailed concern here"}
                      onChange={e => {
                        handleChange(e);
                        setFieldValue("description", e.target.value);
                      }}
                      name="description"
                      value={values.description || ""}
                      error={errors.description && touched.description}
                      helperText={
                        errors.description && touched.description
                          ? `( ${errors.description} )`
                          : ""
                      }
                    />
                <div
                  className={`${
                    isTabView
                      ? "d-flex align-items-center"
                      : "fileInputLabel mt-2"
                  }`}
                >
                  <div className="fileName_wrap">
                    {selectedFile.length ? (
                      selectedFile.map((file, index) => {
                        return (
                          <div
                            className="fileNameLabel"
                            key={`${file.name + index}`}
                          >
                            <div className="file_wrap font12">
                              {file ? file.name : ""}
                            </div>
                            <img
                              className="file_cross"
                              onClick={() =>
                                removeFileHandler(index, setFieldValue)
                              }
                              src={crossBtn}
                            />
                          </div>
                        );
                      })
                    ) : isTabView ? (
                      <>
                        <div className="mb-2"></div>
                        <button
                          className="uploadBtnTab_helpdesk font18 d-flex align-items-center justify-content-center"
                          onClick={e => {
                            handleChange(e);
                            handleFile(e, setFieldValue);
                          }}
                        >
                          <input
                            type="file"
                            name="fileData"
                            onChange={e => {
                              handleChange(e);
                              handleFile(e, setFieldValue);
                            }}
                            className="fileInputTablet"
                            disabled={disableBtn}
                            accept="image/*,audio/*,video/*,.pdf,.ppt,.pptx, .docx, .doc, .csv,.xlsx,.webm"
                          />
                          <img
                            src={uploadIcon}
                            alt={"upload"}
                            // className="icon-image"
                            style={{
                              width: "25px",
                              marginRight: "10px",
                              color: "#03a2ed"
                            }}
                          ></img>
                          Click here to Upload
                        </button>
                      </>
                    ) : (
                      <div className="emptyFileLabel">
                        Upload Attachments
                      </div>
                    )}
                  </div>
                  {!isTabView && (
                    <span className="fileInputButton font32">+</span>
                  )}
                  {!isTabView && (
                    <input
                      type="file"
                      name="fileData"
                      onChange={e => {
                        handleChange(e);
                        handleFile(e, setFieldValue);
                      }}
                      className="fileInput"
                      disabled={disableBtn}
                      accept="image/*,audio/*,video/*,.pdf,.ppt,.pptx, .docx, .doc, .csv,.xlsx,.webm"
                    />
                  )}
                </div>
                    <FlashSnackbar
                      message={"File size exceeding 100MB not allowed"}
                      onClose={() => hideSnackBar()}
                      variant="error"
                      open={showSnackBar}
                    />
                <div className="text-center d-flex justify-content-center mt-3 mb-4">
                      <button
                        disabled={disableSubmit}
                        className="btn btn-dark"
                        type="submit"
                        className={`${isTabView ? "sendBtnTab" : "sendBtn"}`}
                      >
                        {"SEND"}
                      </button>

                </div>
              </form>
            );
          }}
        </Formik>
      </DialogContent>
    </Dialog>
  );
};

export default ConciergeEmailModal;

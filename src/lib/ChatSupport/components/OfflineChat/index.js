import React, { useState } from "react";
import { Font12, Font14, TextBox, Button, TakeBackBtn } from "../../styles";
import styled from "styled-components";

const OfflineChatContainer = styled.div`
  padding: 0px 23px;
  overflow: auto;
`;
const HeadingWrap = styled.div`
  font-weight: 600;
`;
const LabelWrap = styled.div`
  margin-top: 1.875rem;
  font-weight: 600;
`;
const ButtonWrap = styled(Button)`
  background-color: #266ad1;
  color: #fff;
  width: 100%;
  height: 50px;
  border-radius: 4px;
  margin-top: 1.4375rem;
  &:disabled {
    background-color: #266ad191;
  }
`;
const TextArea = styled(TextBox)`
  height: 132px;
  margin: 11px 0;
  padding: 5px;
`;
const TakeBackWrap = styled.div`
  position: relative;
  padding: 17px 0 17px;
  padding-left: 20px;
`;
const TakeBackButton = styled(TakeBackBtn)`
  top: 23px;
  left: 0;
  border-color: #2f70d3;
`;
const OfflineChat = ({ message, onSubmit, takeBack }) => {
  const [offlineMessage, setOfflineMessage] = useState(message);
  const [btnDisable, setBtnDisable] = useState(true);
  return (
    <>
      <OfflineChatContainer>
        <HeadingWrap>
          <TakeBackWrap>
            <TakeBackButton onClick={() => takeBack()}></TakeBackButton>
            <Font14>
            Go back to menu
            </Font14>
          </TakeBackWrap>
          <Font12>
          Sorry, we are not online at the moment. Leave
            <br />
            a message and we'll get back to you.
          </Font12>
        </HeadingWrap>
        <LabelWrap>
          <Font12>
          Message
          </Font12>
        </LabelWrap>
        <TextArea
          onChange={e => {
            setOfflineMessage(e.target.value.trim());
            e.target.value.length == 0
              ? setBtnDisable(true)
              : setBtnDisable(false);
          }}
        ></TextArea>
        <ButtonWrap
          type="button"
          disabled={btnDisable}
          onClick={() => onSubmit(offlineMessage)}
        >
          Submit
        </ButtonWrap>
      </OfflineChatContainer>
    </>
  );
};

export default OfflineChat;

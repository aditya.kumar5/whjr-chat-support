import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { Button } from "../../styles";
import agent from "../../assets/images/agent.svg";

const ActionBox = styled.div`
  display: flex;
  align-items: center;
  margin: 10px 15px;
  padding: 6px 6px 0px 6px;
  border-radius: 7px;
  background-color: #f0f5ff;
  font-size: 0.75em;
  img {
    margin-right: 8px;
  }
  .text {
    flex: 2;
    padding-bottom: 2px;
    font-weight: 700;
    color: ${props => props.theme.colors.primary};
  }
  .actions {
    flex: 3;
    justify-content: flex-end;
    align-items: center;
    display: flex;
    margin-bottom: 3px;
    button {
      padding: 5px 4px;
      &:first-child {
        margin-right: 6px;
      }
      color: ${props => props.theme.colors.textColor.primary};
      background-color: #fff;
    }
  }
`;

const StartChat = ({ startChat, cancel }) => (
  <ActionBox>
    <img src={agent} />
    <div className="text">
    Connect with an agent.
    </div>
    <div className="actions">
      <Button onClick={startChat}>
      Start
      </Button>
      <Button onClick={cancel}>
      Cancel
      </Button>
    </div>
  </ActionBox>
);

StartChat.displayName = "StartChat";

StartChat.propTypes = {
  startChat: PropTypes.func.isRequired,
  cancel: PropTypes.func.isRequired
};

export default StartChat;

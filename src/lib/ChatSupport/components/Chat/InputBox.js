import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { Button, TextBox, InputFile } from "../../styles";
import { getWindow } from "../../utils/helpers/dom";

const MessageContainer = styled.div`
  width: 100%;
  padding: 10px 15px;
  border-top: 1px solid rgba(210, 210, 210, 0.5);
  font-size: 0.75em;
  font-weight: 700;
  form {
    display: flex;
    justify-content: ${props => (props.centered ? "center" : "space-between")};
    align-items: center;
  }
`;

const EndChat = styled(Button)`
  padding: 8px;
  background-color: #c31807;
`;

const MessageBox = styled.div`
  display: flex;
  align-items: center;
  flex-grow: 1;
  margin: 0px 10px;
  textarea {
    margin: 0px;
    padding: 0px;
    height: ${props => (props.mobile ? "25px" : "20px")};
    min-height: ${props => (props.mobile ? "25px" : "20px")};
    max-height: 60px;
    font-size: 1em;
    border: none;
  }
  @media (max-width: 768px) {
    textarea {
      font-size: 16px;
    }
  }
`;

const SendButton = styled(Button)`
  padding: 6px 7px;
  border-radius: 100%;
  svg {
    position: relative;
    right: 1px;
  }
`;

const InputFileWrap = styled.div`
  position: relative;
  background-color: rgba(210, 210, 210, 0.5);
  width: 30px;
  height: 30px;
  border-radius: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
`;
const AttachImg = styled.svg`
  width: 16px;
  height: 18px;
`;

const InputFileStyled = styled(InputFile)`
  position: absolute;
  width: 100%;
  padding: 5px;
  right: 0px;
  opacity: 0;
  border-radius: 100%;
`;

const InitializingChat = styled.div`
  display: flex;
  flex-grow: 1;
  align-items: center;
  .chat-end-load,
  .chat-input-load > div {
    opacity: 0.99;
    border-radius: 4px;
    background-image: linear-gradient(
        to right,
        rgba(226, 226, 226, 0.5) 2%,
        rgba(255, 255, 255, 0.5) 98%
      ),
      linear-gradient(to bottom, #d4d4d4, #d4d4d4);
  }
  .chat-end-load {
    width: 34px;
    height: 30px;
    margin-right: 8px;
  }
  .chat-input-load {
    flex-grow: 1;
    margin-right: 8px;
  }
  .chat-input-load > div {
    flex-grow: 1;
    height: 12px;
    margin-bottom: 3px;
    &:last-child {
      width: 35%;
    }
  }
`;

const isMobile = getWindow().w < 768 ? true : false;

const InputBox = ({
  message,
  messageHandler,
  sendMessage,
  endChat,
  chatEnded = false,
  allowedExtensions,
  disabledUpload,
  enableActions = true
}) => (
  <MessageContainer
    onSubmit={e => {
      e.preventDefault();
    }}
    centered={chatEnded}
  >
    {enableActions ? (
      <form>
        <EndChat type="button" onClick={() => endChat()}>
          {!chatEnded ? (
            <span>
              End
            </span>
          ) : (
            <span>
              End this chat
            </span>
          )}
        </EndChat>
        {!chatEnded && (
          <MessageBox mobile={isMobile}>
            <TextBox
              placeholder={"Write your query here..."}
              value={message}
              onChange={e => {
                messageHandler(e);
                if (!e.target.value) {
                  e.target.style.height = isMobile ? "25px" : "20px";
                }
              }}
              onKeyPress={e => {
                if (e.key === "Enter") {
                  if (!e.shiftKey) {
                    e.preventDefault();
                    sendMessage(message);
                  } else {
                    e.target.style.height = e.target.scrollHeight + "px";
                  }
                } else if (
                  e.target.scrollHeight > (isMobile ? 25 : 20) &&
                  e.target.scrollHeight < 60
                ) {
                  e.target.style.height = e.target.scrollHeight + "px";
                }
              }}
            />
          </MessageBox>
        )}
        {!chatEnded && message.length ? (
          <SendButton type="submit" onClick={() => sendMessage(message)}>
            <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18">
              <g fill="none">
                <path
                  fill="#FFF"
                  d="M17.889 1.348l-7.032 15.937c-.325.737-1.37.747-1.709.015l-2.667-5.78-5.78-2.668c-.731-.338-.724-1.384.014-1.71L16.653.113c.78-.345 1.58.454 1.236 1.236z"
                />
                <path
                  fill="#266AD1"
                  d="M7.123 9.544l1.42-1.42a.943.943 0 011.333 1.333l-1.42 1.42 1.677 3.633 5.244-11.887L3.49 7.867l3.633 1.677z"
                />
              </g>
            </svg>
          </SendButton>
        ) : (
          !chatEnded &&
          !message.length && (
            <InputFileWrap>
              <InputFileStyled
                onChange={e => sendMessage(e)}
                accept={allowedExtensions}
                disabled={disabledUpload}
              />
              <AttachImg
                xmlns="http://www.w3.org/2000/svg"
                width="16"
                height="18"
                viewBox="0 0 16 18"
              >
                <path d="M14.691 1.311c-1.745-1.748-4.585-1.748-6.33 0L1.696 7.988c-.205.206-.205.54 0 .746.206.206.54.206.745 0l6.665-6.677c1.293-1.295 3.547-1.295 4.84 0 1.335 1.337 1.335 3.512 0 4.85L4.64 16.333c-.796.797-2.183.797-2.979 0-.821-.823-.821-2.161 0-2.984l8.934-9.055c.299-.299.819-.299 1.117 0 .308.308.308.81 0 1.12l-8.561 8.681c-.178.178-.178.568 0 .746.206.206.539.206.744 0l8.562-8.682c.719-.72.719-1.89 0-2.611-.696-.698-1.911-.696-2.606 0L.917 12.604c-1.223 1.225-1.223 3.251 0 4.476.597.598 1.39.92 2.234.92.844 0 1.637-.322 2.234-.92l9.306-9.428c1.745-1.748 1.745-4.592 0-6.34z" />
              </AttachImg>
            </InputFileWrap>
          )
        )}
      </form>
    ) : (
      <InitializingChat>
        <div className="chat-end-load"></div>
        <div className="chat-input-load">
          <div></div>
          <div></div>
        </div>
      </InitializingChat>
    )}
  </MessageContainer>
);

InputBox.displayName = "InputBox";

InputBox.propTypes = {
  sendMessage: PropTypes.func.isRequired,
  endChat: PropTypes.func.isRequired,
  messageHandler: PropTypes.func.isRequired,
  message: PropTypes.string.isRequired,
  chatEnded: PropTypes.bool,
  allowedExtensions: PropTypes.string,
  enableActions: PropTypes.bool
};

export default InputBox;

import { makeStyles } from '@material-ui/core/styles';

export default makeStyles((theme) => ({
    rootTabs: {
        borderBottom: "1px solid #e8e8e8",
        marginLeft: "5px"
    },
    indicator: {
        backgroundColor: "#1890ff"
    },
    rootTab: {
        textTransform: "none",
        minWidth: 50,
        fontSize: 11,
        padding: "4px 4px",
        fontWeight: theme.typography.fontWeightRegular,
        marginRight: "6px",
        "&:hover": {
            color: "#40a9ff",
            opacity: 1
        },
        "&$selected": {
            color: "#1890ff",
            fontWeight: theme.typography.fontWeightMedium
        },
        "&:focus": {
            color: "#40a9ff"
        }
    },
    root: {
        flexGrow: 1,
        alignItems: "center"
    },
    padding: {
        padding: theme.spacing(3)
    },
    demo1: {
        backgroundColor: theme.palette.background.paper
    },
    internetChecking: {
        textAlign: "center"
    },
    loaderIcon: {
        margin: "50px",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        "& svg": {
            animation: "$spin 2s infinite linear ",
            fontSize: "48px",
            position: "absolute",
        },
        "& span": {
            position: "absolute",
            fontSize: "10px"
        }
    },
    "@keyframes spin": {
        from: {
            transform: "rotate(0deg)"
        },
        to: {
            transform: "rotate(359deg)"
        }
    },
    internetSpeedInfo: {},
    speedContainer: {
        margin: "10px 0px"
    },
    circularProgress: {
        transform: "rotate(90deg) !important"
    },
    circularProgressColor: {
        color: "#5970eb"
    },
    circularInner: {
        width: "82%",
        height: "82%",
        backgroundColor: "#f3f5f7",
        borderRadius: "50%",
        left: "10px",
        top: "10px",
        "& div": {
            fontSize: "28px",
            color: "#000",
            lineHeight: "31px",
            paddingTop: "20px",
            fontWeight: "600"
        },
        "& span": {
            display: "block",
            fontSize: "12px"
        }
    },
    internetStatus: {
        "& .status": {
            display: "flex",
            alignItems: "center",
            fontSize: "12px",
            marginTop: "60px",
            justifyContent: "center",
            "&.summary": {
                display: "block",
                "&.talk-to-agent": {
                    marginTop: "30px"
                }
            }
        },

        "& span": {
            display: "block",
            marginLeft: "5px"
        },
    },
    formDiv: {
        height: '100%',
        width: '100%',
    },
    progressDiv: {
        height: '75%',
        width: '100%',
        position: 'absolute',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        background: 'white',
    },
    mt25: {
        marginTop: "25px",
    },
    btnConnect: {
        color: "#fff",
        marginTop: "16px",
        fontSize: "12px",
        "&:hover": {
            opacity: 0.8
        }
    },
    btnOrange: {
        backgroundColor: "#ff8d1b",
        "&:hover": {
            backgroundColor: "#ff8d1b",
        }
    },
    btnDark: {
        backgroundColor: "#5A7184",
        "&:hover": {
            backgroundColor: "#5A7184",
        }
    },
    btnContainer: {
        display: "flex",
        justifyContent: "center",
        flexDirection: "column",
        alignItems: "center"
    },
    backButton: {
        marginLeft: "5px",
        marginTop: "15px",
        cursor: "pointer",
        color: "#1890ff",
    }
}));
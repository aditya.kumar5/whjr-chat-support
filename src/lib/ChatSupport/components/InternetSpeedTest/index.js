import React, { useEffect, useState } from "react";
import { withStyles } from "@material-ui/core/styles";
import {
  Tabs,
  Tab,
  Box,
  Typography,
  CircularProgress,
  Button
} from "@material-ui/core";
import {
  Refresh,
  CheckCircleRounded,
  Error,
  KeyboardArrowLeft
} from "@material-ui/icons";
import useStyles from "./style";
import TypeformHandler from "./TypeformHandler";
import { EVENT_NAMES } from "../../utils/constants/segmentEvents";
import {socket__getLatestSpeed} from "../../utils/helpers/socket-lib/socket-util";

const TabPanel = props => {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`wrapped-tabpanel-${index}`}
      aria-labelledby={`wrapped-tab-${index}`}
      {...other}
    >
      {value === index && <Box p={1.2}>{children}</Box>}
    </div>
  );
};

const allyProps = index => {
  return {
    id: `wrapped-tab-${index}`,
    "aria-controls": `wrapped-tabpanel-${index}`
  };
};

const SpeedSpinner = props => {
  const classes = useStyles();
  return (
    <Box
      className={classes.speedContainer}
      position="relative"
      display="inline-flex"
    >
      <CircularProgress
        size={110}
        thickness={2}
        classes={{
          root: classes.circularProgress,
          colorPrimary: classes.circularProgressColor
        }}
        variant="determinate"
        {...props}
      />
      <Box className={classes.circularInner} position="absolute">
        <Typography variant="caption" component="div" color="textSecondary">
          {props.value ? props.value : 0}
        </Typography>
        <Typography variant="h6" component="span" color="textSecondary">
          Mbps
        </Typography>
      </Box>
    </Box>
  );
};

export default function InternetSpeedTest({
  chatIssueHandler,
  activeParams,
  mode,
  openChat,
  resetChatState,
  handleEventTrack,
  endTeacherClass,
  tabArray,
  speedLimit,
  teacherInternetSpeed,
  studentInternetSpeed
}) {
  const classes = useStyles();
  const [tabValue, setTabValue] = useState("");
  const [progress, setProgress] = useState(80);
  const [counter, setCounter] = useState(10);
  const [openEndClss, setOpenEndClss] = useState(false);
  const [hideProgressBar, setHideProgressBar] = useState(false);
  const [isResolved, setIsResolved] = useState(null);
  const [startInternetTroubleShoot, setStartInternetTroubleShoot] = useState(
    false
    );
  const [answers, setAnswers] = useState([]);
  const teacherKeysLength = Object.keys(teacherInternetSpeed).length;

  const handleTabChange = (event, newValue) => {
    setStartInternetTroubleShoot(false);
    setIsResolved(null);
    setTabValue(newValue);
    setHideProgressBar(false);
    const userInternetDetails = tabArray.find((data) => {
      if (data.id === newValue) {
          return data;
      }
    });
    handleEventTrack(EVENT_NAMES.TAB_CHANGE, userInternetDetails);
  };

  const onTroubleShootclick = () => {
    setStartInternetTroubleShoot(true);
    handleEventTrack(EVENT_NAMES.TROUBLESHOOT_BUTTON_CLICK, {});
  }

  const callAgent = (type, speed) => {
    let agentData = { type, speed, responseMsgs: "" };
    if (isResolved === false && openEndClss) {
      agentData = {
        ...agentData,
        responseMsgs: answers.join("/")
      }
    }
    chatIssueHandler("connect-agent", agentData);
    handleEventTrack(EVENT_NAMES.AGENT_BUTTON_CLICK, {});
  }

  const goToEndClass = () => {
    setOpenEndClss(true);
    setIsResolved(false);
  }

  useEffect(() => {
    if (mode === "teacher") {
      socket__getLatestSpeed(activeParams.teacherId);
    }
  }, []);

  useEffect(() => {
    if (teacherKeysLength !== 0) {
      setTabValue(teacherInternetSpeed.id);
      handleEventTrack(EVENT_NAMES.TEACHER_TAB, teacherInternetSpeed);
    }
  }, [teacherInternetSpeed]);

  useEffect(() => {
    if (counter !== 0) {
      const countInterval = setInterval(() => {
        setCounter(counter - 1);
      }, 1000);
      return () => clearInterval(countInterval);
    }
  }, [counter])


  const troubleShootButton = () => {
    return (
      <Button
        onClick={() => onTroubleShootclick()}
        align="center"
        variant="contained"
        color="primary"
        className={`${classes.btnConnect} ${classes.btnOrange}`}
      >
        Start Troubleshooting
      </Button>
    );
  }

  const studentNotInclass = () => {
    return (
      <div className={classes.mt25}>
        <Typography align="center">
        Seems like the student is not in the class, please
            call them using the link above. If you reached the
            student, click on the start troubleshooting button
            below.
        </Typography>
        <div className={classes.btnContainer}>
          {troubleShootButton()}
          <Button
            align="center"
            variant="contained"
            color="primary"
            onClick={() => goToEndClass()}
            className={`${classes.btnConnect} ${classes.btnDark}`}
          >
            Not able to reach student
          </Button>
        </div>
      </div>
    );
  }

  const connectAgent = (type, speed) => {
    return (
      <Button
        onClick={() => callAgent(type, speed)}
        align="center"
        variant="contained"
        color="primary"
        className={`${classes.btnConnect} ${classes.btnOrange}`}
      >
        Connect with an agent
      </Button>
    );
  }

  return (
    <div className={classes.root}>
      <Tabs
        value={tabValue}
        onChange={handleTabChange}
        aria-label="ant example"
        classes={{
          root: classes.rootTabs,
          indicator: classes.indicator
        }}
      >
        <KeyboardArrowLeft className={classes.backButton} onClick={() => resetChatState()}/>
        {tabArray.map(item => {
          return (
            <Tab
              value={item.id}
              label={teacherInternetSpeed.id === item.id ? "Me" : item.name}
              {...allyProps(item.id)}
              classes={{
                root: classes.rootTab
              }}
            />
          );
        })}
      </Tabs>

      <div className={classes.demo1}>
        {teacherKeysLength === 0 ? (
          <Box className={classes.internetChecking} p={3}>
            <Typography>
            Doing on initial internet Check...
            </Typography>
            <div className={classes.loaderIcon}>
              <Refresh className={classes.refreshIcon} />
              {counter !== 0 && <span className={classes.timer}>{counter}</span>}
            </div>
          </Box>
        ) : (
          tabArray.map(user => {
            return (
              <TabPanel value={tabValue} index={user.id}>
                <Box align="center" className={classes.internetSpeedInfo}>
                  <Box className={classes.internetStatus}>
                    {!startInternetTroubleShoot && isResolved === null && user.isJoined && (
                      <>
                        <Typography align="center">
                        Internet Speed is
                        </Typography>
                        <SpeedSpinner value={user.speed.mbps} speed={user.speed.mbps} />
                        {parseInt(user.speed.mbps) >= parseInt(speedLimit) ? (
                          <>
                            <Typography className="status" align="left">
                              <CheckCircleRounded htmlColor="#44c886" />
                              <span>
                              The internet seems to be working fine. If the
                                  issue is still persisting chat with an agent.
                              </span>
                            </Typography>
                            {connectAgent(user.type, user.speed.mbps)}
                          </>
                        ) : (
                          <>
                            <Typography className="status" align="center">
                              <Error htmlColor="#ff8d1b" />
                              <span>
                              Seems like there's an internet issue.
                              </span>
                            </Typography>
                            {user.id === teacherInternetSpeed.id ? connectAgent(user.type, user.speed.mbps) : troubleShootButton()}
                          </>
                        )}
                      </>
                    )}

                    {/* IF STUDENT NOT IN CLASS */}
                    {!startInternetTroubleShoot && isResolved === null && !openEndClss && !user.isJoined && studentNotInclass()}

                    {/* IF ISSUE STILL NOT RESOLVED */}
                    {(isResolved === false && openEndClss) && (
                      <>
                        <Typography className="status summary" align="center">
                        Seems like this problem is not solvable.
                        </Typography>
                        <Button
                          align="center"
                          variant="contained"
                          color="primary"
                          onClick={endTeacherClass}
                          className={`${classes.btnConnect} ${classes.btnOrange}`}
                        >
                          End Class
                        </Button>

                        <Typography
                          className="status summary talk-to-agent"
                          align="center"
                        >
                          If you think an agent can help.
                        </Typography>
                        {connectAgent("student", studentInternetSpeed.mbps)}
                      </>
                    )}

                    {/* typeForm Component */}
                    {startInternetTroubleShoot &&
                      user.id !== teacherInternetSpeed.id && (
                        <div className={classes.formDiv}>
                          <TypeformHandler
                            setIsResolved={setIsResolved}
                            setOpenEndClss={setOpenEndClss}
                            isChatSupport={true}
                            openChat={openChat}
                            resetChatState={resetChatState}
                            startInternetTroubleShoot={
                              startInternetTroubleShoot
                            }
                            setStartInternetTroubleShoot={
                              setStartInternetTroubleShoot
                            }
                            handleEventTrack={handleEventTrack}
                            setHideProgressBar={setHideProgressBar}
                            setAnswers={setAnswers}
                          />
                          {!hideProgressBar && <div className={classes.progressDiv}>
                            <CircularProgress />
                          </div>}
                        </div>
                      )}
                  </Box>
                </Box>
              </TabPanel>
            );
          })
        )}
      </div>
    </div>
  );
}

import React from "react";

import styled from "styled-components";

const Tick = styled.div`
  width: 60px;
  height: 60px;
  position: relative;
  margin: 25px auto 15px auto;
  padding: 20px 15px;
  border: 2px solid #7fc77e;
  border-radius: 100%;
  .check {
    display: inline-block;
    height: 25px;
    width: 13px;
    position: absolute;
    top: 50%;
    right: 50%;
    transform: rotate(45deg) translate(-50%, -60%);
    border-bottom: 3px solid #7fc77e;
    border-right: 3px solid #7fc77e;
  }
`;

const Message = styled.div`
  margin-bottom: 25px;
  font-size: 0.875em;
  font-weight: 700;
`;

const ThankYou = ({}) => (
  <>
    <Tick>
      <span className="check"></span>
    </Tick>
    <Message>
    Thanks for your feedback.
    </Message>
  </>
);

ThankYou.displayName = "ThankYou";

export default ThankYou;

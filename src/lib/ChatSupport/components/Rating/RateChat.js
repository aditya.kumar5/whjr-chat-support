import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

import { Button } from "../../styles";

const AgentName = styled.div`
  margin-top: 5px;
  margin-bottom: 5px;
  font-size: 0.75em;
  color: ${props => props.theme.colors.textColor.secondary};
`;

const Title = styled.h1`
  margin-bottom: 12px;
  font-size: 0.875em;
`;

const Rating = styled.div`
  display: flex;
  justify-content: center;
  font-size: 0.75em;
  font-weight: 700;
  color: ${props => props.theme.colors.textColor.secondary};
  > div:first-child {
    margin-right: 20px;
  }
  .rate-img {
    min-width: 40px;
    min-height: 40px;
    padding: 8px;
    border: 2px solid #dedede;
    border-radius: 100%;
    cursor: pointer;
  }
  .rate-bad {
    color: #df0c0c;
    .rate-img {
      border: 1px solid rgba(151, 151, 151, 0.16);
      background-color: #ffc8c8;
    }
  }
`;

const LinkBtn = styled.div`
  margin-top: 15px;
  font-size: 0.75em;
  font-weight: 600;
  color: #1153b5;
  text-decoration: underline;
  cursor: pointer;
`;

const Comment = styled.textarea.attrs(props => ({
  rows: 3
}))`
  display: block;
  margin-top: 1.5rem;
  margin-bottom: 10px;
  padding: 10px 15px;
  width: 100%;
  font-size: 0.75em;
  font-weight: bold;
  color: ${props => props.theme.colors.textColor.secondary};
  border-radius: 4px;
  border: solid 1px #dedede;
  outline: none;
  resize: none;
`;

const RateChat = ({
  endedBy,
  agentName = "",
  skipChatRating,
  rateChat,
  handleRatingChange,
  askComment = false,
  isRated = false,
  submitComment,
  comment = "",
  handleCommentChange
}) => (
  <>
    {endedBy === "agent-end" && (
      <AgentName>{`${agentName} has left.`}</AgentName>
    )}
    <Title>
      How would you rate this chat?
    </Title>
    <Rating>
      <div>
        <div className="rate-img" onClick={() => handleRatingChange("good")}>
          <svg xmlns="http://www.w3.org/2000/svg" width="21" height="19">
            <path
              d="M1 7.2v9.6c0 .663.56 1.2 1.25 1.2H6V6H2.25C1.56 6 1 6.537 1 7.2zm16.818-.129h-3.492a.582.582 0 01-.51-.299.617.617 0 01-.007-.602l1.231-2.278a1.85 1.85 0 00.077-1.608A1.774 1.774 0 0013.92 1.24l-.868-.223a.58.58 0 00-.582.183L7.758 6.58A3.075 3.075 0 007 8.61v6.354C7 16.638 8.326 18 9.955 18h5.878c1.327 0 2.5-.919 2.85-2.234l1.261-5.948A2.31 2.31 0 0020 9.314c0-1.236-.98-2.243-2.182-2.243h0z"
              fill={isRated && !askComment ? "#44CB47" : "#FFF"}
              stroke={isRated && !askComment ? "" : "#979797"}
            />
          </svg>
        </div>
        <div>
        Good
      </div>
      </div>
      <div className={`${askComment ? "rate-bad" : ""}`}>
        <div className="rate-img" onClick={() => handleRatingChange("bad")}>
          <svg xmlns="http://www.w3.org/2000/svg" width="21" height="19">
            <path
              d="M20 11.8V2.2c0-.663-.56-1.2-1.25-1.2H15v12h3.75c.69 0 1.25-.537 1.25-1.2zm-16.818.129h3.492c.305 0 .458.209.51.299.05.09.154.329.007.602L5.96 15.108a1.85 1.85 0 00-.077 1.608c.221.525.657.905 1.197 1.044l.868.223a.58.58 0 00.582-.183l4.712-5.38c.49-.559.758-1.28.758-2.03V4.036C14 2.362 12.674 1 11.045 1H5.167c-1.327 0-2.5.919-2.85 2.234L1.056 9.182A2.31 2.31 0 001 9.686c0 1.236.98 2.243 2.182 2.243h0z"
              fill={askComment ? "#DF0C0C" : "#FFF"}
              stroke={askComment ? "" : "#979797"}
            />
          </svg>
        </div>
        <div>
          Bad
        </div>
      </div>
    </Rating>

    {askComment && (
      <form
        onSubmit={e => {
          e.preventDefault();
        }}
      >
        <Comment
          placeholder={"Share your comment here"}
          onChange={e => handleCommentChange(e)}
        />
        <Button
          type="submit"
          style={{ width: "100%" }}
          onClick={() => submitComment(comment)}
        >
          Submit
        </Button>
      </form>
    )}

    <LinkBtn onClick={() => skipChatRating()}>
      {endedBy === "agent-end" ? (
        <span>
          Continue this chat
        </span>
      ) : (
        !askComment && (
          <span>
            I don't want to rate
          </span>
        )
      )}
    </LinkBtn>
  </>
);

RateChat.displayName = "RateChat";

RateChat.propTypes = {
  endedBy: PropTypes.string.isRequired,
  agentName: PropTypes.string,
  skipChatRating: PropTypes.func.isRequired,
  rateChat: PropTypes.func.isRequired,
  handleRatingChange: PropTypes.func.isRequired,
  askComment: PropTypes.bool.isRequired,
  isRated: PropTypes.bool.isRequired,
  submitComment: PropTypes.func.isRequired,
  comment: PropTypes.string.isRequired,
  handleCommentChange: PropTypes.func.isRequired
};

export default RateChat;

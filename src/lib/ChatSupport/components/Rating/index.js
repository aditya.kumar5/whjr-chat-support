import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

import RateChat from "./RateChat";
import ThankYou from "./ThankYou";

const RatingContainer = styled.div`
  position: absolute;
  top: 0px;
  left: 0px;
  width: 100%;
  height: 100%;
  background-color: rgba(0, 0, 0, 0.42);
`;

const RatingCard = styled.div`
  position: absolute;
  bottom: 0px;
  left: 0px;
  padding: 25px 20px;
  width: 100%;
  min-height: 45%;
  height: auto;
  text-align: center;
  background-color: #fff;
  @media (max-width: 768px) {
    min-height: 22%;
  }
`;

const CloseBtn = styled.span`
  position: absolute;
  top: 0px;
  right: 5px;
  padding: 2px;
  font-size: 1.5em;
  line-height: 0.7;
  color: ${props => props.theme.colors.textColor.secondary};
  cursor: pointer;
`;

let zChat = null;

class Rating extends React.Component {
  state = {
    ratingGiven: false, //rating + comment (if applicable)
    askComment: false,
    comment: "",
    showThanks: false
  };
  componentDidMount() {
    zChat = window.zChat;
  }
  handleRatingChange = rating => {
    switch (rating) {
      case "good":
        this.setState({ askComment: false }, () => this.rateChat("good"));
        this.props.handleEventRating("good");
        break;
      case "bad":
        this.setState({ askComment: true }, () => this.rateChat("bad"));
        this.props.handleEventRating("bad");
        break;
    }
  };
  handleCommentChange = e => {
    this.setState({ comment: e.target.value });
  };
  rateChat = rating => {
    zChat.sendChatRating(rating);
    if (rating === "good") {
      this.setState({ ratingGiven: true }, () =>
        setTimeout(() => {
          this.setState({ showThanks: true }, () =>
            setTimeout(() => this.close(), 1000)
          );
        }, 1000)
      );
      this.props.endChat(false);
    }
  };
  submitComment = comment => {
    if (this.state.askComment && comment && comment.trim()) {
      zChat.sendChatComment(comment);
    }
    this.setState({ ratingGiven: true, showThanks: true }, () =>
      setTimeout(() => this.close(), 1000)
    );
    this.props.endChat(false);
  };
  skipChatRating = () => {
    const isRated = this.state.askComment ? true : false;
    if (
      this.state.askComment &&
      this.state.comment &&
      this.state.comment.trim()
    ) {
      zChat.sendChatComment(this.state.comment);
    }
    this.props.skipChatRating(isRated);
  };
  close = () => {
    if (this.state.ratingGiven) {
      this.props.endChat();
    } else {
      this.props.close();
    }
  };
  render() {
    return (
      <RatingContainer>
        <RatingCard>
          {!this.state.showThanks ? (
            <>
              <CloseBtn onClick={() => this.close()}>&times;</CloseBtn>
              <RateChat
                endedBy={this.props.endedBy}
                agentName={this.props.agentName}
                skipChatRating={this.skipChatRating}
                rateChat={this.rateChat}
                handleRatingChange={this.handleRatingChange}
                askComment={this.state.askComment}
                isRated={this.state.ratingGiven}
                submitComment={this.submitComment}
                comment={this.state.comment}
                handleCommentChange={this.handleCommentChange}
              />
            </>
          ) : (
            <ThankYou />
          )}
        </RatingCard>
      </RatingContainer>
    );
  }
}

Rating.displayName = "Rating";

Rating.propTypes = {
  endedBy: PropTypes.string.isRequired,
  agentName: PropTypes.string,
  close: PropTypes.func.isRequired,
  skipChatRating: PropTypes.func.isRequired,
  endChat: PropTypes.func.isRequired
};

export default Rating;

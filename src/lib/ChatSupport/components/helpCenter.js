import React from "react";
import styled, { keyframes } from "styled-components";
import { HELP_MAIL, QUESTION_MARK } from "../utils/constants/cdnImagePath";
import HelpButton from "./helper";
import { Dialog } from "@material-ui/core";

const hideChatContainer = keyframes`
0% {
  transform: translateY(0);
}
99% {
  opacity: 0;
  transform: translateY(50%);
}
100% {
  visibility: hidden;
  opacity: 0;
}
`;

const openChatContainer = keyframes`
0% {
  visibility: hidden;
  opacity: 0;
}
1% {
  visibility: visible;
  opacity: 0;
  transform: translateY(50%);
}
100% {
  visibility: visible;
  opacity: 1;
  transform: translateY(0);
}
`;

export const ChatContainer = styled.div`
  position: fixed;
  right: ${props => props.right};
  bottom: 25px;
  width: 320px;
  display: flex;
  flex-direction: column;
  max-width: 100vw;
  height: 430px;
  max-height: 85vh;
  visibility: hidden;
  opacity: 0;
  overflow: hidden;
  border-radius: 8px;
  box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.38);
  background-color: #ffffff;
  z-index: 9999999;
  line-height: normal;
  letter-spacing: normal;
  font-size: ${props => props.theme.font.fontSize};
  font-family: ${props => props.theme.font.fontFamily};
  color: ${props => props.theme.colors.textColor.primary};
  animation: ${hideChatContainer} 0.5s backwards;
  &.chat-visible {
    animation: ${openChatContainer} 0.5s forwards;
  }
  @media (max-width: 768px) {
    max-height: 100%;
    height: 100vh;
    width: 100vw;
    border-radius: 0;
    left: 0;
    top: 0;
  }
`;

export const HelpSection = styled.div`
  .help-section {
    max-height: 14rem;
    padding: 1.5rem;
    bottom: 7%;
    max-width: 17rem;
    right: 2%;
    left: auto;
    top: auto;
  }
`;

const HelpCenter = props => {
  let {
    intl,
    helpSectionStatus,
    helpSection,
    showPlayGround,
    close,
    sendEvents,
    student,
    teacher
  } = props;
  return helpSectionStatus ? (

      <Dialog onClose={close} open={helpSection}>
        <HelpSection>
        <ChatContainer
        className={`${helpSection ? "chat-visible help-section" : ""}`}
        right={showPlayGround ? "34%" : "25px"}
      >
        <div>
          <HelpButton
            text={"Visit Help Center"}
            heading={"Find solution to your issues"}
            srcURL={HELP_MAIL}
            path="/t/helpCenter"
            sendEvents={sendEvents}
            close={close}
            teacher={teacher}
            student={student}
          />

          <hr />

          <HelpButton
            text={"Write to Us"}
            heading={"Send an Email"}
            srcURL={QUESTION_MARK}
            path="/t/write-to-us"
            sendEvents={sendEvents}
            close={close}
            teacher={teacher}
            student={student}
          />
        </div>
      </ChatContainer>
      </HelpSection>
    </Dialog>
  ) : null;
};

export default HelpCenter;

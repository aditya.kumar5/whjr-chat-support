import React from "react";
import { Modal } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import LanguageIcon from "@material-ui/icons/Language";
import SchoolOutlinedIcon from "@material-ui/icons/SchoolOutlined";
import CloseIcon from "@material-ui/icons/Close";
import IconButton from "@material-ui/core/IconButton";

const useStyles = makeStyles((theme) => ({
  outerDiv: {
    position: "fixed",
    width: "430px",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    backgroundColor: theme.palette.background.paper,
    boxShadow: "0px 4px 4px rgb(0 0 0 / 25%)",
    borderRadius: "8px",
    padding: theme.spacing(4),
    outline: "none",
    padding: "25px 40px"
  },
  closeIcon: {
    position: "absolute",
    top: "16px",
    right: "16px",
    color: "#959EAD",
    width: "16px",
    height: "16px"
  },
  iconDiv: {
    background: "linear-gradient(145.35deg, #00B5CE 0.71%, #008FA3 93.55%)",
    color: "white",
    width: "32px",
    height: "32px",
    left: "569px",
    top: "216px",
    borderRadius: "50%",
    padding: "5px",
    marginRight: "8px"
  },
  headerDiv: {
    display: "flex",
    justifyContent: "center",
    flex: "1",
    alignItems: "center"
  },
  text: {
    fontSize: "1.25em",
    lineHeight: "20px",
    fontWeight: "bold"
  },
  innerText: {
    fontSize: "0.875em",
    lineHeight: "24px",
    textAlign: "center",
    color: "#5A7184",
    marginTop: "20px",
    marginBottom: "15px",
    paddingLeft: "20px",
    paddingRight: "20px"
  },
  buttonDiv: {
    display: "flex",
    justifyContent: "space-evenly"
  },
  proceedButton: {
    width: "196px",
    height: "48px",
    border: "1px solid #FF8D1A",
    borderRadius: "4px",
    background: "white",
    fontSize: "1.125em",
    color: "#FF8D1A",
    lineHeight: "23px",
    fontWeight: "bold"
  },
  rescheduleButton: {
    width: "196px",
    height: "48px",
    background: "#FF8D1A",
    borderRadius: "4px",
    border: "#FF8D1A",
    fontSize: "1.125em",
    lineHeight: "23px",
    color: "#FFFFFF",
    fontWeight: "bold",
    textTransform: "uppercase",
    flex: "1",
    marginTop: "15px"
  },
  textDecor: {
    textDecoration: "underline",
    cursor: "pointer"
  }
}));

const ChatIntermediateStep = ({
  data,
  modal,
  handleReschedule,
  handleStartChat,
  handleReschedulePopup
}) => {
  const classes = useStyles();
  return (
    <Modal
      open={modal}
      onClose={() => {
        handleReschedule(null, false);
      }}
      aria-labelledby="simple-modal-title"
      aria-describedby="simple-modal-description"
    >
      <div className={classes.outerDiv}>
        <IconButton
          className={classes.closeIcon}
          onClick={() => {
            handleReschedule(null, false);
          }}
        >
          <CloseIcon />
        </IconButton>
        <div className={classes.headerDiv}>
          {data && data.name === "Language mismatch" ? (
            <LanguageIcon className={classes.iconDiv} />
          ) : (
            <SchoolOutlinedIcon className={classes.iconDiv} />
          )}
          <span className={classes.text}>{data && data.name} ?</span>
        </div>

        <div className={classes.buttonDiv}>
          <button
            onClick={() => {
              handleReschedulePopup({
                isVisible: true,
                mismatchCaseValue:
                  data.name
                    .toLowerCase()
                    .split(" ")
                    .join("_") === "grade_mismatch"
                    ? data.name
                        .toLowerCase()
                        .split(" ")
                        .join("_")
                    : "lang_mismatch"
              });
              handleReschedule(null, false);
            }}
            className={classes.rescheduleButton}
          >
            Reschedule
          </button>
        </div>
        <div className={classes.innerText}>
          Please click on the RESCHEDULE button to resolve this issue or raise a
          <span
            onClick={() => {
              handleStartChat(data.support_dept, data.index, true);
              handleReschedule(null, true);
            }}
            className={classes.textDecor}
          >
            {" "}
            chat
          </span>{" "}
          .
        </div>
      </div>
    </Modal>
  );
};

export default ChatIntermediateStep;

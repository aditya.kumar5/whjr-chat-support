import React from "react";
import styled from "styled-components";

const HelpContainer = styled.div`
  color: #5a7184;
  img {
    width: 1.5rem;
  }
  .content {
    color: #488da4;
    cursor: pointer;
  }
`;

const HelpButton = props => {
  let {
    heading = "",
    srcURL = "",
    text = "",
    path = "",
    close,
    sendEvents,
    student,
    teacher
  } = props;
  const handler = () => {
    props.history.push(path);
    close();
    localStorage.setItem("WriteToUSsubNode", "desktop");
    sendEvents({
      event: "helpdesk_entity_clicked",
      properties: {
        user_id: teacher.id || student.id,
        user_email: teacher.email || student.email,
        user: teacher ? "Teacher" : "Student",
        action_time: new Date().getTime(),
        entity: path.includes("/write-to-us")
          ? "write_to_us"
          : "visit_help_center"
      }
    });
  };
  return (
    <HelpContainer onClick={handler}>
      <div>{heading}</div>
      <div className="heading content d-flex align-items-center font18 mt-1">
        <img src={srcURL} className="mr-2" />
        <div className="cursor-pointer">{text}</div>
      </div>
    </HelpContainer>
  );
};

export default HelpButton

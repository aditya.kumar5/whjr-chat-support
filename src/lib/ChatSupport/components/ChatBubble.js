import React, { useEffect } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { Button } from "../styles";
import {
  HELP_ICON,
  HELP_MAIL_WHITE
} from "../utils/constants/cdnImagePath";
import MobileChatIcon from "../assets/images/chat.png";
// import { getChatConfig } from "../../../reducers";
import { DeviceTypeContext, DEVICE_TYPES } from "../utils/constants/device-type";
import { SignalCellularNull } from "@material-ui/icons";

const helpIcon = HELP_ICON;
const ChatIcon = HELP_MAIL_WHITE;

const ChatBubbleWrap = styled.div`
  width: ${(props) => props.isTabView && "45px"};
  position: fixed;
  right: ${(props) => (props.isAskEnabled ? "50px" : "25px")};
  bottom: ${(props) =>
    props.showPlayGround || props.isAskEnabled ? "60px" : "10px"};
  z-index: ${(props) => (props.isAskEnabled ? "9" : "9999999")};
  @media (max-width: 768px) {
    right: 12px;
    bottom: 25px;
  }
`;
const StyledBubble = styled(Button)`
  font-size: ${(props) => props.isTabView && "18px"};
  font-weight: ${(props) => props.isTabView && "normal"};
  background-image: ${(props) =>
    props.isTabView &&
    "linear-gradient(to left, rgb(8, 127, 237), rgb(1, 183, 238));"};
  width: 100%;
  padding: 12px 20px;
  border-radius: 2rem;
  display: ${(props) => (props.hidden ? "none" : "initial")};
  box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.38);
  font-family: ${(props) => props.theme.font.fontFamily};
  background: var(--inclass-zendesk-button-bg);
  color: var(--inclass-zendesk-button-color);
  img {
    margin-right: ${(props) => (props.showPlayGround ? "0ox" : "6px")};
  }

  @media screen and (orientation: landscape) {
    height: ${(props) => props.isTabView && "42px"};
    width: ${(props) => props.isTabView && "42px"};
    padding: ${(props) => props.isTabView && "0"};
    img {
      margin-right: ${(props) => props.isTabView && "0"};
    }
  }
  @media (max-width: 768px) {
    height: 42px;
    width: 42px;
    padding: 0;
    img {
      margin-right: 0;
    }
  }
`;
export const NotificationBubble = styled.span`
  position: absolute;
  background-color: #e52828;
  height: 30px;
  width: 30px;
  z-index: 9999999;
  bottom: 35px;
  right: 118px;
  color: #fff;
  border-radius: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  border: 1px solid #fff;
  @media (max-width: 768px) {
    right: 15px;
  }
`;
const ChatBubbleText = styled.span`
  display: ${(props) => props.isTabView && "none"};
  @media (max-width: 768px) {
    display: none;
  }
`;
const DesktopChatImage = styled.img`
  display: inline;
  width: 1.5rem;
  @media (max-width: 768px) {
    display: none;
  }
`;
const MobileChatImage = styled.img`
  width: 21px;
  height: 20px;
  @media (min-width: 769px) {
    display: none;
  }
`;

const ChatBubble = ({
  chatDataAll,
  handleClick,
  handleHelpClick,
  hidden = true,
  notificationCount,
  showPlayGround,
  isTeacher = false,
  pathName,
  isHelpSectionActive = false,
  setHelpSectionFlag,
  d__openChatwindow,
  resetHelpSectionFlag,
  resetHelpCenter,
  className,
  location
}) => {
  let chatBubbleLabel = null;
  if(chatDataAll){
    chatBubbleLabel = chatDataAll.chatBubbleLabel;
  }
  const isDashBoardPage = pathName.includes("dashboard");
  let deviceType = React.useContext(DeviceTypeContext);
  let isTabView = deviceType === DEVICE_TYPES.TAB.isTab ? true : false;
  let showTabBubble = isTabView && isDashBoardPage;
  console.log("chatData>>>", chatDataAll)

  // useEffect(() => {
  //   console.log("chatData>>>", chatDataAll)
  //   if (chatDataAll && chatDataAll.openChatWindow) {
  //     setHelpSectionFlag();
  //     d__openChatwindow(!chatDataAll.openChatWindow);
  //   }
  //   if (chatDataAll && chatDataAll.openChatWindow == "RESET") {
  //     resetHelpSectionFlag();
  //   }
  //   if (chatDataAll && chatDataAll.openChatWindow == "RESET_HELP_CENTER") {
  //     resetHelpCenter();
  //   }
  // }, [chatDataAll]);

  return (
    <>
      <ChatBubbleWrap
        isAskEnabled={
          location.indexOf("dashboard") !== -1 ||
          location.indexOf("my-projects") !== -1
        }
        className={className}
        showPlayGround={showPlayGround}
        isTabView={isTabView}
      >
        {notificationCount > 0 && !isHelpSectionActive && (
          <NotificationBubble hidden={hidden}>
            {notificationCount}
          </NotificationBubble>
        )}
        {isTeacher ? (
          <StyledBubble
            onClick={() => handleClick(true)}
            hidden={hidden}
            showPlayGround={showPlayGround}
            isTabView={isTabView}
          >
            <span>
              {!chatBubbleLabel && <DesktopChatImage src={ChatIcon} />}
              {!chatBubbleLabel && <MobileChatImage src={MobileChatIcon} />}
              <ChatBubbleText>
                {chatBubbleLabel
                  ? chatBubbleLabel
                  : !showPlayGround &&
                    !showTabBubble &&
                    (isHelpSectionActive ? (
                      "HELP"
                    ) : (
                      "Chat with us"
                    ))}
              </ChatBubbleText>
            </span>
          </StyledBubble>
        ) : (
          <StyledBubble
            onClick={() => handleHelpClick(true)}
            hidden={hidden}
            showPlayGround={showPlayGround}
            isTabView={isTabView}
          >
            <span>
              {!chatBubbleLabel && <DesktopChatImage src={helpIcon} />}
              {!chatBubbleLabel && <MobileChatImage src={helpIcon} />}
              <ChatBubbleText isTabView={isTabView}>
                {chatBubbleLabel
                  ? chatBubbleLabel
                  : !showPlayGround &&
                    !showTabBubble && (
                      "Need Help"
                    )}
              </ChatBubbleText>
            </span>
          </StyledBubble>
        )}
      </ChatBubbleWrap>
    </>
  );
};

ChatBubble.displayName = "ChatBubble";

ChatBubble.propTypes = {
  hidden: PropTypes.bool,
  handleClick: PropTypes.func,
  notificationCount: PropTypes.number
};

// const mapStateToProps = (state) => ({
//   chatDataAll: getChatConfig(state)
// });

// const mapDispatchToProps = (dispatch) => ({
//   d__openChatwindow: (data) => {
//     dispatch(openChatWindow.chatWindow(data));
//   }
// });

// export default connect(mapStateToProps, mapDispatchToProps)(ChatBubble);

export default ChatBubble;

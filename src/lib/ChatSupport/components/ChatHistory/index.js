import React, { Component } from "react";
import styled from "styled-components";
import { LoadingBubble } from "../../styles";
import MessageList from "../MessageList";
import debounce from "lodash/debounce";

const HistoryChatWrap = styled.div`
  overflow-y: auto;
`;
const Loading = styled(LoadingBubble)`
  top: 47px;
  z-index: 99999;
`;
const HistoryNotFoundWrap = styled.div`
  margin: auto;
  display: flex;
  height: 100%;
  align-items: center;
  color: #1e1e1e;
`;
let zChat = null;
class ChatHistory extends Component {
  state = {
    connectionStatus: null,
    accountConnectionStatus: null,
    historyMessages: this.props.pastChats.length ? this.props.pastChats : [],
    loadingMoreChats: false,
    moreChatsAvailable: false,
    loadCounter: 0
  };
  componentDidMount() {
    zChat = window.zChat;
    this.prevChats = this.props.pastChats.length
      ? [...this.props.pastChats]
      : [];
    const connectionStatus = zChat.getConnectionStatus();
    if (connectionStatus === "closed") this.reconnect();
    this.setState(
      {
        connectionStatus,
        accountConnectionStatus: zChat.getAccountStatus()
      },
      () => {
        const currentChatlog = zChat.getChatLog() || [];
        if (this.props.chatAvailable && this.prevChats.length == 0) {
          this.initEvents();
          this.fetchHistory();
        } else {
          setTimeout(() => {
            if (this.refs.chatHistory) {
              this.refs.chatHistory.scroll(
                0,
                this.refs.chatHistory.firstChild.offsetHeight
              );
            }
          }, 0);
        }
        if (currentChatlog.length > 0) {
          currentChatlog.forEach(data => this.handleChatInsertion(data, true));
        }
      }
    );
  }
  componentWillUnmount() {
    zChat.un("history", this.evtHandler);
  }
  prevChats = [];
  onEvent = (evt, data) => {
    this.handleChatInsertion(data);
  };
  checkIfChatExist = (oldChat, newChat) => {
    if (
      oldChat.timestamp === newChat.timestamp &&
      oldChat.type === newChat.type
    ) {
      if (oldChat.type === "chat.msg" && oldChat.msg === newChat.msg) {
        return true;
      } else if (
        oldChat.type === "chat.memberjoin" ||
        oldChat.type === "chat.memberleave" ||
        oldChat.type === "chat.rating"
      ) {
        return true;
      } else {
        return false;
      }
    }
  };

  insertPosition = -1;
  handleChatInsertion = (chatData, checkIfExist = false) => {
    if (checkIfExist) {
      if (this.prevChats.filter(x => this.checkIfChatExist(x, chatData)).length)
        return;
    }
    if (this.prevChats.length === 0 && this.props.pastChats.length === 0) {
      this.prevChats.push(chatData);
    } else {
      let positionAvailable = true;
      //first element will decide
      if (
        new Date(chatData.timestamp) > new Date(this.prevChats[0].timestamp)
      ) {
        //greater than 1st element //below existing element
        this.insertPosition = this.prevChats.findIndex(
          x => new Date(x.timestamp) > new Date(chatData.timestamp)
        ); // if index == -1 then push at last
      } else {
        //smaller than 1st element //above existing element
        this.prevChats.unshift(chatData);
        positionAvailable = false;
      }
      if (positionAvailable) {
        if (this.insertPosition == -1) {
          this.prevChats.push(chatData);
        } else {
          this.prevChats.splice(this.insertPosition, 0, chatData);
          this.insertPosition = -1;
        }
      }
    }
    const tempData = [...this.prevChats];
    tempData.forEach((data, index) => {
      if (data.type === "chat.memberjoin") {
        if (data.nick === "visitor" && data.first) {
          tempData[index] = {
            messageType: "date",
            msg: data.timestamp
          };
        } else {
          tempData[index] = this.handleMemberJoinObjects(data, "joined");
        }
      } else if (data.type === "chat.memberleave") {
        tempData[index] = this.handleMemberJoinObjects(data, "left");
      }
    });
    this.setState({ historyMessages: tempData, loadingMoreChats: false });
    this.props.handlePastChats(tempData);
    if (
      this.prevChats.length > 0 &&
      this.props.pastChats.length === 0 &&
      this.refs.chatHistory
    )
      this.refs.chatHistory.scroll(
        0,
        this.refs.chatHistory.firstChild.offsetHeight
      );
    if (this.state.loadCounter >= 1 && this.refs.chatHistory)
      this.refs.chatHistory.scroll(0, this.refs.chatHistory.offsetHeight);
  };
  handleMemberJoinObjects = (data, msg) => {
    const chatData = { ...data };
    if (data.type === "chat.memberleave" || data.type === "chat.memberjoin") {
      chatData.msg = `${chatData.display_name} has ${msg}.`;
      chatData.messageType = "info";
    }
    return chatData;
  };
  initEvents = () => {
    zChat.on(
      "history",
      (this.evtHandler = data => {
        this.onEvent("history", data);
      })
    );
  };
  reconnect = () => {
    zChat.reconnect();
  };
  prevChatAvailable = true;
  fetchHistory = () => {
    this.setState({ loadingMoreChats: true });
    const loadCounter = this.state.loadCounter + 1;
    zChat.fetchChatHistory((err, data) => {
      if (!err) {
        if (data.has_more) {
          this.setState({ moreChatsAvailable: data.has_more, loadCounter });
          this.prevChatAvailable = data.has_more;
        } else {
          this.setState({
            moreChatsAvailable: data.has_more,
            loadingMoreChats: false,
            loadCounter
          });
          this.prevChatAvailable = data.has_more;
          this.props.handlePastChats(null, data.has_more);
        }
      } else {
        this.setState({
          moreChatsAvailable: true,
          loadingMoreChats: false,
          loadCounter
        });
      }
    });
  };
  scrollEvent = e => {
    e.persist();
    if (!this.debouncedFn) {
      this.debouncedFn = debounce(() => {
        if (
          e.target &&
          e.target.scrollTop === 0 &&
          (this.state.moreChatsAvailable || this.props.chatAvailable)
        ) {
          if (this.prevChats.length && this.state.loadCounter < 1)
            this.initEvents();
          if (this.props.chatAvailable && this.prevChatAvailable)
            this.fetchHistory();
        }
      }, 300);
    }
    this.debouncedFn();
  };
  render() {
    return (
      <>
        {this.state.loadingMoreChats && (
          <Loading>
            Loading...
          </Loading>
        )}
        {this.prevChats.length > 0 && (
          <HistoryChatWrap ref="chatHistory" onScroll={this.scrollEvent}>
            <MessageList messages={this.state.historyMessages} />
          </HistoryChatWrap>
        )}
        {this.prevChats.length == 0 && !this.state.loadingMoreChats && (
          <HistoryNotFoundWrap>
            No past chats found.
          </HistoryNotFoundWrap>
        )}
      </>
    );
  }
}

ChatHistory.displayName = "ChatHistory";

export default ChatHistory;

import React from "react";
import _get from "lodash/get";
import styled from "styled-components";
// import { connect } from "react-redux";
// import { getAppConfigData } from "../../../reducers";
import { getSupportData } from "../utils/helpers/helper";

import conciergeIcon from "../assets/images/conciergeIcon.png";
import chatIcon from "../assets/images/chatIcon.png";
import phoneIcon from "../assets/images/phoneIcon.png";
import emailIcon from "../assets/images/emailIcon.png";
import emailCeoIcon from "../assets/images/emailCeoIcon.png";

const NeedHelp = ({
  student,
  handleHelpCurState,
  openHelpModal,
  appConfigData
}) => {
  //set student information here
  const studentStatus =
    student && student.trialStatus ? student.trialStatus : "";
  const { configs = null } = appConfigData;
  const { supportNumber, supportEmail } = getSupportData(
    configs
      ? studentStatus === "paid"
        ? configs["studentHelpDesk"]
        : configs["studentHelpDesk_Trial"]
      : {}
  );

  const HelpHeader = styled.div`
    display: -webkit-box;
    display: -webkit-flex;
    display: -ms-flexbox;
    display: flex;
    -webkit-align-items: center;
    -webkit-box-align: center;
    -ms-flex-align: center;
    align-items: center;
    padding-left: 20px;
    padding-right: 20px;
    color: black;
    background-color: white;
    font-size: 0.875em;
    position: relative;
    font-weight: bold;
    height: 69px;
    min-height: 40px;
    border-bottom: 1px solid rgb(0, 0, 0, 0.1);
    border-radius: 8px 8px 0px 0px;
    h1 {
      flex-grow: 1;
      // margin: 0px 10px 0px 25px;
      margin: 0px 10px 0px 14px;
      text-align: center;
      font-size: 18px;
      font-weight: 800;
      color: #1e1e1e;
    }
    .minimize-help {
      width: 30px;
      height: 40px;
      display: flex;
      justify-content: center;
      align-items: center;
      line-height: normal;
      background-color: transparent;
      border: none;
      padding: 0px;
      color: inherit;
      outline: none;
      > span {
        border-bottom: 2px solid #1e1e1e;
        width: 13px;
      }
    }
  `;

  const HelpContent = styled.div`
    // height: 74px;
    -webkit-align-items: center;
    -webkit-box-align: center;
    -ms-flex-align: center;
    align-items: center;
    padding-left: 24px;
    padding-right: 24px;
    cursor: pointer;
    h2 {
      font-weight: bold;
      font-size: 16px;
      color: #191919;
      // padding-bottom: 19px;
    }
    h4 {
      font-weight: normal;
      font-size: 12px;
      color: #191919;
      opacity: 0.7;
      // padding-top: 14px;
    }
    .text_black {
      text-decoration: none;
    }
    .text_black:hover {
      color: #191919;
    }
    .help-options {
      display: flex;
      flex-direction: row;
      flex-wrap: nowrap;
      justify-content: space-between;
      padding-top: 14px;
      padding-bottom: 14px;
      align-items: center;
    }
    :not(:last-child) .help-options {
      border-bottom: 1px solid rgb(0, 0, 0, 0.1);
    }
    .help-icons {
      right: 0;
    }
    .main-title {
      // border-bottom: 1px solid rgb(0,0,0, .1);
    }
    :hover {
      background-color: #f9f9f9;
    }
  `;

  return (
    <>
      <HelpHeader>
        <div className="concierge-icon">
          <img src={conciergeIcon} alt="Concierge" />
        </div>
        <h1>
        Your Personal Concierge
        </h1>
        <button
          className="minimize-help"
          onClick={openHelpModal.bind(this, false, true)}
        >
          <span></span>
        </button>
      </HelpHeader>
      <HelpContent onClick={handleHelpCurState.bind(this, "chat")}>
        <div className="help-options">
          <div className="help-texts">
            <h4>
            Chat - Instant Help
            </h4>
            <h2 className="main-title">
            Chat with us
            </h2>
          </div>
          <div className="help-icons">
            <img src={chatIcon} alt="Chat" />
          </div>
        </div>
      </HelpContent>
      <HelpContent>
        <div className="help-options">
          <div className="help-texts">
            <h4>
            Call - Wait time less than 30s
            </h4>
            <h2 className="main-title">
              <a className="text_black" href={`tel:${supportNumber}`}>
                {supportNumber}
              </a>
            </h2>
          </div>
          <div className="help-icons">
            <img src={phoneIcon} alt="Phone" />
          </div>
        </div>
      </HelpContent>
      <HelpContent onClick={handleHelpCurState.bind(this, "email")}>
        <div className="help-options">
          <div className="help-texts">
            <h4>
            Email - Takes upto 24 hours
            </h4>
            <h2 className="main-title">{supportEmail}</h2>
          </div>
          <div className="help-icons">
            <img src={emailIcon} alt="Email" />
          </div>
        </div>
      </HelpContent>
      <HelpContent onClick={handleHelpCurState.bind(this, "email-ceo")}>
        <div className="help-options">
          <div className="help-texts">
            <h4>
            No Resolution?
            </h4>
            <h2>
            Write to our CEO
            </h2>
          </div>
          <div className="help-icons">
            <img src={emailCeoIcon} alt="Email CEO" />
          </div>
        </div>
      </HelpContent>
    </>
  );
};

// const mapStateToProps = state => {
//   return {
//     appConfigData: getAppConfigData(state)
//   };
// };

// export default connect(mapStateToProps, null)(NeedHelp);

export default NeedHelp;

import React, { Fragment, Component } from "react";

// import Draggable from "react-draggable";

import FeedbackEscalation from "./FeedbackEscalation";

import PropTypes from "prop-types";

import "./style.scss";
import _get from "lodash/get";
import { setPaidStatus } from "../../utils/helpers/helper";
import { BUTTON_TYPE } from "../../utils/constants/segmentEvents";
import {
  ombudsmanCloseEventOnSegment,
  feedbackEscalationCloseEvent,
  successGrievanceSubmitEvent,
  ombudsmanOpenEventOnSegment
} from "./utils";
const escalateAvatar = require("../../assets/images/CEO_icon.svg");

// const RenderFeedbackEscalation = ({student, onSubmit }) => {

//   const {name , parentName, id: studentId} = student;

//   const [showEscalation, setEscalation] = useState(false);
//   //TODO: change
//   const {
//     escalateFeedbackComment = "",
//     onSuccessEscalation = false
//   } = {};

//   const textDesc = `I am Karan Bajaj, Founder and CEO of WhitehatJr. ${name} is the heart of our platform and we care deeply for you. Please raise any critical, unresolved issues, including service or ethical issues, directly to me.`;
//   const bottomDesc = `Your feedback will be 100% anonymous and acted upon promptly.`;
//   const feedbackInfo = {
//     toName: parentName,
//     textDesc,
//     bottomDesc
//   };

//   return (
//     <>
//       {showEscalation ? (
//         <FeedbackEscalation
//           feedbackInfo={feedbackInfo}
//           onSuccessEscalation={false}
//           closeFeedbackEscalation={() =>
//             {
//               setEscalation(!showEscalation);
//             }
//             // this.setState({
//             //   isShowFeedbackEscalation: !isShowFeedbackEscalation,
//             //   onSuccessEscalation: false,
//             //   escalateFeedbackComment: ""
//             // })
//           }
//           updateEscalateFeedback={event =>
//             this.setState({ escalateFeedbackComment: event.target.value })
//           }
//           submitEscalateFeedback={onSubmitEscalateFeedback}
//           escalateFeedbackComment={escalateFeedbackComment}
//         />
//       ) : null}
//     </>
//   );
// }

// RenderFeedbackEscalation.propTypes = {
// student: PropTypes.shape({id: PropTypes.string , name: PropTypes.string, parentName: PropTypes.string}),
// onSubmit: PropTypes.func
// }

// // TODO: replace onSubmit with noops
// RenderFeedbackEscalation.defaultProps = {
//   student: {},
//   onSubmit: () => {}
// };

class WriteToCeo extends Component {
  static propTypes = {
    nameInfo: PropTypes.shape({
      id: PropTypes.string,
      name: PropTypes.string,
      parentName: PropTypes.string
    }),
    // teacher: PropTypes.shape({
    //   name: PropTypes.string
    // }),
    submitFeedbackEscalation: PropTypes.func,
    onSubmit: PropTypes.func,
    isClickedFromHelpBox: PropTypes.bool,
    closeHelpHandler: PropTypes.func
  };

  state = {
    showPopOver: false,
    showEscalationPopup: false,
    comment: "",
    onSuccessEscalation: false
  };

  componentDidMount() {
    if (this.props.isClickedFromHelpBox) {
      this.setState({ showPopOver: false, showEscalationPopup: true });
    }
  }

  onSubmitEscalateFeedback = () => {
    const { nameInfo, submitFeedbackEscalation } = this.props;
    const { parentName, activeCourse } = nameInfo;
    const { comment = "" } = this.state;
    const id = _get(nameInfo, "id", null);
    const courseType = _get(activeCourse, "courseType", null);
    const submitData = {
      submittedById: id,
      submittedByType: !parentName ? `TEACHER` : `STUDENT`,
      message: comment,
      paidStatus: setPaidStatus(
        !parentName ? `TEACHER` : `STUDENT`,
        this.props.nameInfo
      ),
      courseType: parentName ? courseType : null
    };
    if (submitData && id) {
      const { ab_version, sendEvents } = this.props;
      // onSubmit(submitData);
      submitFeedbackEscalation(submitData);
      successGrievanceSubmitEvent({ ab_version, sendEvents });
      setTimeout(() => {
        this.setState({ onSuccessEscalation: true });
      }, 1000);
    }
  };

  closeFeedbackPopup = () => {
    // setEscalation(!showEscalation);
    const { ab_version, sendEvents } = this.props;
    this.setState({
      comment: "",
      showEscalationPopup: false,
      onSuccessEscalation: false
    });
    feedbackEscalationCloseEvent({ ab_version, sendEvents });

    //when ceo popup open from the needhelp box
    if (this.props.isClickedFromHelpBox) {
      this.props.closeHelpHandler("email-ceo", false);
    }
  };

  backToHelpBox = () => {
    const { ab_version, sendEvents } = this.props;
    this.setState({
      comment: "",
      showEscalationPopup: false,
      onSuccessEscalation: false
    });
    feedbackEscalationCloseEvent({ ab_version, sendEvents });

    //when ceo popup open from the needhelp box
    this.props.closeHelpHandler("email-ceo", true);
  };

  render() {
    const {
      state: {
        showPopOver,
        comment,
        showEscalationPopup,
        onSuccessEscalation = false
      },
      props: { nameInfo },
      onSubmitEscalateFeedback
    } = this;
    const { ab_version, sendEvents } = this.props;
    // const {name:teacherName} = teacher;
    const { name, parentName, id } = nameInfo;
    const textDesc = parentName ? (
      `${name} is the heart of our platform and we care deeply for you. Please raise any critical unresolved issues, including service or ethical issues, directly to our CEO.`
    ) : (
      `Teachers are the heart of our platform and we care deeply for you. Please raise any critical unresolved issues, including service or ethical issues, directly to our CEO.`
    );
    const bottomDesc = (
      `Your feedback will be 100% anonymous and acted upon promptly.`
    );
    const feedbackInfo = {
      toName: parentName || name,
      textDesc,
      bottomDesc
    };

    return (
      <Fragment>
        {showEscalationPopup && (
          <FeedbackEscalation
            hideAvatar={true}
            feedbackInfo={feedbackInfo}
            onSuccessEscalation={onSuccessEscalation}
            closeFeedbackEscalation={this.closeFeedbackPopup}
            updateEscalateFeedback={
              ({ target: { value } }) => {
                this.setState({ comment: value });
              }
              // this.setState({ escalateFeedbackComment: event.target.value })
            }
            submitEscalateFeedback={onSubmitEscalateFeedback}
            escalateFeedbackComment={comment}
            isClickedFromHelpBox={
              this.props.isClickedFromHelpBox ? true : false
            }
            backToHelpBox={this.backToHelpBox}
          />
        )}
        <div className={"parent"}>
          <Fragment>
            {!this.props.isClickedFromHelpBox && (
              <div className="parent-img-wrap">
                <img
                  className="cursor_pointer"
                  draggable={false}
                  src={escalateAvatar}
                  onClick={() => {
                    this.setState({ showEscalationPopup: true });
                    ombudsmanOpenEventOnSegment({
                      ab_version,
                      sendEvents,
                      button_type: BUTTON_TYPE.KARAN_FACE
                    });
                  }}
                />
              </div>
            )}

            {showPopOver && (
              <Fragment>
                <span
                  className="icon-close font10 cross-butn"
                  onClick={() => {
                    this.setState({ showPopOver: false });
                    ombudsmanCloseEventOnSegment({ ab_version, sendEvents });
                  }}
                />
                <div
                  onClick={() => {
                    this.setState({ showEscalationPopup: true });
                    ombudsmanOpenEventOnSegment({
                      ab_version,
                      sendEvents,
                      button_type: BUTTON_TYPE.RAISE
                    });
                  }}
                  className="text-left tooltip tooltip-right text_dec_none m-0"
                >
                  <div className="heading_bold font14">
                    Raise your Grievance
                  </div>
                  <div className="heading_reg font12">
                    Critical, Unresolved Issues? Write directly to CEO.
                  </div>
                </div>
              </Fragment>
            )}
          </Fragment>
        </div>
      </Fragment>
    );
  }
}

export default WriteToCeo;

import React, { useState } from "react";
import _throttle from "lodash/throttle";
import { CTA_THROTTLE_TIMEOUT } from "../../utils/config/global";
import styled from "styled-components";
import ButtonComponent from "./ButtonComponent";

const StyledLoaderContainer = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background: rgba(360, 360, 360, 0.6);
`;

const CustomThrottleButton = props => {
  const {
    throttleTime = CTA_THROTTLE_TIMEOUT,
    disabled = false,
    showOverlay = false,
    loadingFinished = false,
    newButton = false,
    type
  } = props;
  const [isClicked, setClickedButtonState] = useState(false);
  const clickEvent = e => {
    // setClickedButtonState(true);
    // props.onClick && props.onClick(e);
    // setTimeout(() => setClickedButtonState(false), throttleTime);
  };

  let ComponentName = newButton ? ButtonComponent : "button";

  return (
    <ComponentName
      {...props}
      onClick={_throttle(clickEvent, throttleTime)}
      disabled={isClicked || disabled}
      type={type}
    >
      {isClicked && !showOverlay && !loadingFinished ? (
        <span
          className="spinner-border spinner-border-sm"
          role="status"
          aria-hidden="true"
        />
      ) : (
        props.children
      )}
      {isClicked && showOverlay && !loadingFinished ? (
        <StyledLoaderContainer
          className={"d-flex align-items-center justify-content-center"}
        >
          <div
            className="spinner-border text-info spinner-border-sm"
            role="status"
            aria-hidden="true"
          />
        </StyledLoaderContainer>
      ) : null}
    </ComponentName>
  );
};

export default CustomThrottleButton;

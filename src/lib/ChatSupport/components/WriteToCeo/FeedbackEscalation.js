import React from "react";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import styled from "styled-components";
import CustomThrottleButton from "./CustomThrottleButton";
import avatarImage from "../../assets/images/escalateAvatar.png";
import helpDeskMessage from "../../assets/images/helpDeskMessage.png";
import helpDeskQuote from "../../assets/images/helpDeskQuote.png";
import backIcon from "../../assets/images/backIcon.svg";

const StyleFeedbackContainer = styled.div`
  background-color: #def5ff;
  padding: 15px;
  .rad4 {
    border-radius: 4px;
  }
  .content-block {
    display: block;
    padding: 0 15px;
  }
  .text_black_imp:focus {
    outline: none;
    border: 1px solid #ff6400;
  }
  .wrap-block {
    border: solid 1px #f5f5f5;
    background-color: #007ae1;
    border-radius: 68px;
    border-bottom-left-radius: 0;
  }
  .quote-block {
    width: 120px;
    text-align: left;
    position: absolute;
    left: -130px;
    top: -25px;
  }
  .align-text {
    text-align: justify;
  }
  .back-icon-email {
    flex: 0 0 auto;
    display: flex;
    width: 16px;
  }

  @media only screen and (max-width: 767px) {
    .content-block {
      padding: 0;
    }
    .quote-block {
      width: 50px;
      text-align: left;
      position: absolute;
      left: -20px;
      top: 0;
    }
    .align-text {
      text-align: left;
    }
  }
`;

const TakeBackButtonWrap = styled.div`
  position: absolute;
  padding: 20px 20px;
  cursor: pointer;
  left: 0;
`;

const FeedbackEscalation = props => {
  const isMobile = window.orientation > -1;

  return (
    <Dialog
      open={true}
      fullWidth={true}
      maxWidth={"md"}
      fullScreen={false}
      onClose={props.closeFeedbackEscalation}
    >
      <StyleFeedbackContainer>
        <DialogActions>
          {props.isClickedFromHelpBox && (
            <TakeBackButtonWrap onClick={props.backToHelpBox}>
              <img src={backIcon} className="back-icon-email" alt="back" />
            </TakeBackButtonWrap>
          )}
          <span
            className="icon-close font14"
            onClick={props.closeFeedbackEscalation}
          />
        </DialogActions>
        <div className="p-sm-0">
          <div className="row col-md-11 content-block d-flex justify-content-center align-items-center mx-auto">
            <div className="row col-md-12 heading_bold font18 d-flex align-items-center justify-content-center text-center py-5 px-3 wrap-block">
              {!props.hideAvatar && (
                <div className="col-md-5">
                  Avatar
                    {alt => (
                      <img src={avatarImage} alt={alt} className="w-75" />
                    )}
                </div>
              )}
              <div className="col-md-7">
                <div className="position-relative">
                  <img
                    src={helpDeskQuote}
                    className={`text-left position-absolute quote-block`}
                  />
                </div>
                <div className="heading_bold text_white font20 text-left my-3">
                  {`Dear ${props.feedbackInfo.toName}`}
                </div>

                {props.onSuccessEscalation ? (
                  <p className="heading_reg text_white font14 align-text">
                    I am Trupti Mukker, CEO of WhiteHatJr. Feel free to raise any major issues (including ethical issues) you found with respect to your experience on WhiteHatJr. Complete anonymity is ensured for all ethical issues.
                  </p>
                ) : (
                  <p className="heading_reg text_white font14 align-text">
                    {props.feedbackInfo.textDesc}
                    <br /> <br />
                    {props.feedbackInfo.bottomDesc}
                  </p>
                )}
              </div>
            </div>

            {!props.onSuccessEscalation ? (
              <>
                <div className="row col-md-12 pt-4 px-0">
                  <div className="heading_bold font20 mb-3">
                  Reach out to the CEO
                  </div>
                  Please let us know your detailed concerns
                    {placeholder => (
                      <textarea
                        name={"feedback"}
                        value={props.escalateFeedbackComment}
                        placeholder={placeholder}
                        className="heading_reg font14 chat_box_textarea rad4 text_black_imp p-3"
                        style={{ height: 110, borderRadius: "8px" }}
                        onChange={props.updateEscalateFeedback}
                      />
                    )}
                </div>
                <div className="col-12 my-3 text-right px-0">
                  <CustomThrottleButton
                    color="secondary"
                    className="btn btn_orange rad4 px-5 py-2 cursor_pointer"
                    onClick={props.submitEscalateFeedback}
                    disabled={!props.escalateFeedbackComment}
                  >
                    <div className="heading_bold font14">
                    SEND
                    </div>
                  </CustomThrottleButton>
                </div>
              </>
            ) : (
              <>
                <div className="row col-md-12 pt-4 px-0">
                  <div className="col-12 text-center mb-3">
                      Message
                      {text => <img src={helpDeskMessage} alt={text} />}
                  </div>
                  <div className="col-12 heading_reg text-center mb-3">
                    <div>
                    Your message has been received and my office
                    </div>
                    <div>
                    will personally look into your matter immediately.
                    </div>
                  </div>
                  <div className="col-12 text-center my-3 px-0">
                    <button
                      className="btn btn_orange rad4 px-5 py-2"
                      onClick={props.closeFeedbackEscalation}
                    >
                      <div className="heading_bold font14">
                      Close
                      </div>
                    </button>
                  </div>
                </div>
              </>
            )}
          </div>
        </div>
      </StyleFeedbackContainer>
    </Dialog>
  );
};

export default FeedbackEscalation;

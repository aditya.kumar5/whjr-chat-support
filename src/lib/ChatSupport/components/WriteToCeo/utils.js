import { sendEventsOnSegmentHOF } from "../../utils/helpers/sendEventOnSegment";
import {
  EVENT_NAMES,
  COMPONENT_POSITION,
  PAGE_SOURCE,
  BUTTON_TYPE,
  EVENT_CATEGORY,
  BUTTON_STATE
} from "../../utils/constants/segmentEvents";
export const ombudsmanCloseEventOnSegment = ({ ab_version, sendEvents }) => {
  sendEventsOnSegmentHOF({
    sendEvents,
    eventName: EVENT_NAMES.OMBUDSMAN_CLOSED,
    properties: {
      ab_version,
      position: COMPONENT_POSITION.LEFT_PANEL,
      event_category: EVENT_CATEGORY.OMBUDSMAN_CLOSED,
      page_source: PAGE_SOURCE.DASHBOARD,
      button_type: BUTTON_TYPE.CROSS,
      button_state: BUTTON_STATE.CLICKABLE
    }
  });
};
export const feedbackEscalationCloseEvent = ({ ab_version, sendEvents }) => {
  sendEventsOnSegmentHOF({
    sendEvents,
    eventName: EVENT_NAMES.GRIEVANCE_POPUP_CLOSED,
    properties: {
      ab_version,
      position: COMPONENT_POSITION.GRIEVANCE_POP_UP,
      event_category: EVENT_CATEGORY.OMBUDSMAN_OPEN,
      page_source: PAGE_SOURCE.DASHBOARD,
      button_type: BUTTON_TYPE.CROSS,
      button_state: BUTTON_STATE.CLICKABLE
    }
  });
};

export const successGrievanceSubmitEvent = ({ ab_version, sendEvents }) => {
  sendEventsOnSegmentHOF({
    sendEvents,
    eventName: EVENT_NAMES.GRIEVANCE_MESSAGE_SENT,
    properties: {
      ab_version,
      position: COMPONENT_POSITION.GRIEVANCE_POP_UP,
      event_category: EVENT_CATEGORY.OMBUDSMAN_OPEN,
      page_source: PAGE_SOURCE.DASHBOARD,
      button_state: BUTTON_STATE.CLICKABLE
    }
  });
};

export const ombudsmanOpenEventOnSegment = ({
  ab_version,
  sendEvents,
  buttonType
}) => {
  sendEventsOnSegmentHOF({
    sendEvents,
    eventName: EVENT_NAMES.OMBUDSMAN_OPENED,
    properties: {
      ab_version,
      position: COMPONENT_POSITION.LEFT_PANEL,
      event_category: EVENT_CATEGORY.OMBUDSMAN_OPEN,
      page_source: PAGE_SOURCE.DASHBOARD,
      button_state: BUTTON_STATE.CLICKABLE
    }
  });
};

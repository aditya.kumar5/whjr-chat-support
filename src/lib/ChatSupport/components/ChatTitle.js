import React from "react";
import PropTypes from "prop-types";
import { styled } from "styled-components";

import { TakeBackBtn } from "../styles";
import { ReactComponent as BackIcon } from "../assets/backIcon.svg";


const ChatTitle = ({
  chatState,
  resetChatState,
  openChat,
  isClickedFromHelpBox,
  closeHelpHandler
}) => {
  const ChatHeader = styled.div`
  display: -webkit-box;
  display: -webkit-flex;
  display: -ms-flexbox;
  display: flex;
  -webkit-align-items: center;
  -webkit-box-align: center;
  -ms-flex-align: center;
  align-items: center;
  padding-left: 20px;
  padding-right: 10px;
  color: ${props => props.theme.colors.secondary};
  background-color: ${props => props.theme.colors.primary};
  font-size: 0.875em;
  position: relative;
  font-weight: bold;
  height: 40px;
  min-height: 40px;
  h1 {
    flex-grow: 1;
    margin: 0px 10px 0px 25px;
    text-align: center;
    font-size: inherit;
    font-weight: inherit;
  }
  .minimize-chat {
    width: 30px;
    height: 40px;
    display: flex;
    justify-content: center;
    align-items: center;
    line-height: normal;
    background-color: transparent;
    border: none;
    padding: 0px;
    color: inherit;
    outline: none;
    > span {
      border-bottom: 2px solid #fff;
      width: 13px;
    }
  }
  .back-icon-chat {
    flex: 0 0 auto;
    display: flex;
    width: 16px;
    > path {
      fill: #fff;
    }
  }
`;

const TakeBackButton = styled(TakeBackBtn)`
  top: 16px;
  left: 20px;
  border-color: #fff;
`;

const TakeBackButtonWrap = styled.div`
  position: absolute;
  padding: 20px 20px;
  cursor: pointer;
  left: 0;
`;

  const backToHelpBox = () => {
    closeHelpHandler("chat", true);
  };
  return (
    <ChatHeader>
      {chatState === "chat-history" && (
        <TakeBackButtonWrap onClick={resetChatState}>
          <TakeBackButton />
        </TakeBackButtonWrap>
      )}
      {isClickedFromHelpBox && chatState !== "chat-history" && (
        <TakeBackButtonWrap onClick={backToHelpBox}>
          {/* <img
          src={backIcon}
          className="back-icon-chat"
          alt="back"
        ></img> */}
          {/* <BackIcon fill="#fff" /> */}
          <BackIcon className="back-icon-chat" fill="#fff" />
        </TakeBackButtonWrap>
      )}
      <h1>
      Chat with us
      </h1>
      <button className="minimize-chat" onClick={() => openChat(false)}>
        <span></span>
      </button>
    </ChatHeader>
  );
};

ChatTitle.displayName = "ChatTitle";

ChatTitle.propTypes = {
  chatState: PropTypes.string,
  resetChatState: PropTypes.func,
  openChat: PropTypes.func.isRequired
};

export default ChatTitle;

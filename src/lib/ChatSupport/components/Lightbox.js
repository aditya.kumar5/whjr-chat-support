import React, { PureComponent } from "react";
import { Modal } from "@material-ui/core";
import styled from "styled-components";
import RightArrow from "@material-ui/icons/ChevronRight";
import LeftArrow from "@material-ui/icons/ChevronLeft";

const StyledContainer = styled.div`
  position: fixed;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  width: 55vw;
  height: 50vh;
  border-radius: 8px;
  background-color: rgba(255, 255, 255, 0.8);
  .media {
    position: relative;
    width: 100%;
    height: 100%;
    overflow: hidden;
  }
  img,
  video,
  iframe {
    width: 100%;
    height: 100%;
    max-width: 100%;
    max-height: 100%;
    border-radius: 8px;
  }
  audio {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
  }
  .icon-close {
    position: absolute;
    right: -20px;
    top: -20px;
  }
  .prev,
  .next {
    position: absolute;
    top: 50%;
    transform: translate(-50%, -50%);
    color: #fff;
    &:hover {
      cursor: pointer;
    }
    svg {
      font-size: 5rem;
    }
  }
  .prev {
    left: -40px;
  }
  .next {
    right: -120px;
  }
  .object-contain {
    object-fit: contain;
  }

  @media only screen and (max-width: 992px) {
    width: 75vw;
  }
  @media only screen and (max-width: 767px) {
    .prev {
      left: -25px;
    }
    .next {
      right: -95px;
    }
  }
`;

class Lightbox extends PureComponent {
  state = {
    curItem: null,
    curIndex: null
  };
  componentDidMount() {
    if (this.props.media.length) {
      this.setState({ curItem: this.props.media[0], curIndex: 0 });
    }
  }
  changeItem = step => {
    const next = this.state.curIndex + step;
    if (step < 0 && this.state.curIndex > 0) {
      this.setState({ curItem: this.props.media[next], curIndex: next });
    } else if (step > 0 && this.state.curIndex < this.props.media.length - 1) {
      this.setState({ curItem: this.props.media[next], curIndex: next });
    }
  };
  render() {
    return (
      <Modal
        open={true}
        // onClose={this.props.handleClose}
        aria-labelledby="media carousel"
        aria-describedby="media carousel"
        style={{
          zIndex: "1000000"
        }}
      >
        <StyledContainer>
          <span
            // onClick={() => this.props.handleClose()}
            className="icon-close text_disabled cursor_pointer"
          />
          {this.props.media.length && this.props.media.length > 1 && (
            <span className="prev" onClick={() => this.changeItem(-1)}>
              <LeftArrow />
            </span>
          )}
          {this.props.media.length && this.props.media.length > 1 && (
            <span className="next" onClick={() => this.changeItem(1)}>
              <RightArrow />
            </span>
          )}
          {this.state.curItem && (
            <div className="media">
              {(() => {
                if (this.state.curItem.type.includes("video")) {
                  if (!this.state.curItem.src.includes("youtube")) {
                    return (
                      <video controls>
                        <source src={this.state.curItem.src} type="video/mp3" />
                        <source src={this.state.curItem.src} type="video/mp4" />
                        <source src={this.state.curItem.src} type="video/mov" />
                        <source src={this.state.curItem.src} type="video/avi" />
                        <source src={this.state.curItem.src} type="video/ogg" />
                        <source
                          src={this.state.curItem.src}
                          type="video/webm"
                        />
                        Sorry, your browser doesn't support embedded videos.
                      </video>
                    );
                  } else {
                    return (
                      <iframe
                        src={this.state.curItem.src}
                        frameBorder="0"
                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                      />
                    );
                  }
                } else if (this.state.curItem.type.includes("audio")) {
                  return (
                    <audio controls>
                      <source src={this.state.curItem.src} type="audio/mp3" />
                      <source src={this.state.curItem.src} type="audio/mp4" />
                      Your browser does not support this audio.
                    </audio>
                  );
                } else {
                  return (
                    <img
                      className="object-contain"
                      alt={"Project Image"}
                      src={this.state.curItem.src}
                    />
                  );
                }
              })()}
            </div>
          )}
        </StyledContainer>
      </Modal>
    );
  }
}

export default Lightbox;

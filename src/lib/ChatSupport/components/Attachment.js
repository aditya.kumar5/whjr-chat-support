import React, { useState } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import doc from "../assets/images/doc.png";
import ppt from "../assets/images/ppt.png";
import pdf from "../assets/images/pdf.png";
import video from "../assets/images/video.png";
import xls from "../assets/images/xls.png";
import audio from "../assets/images/audio.png";
import image from "../assets/images/img.png";
import attachment from "../assets/images/attachment.png";
import Lightbox from "./Lightbox";

const ImageAttachment = styled.img`
  width: 100%;
  cursor: pointer;
  border-radius: 8px;
`;
const ImageAttachments = styled(ImageAttachment)`
  &.agent-image {
    border-top-left-radius: 0;
  }
  &.user-image {
    border-top-right-radius: 0;
  }
`;
const AttachmentWrap = styled.div`
  display: flex;
  align-items: center;
  padding: 8px 10px;
  border-radius: 8px;
  font-size: 0.75em;
  font-weight: 600;
  word-break: break-word;
  white-space: pre-line;
  justify-content: space-between;
  cursor: pointer;
`;
const AttachmentWraps = styled(AttachmentWrap)`
  &.agent-attachment {
    margin-right: auto;
    background-color: #fff;
    border-top-left-radius: 0px;
    border: solid 1px #e4e4e4;
  }
  &.user-attachment {
    margin-left: auto;
    background-color: #e8eaec;
    border-top-right-radius: 0px;
  }
`;
const AttachmentImage = styled.img`
  width: 24px;
  height: 27px;
`;
const AttachmentContentWrap = styled.div`
  max-width: 84%;
`;
const AttachmentTitle = styled.div`
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  width: 130px;
`;
const AttachmentSubtitle = styled.div`
  display: flex;
  align-items: center;
  &.agent-subtitle {
    justify-content: flex-start;
  }
  &.user-subtitle {
    justify-content: flex-end;
  }
`;
const AttachmentDownload = styled.a`
  font-size: 0.825em;
  margin-left: 10px;
  color: #007bff !important;
`;
const AttachmentSize = styled.span`
  text-transform: uppercase;
`;
const ProgressBar = styled.div`
  width: 100%;
  height: 3px;
  background-color: #fff;
  border-radius: 1.5px;
  margin: 10px 0 6px;
`;
const ProgressBarInner = styled.div`
  height: 100%;
  background-color: #266ad1;
  border-radius: 1.5px;
  width: 0%;
`;
const Attachment = ({
  url,
  name,
  mime_type,
  size,
  type,
  msgType,
  progressBarWidth,
  err
}) => {
  const getFileImageBasisExtension = () => {
    if (mime_type.includes("audio")) return audio;
    else if (mime_type.includes("video")) return video;
    else if (mime_type.includes("pdf")) return pdf;
    else if (
      mime_type.includes("msword") ||
      mime_type.includes("wordprocessingml")
    )
      return doc;
    else if (
      mime_type.includes("excel") ||
      mime_type.includes("spreadsheetml") ||
      mime_type.includes("csv")
    )
      return xls;
    else if (
      mime_type.includes("powerpoint") ||
      mime_type.includes("presentationml")
    )
      return ppt;
    else if (mime_type.includes("image")) return image;
    else return attachment;
  };
  const handleMediaShow = () => {
    // if (url) {
    //   if (
    //     !mime_type.includes("audio") &&
    //     !mime_type.includes("video") &&
    //     !mime_type.includes("image")
    //   )
    //     return;
    //   const mediaObj = {
    //     type: mime_type,
    //     src: url,
    //     name: name
    //   };
    //   setMedia(new Array(mediaObj));
    //   setShowLightBox(true);
    // }
  };
  const [media, setMedia] = useState([]);
  const [showLightBox, setShowLightBox] = useState(false);
  const handleLightBoxClose = () => {
    // setShowLightBox(false);
    // setMedia([]);
  };
  const handleFileDownload = e => {
    e.persist();
    e.stopPropagation();
    setTimeout(() => {
      e.target.href = url;
    }, 0);
  };
  const barWidth = {
    width: progressBarWidth,
    transition: "width 3s"
  };
  return (
    <>
      {mime_type.includes("image") && url && msgType == "chat.file" && (
        <ImageAttachments
          src={url}
          alt={name}
          onClick={() => handleMediaShow()}
          className={type === "agent" ? "agent-image" : "user-image"}
        />
      )}
      {((msgType == "chat.file" && !mime_type.includes("image")) ||
        (mime_type.includes("image") && !msgType)) && (
        <AttachmentWraps
          className={type === "agent" ? "agent-attachment" : "user-attachment"}
          onClick={() => handleMediaShow()}
        >
          <AttachmentImage src={getFileImageBasisExtension()} />
          <AttachmentContentWrap>
            <AttachmentTitle>{name}</AttachmentTitle>
            <AttachmentSubtitle
              className={type === "agent" ? "agent-subtitle" : "user-subtitle"}
            >
              {url ? (
                <>
                  <AttachmentSize>
                    {(size / 1000000).toFixed(2)} MB
                  </AttachmentSize>
                  <AttachmentDownload
                    download={name}
                    onClick={e => handleFileDownload(e)}
                  >
                    Download
                  </AttachmentDownload>
                </>
              ) : (
                <ProgressBar hidden={err}>
                  <ProgressBarInner style={barWidth}></ProgressBarInner>
                </ProgressBar>
              )}
            </AttachmentSubtitle>
          </AttachmentContentWrap>
        </AttachmentWraps>
      )}
      {showLightBox && (
        <Lightbox media={media} handleClose={handleLightBoxClose} />
      )}
    </>
  );
};

Attachment.displayName = "Attachment";

Attachment.propTypes = {
  url: PropTypes.string,
  name: PropTypes.string,
  mime_type: PropTypes.string.isRequired,
  size: PropTypes.number,
  type: PropTypes.string.isRequired,
  progressBarWidth: PropTypes.string,
  err: PropTypes.string
};

export default Attachment;

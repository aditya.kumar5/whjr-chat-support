import React, { Component, ref } from "react";
import PropTypes from "prop-types";
import DecisionTreeAB from "./DecisionTreeAB";
import {
  ChatContainer,
  ChatHead
} from "../../styles";
import _get from "lodash/get";
import moment from "moment-timezone";
import { getCustomLogger } from "../../utils/helpers/logger";

const PRECHAT_ACTION = {
  CHATBOT: "CUSTOM_CHATBOT",
  INTERNET_SPEED: "CUSTOM_INTERNET_SPEED",
  CUSTOM_ZENDESK: "CUSTOM_ZENDESK",
  ROUTING: "ROUTING"
};
let enableOptionTimer = null;
const STUDENT_NOT_JOINED = "student has not joined";
const chatLogSpanId = "PRECHAT_AB";
const prechatABlogger = getCustomLogger("prechatABlogger");
class Prechat_AB extends Component {
  state = {
    disableOption: false
  };

  componentDidUpdate() {
    if (_get(this.props, "issueData_AB", []).length) {
      this.handleChatbotBtnEnable();
    }
  }

  componentDidMount(){
    try{
      if (_get(this.props, "issueData_AB", []).length) {
        this.handleChatbotBtnEnable();
        prechatABlogger && prechatABlogger.debug(`${chatLogSpanId} issues loaded : ${JSON.stringify(_get(this.props, "issueData_AB", []))}`);
      }
    }catch(e){
      prechatABlogger && prechatABlogger.debug(`${chatLogSpanId} error in parsing issues`);
    }
  }

  componentWillUnmount(){
    clearTimeout(enableOptionTimer);
    enableOptionTimer = null;
  }

  handleChatbotBtnEnable = () => {
    const classStart = moment(this.props.classStartTime);
    const diffInTimeFromClassStart = moment().diff(classStart, "minutes");
    if (moment()>=classStart &&  //after class start
    diffInTimeFromClassStart < 5 && !this.state.disableOption
    ) {
      if(!enableOptionTimer) {
        this.setState({
          disableOption: true
        });
        const timeDiff = 5-diffInTimeFromClassStart
        enableOptionTimer = setTimeout(this.handleDisableOption, 1000 * 60 * timeDiff);
      }
    }else if(moment()<classStart && !this.state.disableOption){ // before class start
      this.setState({
        disableOption: true
      });
      const diffInTimeBeforeClassStart = classStart.diff(moment(), "minutes") + 5;
      enableOptionTimer = setTimeout(this.handleDisableOption, 1000 * 60 * diffInTimeBeforeClassStart);
      }
  };

  handleDisableOption = () => {
    this.setState({ disableOption: false });
    clearTimeout(enableOptionTimer);
    enableOptionTimer = null;
  }

  handlePastChats = () => {
    this.props.chatTrigger("chat-history", null);
  };
  handleChatState = (action = "CUSTOM", type = "") => {
    //TODO: define custom action above before adding a case
    switch (action) {
      case PRECHAT_ACTION.CHATBOT:
        this.props.chatBotShow();
        this.props.hideChat(false);
        break;
      case PRECHAT_ACTION.INTERNET_SPEED:
        this.props.chatTrigger("tech-issue", null);
        break;
      case PRECHAT_ACTION.CUSTOM_ZENDESK:
        //TODO: direct chat initialize
        break;
      case PRECHAT_ACTION.ROUTING:
        this.props.chatTrigger("prechat", null);
        break;
    }
  };

  handleAllIssues = (index = 0) => {
    const issue = this.props.issueData_AB[index];
    if (
      (this.state.disableOption) &&
      issue.name.toLowerCase() === STUDENT_NOT_JOINED
    ) {
      return;
    }
    const issueSelected = this.props.issueData_AB[index];
    this.handleChatState(issueSelected.action);
  };

  render() {
    return (
      <>
        <ChatContainer>
          <ChatHead>
          Select an issue type
          </ChatHead>
          <DecisionTreeAB
            issues={this.props.issueData_AB}
            chatIssueHandler={this.handleAllIssues}
            disableOption={this.state.disableOption}
            studentNotJoined={STUDENT_NOT_JOINED}
            studentJoined={this.props.studentJoined}
          />
        </ChatContainer>
      </>
    );
  }
}

Prechat_AB.displayName = "Prechat";

Prechat_AB.propTypes = {
  startChat: PropTypes.func,
  issueData: PropTypes.object,
  showHistoryBtn: PropTypes.bool,
  chatTrigger: PropTypes.func
};

export default Prechat_AB;

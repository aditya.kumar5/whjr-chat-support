import React from "react";
import PropTypes from "prop-types";
import { UnorderedList, List } from "../../styles";
import styled from "styled-components";
import moment from "moment-timezone";

const RaiseRequest = styled("span")`
  font-size: 14px;
  color: #a461d8;
  position: relative;
  left: 18px;
  :before {
    content: "";
    background-color: #a461d8;
    height: 10px;
    width: 10px;
    position: absolute;
    border-radius: 50%;
    top: 6px;
    left: -14px;
    border: 1px solid #f6effb;
    animation: 1s blink ease infinite;
  }
  @keyframes blink {
    from,
    to {
      opacity: 0;
    }
    50% {
      opacity: 1;
    }
  }
`;

const DecisionTreeAB = ({
  issues,
  chatIssueHandler,
  disableOption,
  studentNotJoined
}) => {
  return (
    <>
      <UnorderedList>
        {issues &&
          issues.map(
            (issue, index) =>
              issue.name && (
                <List
                  onClick={() => chatIssueHandler(index)}
                  key={index}
                  className={studentNotJoined === issue.name.toLowerCase() && disableOption ? "disabledOption" : "" }
                >
                  {issue.name}{" "}
                  <RaiseRequest hidden={!issue.highlight}>
                    {issue.highlight}
                  </RaiseRequest>
                </List>
              )
          )}
      </UnorderedList>
    </>
  );
};

DecisionTreeAB.displayName = "DecisionTreeAB";

DecisionTreeAB.defaultProps = {};

DecisionTreeAB.propTypes = {
  issues: PropTypes.array,
  chatIssueHandler: PropTypes.func
};

export default DecisionTreeAB;

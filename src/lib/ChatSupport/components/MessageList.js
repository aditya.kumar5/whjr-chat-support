import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

import { UserIcon, LoadingBubble, Button } from "../styles";
import Message from "./Message";

const MessageListContainer = styled.div`
  position: relative;
  display: flex;
  flex-grow: 1;
  overflow-x: hidden;
  overflow-y: scroll;
  -webkit-overflow-scrolling: touch;
  > div {
    margin-top: auto;
    padding: 10px 16px;
    display: flex;
    flex-direction: column;
    align-items: stretch;
    overflow-y: auto;
    overflow-x: hidden;
    width: 100%;
    height: 100%;
  }
`;

const MessageItem = styled.div`
  display: flex;
  &:first-child {
    margin-top: auto;
  }
  &.agent-message {
    margin-left: 38px;
  }
`;

const AgentIcon = styled(UserIcon)`
  align-self: flex-end;
  margin-left: -38px;
  margin-right: 6px;
  margin-bottom: 6px;
`;

const StatusMessage = styled.div`
  margin: 15px 0px 0px 0px;
  font-size: 0.75em;
  font-weight: 700;
  color: ${props => props.theme.colors.textColor.secondary};
  text-align: center;
`;

const Reconnecting = styled(LoadingBubble)`
  bottom: 15px;
`;

const Retry = styled(Button)`
  background-color: transparent;
  border: none;
  color: #266ad1;
  text-decoration: underline;
`;

const MessageList = ({
  messages = [],
  connectionStatus = "",
  accountConnectionStatus = "",
  statusMsg = "",
  resendMessage,
  reconnect,
  barWidth
}) => (
  <MessageListContainer>
    <div>
      {messages.map((message, index) => (
        <MessageItem
          key={index}
          className={`${
            (message.type === "chat.msg" ||
              message.type === "chat.file" ||
              (message.type === "typing" && message.display_name)) &&
            message.nick &&
            message.nick !== "visitor"
              ? "agent-message"
              : ""
          }`}
        >
          {(message.type === "chat.msg" ||
            message.type === "chat.file" ||
            message.type === "typing") &&
            message.nick &&
            message.nick !== "visitor" &&
            message.display_name &&
            (index === messages.length - 1 ||
              (messages[index + 1].type !== "chat.msg" &&
                messages[index + 1].type !== "typing") ||
              (messages[index + 1].nick &&
                messages[index + 1].nick === "visitor") ||
              message.nick !== messages[index + 1].nick) && (
              <AgentIcon>
                {message.display_name.charAt(0).toUpperCase()}
              </AgentIcon>
            )}
          <Message
            message={message}
            resendMessage={() => {
              resendMessage(index);
            }}
            barWidth={barWidth}
          />
        </MessageItem>
      ))}
      {statusMsg && <StatusMessage>{statusMsg}</StatusMessage>}
      {connectionStatus === "closed" && (
        <StatusMessage>
          Unable to Connect -{" "}
          <Retry onClick={() => reconnect()}>
          Retry now
          </Retry>
        </StatusMessage>
      )}
      {(connectionStatus === "connecting" ||
        accountConnectionStatus === "offline" ||
        accountConnectionStatus === "away") && (
        <Reconnecting>
          Reconnecting{" "}
        </Reconnecting>
      )}
    </div>
  </MessageListContainer>
);

MessageList.displayName = "MessageList";

MessageList.propTypes = {
  messages: PropTypes.array.isRequired,
  statusMsg: PropTypes.string,
  resendMessage: PropTypes.func,
  connectionStatus: PropTypes.string,
  accountConnectionStatus: PropTypes.string,
  reconnect: PropTypes.func,
  barWidth: PropTypes.string
};

export default MessageList;

import React from "react";
import PropTypes from "prop-types";
import { UnorderedList, List } from "../../styles";

const DecisionTreeSelector = ({
  issues,
  chatIssueHandler,
  prechatABdata = []
}) => {
  return (
    <>
      <UnorderedList>
        {issues &&
          issues.map(
            (issue, index) =>
              issue.name && (
                <List
                  onClick={() => chatIssueHandler(index)}
                  key={index}
                  hidden={
                    prechatABdata.filter(
                      x => x.name.toLowerCase() === issue.name.toLowerCase()
                    ).length > 0
                  }
                >
                  {issue.name}
                </List>
              )
          )}
      </UnorderedList>
    </>
  );
};

DecisionTreeSelector.displayName = "DecisionTreeSelector";

DecisionTreeSelector.defaultProps = {};

DecisionTreeSelector.propTypes = {
  issues: PropTypes.array,
  chatIssueHandler: PropTypes.func,
};

export default DecisionTreeSelector;

import React, { Component } from "react";
import PropTypes from "prop-types";
import {
  ChatContainer,
  ChatHead,
  ChatBody,
  BackButton,
  IssueTitle, 
  PastChatBtn,
  PastChatIcon
} from "../../styles";
import DecisionTreeSelector from "./DecisionTreeSelector";
import pastChat from "../../assets/images/past.png";
import _get from "lodash/get";
import ChatIntermediateStep from "../ChatIntermediateStep";
import { EVENT_NAMES, BUTTON_TYPE } from "../../utils/constants/segmentEvents";
import { checkReschedule } from "../../utils/constants/constant";

const chatLogSpanId = "ZENDESK CHAT";
class Prechat extends Component {
  state = {
    selected: false,
    selectedIndex: -1,
    issues: this.props.issueData ? this.props.issueData.issue_category : [],
    selectedIssue: "",
    selectedSubIssue: null,
    modal: false
  };

  componentDidUpdate(prevProps, currState) {
    if (
      this.props.issueData &&
      this.handleArrayComarision(
        this.props.issueData.issue_category,
        currState.issues
      )
    ) {
      this.handleStateReset();
    }
  }

  handleArrayComarision = (arr1, arr2) => {
    let flag = false;
    if (arr1 && arr2 && arr1.length != arr2.length) return true;
    if (arr1 && arr2 && arr1.length == arr2.length) {
      for (let i = 0; i < arr1.length; i++) {
        if (arr1[i].issue_sub_category && arr2[i].issue_sub_category) {
          if (
            arr1[i].issue_sub_category.length !=
            arr2[i].issue_sub_category.length
          ) {
            flag = true;
            return flag;
          } else {
            for (let j = 0; j < arr1[i].issue_sub_category.length; j++) {
              if (
                arr1[i].issue_sub_category[j]._id &&
                arr2[i].issue_sub_category[j]._id &&
                arr1[i].issue_sub_category[j]._id !=
                  arr2[i].issue_sub_category[j]._id
              )
                flag = true;
            }
          }
        }
      }
    }
    if (arr1 && arr2 && arr1.length && arr2.length) {
      for (let i = 0; i < arr1.length; i++) {
        if (arr1[i]._id != arr2[i]._id) return true;
      }
    }
    if (arr1 && !arr2) {
      return true;
    }
    return flag;
  };

  handleSelectedSubIssue = (index) => {
    try {
      const department = this.state.issues[index].support_dept;
      this.props.chatLogger &&
        this.props.chatLogger.debug(`${chatLogSpanId} : sub-issue select`, {
          chatData: {
            connectionStatus: window.zChat
              ? window.zChat.getConnectionStatus()
              : "not initialized",
            issueCount: this.state.issues ? this.state.issues.length : 0,
            issueList: JSON.stringify(this.state.issues),
            userSelection: {
              issueName: this.state.issues[index].name,
              deptName: _get(department, "name", null),
              deptId: _get(department, "dept_id", null),
            }
          }
        });
      this.handleStartChat(department, index);
    } catch (e) {
      console.error(e);
      this.props.chatLogger &&
        this.props.chatLogger.error(
          `${chatLogSpanId} : Error in handleSelectedSubIssue()`,
          JSON.stringify(e)
        );
    }
  };

  handleStartChat = (department, index = null, status) => {
    this.props.chatTrigger("start-chat", {
      deptId: department.dept_id,
      deptName: department.name,
      issueType: this.state.selectedIssue,
      issueDetail: status
        ? this.state.selectedSubIssue.name
        : index != null && index >= 0
        ? this.state.issues[index].name
        : ""
    });
  };

  handlingStateReset = () => {
    this.setState({
      selected: false,
      selectedIndex: -1,
      issues:
        this.props.issueData && this.props.issueData.issue_category
          ? this.props.issueData.issue_category
          : [],
    });
    if (!this.state.selected && this.state.selectedIndex === -1)
      this.props.chatLogger &&
        this.props.chatLogger.debug(
          `${chatLogSpanId} : handling back from sub-issue to issue list`
        );
  };

  handlingStateReset = () => {
    this.setState({
      selected: false,
      selectedIndex: -1,
      issues:
        this.props.issueData && this.props.issueData.issue_category
          ? this.props.issueData.issue_category
          : []
    });
    if (!this.state.selected && this.state.selectedIndex === -1)
      this.props.chatLogger &&
        this.props.chatLogger.debug(
          `${chatLogSpanId} : handling back from sub-issue to issue list`
        );
  };

  handleStateReset = (layer = null) => {
    try {
      if (
        _get(this.props, "prechatABlayer", []).length &&
        !this.state.selected &&
        layer
      ) {
        this.props.chatTrigger("prechat_ab", null);
      } else {
        this.handlingStateReset();
      }
    } catch (e) {
      console.error(e);
      this.props.chatLogger &&
        this.props.chatLogger.error(
          `${chatLogSpanId} : Error in handleStateReset()`,
          JSON.stringify(e)
        );
    }
  };

  handleReschedule = (data, check) => {
    this.setState({ selectedSubIssue: data, modal: !this.state.modal });
    this.props.openChat(check);
  };

  handleAllIssues = (index) => {
    try {
      if (this.state.selected) {
        //subIssues
        let data = this.state.issues[index];
        if (
          // to check if teacher wasnts to reschedule (Reschedule POPUP)
          this.props.inClassReschedule &&
          checkReschedule.includes(data.name.toLowerCase())
        ) {
          data.index = index;
          this.handleReschedule(data, false);
        } else this.handleSelectedSubIssue(index);
      } else {
        const data = this.state.issues[index].issue_sub_category;
        if (data.length == 1 && !data[0].name) {
          //in case if there are no subIssues just issue + that issue should be unique
          const departmentData = data[0].support_dept;
          this.setState(
            { selectedIssue: this.state.issues[index].name },
            () => {
              this.handleStartChat(departmentData);
            }
          );
        } else if (data.length == 1 && data[0].name) {
          //if we have 1 issue sub category then user should be directed to chat w/o selecting subIssue
          const departmentData = data[0].support_dept;
          const selectedIssue = this.state.issues[index].name;
          this.setState({ selectedIssue, issues: data }, () => {
            this.handleStartChat(departmentData, 0);
          });
        } else {
          const selectedIssue = this.state.issues[index].name;
          this.setState({
            selected: true,
            issues: data,
            selectedIndex: index,
            selectedIssue
          });
        }
        this.props.chatLogger &&
          this.props.chatLogger.debug(`${chatLogSpanId} : issue select`, {
            chatData: {
              connectionStatus: window.zChat
                ? window.zChat.getConnectionStatus()
                : "not initialized",
              issueCount: this.state.issues ? this.state.issues.length : 0,
              issueList: JSON.stringify(this.state.issues),
              userSelection: {
                issueName: this.state.issues[index].name,
              }
            }
          });

        if (this.props.handleEventTrack) {
          const args = {
            button_type: BUTTON_TYPE.ARROW,
            concierge_issue_type: this.state.issues[index].name
          };
          this.props.handleEventTrack(
            EVENT_NAMES.CONCIERGE_ISSUE_CLICKED,
            args
          );
        }
      }
    } catch (e) {
      this.props.chatLogger &&
        this.props.chatLogger.error(
          `${chatLogSpanId} : Error in handleAllIssues()`,
          JSON.stringify(e)
        );
    }
  };

  handlePastChats = () => {
    this.props.chatTrigger("chat-history", null);
  };

  render() {
    return (
      <>
        <ChatContainer>
          {_get(this.props, "prechatABlayer", []).length ? (
            <ChatBody>
              <BackButton
                onClick={() => this.handleStateReset("prechatLayer")}
              />
              {!this.state.selected ? (
                <IssueTitle>
                  Select an issue type
                </IssueTitle>
              ) : (
                <IssueTitle>
                  What issue are you facing?
                </IssueTitle>
              )}
            </ChatBody>
          ) : !this.state.selected ? (
            <ChatHead>
              Select an issue type
            </ChatHead>
          ) : (
            <ChatBody>
              <BackButton onClick={() => this.handleStateReset()} />
              <IssueTitle>
              What issue are you facing?
              </IssueTitle>
            </ChatBody>
          )}
          <DecisionTreeSelector
            issues={this.state.issues}
            chatIssueHandler={this.handleAllIssues}
            prechatABdata={_get(this.props, "prechatABlayer", [])}
          />
          {this.props.showHistoryBtn && (
            <PastChatBtn onClick={() => this.handlePastChats()}>
              <PastChatIcon src={pastChat} />
              View past chats
            </PastChatBtn>
          )}
          {this.props.inClassReschedule && (
            <ChatIntermediateStep
              handleReschedulePopup={(status) => {
                this.props.handleReschedulePopup(status);
              }}
              data={this.state.selectedSubIssue}
              modal={this.state.modal}
              handleReschedule={this.handleReschedule}
              handleStartChat={this.handleStartChat}
            />
          )}
        </ChatContainer>
      </>
    );
  }
}

Prechat.displayName = "Prechat";

Prechat.propTypes = {
  startChat: PropTypes.func,
  issueData: PropTypes.object,
  showHistoryBtn: PropTypes.bool,
  chatTrigger: PropTypes.func,
  chatLogger: PropTypes.object,
  inClassReschedule: PropTypes.bool
};

export default Prechat;

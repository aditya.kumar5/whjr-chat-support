import React, { Component } from "react";
import PropTypes from "prop-types";
import styled, { keyframes } from "styled-components";
// import { connect } from "react-redux";
// import { withRouter } from "react-router-dom";
import moment from "moment";
import _get from "lodash/get";
import _isEqual from "lodash/isEqual";
import Theme from "./theme";
import ChatBubble from "./components/ChatBubble";
import ChatTitle from "./components/ChatTitle";
import Prechat from "./components/Prechat";
import Prechat_AB from "./components/Prechat_AB";
import InternetSpeedTest from "./components/InternetSpeedTest";
import Chat from "./components/Chat";
import OfflineChat from "./components/OfflineChat";
import ChatHistory from "./components/ChatHistory";
// import {
//   otpDialogShow,
//   submitFeedbackEscalation,
//   segmentEvents,
//   triggerChat,
//   endClassTeacher,
//   navigateToPath,
//   refreshSessionClass,
//   fetchTypeformResponse,
//   chatbotAction,
//   chatMinimizeForToggle,
//   classNotCompletedPopup
// } from "../../actions";
// import { s__getStudent } from "../../selectors/student";
// import {
//   s__getTeacher,
//   s__isReportCardFeedbackQuestionsAvailable
// } from "../../selectors/teacher";
// import { isParentalVerified } from "../../selectors/authUser";
// import { s__featureConfig } from "../../selectors/features";

// import {
//   getClassInterfacingDetails,
//   getAppConfigData,
//   getTeacherUpcomingClass
// } from "../../reducers";
// import {
//   eventSegmentProperties,
//   eventUserSegmentProperties
// } from "../../helpers/sendEventSegment";
import NeedHelp from "./components/NeedHelp";
import ConciergeHelpDesk from "./components/ConciergeHelpDesk";
import WriteToCeo from "./components/WriteToCeo";
import { getCustomLogger } from "./utils/helpers/logger";
import { setCookie, getCookie } from "./utils/helpers/utm";
import { R__JOIN_CLASS, T__JOIN_CLASS } from "./utils/constants/routesMap";
import NetworkSpeed from "network-speed";
import {
  AB_VERSIONS,
  EVENT_CATEGORY,
  EVENT_NAMES,
  COMPONENT_POSITION,
  PAGE_SOURCE,
  BUTTON_TYPE
} from "./utils/constants/segmentEvents";
import VisibilitySensor from "react-visibility-sensor";
// import { trackEvent } from "@whitehatjr/whjr-analytics";
import { sendEventsOnSegmentHOF } from "./utils/helpers/sendEventOnSegment";
// import { s__outputActivity } from "../../selectors/arts";
import { closePIP, h__isArtScope,getStudentsInfo,endClassSubmit,subscribe } from "./utils/helpers/helper";
import { trackFirebaseEvent, firebaseSync } from "./utils/helpers/firebase";
import firebase from "./utils/helpers/firebase";
import HelpCenter from "./components/helpCenter";
import { config } from "./utils/config";
import { VCEvents } from "./utils/constants/VcEvents";
import { OPS_CONFIG_CONSTANTS } from "./utils/constants/opsConfig";
import {
  initSocketService,
  sendSocketMessage
} from "./utils/helpers/socket-lib/socket-util";

const openChatContainer = keyframes`
0% {
  visibility: hidden;
  opacity: 0;
}
1% {
  visibility: visible;
  opacity: 0;
  transform: translateY(50%);
}
100% {
  visibility: visible;
  opacity: 1;
  transform: translateY(0);
}
`;

const hideChatContainer = keyframes`
0% {
  transform: translateY(0);
}
99% {
  opacity: 0;
  transform: translateY(50%);
}
100% {
  visibility: hidden;
  opacity: 0;
}
`;

export const HelpSection = styled.div`
  .help-section {
    max-height: 14rem;
    padding: 1.5rem;
    bottom: 7%;
    max-width: 17rem;
    right: 2%;
    left: auto;
    top: auto;
  }
`;

export const ChatContainer = styled.div`
  position: fixed;
  right: ${(props) => props.right};
  bottom: 25px;
  width: 320px;
  display: flex;
  flex-direction: column;
  max-width: 100vw;
  height: 430px;
  max-height: 85vh;
  visibility: hidden;
  opacity: 0;
  overflow: hidden;
  border-radius: 8px;
  box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.38);
  background-color: #ffffff;
  z-index: 9999999;
  line-height: normal;
  letter-spacing: normal;
  font-size: ${(props) => props.theme.font.fontSize};
  font-family: ${(props) => props.theme.font.fontFamily};
  color: ${(props) => props.theme.colors.textColor.primary};
  animation: ${hideChatContainer} 0.5s backwards;
  &.chat-visible {
    animation: ${openChatContainer} 0.5s forwards;
  }
  @media (max-width: 768px) {
    max-height: 100%;
    height: 100vh;
    width: 100vw;
    border-radius: 0;
    left: 0;
    top: 0;
  }
`;

export const HelpContainer = styled.div`
  position: fixed;
  right: ${(props) => props.right};
  bottom: 124px;
  // width: 317px;
  // width: 330px;
  width: 345px;
  display: flex;
  flex-direction: column;
  max-width: 100vw;
  // height: 383px;
  height: 371px;
  max-height: 85vh;
  visibility: hidden;
  opacity: 0;
  // overflow: hidden;
  border-radius: 8px;
  box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.38);
  background-color: #ffffff;
  z-index: 9999999;
  line-height: normal;
  letter-spacing: normal;
  font-size: ${(props) => props.theme.font.fontSize};
  font-family: ${(props) => props.theme.font.fontFamily};
  color: ${(props) => props.theme.colors.textColor.primary};
  animation: ${hideChatContainer} 0.5s backwards;
  :before {
    width: 0;
    height: 0;
    border-left: 20px solid transparent;
    border-right: 20px solid transparent;
    border-top: 19px solid #fff;
    content: "";
    position: absolute;
    z-index: 99999999;
    bottom: -13px;
    right: 45px;
  }
  &.help-visible {
    animation: ${openChatContainer} 0.5s forwards;
  }
  @media (max-width: 768px) {
    max-height: 100%;
    height: 100vh;
    width: 100vw;
    border-radius: 0;
    left: 0;
    top: 0;
  }
`;

const zendeskScript = "https://dev.zopim.com/web-sdk/1.11.2/web-sdk.js";
let zChat = window.zChat;
const chatLogger = getCustomLogger("chatLogger");
const chatLogSpanId = "ZENDESK CHAT";
const testNetworkSpeed = new NetworkSpeed();
let tabArray = [];
const chatbotLogger = getCustomLogger("chatbotLogger");
const chatbotLogId = "YM CHATBOT";
class ChatSupport extends Component {
  state = {
    chatInit: false,
    chatVisible: false,
    openChat: false,
    curState:
      // this.props.location.pathname.split("/")[2] === "joinClass" &&
      this.props.teacher &&
      _get(this.props, "getChatABdata.dataList", []).length
        ? "prechat_ab"
        : "prechat", //prechat_ab, prechat, chat, rate-chat, chat-history, offline-chat
    curDeptInfo: null,
    offlineMessage: "",
    chatInfo: {},
    notificationCount: 0,
    currTimestamp: null,
    pastChats: [],
    pastChatsAvailable: true,
    otpRequired: false,
    openHelpBox: false,
    helpCurState: "", //chat, email, email-ceo
    helpboxView: false,
    studentJoined: false,
    helpSectionStatus: false,
    isMinimized: false,
    activeParams: {},
    studentInternetSpeed: {},
    teacherInternetSpeed: {},
    techDeptInfo: null,
    firebaseEvents: [],
    firebaseSyncGIF: null,
    totalParticipants: [],
    inClassReschedule: false
  };

  componentDidMount() {
    this.setState({ currTimestamp: Date.now() });
    this.initScript();
    this.checkIfStudentJoined();
    if (this.props.classInterfaceData && this.props.classInterfaceData.id) {
      this.checkStudentAvailability();
      this.initFirebaseSessionEvent();
      this.setClassTab();
    }
  }

  componentDidUpdate(prevProps, prevState) {
    // In order to check whether the teacher lies in INCLASS_INSTANT_CLASS_RESCHEDULE feature config or not.
    if (
      _get(this.props, "location.pathname") === T__JOIN_CLASS &&
      _get(this.props, "featureConfig", []) !==
        _get(prevProps, "featureConfig", [])
    ) {
      let checkReschedule = false;
      _get(this.props, "featureConfig", []).map((item) => {
        item.name === OPS_CONFIG_CONSTANTS.INCLASS_INSTANT_CLASS_RESCHEDULE &&
          (checkReschedule = true);
      });
      let checkIsTrial = _get(this.props, "classInterfaceData.isTrial", false);
      checkReschedule &&
        checkIsTrial &&
        this.setState({ inClassReschedule: true });
    }

    if (prevProps.showChat !== this.props.showChat) {
      this.showChat(this.props.showChat);
    }
    if (
      _get(prevProps, "classInterfaceData.session_events", []).length !==
      _get(this.props, "classInterfaceData.session_events", []).length
    ) {
      this.checkIfStudentJoined();
    }
    this.handleChatABwhenChatisOpen(prevProps); // for handling ab_chat screen
    if (
      h__isArtScope() &&
      prevProps.outputActivityState !== this.props.outputActivityState
    ) {
      if (this.props.outputActivityState.openHelpChatModal) {
        this.openHelpModal(true, true);
      }
    }

    if (prevProps.appConfigData !== this.props.appConfigData) {
      //enable help section based on student and teacher courses
      this.enableHelpSection();
    }

    if (
      this.state.helpSectionStatus &&
      window.location.pathname.includes(R__JOIN_CLASS)
    ) {
      this.setState({ helpSectionStatus: false });
    }
    if (
      !_isEqual(prevProps.chatTriggerData, this.props.chatTriggerData) &&
      Object.keys(this.props.chatTriggerData).length
    ) {
      this.handleAutoChatStart();
    }

    if (
      prevProps.classInterfaceData.teacherId !==
      this.props.classInterfaceData.teacherId
    ) {
      this.initializeParams();
    }

    if (prevState.studentInternetSpeed !== this.state.studentInternetSpeed) {
      if (
        Object.keys(this.state.studentInternetSpeed).length !== 0 &&
        this.props.mode === "student"
      ) {
        const studentId = _get(this.props.student, "id", null);
        const { teacherId } = this.props.classInterfaceData;
        sendSocketMessage({
          type: "LATEST_SPEED",
          state: {
            internetStatus: this.state.studentInternetSpeed,
            source: this.props.mode === "student" ? studentId : teacherId
          },
          target: "*"
        });
      }
    }

    if (
      (this.props.classInterfaceData && this.props.classInterfaceData.id) !==
      (prevProps.classInterfaceData && prevProps.classInterfaceData.id)
    ) {
      this.initFirebaseSessionEvent();
    }

    // CHECK WHETHER STUDENT IS IN THE CLASS USING TWILIO
    if (
      (this.props.classInterfaceData && this.props.classInterfaceData.id) !==
        (prevProps.classInterfaceData && prevProps.classInterfaceData.id) ||
      this.state.totalParticipants.length !== prevState.totalParticipants.length
    ) {
      this.checkStudentAvailability();
    }

    // TO RESET THE CHAT TAB STATES WHEN NOT ON JOIN CLASS
    if (
      !_isEqual(
        _get(prevProps, "getChatABdata.dataList", []).length,
        _get(this.props, "getChatABdata.dataList", []).length
      )
    ) {
      if (window.location.pathname.includes(R__JOIN_CLASS)) {
        this.setClassTab();
      } else {
        this.setState({ teacherInternetSpeed: {} }, () => {
          tabArray = [];
        });
      }
    }
  }

  checkStudentAvailability = () => {
    subscribe(VCEvents.PARTICIPANTS_UPDATE, (participants) => {
      const allIdentities = participants.map(({ identity }) => identity);
      this.setState({ totalParticipants: allIdentities }, () => {
        tabArray.forEach((user) => {
          const matchFound = allIdentities.find(
            (item2) => item2 === user.userId
          );
          const matchNotFound = !allIdentities.find(
            (item2) => item2 === user.userId
          );
          if (matchFound) {
            user.isJoined = true;
          }
          if (matchNotFound && user.name !== "Me") {
            user.isJoined = false;
          }
        });
      });
    });
  };

  setClassTab = () => {
    const teacherInfo = _get(this.props, "classInterfaceData.teacher", "");
    let classBooking = _get(this.props, "classInterfaceData.class_booking", "");
    const studentBookings = getStudentsInfo(classBooking);
    studentBookings.map((std) => {
      tabArray.push({
        type: "student",
        name: std.name,
        id: std.id,
        userId: std.userId,
        isJoined: false,
        speed: {}
      });
    });
    tabArray = [
      {
        type: "teacher",
        name: "Me",
        id: teacherInfo.id,
        userId: teacherInfo.userId,
        isJoined: true,
        speed: {}
      },
      ...tabArray
    ];
  };

  resetTabData = () => {
    this.setState({ teacherInternetSpeed: {} }, () => {
      this.props.d__resetFormData();
      tabArray = tabArray.map((tab) => ({ ...tab, speed: {} }));
    });
  };

  onSessionChangeEvent(snapshot) {
    let value = snapshot.val();
    if (value && value.lastUpdatedTimestamp) {
      if (this.props.classInterfaceData.id) {
        this.props.refreshSessionAction({
          sessionId: this.props.classInterfaceData.id
        });
      }
    }
  }

  onTimerGIFEvent(snapshot, timerGIFEvent) {
    this.setState({ firebaseSyncGIF: timerGIFEvent });
  }

  initFirebaseSessionEvent() {
    if (this.props.classInterfaceData.id) {
      let eventTrack = trackFirebaseEvent(
        `protected/sessions/${this.props.classInterfaceData.id}`,
        (snapshot) => this.onSessionChangeEvent(snapshot)
      );
      let timerGIFEvent = firebaseSync(
        `protected_math_session/${this.props.classInterfaceData.id}`,
        (snapshot) => this.onTimerGIFEvent(snapshot, timerGIFEvent)
      );

      let firebaseEvents = [...this.state.firebaseEvents];
      firebaseEvents.push(eventTrack);
      this.setState({
        firebaseEvents
      });
    }
  }

  handleSocketEvents = ({ type, target, state }) => {
    switch (type) {
      case "GET_LATEST_SPEED":
        if (state && this.props.mode === "student") {
          this.getNetworkDownloadSpeed();
        }
        break;
      case "LATEST_SPEED":
        if (state && this.props.mode === "teacher") {
          this.setState(
            {
              studentInternetSpeed: state.internetStatus
            },
            () => {
              const studentId = _get(this.props.student, "id", null);
              this.setSpeedData(state.source, state.internetStatus);
            }
          );
        }
        break;
      default:
        break;
    }
  };

  initializeParams = () => {
    const studentId = _get(this.props.student, "id", null);
    const { teacherId, vcMeetingId } = this.props.classInterfaceData;
    let params = {
      roomId: vcMeetingId,
      studentId,
      teacherId,
      mode: this.props.mode
    };
    this.setState({ activeParams: params });
    initSocketService({
      ...params,
      userId: params.mode === "teacher" ? teacherId : studentId,
      cb: this.handleSocketEvents,
      connectCb: () => {}
    });
  };

  enableHelpSection = () => {
    let { teacher, student, appConfigData } = this.props;
    let configCourse = _get(
      appConfigData,
      "configs.HELP_SECTION_ACTIVE.courseList",
      []
    );
    let teacherSkillsSet = _get(teacher, "teacher_skills", []);
    let teacherCourseSkills = teacherSkillsSet.filter(
      (item) => item.skillName === "course"
    );
    let teacherCourse = teacherCourseSkills.find((item) =>
      configCourse.includes(_get(item, "course.courseType", ""))
    );
    let studentSkillsSet = configCourse.find(
      (item) => item == _get(student, "activeCourse.courseType", "")
    );

    let helpSectionStatus = _get(
      appConfigData.configs,
      "HELP_SECTION_ACTIVE.help_section_active_status",
      false
    );
    if (
      !window.location.pathname.includes(R__JOIN_CLASS) &&
      (teacherCourse || studentSkillsSet)
    ) {
      this.setState({ helpSectionStatus });
    }
  };

  setHelpSectionFlag = (status) => {
    this.setState({ helpSectionStatus: false }, () => {
      this.setState({ openChat: true });
    });
  };

  resetHelpSectionFlag = (status) => {
    this.setState({ helpSectionStatus: true }, () => {
      this.setState({ openChat: false });
    });
  };

  componentWillUnmount() {
    this.getEvents().forEach((evt) => {
      zChat && zChat.un(evt, this.evtHandler);
    });
    zChat && zChat.logout();
    this.showChat(false);
  }

  checkIfStudentJoined = () => {
    if (
      _get(this.props, "classInterfaceData.session_events", []).filter(
        (x) => x.eventName.toLowerCase() === "studentjoined"
      ).length > 0
    ) {
      this.setState({
        studentJoined: true
      });
    } else {
      this.setState({
        studentJoined: false
      });
    }
  };

  getEvents = () => {
    return ["account_status", "chat"];
  };

  initScript = () => {
    let chatScript = document.createElement("script");
    chatScript.setAttribute("src", zendeskScript);
    chatScript.setAttribute("id", "zdk-snippet");
    chatScript.onload = this.initChat.bind(this, this.props.accountKey);
    document.body.appendChild(chatScript);
  };
  initChat = (accountKey) => {
    try {
      zChat = window.zChat;
      if (
        this.props.authenticatedUser &&
        this.props.accountKey &&
        this.props.getChatToken
      ) {
        this.authenticateUser(this.props.accountKey, this.props.getChatToken);
      }

      this.getEvents().forEach((evt) => {
        zChat.on(
          evt,
          (this.evtHandler = (data) => {
            this.onEvent(evt, data);
          })
        );
      });
    } catch (e) {
      chatLogger && chatLogger.error(`${chatLogSpanId} : `, JSON.stringify(e));
      console.error(e);
    }
  };

  sendSegmentEventAction = async (obj, eventName) => {
    let segmentObj = this.toLowerCaseprops.eventSegmentProperties();
    let userPropRemain = await this.props.eventUserSegmentProperties();
    this.props.sendEventAction({
      event: eventName,
      properties: {
        ...segmentObj,
        ...userPropRemain,
        ...obj
      }
    });
  };

  onEvent = (evt, data) => {
    switch (evt) {
      case "account_status":
        if (zChat.isChatting() && !this.state.chatInit) {
          //another tab/browser/device
          this.setState({ curState: "chat" });
        }
        this.setState({ chatInit: true });
        break;
      case "chat":
        if (
          this.state.chatInit &&
          zChat.isChatting() &&
          this.state.curState !== "chat"
        ) {
          this.setState({ curState: "chat" });
        }
        break;
    }
  };
  authenticateUser = (accountKey, getJwtToken) => {
    const { teacher, student } = this.props;
    try {
      zChat.init({
        account_key: accountKey,
        authentication: {
          jwt_fn: (callback) => {
            getJwtToken()
              .then((res) => {
                if (res.token) {
                  chatLogger &&
                    chatLogger.debug(`${chatLogSpanId} : JWT authentication`);
                  callback(res.token);
                  if (this.props.showChat) {
                    this.showChat(true);
                  }
                }
              })
              .catch((e) => {
                chatLogger &&
                  chatLogger.error(
                    `${chatLogSpanId} : JWT authentication failed`,
                    JSON.stringify(e)
                  );
              });
          }
        }
      });
    } catch (e) {
      chatLogger &&
        chatLogger.error(
          `${chatLogSpanId} : Error in auth `,
          JSON.stringify(e)
        );
      console.error(e);
    }
  };

  handleChatABwhenChatisOpen = (prevProps) => {
    if (this.props.getChatABdata) {
      if (
        !["chat", "rate-chat", "chat-history", "offline-chat"].includes(
          this.state.curState
        )
      ) {
        if (
          !_isEqual(
            prevProps.getChatABdata.dataList,
            this.props.getChatABdata.dataList
          )
        ) {
          if (
            this.props.getChatABdata.dataList.length &&
            this.props.teacher &&
            this.state.curState !== "prechat_ab"
          ) {
            this.updateChatState("prechat_ab", null);
          } else if (
            !this.props.getChatABdata.dataList.length &&
            this.state.curState !== "prechat"
          ) {
            this.updateChatState("prechat", null);
          }
        }
      }
    }
  };
  handleAutoChatStart = () => {
    const chatData = _get(this.props, "chatTriggerData", null);
    if (this.props.showChat && chatData) {
      this.openChat(true);
      this.handleAgentOnlineState(chatData);
      chatLogger &&
        chatLogger.debug(
          `${chatLogSpanId} : Triggering chat from outer source`,
          {
            chatData
          }
        );
    }
  };

  handleChatABwhenChatisOpen = (prevProps) => {
    if (this.props.getChatABdata) {
      if (
        !["chat", "rate-chat", "chat-history", "offline-chat"].includes(
          this.state.curState
        )
      ) {
        if (
          !_isEqual(
            prevProps.getChatABdata.dataList,
            this.props.getChatABdata.dataList
          )
        ) {
          if (
            this.props.getChatABdata.dataList.length &&
            this.props.teacher &&
            this.state.curState !== "prechat_ab"
          ) {
            this.updateChatState("prechat_ab", null);
          } else if (
            !this.props.getChatABdata.dataList.length &&
            this.state.curState !== "prechat"
          ) {
            this.updateChatState("prechat", null);
          }
        }
      }
    }
  };

  handleAgentOnlineState = (data) => {
    const departmentId = data.deptId;
    const departmentStatus = zChat.getDepartment(departmentId);
    if (departmentStatus && departmentStatus.status !== "online") {
      this.addOfflineChatTag();
      this.setState({ curState: "offline-chat", curDeptInfo: data });
      chatLogger &&
        chatLogger.debug(`${chatLogSpanId} : Handling offline chat`, {
          chatData: {
            data: data,
            zendeskDepartmentData: departmentStatus,
            departmentStatus: departmentStatus.status
          }
        });
    } else {
      this.setState({ curState: "chat", chatInfo: data });
      chatLogger &&
        chatLogger.debug(`${chatLogSpanId} : Handling online chat`, {
          chatData: {
            data: data,
            zendeskDepartmentData: departmentStatus,
            departmentStatus: departmentStatus.status
          }
        });
    }
  };
  addOfflineChatTag = () => {
    let offlineTags = null;
    try {
      const tags = JSON.parse(getCookie("chatSupportTags"));
      offlineTags = JSON.parse(getCookie("chatOfflineTags"));
      if (tags && typeof tags === "object" && tags.length) {
        //remove all tags from previous chat
        zChat.removeTags(tags);
      }
    } catch (e) {
      chatLogger &&
        chatLogger.error(
          `${chatLogSpanId} : Error in addding tags `,
          JSON.stringify(e)
        );
      console.error(e);
    }
    zChat.addTags(offlineTags);
    setCookie("chatSupportTags", JSON.stringify(offlineTags));
  };
  handleOfflineAgentMessage = (message) => {
    const { display_name, email } = zChat.getVisitorInfo();
    chatLogger &&
      chatLogger.debug(`${chatLogSpanId} : Sending offline message`, {
        chatData: {
          department: this.state.curDeptInfo.deptId
        }
      });
    zChat.sendOfflineMsg(
      {
        name: display_name,
        email: email,
        phone: "",
        message: message,
        department: this.state.curDeptInfo.deptId
      },
      (err) => {
        if (!err) {
          this.resetChatState(false);
        }
        chatLogger &&
          chatLogger.error(
            `${chatLogSpanId} : Error in sending offline message`,
            JSON.stringify(err)
          );
      }
    );

    if (Object.keys(this.props.chatTriggerData || {}).length) {
      this.props.d__triggerChatEnd();
      this.setState({ isMinimized: false });
    }
  };
  resetChatState = (openChatWindow = true) => {
    this.setState(
      {
        curState:
          this.props.location.pathname.split("/")[2] === "joinClass" &&
          _get(this.props, "getChatABdata.dataList", []).length &&
          this.props.teacher
            ? "prechat_ab"
            : "prechat",
        openChat: openChatWindow,
        offlineMessage: ""
      },
      () => {
        if (Object.keys(this.props.chatTriggerData || {}).length)
          this.props.d__triggerChatEnd();
        this.resetTabData();
        this.setState({ isMinimized: false });
      }
    );
  };
  openChat = (show) => {
    const { student, isParentalVerified, teacher, appConfigData } = this.props;
    //enable help section based on student and teacher courses
    let configCourse = _get(
      appConfigData,
      "configs.HELP_SECTION_ACTIVE.courseList",
      []
    );
    let teacherSkillsSet = _get(teacher, "teacher_skills", []);
    let teacherCourseSkills = teacherSkillsSet.filter(
      (item) => item.skillName === "course"
    );
    let teacherCourse = teacherCourseSkills.find((item) =>
      configCourse.includes(_get(item, "course.courseType", ""))
    );
    let studentSkillsSet = configCourse.find(
      (item) => item == _get(student, "activeCourse.courseType", "")
    );

    const isOtpOnCancellation = _get(student, "isOtpOnCancellation", false);
    if (isOtpOnCancellation && !isParentalVerified) {
      this.props.d__showOtpDialog({
        showDialog: "CHAT_WITH_US",
        title: "Chat Confirm",
        extraData: {
          actionType: "CHAT_WITH_US",
          data: {}
        }
      });
      this.setState({ otpRequired: true });
    } else {
      this.openChatConfirmed(show);
      if (
        _get(
          this.props,
          "appConfigData.configs.HELP_SECTION_ACTIVE.help_section_active_status",
          false
        ) &&
        !window.location.pathname.includes(R__JOIN_CLASS) &&
        (teacherCourse || studentSkillsSet)
      )
        this.setState({ helpSectionStatus: !this.state.helpSectionStatus });
    }
  };
  setHelpSection = (show) => {
    let { teacher, student } = this.props;
    this.setState({ helpSection: !this.state.helpSection });
    sendEventsOnSegmentHOF({
      sendEvents: this.props.sendEventAction,
      eventName: "helpdesk_entity_clicked",
      properties: {
        user_id: teacher.id || student.id,
        user_email: teacher.email || student.email,
        user: teacher ? "Teacher" : "Student",
        action_time: new Date().getTime(),
        entity_source: _get(window, "location.pathname", "").split("/").pop(),
        entity: "Help"
      }
    });
  };

  inClassSegmentEvent(show) {
    const { teacher, student } = this.props;

    let classSessionData = _get(
      this.props,
      "classInterfaceData.class_booking.session_id",
      ""
    );
    if (classSessionData != "") {
      if (_get(teacher, "email", null) != null) {
        if (show) {
          this.sendSegmentEventAction({}, "Teacher concierge chat opened");
        } else {
          this.sendSegmentEventAction({}, "Teacher concierge chat closed");
        }
      } else if (_get(student, "email", null) != null) {
        if (show) {
          this.sendSegmentEventAction({}, "Student concierge chat opened");
        } else {
          this.sendSegmentEventAction({}, "Student concierge chat closed");
        }
      }
    }
  }
  openChatConfirmed = (show) => {
    this.inClassSegmentEvent(show);
    try {
      if (!show) this.setState({ timestamp: Date.now() });
      this.setState({ openChat: show, notificationCount: 0 });
      if (this.props.invokeNotificationCountEvent) {
        this.handleNotificationCountChange(0);
      }
      chatLogger &&
        chatLogger.debug(`${chatLogSpanId} : chat visible`, {
          chatData: {
            show
          }
        });
      if (zChat && zChat.isChatting() && this.state.chatInit) {
        //another tab/browser/device
        this.setState({ curState: "chat" });
        chatLogger &&
          chatLogger.debug(`${chatLogSpanId} : previous chat to continue`);
      }
    } catch (e) {
      console.error(e);
      chatLogger &&
        chatLogger.error(
          `${chatLogSpanId} : Error in openChatConfirmed() method `,
          JSON.stringify(e)
        );
    }
  };
  showChat = (show) => {
    this.setState({ chatVisible: show });
  };

  setSpeedData = (id, speed) => {
    tabArray.map((arr) => {
      if (arr.id === id) {
        arr.speed = speed;
      }
      return arr;
    });
  };

  getNetworkDownloadSpeed = async () => {
    const fileSize = parseInt(
      _get(this.props.appConfigData, "configs.TEACHER_LED_CONFIG.fileSize", 5)
    );
    const baseUrl = `${
      config.SPEED_TEST_URL
    }${fileSize}mb.bin?timestamp=${moment().format()}`;
    const fileSizeInBytes = fileSize * Math.pow(1024, 2);
    const speed = await testNetworkSpeed.checkDownloadSpeed(
      baseUrl,
      fileSizeInBytes
    );
    if (this.props.mode === "teacher") {
      if (this.state.curState === "tech-issue") {
        let teacherInfo = _get(this.props, "classInterfaceData.teacher", "");
        speed["id"] = teacherInfo.id;
        this.setState(
          {
            teacherInternetSpeed: speed
          },
          () => {
            this.setSpeedData(teacherInfo.id, speed);
          }
        );
      }
    } else if (this.props.mode === "student") {
      const studentId = _get(this.props.student, "id", null);
      speed["id"] = studentId;
      this.setState(
        {
          studentInternetSpeed: speed
        },
        () => {
          this.setSpeedData(studentId, speed);
        }
      );
    }

    const args = {
      user_details: speed,
      mode: this.props.mode
    };
    this.handleEventTrack(EVENT_NAMES.INTERNET_SPEED, args);
  };

  updateChatState = (state, data) => {
    switch (state) {
      case "prechat_ab":
        this.setState({
          curState: "prechat_ab",
          curDeptInfo: null,
          chatInfo: {}
        });
        chatLogger &&
          chatLogger.debug(`${chatLogSpanId} : chat action`, {
            chatData: {
              state
            }
          });
        break;
      case "prechat":
        this.setState({
          curState: "prechat",
          curDeptInfo: null,
          chatInfo: {}
        });
        chatLogger &&
          chatLogger.debug(`${chatLogSpanId} : chat action`, {
            chatData: {
              state
            }
          });
        break;
      case "start-chat":
        if (data.deptId && Number.isInteger(data.deptId)) {
          this.handleAgentOnlineState(data);
        }
        chatLogger &&
          chatLogger.debug(`${chatLogSpanId} : chat action`, {
            chatData: {
              state
            }
          });
        break;
      case "chat-history":
        this.setState({ curState: "chat-history", curDeptInfo: null });
        const args = { button_type: BUTTON_TYPE.ARROW };
        this.handleEventTrack(EVENT_NAMES.CONCIERGE_PAST_CHAT_CLICKED, args);
        chatLogger &&
          chatLogger.debug(`${chatLogSpanId} : chat action`, {
            chatData: {
              state
            }
          });
        break;
      case "tech-issue":
        const isChatABData = _get(this.props, "getChatABdata.dataList", []);
        if (isChatABData.length > 0) {
          const techFilteredData = isChatABData.find((obj) => {
            return obj.action === "CUSTOM_INTERNET_SPEED";
          });
          const techIssueInfo = {
            deptId: parseInt(techFilteredData.dept),
            deptName: "",
            issueType: techFilteredData.name,
            issueDetail: ""
          };
          this.setState({ techDeptInfo: techIssueInfo });
        }
        this.setState({ curState: "tech-issue" }, () => {
          this.getNetworkDownloadSpeed();
          const { teacherId, vcMeetingId } = this.props.classInterfaceData;
          const event_name = EVENT_NAMES.TECH_ISSUE;
          const args = {
            teacherId,
            vcMeetingId
          };
          this.handleEventTrack(event_name, args);
        });
        break;
      case "connect-agent":
        this.setState(
          (prevState) => ({
            techDeptInfo: {
              ...prevState.techDeptInfo,
              issueDetail: data.responseMsgs,
              speedData: data.speed ? data : {}
            }
          }),
          () => {
            this.props.d__triggerChatStart(this.state.techDeptInfo);
            this.handleAgentOnlineState(this.state.techDeptInfo);
          }
        );
    }
  };
  handlePastChats = (data, pastChatsAvailable = null) => {
    if (data) {
      this.state.pastChats = data;
    } else {
      this.state.pastChatsAvailable = pastChatsAvailable;
    }
  };
  handleNewChatIncrement = () => {
    let notificationCount = this.state.notificationCount;
    notificationCount++;
    this.setState({ notificationCount });
    if (this.props.invokeNotificationCountEvent) {
      this.handleNotificationCountChange(notificationCount);
    }
  };
  handleChatEnd = (isCancel = false) => {
    if (isCancel) {
      const args = {
        button_type: BUTTON_TYPE.ARROW,
        concierge_issue_type: this.state.chatInfo.issueType
          ? this.state.chatInfo.issueType
          : ""
      };
      this.handleEventTrack(EVENT_NAMES.CONCIERGE_CHAT_CANCELLED_CLICKED, args);
    }

    if (!isCancel) {
      this.openChat(false);
      this.resetTabData();
      if (Object.keys(this.props.chatTriggerData || {}).length) {
        this.props.d__triggerChatEnd();
      }

      const args = {
        button_type: BUTTON_TYPE.ARROW,
        concierge_issue_type: this.state.chatInfo.issueType
          ? this.state.chatInfo.issueType
          : ""
      };
      this.handleEventTrack(EVENT_NAMES.CONCIERGE_CHAT_END_CLICKED, args);
      this.setState({ isMinimized: false });
    }
    this.updateChatState(
      window.location.pathname.split("/")[2] === "joinClass" &&
        _get(this.props, "getChatABdata.dataList", []).length &&
        this.props.teacher
        ? "prechat_ab"
        : "prechat"
    );
    chatLogger && chatLogger.debug(`${chatLogSpanId} : chat has ended`);
  };

  //open help modal box
  openHelpModal = (show, isClicked = false) => {
    this.setState({ openHelpBox: show, helpCurState: "" });

    //triggered the event when user manually clicked for minimized need help
    if (isClicked) {
      const args = { button_type: BUTTON_TYPE.CROSS };
      this.handleEventTrack(EVENT_NAMES.CONCIERGE_MINIMIZED_CLICKED, args);
    }
  };

  handleHelpCurState = (curState) => {
    this.setState({ openHelpBox: false, helpCurState: curState });

    if (curState === "chat") {
      this.openChat(true);
      const args = {
        button_type: BUTTON_TYPE.ARROW
      };
      this.handleEventTrack(EVENT_NAMES.CONCIERGE_CHAT_CLICKED, args);
    }
  };

  closeHelpHandler = (type, isBackButton) => {
    this.setState({ openHelpBox: false, helpCurState: "" });
    if (type === "chat") {
      this.openChat(false);
    }
    if (isBackButton) {
      this.setState({ openHelpBox: true });

      const args = {
        button_type: BUTTON_TYPE.ARROW
      };
      this.handleEventTrack(EVENT_NAMES.CONCIERGE_BACK_ARROW_CLICKED, args);
    }
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    const isOtpOnCancellation =
      nextProps.student && nextProps.student.isOtpOnCancellation;
    const openChat =
      isOtpOnCancellation &&
      prevState.otpRequired &&
      nextProps.isParentalVerified;
    if (isOtpOnCancellation && prevState.otpRequired) {
      const st = {
        ...prevState,
        otpRequired: isOtpOnCancellation && !nextProps.isParentalVerified,
        openChat: openChat,
        notificationCount: 0
      };
      if (openChat) {
        st.timestamp = Date.now();
      }
      if (zChat && zChat.isChatting() && prevState.chatInit) {
        st.curState = "chat";
      }
      return st;
    }
    return null;
  }

  helpVisibleEventB = (visible) => {
    this.setState({ helpboxView: visible });
    if (visible) {
      sendEventsOnSegmentHOF({
        sendEvents: this.props.sendEventAction,
        eventName: EVENT_NAMES.IMPRESSION_SENT,
        properties: {
          ab_version: AB_VERSIONS.B_VERSION,
          position: COMPONENT_POSITION.RIGHT_PANEL,
          page_source: PAGE_SOURCE.DASHBOARD,
          event_category: EVENT_CATEGORY.CONCIERGE
        }
      });
    }
  };

  handleEventTrack = (eventName, args) => {
    if (this.props.student || this.props.teacher) {
      sendEventsOnSegmentHOF({
        sendEvents: this.props.sendEventAction,
        eventName: eventName,
        properties: {
          ab_version: AB_VERSIONS.B_VERSION,
          position: COMPONENT_POSITION.RIGHT_PANEL,
          event_category: EVENT_CATEGORY.CONCIERGE,
          page_source: PAGE_SOURCE.DASHBOARD,
          ...args
        }
      });
    }
  };

  handleChatbot = () => {
    chatbotLogger &&
      chatbotLogger.debug(`${chatbotLogId} opened by teacher with Id `, {
        teacherId: _get(this.props, "teacher.id", "")
      });
    this.props.showChatbot();
  };
  checkJoinClass = () => window.location.pathname === "/s/joinClass";

  handleNotificationCountChange = (notificationCount) => {
    const anotherEvent = new CustomEvent("onNotificationCountChange", {
      detail: {
        notificationCount
      }
    });
    document.dispatchEvent(anotherEvent);
  };

  openActiveChat = () => {
    const activeChatData = localStorage.getItem("activeChat");
    try {
      this.props.d__openChatwindow({ ...JSON.parse(activeChatData) });
    } catch (e) {
      console.log(e);
    }
  };
  teacherEndClass = (feedbackResponseResult = []) => {
    let classData = this.props.classInterfaceData;
    classData = {
      ...classData,
      ncReasonCode: "Tech Issue",
      ncReasonDesc: "NST",
      classStatus: "not_completed",
      remark: "Slow Internet Speed"
    };
    endClassSubmit(
      feedbackResponseResult,
      false,
      classData,
      this.props.classInterfaceData,
      this.props.isReportCardFeedbackQuestionsAvailable,
      this.state.firebaseEvents,
      this.state.firebaseSyncGIF
    ).then((response) => {
      this.props.endClass(response.requestParam);
    });
    const firebaseRef = firebase
      .database()
      .ref(`public/mobile-sessions-sync/${classData.bookingId}`);
    firebaseRef.once("value").then(function (snapshot) {
      const data = snapshot.val();
      if (data && data.currentActivity) {
        firebaseRef.update({
          completed: true
        });
      }
    });
    closePIP();
    this.sendSegmentEventAction({}, "Class end confirmed");
    // trackEvent(
    //   {
    //     booking_id: _get(classData, "bookingId"),
    //     nc_reason_code: _get(classData, "ncReasonCode"),
    //     nc_reason_desc: _get(classData, "ncReasonDesc"),
    //     marked_from: "Teacher",
    //     status: _get(classData, "status"),
    //     course_type: "Math",
    //     class_session_id: _get(classData, "class_booking.sessionId")
    //   },
    //   "End Class due to network issue"
    // );
    this.props.navigateToPath("/t/dashboard");
    this.handleEventTrack(EVENT_NAMES.END_CLASS, classData);
    this.setState({ curState: "prechat" }, () => {
      this.openChat(false);
    });
  };

  render() {
    console.log("props and state internal>>>", this.props, this.state)
    const studentId = _get(this.props.student, "id", null);
    const chatBubbleVisible = !this.state.chatVisible || this.state.openChat;
    return (
      <Theme>
        <div id="chat-zdk">
          <ChatContainer
            className={`${
              this.state.openChat &&
              this.state.chatVisible &&
              !this.state.otpRequired
                ? "chat-visible"
                : ""
            }`}
            right={this.props.showPlayGround ? "34%" : "25px"}
          >
            <ChatTitle
              chatState={this.state.curState}
              resetChatState={this.resetChatState}
              openChat={(val) => {
                this.setState({ isMinimized: true });
                this.openChat(val);
              }}
              isClickedFromHelpBox={
                this.state.helpCurState === "chat" ? true : false
              }
              closeHelpHandler={this.closeHelpHandler}
            />
            {this.state.curState === "prechat_ab" && (
              <Prechat_AB
                issueData_AB={_get(this.props, "getChatABdata.dataList", [])}
                chatTrigger={this.updateChatState}
                showHistoryBtn={true}
                handleEventTrack={this.handleEventTrack}
                chatBotShow={this.handleChatbot}
                hideChat={this.openChat}
                studentJoined={this.state.studentJoined}
                classStartTime={_get(
                  this.props,
                  "classInterfaceData.class_booking.startTime",
                  ""
                )}
              />
            )}
            {this.state.curState === "prechat" && (
              <Prechat
                issueData={this.props.getChatIssues}
                prechatABlayer={_get(this.props, "getChatABdata.dataList", [])}
                chatTrigger={this.updateChatState}
                showHistoryBtn={true}
                handleEventTrack={this.handleEventTrack}
                openChat={this.openChat}
                chatLogger={chatLogger}
                handleReschedulePopup={
                  this.props.d__toggleClassNotCompletedPopupVisibility
                }
                inClassReschedule={this.state.inClassReschedule}
              />
            )}
            {this.state.curState === "tech-issue" && (
              <InternetSpeedTest
                mode={this.props.mode}
                activeParams={this.state.activeParams}
                chatIssueHandler={this.updateChatState}
                openChat={this.openChat}
                resetChatState={this.resetChatState}
                teacherInternetSpeed={this.state.teacherInternetSpeed}
                studentInternetSpeed={this.state.studentInternetSpeed}
                handleEventTrack={this.handleEventTrack}
                endTeacherClass={this.teacherEndClass}
                tabArray={tabArray}
                speedLimit={_get(
                  this.props.appConfigData,
                  "configs.TEACHER_LED_CONFIG.internetSpeed",
                  0
                )}
              />
            )}

            {this.state.curState === "offline-chat" && (
              <OfflineChat
                message={this.state.offlineMessage}
                onSubmit={this.handleOfflineAgentMessage}
                takeBack={this.resetChatState}
              />
            )}
            {this.state.curState === "chat" && (
              <Chat
                chatInfo={this.state.chatInfo}
                chatEnded={this.handleChatEnd}
                tags={this.props.tags}
                newChatHandle={this.handleNewChatIncrement}
                timestamp={this.state.currTimestamp}
                autoStart={this.props.autoStart}
                handleEventTrack={this.handleEventTrack}
              />
            )}
            {this.state.curState === "chat-history" && (
              <ChatHistory
                handlePastChats={this.handlePastChats}
                pastChats={this.state.pastChats}
                chatAvailable={this.state.pastChatsAvailable}
              />
            )}
          </ChatContainer>
          <HelpContainer
            className={`${this.state.openHelpBox ? "help-visible" : ""}`}
            right={this.props.showPlayGround ? "34%" : "25px"}
          >
            {this.state.helpCurState === "email" && (
              <ConciergeHelpDesk
                classes=""
                supportNumber=""
                id={studentId}
                type={`STUDENT`}
                student={this.props.student}
                teacher={null}
                isClickedFromHelpBox={true}
                closeHelpHandler={this.closeHelpHandler}
              />
            )}
            {this.state.helpCurState === "email-ceo" && (
              <WriteToCeo
                nameInfo={
                  this.props.teacher ? this.props.teacher : this.props.student
                }
                submitFeedbackEscalation={this.props.submitFeedbackEscalation}
                ab_version={AB_VERSIONS.A_VERSION}
                sendEvents={this.props.sendEventAction}
                isClickedFromHelpBox={true}
                closeHelpHandler={this.closeHelpHandler}
              />
            )}
            {this.state.openHelpBox && (
              <>
                <VisibilitySensor
                  active={!this.state.helpboxView}
                  onChange={this.helpVisibleEventB}
                >
                  <NeedHelp
                    student={this.props.student}
                    openHelpModal={this.openHelpModal}
                    handleHelpCurState={this.handleHelpCurState}
                    appConfigData={this.props.appConfigData}
                  />
                </VisibilitySensor>
              </>
            )}
          </HelpContainer>

          <HelpCenter
            helpSectionStatus={this.state.helpSectionStatus}
            helpSection={this.state.helpSection}
            showPlayGround={this.state.showPlayGround}
            close={() => this.setState({ helpSection: false })}
            sendEvents={this.props.sendEventAction}
            student={this.props.student}
            teacher={this.props.teacher}
            chatDataAll={this.props.chatDataAll}
          />

          {this.state.helpSectionStatus && this.state.isMinimized && this.props.chatDataAll (
            <ChatBubble
              hidden={
                this.props.invokeNotificationCountEvent
                  ? this.checkJoinClass()
                    ? true
                    : chatBubbleVisible
                  : chatBubbleVisible
              }
              handleClick={this.openChat}
              notificationCount={this.state.notificationCount}
              showPlayGround={this.props.isPlaygroundOpen}
              pathName={_get(this.props, "location.pathname")}
              isTeacher={this.props.teacher ? true : false}
              className="minimized-chat"
              setHelpSectionFlag={this.setHelpSectionFlag}
              resetHelpSectionFlag={this.resetHelpSectionFlag}
              resetHelpCenter={() =>
                this.setState({ helpSectionStatus: false })
              }
              location={this.props.location.pathname}
              chatDataAll={this.props.chatDataAll}
            />
          )}
          {this.props.chatDataAll && (
            <ChatBubble
            hidden={
              this.props.invokeNotificationCountEvent
                ? this.checkJoinClass()
                  ? true
                  : chatBubbleVisible
                : chatBubbleVisible
            }
            handleClick={
              this.state.helpSectionStatus ? this.setHelpSection : this.openChat
            }
            handleHelpClick={this.openHelpModal}
            notificationCount={this.state.notificationCount}
            showPlayGround={this.props.isPlaygroundOpen}
            pathName={_get(this.props, "location.pathname")}
            isTeacher={this.props.teacher ? true : false}
            isHelpSectionActive={this.state.helpSectionStatus}
            setHelpSectionFlag={this.setHelpSectionFlag}
            resetHelpSectionFlag={this.resetHelpSectionFlag}
            resetHelpCenter={() => this.setState({ helpSectionStatus: false })}
            location={this.props.location.pathname}
          />
          )}
        </div>
      </Theme>
    );
  }
}

ChatSupport.defaultProps = {
  authenticatedUser: true,
  showChat: true,
  autoStart: true,
  tags: {}
};

ChatSupport.propTypes = {
  accountKey: PropTypes.string.isRequired,
  authenticatedUser: PropTypes.bool,
  getChatToken: PropTypes.func,
  tags: PropTypes.object,
  showChat: PropTypes.bool,
  autoStart: PropTypes.bool
};

// const mapStateToProps = (state) => ({
//   student: s__getStudent(state),
//   teacher: s__getTeacher(state),
//   classInterfaceData: getClassInterfacingDetails(state),
//   isParentalVerified: isParentalVerified(state),
//   outputActivityState: s__outputActivity(state),
//   appConfigData: getAppConfigData(state),
//   isReportCardFeedbackQuestionsAvailable: s__isReportCardFeedbackQuestionsAvailable(
//     state
//   ),
//   featureConfig: s__featureConfig(state),
//   teacherUpcomingClasses: getTeacherUpcomingClass(state),
// });

// const mapDispatchToProps = (dispatch) => ({
//   d__showOtpDialog: (data) => {
//     dispatch(otpDialogShow.set(data));
//   },
//   submitFeedbackEscalation: (payload) => {
//     dispatch(submitFeedbackEscalation.request({ payload }));
//   },
//   sendEventAction: (data) => {
//     dispatch(segmentEvents.trackEvent(data));
//   },
//   showChatbot: () => {
//     dispatch(chatbotAction.show());
//   },
//   d__triggerChatEnd: () => {
//     dispatch(triggerChat.end({}));
//   },
//   d__triggerChatStart: (data) => {
//     dispatch(triggerChat.start(data));
//   },
//   chatMinimizeForToggleState: data =>
//     dispatch(chatMinimizeForToggle.chatMinimizeState(data)),
//   endClass: (data) => {
//     dispatch(endClassTeacher.request(data));
//   },
//   navigateToPath: (path) => {
//     dispatch(navigateToPath.request(path));
//   },
//   refreshSessionAction: () => {
//     dispatch(refreshSessionClass.request());
//   },
//   d__resetFormData: () => dispatch(fetchTypeformResponse.reset()),
//   d__toggleClassNotCompletedPopupVisibility: (data) =>
//     dispatch(classNotCompletedPopup.toggleVisibility(data))
// });

// export default connect(
//   mapStateToProps,
//   mapDispatchToProps
// )(withRouter(ChatSupport));
export default ChatSupport;

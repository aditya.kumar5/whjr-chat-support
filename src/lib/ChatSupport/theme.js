import React from "react";
import { ThemeProvider } from "styled-components";

const theme = {
  font: {
    fontSize: "16px",
    fontFamily: "Muli-Regular"
  },
  colors: {
    primary: "#266ad1",
    secondary: "#fff",
    textColor: {
      primary: "#1e1e1e",
      secondary: "#636363",
      error: "#c31807"
    }
  }
};

const Theme = ({ children }) => (
  <ThemeProvider theme={theme}>{children}</ThemeProvider>
);

export default Theme;

import _get from "lodash/get";

export const isValidUrl = value => {
    return /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/i.test(
      value
    );
  };

const cu = path => {
  const REACT_APP_PUBLIC_URL = process.env.REACT_APP_PUBLIC_URL;
  const originUrl = _get(window, "location.origin");
  let showPubUrl;
  // isValidUrl is breaking in storybook so we are using this syntax
  if (isValidUrl) {
    showPubUrl = isValidUrl(REACT_APP_PUBLIC_URL);
  } else {
    showPubUrl = !originUrl;
  }
  return (showPubUrl ? REACT_APP_PUBLIC_URL : originUrl) + "/images/" + path;
};

export const HELP_MAIL_WHITE = cu("HelpSection/mail-white.png");
export const HELP_ICON = cu("icons/help.svg");
export const HELP_MAIL = cu("HelpSection/mail.png");
export const QUESTION_MARK = cu("HelpSection/question.png");
export const ISSUES_TAB = cu("Tablet/icon-consierge-issues.svg");
export const IMMEDIATE_TAB = cu("Tablet/Icon-consierge-immediate.svg");
export const CONNECT_TAB = cu("Tablet/icon-consierge-connect.svg");

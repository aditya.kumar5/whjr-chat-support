export const courseNames = {
    MATH: "MATH",
    CODING: "CODING",
    MUSIC: "MUSIC",
    MUSIC_FOR_ALL: "MUSIC_FOR_ALL",
    ART: "ART",
    ENGLISH: "ENGLISH"
  };

  export const allowedExtensions =
  ".pdf, jpg, .png, .gif, .txt, .ppt, .pptx, .docx, .doc, .csv, .mp4, .mov, .m4v, .avi, .3gp, .m4a, .aac, .mp3, .bmp, .webp, .webm, .xlsx";


  export const checkReschedule = [
    "grade mismatch",
    "language mismatch",
    "desfase de grado",
    "nota incompatível",
    "desfase de lenguaje",
    "língua incompatível"
  ];

  export const supportMedium = {
    email: {
      link: "support@whitehatjr.com",
      desc: "support@whitehatjr.com"
    },
    phone: {
      link: "02248933975",
      desc: "022 - 489 33 975"
    }
  };
  

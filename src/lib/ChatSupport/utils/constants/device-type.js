import React from "react";

export const DeviceTypeContext = React.createContext();

export const DEVICE_TYPES = {
  MOBILE: 1,
  DESKTOP: 2,
  TAB: {
    LRN10: false,
    isTab: 3
  }
};
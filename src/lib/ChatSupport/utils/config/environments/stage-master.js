export const apiUrl = "https://qa-api.whjr.one/api/V1";
export const apiBYJUUrl = "https://qa-api.byfs.one/api/V1";
export const trUrl = "https://join-stage.whjr.one/api/V1/"; //Teacher recruitment
export const port = 3000;
export const MATH_URL = "https://math-qa.whjr.one/";
export const CODING_URL = "https://code-qa.whjr.one/";
export const MUSIC_URL = "https://music-qa.whjr.one/";
export const MUSIC_FOR_ALL_URL = "https://musicplus-qa.whjr.one/";
export const ART_URL = "https://art-qa.whjr.one/";
export const BYJU_MATH_URL = "https://math-qa.byfs.one/";
export const BYJU_CODING_URL = "https://code-qa.byfs.one/";
export const BYJU_MUSIC_URL = "https://music-qa.byfs.one/";
export const BYJU_MUSIC_FOR_ALL_URL = "https://musicplus-qa.byfs.one/";
export const BYJU_ART_URL = "https://art-qa.byfs.one/";
export const socketUrl =
  "wss://flomrffhg0.execute-api.ap-south-1.amazonaws.com/dev";
export const razor_pay_api_key = "rzp_test_mN7qXShA9j29Z5";
export const stripe_publishable_api_key =
  "pk_test_ijq4VSESkFbUinTLe6ziI2QO00f0bMXN4r";
export const stripe_us_publishable_api_key =
  "pk_test_MYcFwneSwdMspP55igUMLfDt00xRv7odrt";
export const stripe_mx_publishable_api_key =
  "pk_test_51Ihxt3D21O4WGmJDxWDUTcVarVnzUny1lcVZkLih87Q8DIUUy9DJ4zkuXrLG4160NM47LWr6h08wJBkgEYCgnDRK00Jql7xDy7";
export const stripe_br_publishable_api_key =
  "pk_test_51Ij1jxEaUrTuGRNAmnu2ebH4i2CN2Zlg3IywysIM99fZAtXaKHuWvVAnXffa2NcNT4y4cG9Gz3HT0tMmYq4Eqa7a00S16jbFnx";
export const MERCADO_PAGO_MX_PUBLIC_KEY = "TEST-cac19377-b4d7-4c1d-bb43-46bf92b44836";
export const MERCADO_PAGO_BR_PUBLIC_KEY = "TEST-2b96f7c2-825f-4ee8-b1ae-ec5216ac2f8c";
export const PROJECT_WEBSITE_URL = "whjrweb.site/";
export const PAYU_FORM_ACTION = "https://test.payu.in/_payment";
export const WEBSITE_URL = "https://math.whjr.one";
export const PROJECT_REDIRECTION_URL =
  "http://whjrweb.site/websites/redirect.php?siteName=";
export const PAID_EXPERT_URL =
  "https://web.whitehatjr.com/silicon-valley/expert";
export const PAID_STANDARD_URL =
  "https://web.whitehatjr.com/silicon-valley/standard";
export const PAID_NON_EXPERT_URL =
  "https://web.whitehatjr.com/silicon-valley-customer";
export const FIREBASE_DB_URL = "https://wh-realtime.firebaseio.com/";
export const CHAMPIONSHIP_URL = "https://championship.whitehatjr.com";
export const LMS_URL = "https://qa.whjr.education/wp-json/whj/v1/";

export const COMMUNITY_URL = "https://community.whjr.one";
export const BYJU_COMMUNITY_URL = "https://community.byfs.one";
export const CHAMPIONSHIP_HACKATHON_URL =
  CHAMPIONSHIP_URL + "/challenges/hackathon";
export const ZDK_CHAT_KEY = "HvF7nSfF4cVwILUTsvpBmT97jfNS4L32";
export const COMMUNITY_API_URL = "https://championship1-stage.whjr.one";
export const UN_SPLASH_ACCESS_KEY =
  "Sm-TNorfqA0NOGCpJWUgJKe-2vq7R4tY8MkQSIqJu0E";
export const whatsAppShareUrl = "https://math.whjr.one/";
export const UN_SPLASH_API_URL = "https://api.unsplash.com";
export const fbAppId = "220229035972247";
export const STUDENT_WEBSITE_DOMAIN = "whjrweb.site";
export const CLAN_API_URL = "https://championship1-stage.whjr.one/";
export const SHARING_DOMAIN_URL = "https://sharing.whjr.one";
export const SHRING_DOMAIN_URL_BFS = "https://sharing.byfs.one";
export const PRISMIC_ENDPOINT = "https://whitehatjr.prismic.io/api/v2";
export const UPCOMING_CLASS_ROUTE_TO_MATH_URL = "https://mstage-next.whjr.one/";
export const UPCOMING_CLASS_ROUTE_TO_CODING_URL = "https://mcode.whjr.one/";
export const BRANCH_SDK_KEY = "key_live_djT40PA57ykVnyfq8RxrNlfiqxnxWL7P";
export const CAPTCHA_SITE_KEY = "6Lc0F-IZAAAAANHkz_hBZB8wtRd9V_II5f-kmLuP";
export const MATH_COURSE_PLANS = "https://mstage-next.whjr.one/course-plans";
export const IMAGES_CDN = "https://whjr-dev.imgix.net";

export const SEGMENT_KEY = "RBsY7ZmrjqjtUki43GC4dnz4ltgOgDii";
export const PLAY_STORE_APP_URL = "https://whitehat.app.link/1U832V3AIeb";
export const IOS_APP_URL = "https://whitehat.app.link/CtOAoY7AIeb";
export const PLAY_STORE_APP_URL_BYFS = "https://bfs.app.link/Gkn9pcIJuib";
export const IOS_APP_URL_BYFS = "https://bfs.app.link/Gkn9pcIJuib";
export const enableThirdPartyScripts = false;
export const TOPPR_ASK_URL = "https://docpreprod.toppr.com";
export const TOPPR_API_URL = "https://points-preprod.toppr.com";
export const TOPPR_CLIENT_ID = "fy5viy0ngocq18x0ttdx";
export const STORE_FRONT_URL = "https://points-preprod.toppr.com";
export const YM_BOT_ID = "x1633673072839";
export const OPS_URL = "https://ops-stage.whjr.one/";
export const MICROFRONTEND_URL = "https://stage-inclass.whjr.one";
export const BYJU_MICROFRONTEND_URL = "https://stage-inclass.whjr.one";
export const QUIZ_SDK_API_URL = "https://stage-api.whjr.one";
export const IN_CLASS_QUIZ_TENANT_ID = "61af3728d3d3057613686eca"
export const POST_CLASS_QUIZ_TENANT_ID = "619687851824ca26a89cd144";
export const SPEED_TEST_URL = "https://s3-math-aid.whjr.one/f1/";
export const TYPEFORM_TOKEN = {
  FORM_ID: "kkkbX8SP",
  TOKEN: "8yuMmX6dmFwFnKqoYb79w9fqgLjijCjHtsXsWc4d2LDb"
};
export const SOCKET_URL = "wss://hzqpapvxa4.execute-api.eu-west-1.amazonaws.com/prod";
export const TYPEFORM_URL = "https://form.typeform.com/to/cpDlEn2q";
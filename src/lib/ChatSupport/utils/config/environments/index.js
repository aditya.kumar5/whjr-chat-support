import * as development from "./development";
import * as production from "./production";
import * as preproduction from "./preproduction";
import * as stage from "./stage";
import * as stageMaster from "./stage-master";
export const variable = {
  development,
  stage,
  production,
  preproduction,
  "stage-master": stageMaster
};

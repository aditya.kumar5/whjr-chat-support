export const apiUrl = "https://preprod.whitehatjr.com/api/V1/";
export const apiBYJUUrl = "https://preprod.byjusfutureschool.com/api/V1/";
export const trUrl = "https://join-api.whitehatjr.com/api/V1/"; // Teacher recruitment
export const port = 3000;
export const MATH_URL = "https://math-preprod.whitehatjr.com/";
export const CODING_URL = "https://code-preprod.whitehatjr.com/";
export const MUSIC_URL = "https://music-preprod.whitehatjr.com/";
export const MUSIC_FOR_ALL_URL = "https://musicplus-preprod.whitehatjr.com/";
export const ART_URL = "https://art-preprod.whitehatjr.com/";
export const BYJU_MATH_URL = "https://math-preprod.byjusfutureschool.com/";
export const BYJU_CODING_URL = "https://code-preprod.byjusfutureschool.com/";
export const BYJU_MUSIC_URL = "https://music-preprod.byjusfutureschool.com/";
export const BYJU_MUSIC_FOR_ALL_URL =
  "https://musicplus-preprod.byjusfutureschool.com/";
export const BYJU_ART_URL = "https://art-preprod.byjusfutureschool.com/";
export const wboUrl = "https://mathpoc.whjr.one/wbo/boards/";
export const svgWboUrl = "https://mathpoc.whjr.one/wbo/export/";
export const worksheetWboUrl = "https://wbworksheet.whjr.cc/boards/";
export const svgWorksheetWboUrl = "https://wbworksheet.whjr.cc/export/";
export const APPLET_IFRAME_URL = "https://collab-applet.whjr.online";
export const socketUrl =
  "wss://hzqpapvxa4.execute-api.eu-west-1.amazonaws.com/prod";
export const razor_pay_api_key = "rzp_live_2oXNeVhDeRZ5oC";
export const stripe_publishable_api_key =
  "pk_live_IDLbE6MYCDEglrDwQ3PGPbml001IHJpnVA";
export const stripe_us_publishable_api_key =
  "pk_live_RK7hahxSBr9HOlShxMgCpNeI00JrHR3kCl";
export const stripe_br_publishable_api_key =
  "pk_live_51Ij1jxEaUrTuGRNA6pYMVhLidT44gYQLO08wBLe7yQZqTWrmXZEgSS5g5aKBhRxDKz8BNFfuDgm41aUitYEo0e7J00QJQ5CDh9";
export const stripe_mx_publishable_api_key =
  "pk_live_51Ihxt3D21O4WGmJDg82ydj5CDZHzexfMuQhXzbdgAvpuWmpuRtGTy9NW8tmn8bYTBcgeP5nQAw4yj3xUCokn2QBh00gYon226O";
// export const stripe_publishable_api_key =
//     "pk_test_ijq4VSESkFbUinTLe6ziI2QO00f0bMXN4r";
export const MERCADO_PAGO_MX_PUBLIC_KEY = "APP_USR-644face5-6e22-44d6-9782-08a5db4ac374";
export const MERCADO_PAGO_BR_PUBLIC_KEY = "APP_USR-074598b2-c33b-461e-bce8-f2b02c4db2b3";
export const PAYU_FORM_ACTION = "https://secure.payu.in/_payment";
export const PROJECT_WEBSITE_URL = "whjr.site/";
export const LMS_URL = "https://whjr.education/wp-json/whj/v1/";
export const WEBSITE_URL = "https://code.whitehatjr.com";
export const PROJECT_REDIRECTION_URL =
  "https://whjr.site/websites/redirect.php?siteName=";
export const PAID_EXPERT_URL = "https://whitehatjr.com/silicon-valley/expert";
export const PAID_STANDARD_URL =
  "https://whitehatjr.com/silicon-valley/standard";
export const PAID_NON_EXPERT_URL =
  "https://whitehatjr.com/silicon-valley-customer";
export const FIREBASE_DB_URL = "https://wh-realtime-prod.firebaseio.com/";
export const CHAMPIONSHIP_URL = "https://championship.whitehatjr.com";
export const COMMUNITY_URL = "https://community.whitehatjr.com";
export const BYJU_COMMUNITY_URL = "https://community.byjusfutureschool.com";
export const CHAMPIONSHIP_HACKATHON_URL =
  CHAMPIONSHIP_URL + "/challenges/hackathon";
export const ZDK_CHAT_KEY = "RZymsk1iBii1byfSSQS8B8RxMs9JyASD";
export const COMMUNITY_API_URL = "https://student-community.whitehatjr.com";
export const UN_SPLASH_ACCESS_KEY =
  "1231zUQKHxdGxLDa83zBX3Xcn9xIUMNbrMXHMtjw10U";
export const UN_SPLASH_API_URL = "https://api.unsplash.com";
export const fbAppId = "220229035972247";
export const LEVEL_SHARE_URL = "/share?user=:id&level=:level";
export const GET_REWARD_SUBMISSION_STATUS =
  "/gamification/student/:studentId/getRewardSubmission?levelId=:level";
export const REWARD_SUBMISSION_URL =
  "/gamification/student/:studentId/rewardSubmission/?levelId=:level";
export const YOUNG_VISIONARIES_SUBMIT_URL =
  "/gamification/student/:studentId/submitYoungVisionariesUrl?submissionUrl=:submissionUrl";
export const YOUNG_VISIONARIES_STATUS_URL =
  "/gamification/student/:studentId/getYoungVisionariesVideo";
export const YOUNG_VISIONARIES_VIDEO_URL =
  "/gamification/student/:studentId/getYoungVisionariesVimeoUploadLink?redirectUrl=:redirectUrl&fileSize=:fileSize";
export const STUDENT_WEBSITE_DOMAIN = "whjr.site";
export const CLAIM_REWARD_URL =
  "/gamification/student/:studentId/claimReward?rewardLevel=:level&journeyVersion=:version";
export const FEATURE_LIST_URL =
  "/gamification/student/:studentId/getFeatureList";
export const CLAN_API_URL = "https://student-community.whitehatjr.com/";
export const PRISMIC_ENDPOINT = "https://whitehatjr.prismic.io/api/v2";
export const SHARING_DOMAIN_URL = "https://sharing.whitehatjr.com";
export const SHRING_DOMAIN_URL_BFS = "https://sharing.byjusfutureschool.com";
export const BRANCH_SDK_KEY = "key_live_djT40PA57ykVnyfq8RxrNlfiqxnxWL7P";
export const CAPTCHA_SITE_KEY = "6Le8ousZAAAAABI6t3A5m3kQh_18xVZqKuNs8OmG";
export const MATH_COURSE_PLANS = "https://math.whitehatjr.com/course-plans";
export const IMAGES_CDN = "https://whjr.imgix.net";
export const SEGMENT_KEY = "w7YH8EJ1PxsEiWlAazGGdKaKbIXKcZl0";
export const PLAY_STORE_APP_URL = "https://whitehat.app.link/1U832V3AIeb";
export const IOS_APP_URL = "https://whitehat.app.link/CtOAoY7AIeb";
export const PLAY_STORE_APP_URL_BYFS = "https://bfs.app.link/Gkn9pcIJuib";
export const IOS_APP_URL_BYFS = "https://bfs.app.link/Gkn9pcIJuib";
export const enableThirdPartyScripts = true;
export const TOPPR_ASK_URL = "https://doc.toppr.com";
export const TOPPR_API_URL = "https://points.toppr.com";
export const TOPPR_CLIENT_ID = "fy5viy0ngocq18x0ttdx";
export const STORE_FRONT_URL = "https://pointsland.byjusfutureschool.com";
export const YM_BOT_ID = "x1638808678437";
export const OPS_URL = "https://ops-stage.whjr.one/";
export const MICROFRONTEND_URL = "https://stage-inclass.whjr.one";
export const BYJU_MICROFRONTEND_URL = "https://stage-inclass.whjr.one";
export const POST_CLASS_QUIZ_TENANT_ID = "619687851824ca26a89cd144";
export const IN_CLASS_QUIZ_TENANT_ID = "61af3728d3d3057613686eca"
export const QUIZ_SDK_API_URL = "https://apigw.whitehatjr.com:443"
export const SPEED_TEST_URL = "https://s3-math-aid.whjr.one/f1/";
export const TYPEFORM_TOKEN = {
  FORM_ID: "kkkbX8SP",
  TOKEN: "8yuMmX6dmFwFnKqoYb79w9fqgLjijCjHtsXsWc4d2LDb"
};
export const SOCKET_URL = "wss://hzqpapvxa4.execute-api.eu-west-1.amazonaws.com/prod";
export const TYPEFORM_URL = "https://form.typeform.com/to/cpDlEn2q";
export const MATH_WBO_CONFIG = {
  wboUrl: `https://mathpoc.whjr.one/wbo/boards/`,
  svgWboUrl: `https://mathpoc.whjr.one/wbo/export/`,
  worksheetWboUrl: "https://wbworksheet.whjr.cc/boards/",
  svgWorksheetWboUrl: "https://wbworksheet.whjr.cc/export/",
  wboUrlV2: "https://www.whj.whiteboard.chat/boards/",
  svgWboUrlV2: "https://export.whj.whiteboard.chat/exportapi/"
};
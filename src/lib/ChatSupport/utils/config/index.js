import * as development from "./environments/development";
import * as production from "./environments/production";
import * as preproduction from "./environments/preproduction";
import * as stage from "./environments/stage";
import * as stageMaster from "./environments/stage-master";

export const variable = {
    development,
    stage,
    production,
    preproduction,
    "stage-master": stageMaster
  };
  
const ENV = process.env.NODE_ENV || "development";

const branch_name =
  process.env.REACT_APP_VERCEL_GIT_COMMIT_REF || process.env.REACT_APP_BRANCH;
const CRA_MAP = {
  release: "production",
  pre_release: "preproduction",
  master: "stage-master",
  develop: "stage"
};
const NEXT_ENV =
  ENV === "development" ? "development" : CRA_MAP[branch_name] || "development";

const envConfig = variable[NEXT_ENV];

export const badgesPath =
  (process.env.REACT_APP_PUBLIC_URL || "") + "/images/BadgesMap/";
// const envConfig = variable[ENV];

export const config = Object.assign(
  {
    // env: ENV,
    env: NEXT_ENV
  },
  envConfig
);

export const h__isDevEnv = () => NEXT_ENV === "development";

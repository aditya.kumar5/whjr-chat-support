import { socketManager } from "./socket-manager";

let socketInstance = null;

export const initSocketService = ({
  roomId,
  userId,
  socketToken,
  cb,
  connectCb
}) => {
  const uid = userId;
  socketInstance = socketManager({
    uid,
    roomId,
    socketToken:
      socketToken ||
      btoa(
        JSON.stringify({
          uid: uid,
          roomId: roomId
        })
      )
  });
  if (socketInstance && socketInstance.connected && connectCb) {
    connectCb();
  } else {
    socketInstance.onConnect(connectCb);
    socketInstance.onMessage(cb);
  }
};

export const closeSocketService = () => {
  if (socketInstance) socketInstance.close();
};

export const sendSocketMessage = msg => {
  socketInstance && socketInstance.send(msg);
};

export const socket__getLatestSpeed = source => {
  sendSocketMessage({
    type: "GET_LATEST_SPEED",
    state: { source },
    target: "*"
  });
};

export const sendEventsOnSegmentHOF = ({
    sendEvents,
    eventName,
    properties
  }) => {
    let eventDetails = {
      event: eventName,
      properties
    };
    if (sendEvents) {
      sendEvents(eventDetails);
    }
  };
  
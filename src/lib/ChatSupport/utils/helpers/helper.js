import { courseNames } from "../constants/constant";
import { supportMedium } from "../constants/constant";

import _get from "lodash/get";

export const h__getCourseScope = () => courseNames[window.currentCourseName];

export const closePIP = () => {
  const isPIPActive = document.pictureInPictureElement !== null;
  if (isPIPActive) {
    try {
      document.exitPictureInPicture();
    } catch (e) {
      console.log(e);
    }
  }
};

export const h__isArtScope = () => h__getCourseScope() === courseNames.ART;

export const getStudentsInfo = (booking) => {
  if (!booking || !Object.keys(booking).length) {
    return [];
  }

  let studentsBookings = _get(booking, "student_bookings");

  return studentsBookings
    .map((studentBooking) => {
      return {
        ...studentBooking.student,
        bookingStatus: studentBooking.status,
        ncReasonCode: studentBooking.ncReasonCode,
        ncReasonCodeFormatted:
          studentBooking.ncReasonCode === "STUDENT_ABSENT"
            ? "STUDENT ABSENT"
            : "STUDENT TECH ISSUE"
      };
    })
    .sort((student) => (student.bookingStatus === "cancelled" ? 1 : 0));
};

export const getStudentsBookingInfo = function (booking) {
  if (!Object.keys(booking).length) {
    return [];
  }

  let studentsBookings = _get(booking, "student_bookings");

  return studentsBookings.sort((booking) =>
    booking.status === "cancelled" ? 1 : 0
  );
};

// TO GET THE SCREENSHOT CAPTURED URLS FROM PLAYGROUND
const getGIFArrayFromFirebase = (firebaseSyncGIF) => {
  try {
    if (firebaseSyncGIF) {
      return firebaseSyncGIF
        .get()
        .then((snapshot) => {
          try {
            const imagesArr = [];
            const screenshotsArr = [];
            if (snapshot.val()) {
              if (
                snapshot.val() &&
                snapshot.val()["GIFScreenshots"] &&
                snapshot.val()["GIFScreenshots"]["screenshots"]
              ) {
                for (let obj in snapshot.val()["GIFScreenshots"][
                  "screenshots"
                ]) {
                  if (
                    snapshot
                      .val()
                      ["GIFScreenshots"]["screenshots"].hasOwnProperty(obj)
                  ) {
                    const url =
                      snapshot.val()["GIFScreenshots"]["screenshots"][obj];
                    imagesArr.push(url);
                  }
                }
              }
            }
            if (snapshot.val()) {
              if (
                snapshot.val() &&
                snapshot.val()["screenshots"] &&
                snapshot.val()["screenshots"]["whiteboardAndActivityLinks"]
              ) {
                for (let obj in snapshot.val()["screenshots"][
                  "whiteboardAndActivityLinks"
                ]) {
                  if (
                    snapshot
                      .val()
                      ["screenshots"][
                        "whiteboardAndActivityLinks"
                      ].hasOwnProperty(obj)
                  ) {
                    const url =
                      snapshot.val()["screenshots"][
                        "whiteboardAndActivityLinks"
                      ][obj];
                    screenshotsArr.push(url);
                  }
                }
              }
            }
            return {
              imagesList: imagesArr.slice(1),
              screenshotList: screenshotsArr
            };
          } catch (e) {
            console.error(e);
            return {};
          }
        })
        .catch((err) => {
          console.log(err);
          return {};
        });
    } else {
      return {};
    }
  } catch (e) {
    console.error(e);
    return {};
  }
};

// UPDATING/ADDING DATA TO THE ARGUMENT "GIFAndScreenshots"
const enrichGIFAndScreenshots = (GIFAndScreenshots, classInterfaceData) => {
  GIFAndScreenshots.courseClassId = _get(
    classInterfaceData,
    "class_booking.course_class.id"
  );
  GIFAndScreenshots.studentId = _get(
    classInterfaceData,
    "class_booking.studentId"
  );
  GIFAndScreenshots.courseItemId = _get(
    classInterfaceData,
    "class_booking.course_class.courseItemId"
  );
  GIFAndScreenshots.className = _get(
    classInterfaceData,
    "class_booking.course_class.name"
  );
  GIFAndScreenshots.classNumber = _get(
    classInterfaceData,
    "class_booking.course_class.classNumber"
  );
  const studentBookings = _get(
    classInterfaceData,
    "class_booking.student_bookings"
  );
  const studentDetails =
    studentBookings && studentBookings.length > 0
      ? studentBookings[0].student
      : {};
  GIFAndScreenshots.studentName = studentDetails.name || "";
  GIFAndScreenshots.grade = studentDetails.grade || "";
};

// ONE TO ONE END CLASS FUNCTION
export const endClassSubmit = async (
  feedbackResponseResult = [],
  isSos,
  classData,
  classInterfaceData,
  isQuesAvailable,
  firebaseEvents,
  firebaseSyncGIF
) => {
  const { class_booking = {} } = classInterfaceData;
  let studentsBooking = getStudentsBookingInfo(class_booking);

  let messageClassData = {
    id: classData.id,
    classBookingId: classData.bookingId,
    teacherId: classData.teacherId,
    studentStatus: studentsBooking.map((booking) => ({
      id: booking.student.id,
      classStatus: classData.classStatus,
      bookingId: booking.id,
      ncReasonCode: classData.ncReasonCode ? classData.ncReasonCode : null,
      ncReasonDesc: classData.ncReasonDesc,
      remark: classData.remark
    })),
    isSOSEmergency: isSos
  };

  let studentStatusData = _get(messageClassData, "studentStatus.0", {});

  if (
    studentStatusData.classStatus === "not_completed" &&
    (!studentStatusData.ncReasonCode || !studentStatusData.ncReasonDesc) &&
    class_booking.isTrial
  ) {
    alert(
      "You are unable to change class status. Please contact our agent through chat and say the code AB11Q"
    );
    return;
  }

  if (feedbackResponseResult.length > 0) {
    messageClassData["feedbackResponseResult"] = feedbackResponseResult;
  }

  const presentStudents = messageClassData.studentStatus.filter(
    (student) => student.classStatus === "completed"
  );

  let GIFAndScreenshots;
  try {
    GIFAndScreenshots = await getGIFArrayFromFirebase(firebaseSyncGIF);
    enrichGIFAndScreenshots(GIFAndScreenshots, classInterfaceData);
  } catch (error) {
    console.log(error);
  }

  const req = {
    classData: messageClassData,
    GIFAndScreenshots: GIFAndScreenshots,
    navigateToDashboardOnFinish:
      !isSos &&
      (presentStudents.length === 0 ||
        !(class_booking.isTrial || isQuesAvailable))
  };

  // removing firebase events after class end is confirmed
  firebaseEvents.forEach((offEventFunction) => offEventFunction());
  if (firebaseSyncGIF) firebaseSyncGIF.unregister();

  return {
    presentStudents: presentStudents,
    requestParam: req
  };
};

let subscribers = {};

export const subscribe = (event, callback) => {
  let index;

  if (!subscribers[event]) {
    subscribers[event] = [];
  }

  index = subscribers[event].push(callback) - 1;

  return () => {
    subscribers[event].splice(index, 1);
  };
};

export const getSupportData = (supportData, classType = "") => {
  let supportNumber = supportMedium["phone"]["link"];
  let supportEmail = supportMedium["email"]["link"];
  if (supportData && supportData.hasOwnProperty("customerSupport")) {
    supportNumber = supportData["customerSupport"]["number"];
    supportEmail = supportData["customerSupport"]["email"];
  }
  if (supportData && supportData.hasOwnProperty("number")) {
    supportNumber = supportData["number"];
  }
  if (supportData && supportData.hasOwnProperty("email")) {
    supportEmail = supportData["email"];
  }
  return { supportNumber, supportEmail };
};


export const setPaidStatus = (userType, userValue) => {
  if (userType === "STUDENT" && userValue) {
    if (
      userValue.trialStatus === "paid" &&
      _get(userValue, "activeCourse.course_item.classType") === "ONE_TO_TWO"
    ) {
      return "paid_one_to_two";
    } else if (
      userValue.trialStatus === "paid" &&
      _get(userValue, "activeCourse.course_item.classType") === "ONE_TO_ONE"
    ) {
      return "paid";
    } else if (
      userValue.trialStatus === "paid" &&
      !["ONE_TO_TWO", "ONE_TO_ONE"].includes(
        _get(userValue, "activeCourse.course_item.classType")
      )
    ) {
      return "paid_one_to_many";
    } else {
      return "trial";
    }
  } else if (userType === "TEACHER" && userValue) {
    const teacherSkill = userValue.teacher_skills.filter(
      x =>
        x.skillValue.toUpperCase() === "ONE_TO_MANY" ||
        x.skillValue.toUpperCase() === "ONE_TO_TWO"
    );
    if (teacherSkill.length > 0) {
      return `paid_${teacherSkill[0].skillValue.toLowerCase()}`;
    } else {
      return null;
    }
  }
};

export const getCodingOrMathSkill = data => {
  //["coding","math"]
  const courses = data.filter(x => x.course);
  const skillList = [];
  courses.forEach(course => {
    skillList.push(course.course.courseType.toLowerCase());
  });
  return [...new Set(skillList)];
};